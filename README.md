## CSTARS Radiative Transfer Model Implementations

### Status

Please note that development of this software stopped in 2005.
A copy of the code as of then was uploaded to this bitbucket repository in 2014.
Since it is likely that further development of this code has happened elsewhere over 
the years, you are advised to first check out there before trying this version. 
Documentation is very minimal (a project website was shut down long ago, unfortunately). 
At this point, only very minimal updates are expected to occur in this repository. 

The following is adapted from the original README.

----

CSTARS RTM Readme
Center for Spatial Technologies and Remote Sensing
University of California, Davis


Efficient C reimplementation of various well-known radiative transfer
models as well as their inversion. This reimplementation obeys a
necessity for modularization and flexibility.

The model and inversion programs available are:
```
    Model               Inversion
    --------------      ------------------
    rtm.prospect        rtm.inv_prospect
    rtm.sailh           rtm.inv_sailh
    rtm.prospect_sailh  rtm.inv_prospect_sailh
    rtm.prospect_infi   rtm.inv_prospect_infi
    rtm.liberty         rtm.inv_liberty
    rtm.geosail         (not implemented)
```
See the AUTHORS file for credits of original authorship.


### Installing RTM

#### Prerequisite

Besides a standard build environment (make, C compiler, etc)
on your system, the only prerequisite is the F2C
Fortran to C translator Library.  See README.f2c.txt

Once you have cloned this repo on your local machine, 
the steps to compile and install the RTM software are:

```
$ ./reconf
$ ./configure
$ make
$ sudo make install
```

All program names are given the prefix `rtm.` upon installation.
The `--program-prefix` configure option can be used to change this
prefix.

By default, `/usr/local/` is used as base directory to find the F2C
header (`include/f2c.h`) and library (`lib/libf2c.a`). To specify a
different location, use the option `--with-f2c=dir` where dir is the
directory to use as prefix.

See INSTALL for more configure options.
