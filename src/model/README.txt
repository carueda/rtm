inversion
---------
$Id$

	Type:
		make
	to make all available inversion programs

-------------------------------------------------------------
04/02/04
	David Riano
	Infinitive model 1 and infinitive model 3.
	Check Zarco-Tejada et al. IEEE TGRS v39 n7 july 2001

04/16/03
	Option -t added to inv_prospect_sailh to keep track of the best M solutions.
	This used new functionality in the opt module.

04/09/02
	Added option -s to inv_prospect and inv_prospect_sailh 

08/27/01
    In inv_prospect_sailh.c a conditioned code has been added in order to allow for
    error analysis. See ``#ifdef MONITOR_ERROR'' sections.
    See make-inv_prospect_sailh.
    See also doc/error2.lyx (documentation about error analysis).


07/05/01
    inv_sailh ready.

07/03/01
    Reorganization, but not change in functionality.

06/15/01
    Command line interface improved.

06/14/01
    Binary descent search (OPT_BINARY) included with very good resulting performance (as expected).
    See ../optimization/README.
    This assumes that the function to be minimized is unimodal with respect to each variable,
    but it seems this is not the case, although the solutions are good. More testing is required.

    The execution time of the case mentioned on 06/13/01 reduces to 1.45 segs (only 112 evaluations).

    The following test with 5 variables:

    > inv_prospect_sailh  1.5 10,90,.5 .001,.05,.001 .0005,.01,.0005 \
                         t0477f06_soil.norm skyl.txt    \
                         0,.1,.01 1,15,.5 3 30 0 0      \
                         ejemplo_observacion_sensor.txt  binary5.txt binary

    took 3 min 37 segs for 17,181 evaluations.
    (The exhaustive method would take 6 days for 41,708,800 evaluations!)

06/13/01
    I turn on the gcc -O3 option. The execution time in a test (4669 evaluations) reduces
    from 1m7s to 59s. The results are almost the same (in a plot you can't see the differences).


06/12/01
    First version ready.
    This makes use of the optimization module that performs an exhaustive (i.e., very expensive) search.
    (A better method is pending.)


06/05/01
    Starting definition of this project.
