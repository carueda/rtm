sailh
-----
Carlos Rueda                         (see also ../README)


7/05/01
	Reorganization, but not change in functionality.
	
7/02/01
	sailh now makes interpolation for the input functions to be conformant with the
	reflectance function.  See sailh_main.c, make-sailh.
	(NOTE: 0.1-sailh_main.c is the version 0.1 kept as a backup.)

6/06/01
	Some modularization done in order to ease the prospect_sailh integration.
	OK.

6/05/01
	According to Pablo directions, we're going to continue working with the new version
	despite it's producing a spurious point. It seems that this problem doesn't arise
	when more typical inputs are given to the program: specifically with the ts, tv, and
	psr angles: 30 0 0, and not 60 60 15.  
	
	The invocation is as follows (on ceres):
	
	> sailh gy1b1l1.rfl gy1b1l1.tns t0477f06_soil.norm skyl.txt 0.00 2.87 3 30 0 0  canopy_reflectance.txt

	In order to verify the equality of the results:
	
	> calling_sailh gy1b1l1.rfl gy1b1l1.tns t0477f06_soil.norm skyl.txt 0.00 2.87 3 30 0 0  canopy_reflectance.txt2

	Both resulting functions are virtually the same (with no spurious points at all).
	
	
6/04/01
	I have changed the sailhin.dat format to make it simpler: one value per line.
	Then I modified reflh.f accordingly.  Now both systems generate the same results. 
	However the	spurious point persists.


6/01/01
	Testing (spurious point at 504.41 wavelength)
	
	Follow these steps:
	
	> SAILH_TEST=326
	> export SAILH_TEST
	> sailh gy1b1l1.rfl gy1b1l1.tns t0477f06_soil.norm skyl.txt 0.00 2.87 3 60 60 15  canopy_reflectance.txt 2> sailhin.dat
	> cat sailhin.dat
	You see the spurious value RESULT = 0.127730. The complete figure is:
	> plotfunction canopy_reflectance.txt
	Let's try the original program sailh at this point:
	> ./sailh
	> cat sailhout.dat
	You see the spurious value  -.459277E-01 (the same happens with  ~/models/sailh/sailh_prm2).
	Let's use the original program sailh at all points:
	> ~/models/sailh/calling_sailh gy1b1l1.rfl gy1b1l1.tns t0477f06_soil.norm skyl.txt 0.00 2.87 3 60 60 15  canopy_reflectance.txt2
	This obtains canopy_reflectance.txt2:
	> plotfunction canopy_reflectance.txt2
	shows that the spurious value persists
	
	Thus, we have a similar problem that affects both systems (the original and the new ones)
	Also the first function is slightly greater than the second one. (?)
	PENDING.
	

5/31/01
	Effectively, Pablo multiplies the rho, tau, and rhos functions by 100. FIXED.
	A spurious point appears in the resulting canopy_reflectance. PENDING.
	

5/30/01
	Caution: The original program (sailh/fortran, sailh_prm2/C) reads in exact
	positions.
	
	However, Pablo says the resulting canopy_reflectance function seems to be
	divided by 100.

5/29/01

	- sailh, first version ready
	- normalization ready (see norm)
	- skyl function generation ready (see genskyl)
	
	TESTING
		Preliminary tests are incoherent. PENDING
		
		The environmental variable SAILH_TEST can be set for
		sailh to report a certain calculation to stderr. This
		report has the format expected by sailh

5/25/01

	The C version of original program sailh (sailh.f + reflh.f) is working fine.
	(using f2c)
	
	- sailh.c       global variables
	- sailh_prm     accepts arguments (and calls sailh.c)
	  -->  sailh_prm is the function to be used
		
	- reflh_prm.c   accepts arguments or reads from a file
	- reflh_prm2.c  reads from a file
	
	Pruebas:
	
	1) gcc -Wall -I ~/f2c/ -L ~/f2c/libf2c  sailh.c sailh_prm.c reflh_prm2.c -o sailh_prm2 -lf2c -lm
	
	   produces sailh_prm2, a program similar to the original sailh
	   
	2) gcc -Wall -I ~/f2c/ -L ~/f2c/libf2c  sailh.c sailh_prm.c reflh_prm.c main.c -o sailh_prm -lf2c -lm
	
	   produces sailh_prm, a program that expects command line arguments and calls reflh_prm.c.
	   
	Tests OK.
	
	
	What follows:
	
		- sailh: process all wavelengths
		- normalization based on reflectance_list abcsissas
		- interpolation
		- etc.