/* sailh.f -- translated by f2c (version 20010320).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include <stdio.h>
#include "f2c.h"

/* Common Block Declarations */

struct {
    real pi, rd, dg, tts, tto, psi;
} angles_;

#define angles_1 angles_

struct {
    real lai, li[13], f[13], roo, tau, hot;
} sailhcom_;

#define sailhcom_1 sailhcom_

struct {
    real tss, too, tsstoo, rdd, tdd, rsd, tsd, rdo, tdo, rso;
} rfltrn_;

#define rfltrn_1 rfltrn_

/* Subroutine */ int sailh_()
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double tan(), cos(), sqrt(), acos(), sin(), exp(), log();

    /* Local variables */
    static real fhot, fint, cstl, rsod, psir, tsin, sntl, rsos, cs2tl, sn2tl, 
	    m, w, cspsi;
    static integer istep;
    static real tanto, e1, e2, f1, tants, h1, h2, f2, t1, t2, t3, x1, y1, x2, 
	    y2, btran1, btran2, co, do__, sb, ub, ko, sf, uf, cs, ks, ds, ho, 
	    bt1, bt2, bt3, sumint, sw1, sw2, alf, dnd, sbf;
    static integer ili;
    static real dno, sig, dso, sko, bts, sks, rtm, ttl, rtp, bto, att, dns;


/*     SAIL-reflectance model for a leaf canopy layer */
/*             including the hotspot-effect */

/*     W. Verhoef */
/*     31 March 1989 */

/*     Input parameters : */

/*     LAI    : Leaf area index of layer */
/*     LI     : Array of 13 leaf inclination angles , recommended */
/*              5,15,25,35,45,55,65,75,81,83,85,87,89 degrees */
/*     F      : Array of frequencies in % of leaf inclinations */
/*     ROO    : Leaf reflectance (assumed Lambertian) in % */
/*     TAU    : Leaf transmittance (  "       "     ) in % */
/*     TTS    : Solar zenith angle in degrees */
/*     TTO    : View angle from nadir in degrees */
/*     PSI    : Sun-view azimuth difference in degrees */
/*     HOT    : Hotspot-effect parameter , defined as ratio of */
/*              average leaf size and layer thickness */

/*     Output parameters : */

/*     TSS    : Specular transmittance for sunlight */
/*     TOO    : Specular transmittance in direction of view */
/*     TSSTOO : Joint specular transmittance for sunlight and */
/*              viewing for the entire layer */
/*     RDD    : Diffuse-diffuse reflectance */
/*     TDD    : Diffuse-diffuse transmittance */
/*     RSD    : Specular-diffuse reflectance for sunlight */
/*     TSD    : Specular-diffuse transmittance for sunlight */
/*     RDO    : Diffuse-specular reflectance in direction of view */
/*     TDO    : Diffuse-specular transmittance in direction of view */
/*     RSO    : Bidirectional reflectance */


/*     Precalculations */

    rtp = (sailhcom_1.roo + sailhcom_1.tau) * (float).005;
    rtm = (sailhcom_1.roo - sailhcom_1.tau) * (float).005;
    tants = tan(angles_1.rd * angles_1.tts);
    tanto = tan(angles_1.rd * angles_1.tto);
    cspsi = cos(angles_1.rd * angles_1.psi);
/* Computing 2nd power */
    r__1 = tants;
/* Computing 2nd power */
    r__2 = tanto;
    dso = sqrt(r__1 * r__1 + r__2 * r__2 - tants * (float)2. * tanto * cspsi);
    alf = (float)1e6;
    if (sailhcom_1.hot > (float)0.) {
	alf = dso / sailhcom_1.hot;
    }
    psir = angles_1.rd * angles_1.psi;

/*     Initialize sums */

    sbf = (float)0.;
    sks = (float)0.;
    sko = (float)0.;
    sw1 = (float)0.;
    sw2 = (float)0.;

/*     Loop leaf inclination angle */

/*        Determine sums weighed by frequency */

    for (ili = 1; ili <= 13; ++ili) {

	ttl = sailhcom_1.li[ili - 1];
	cstl = cos(angles_1.rd * ttl);
/* Computing 2nd power */
	r__1 = cstl;
	cs2tl = r__1 * r__1;
	sn2tl = (float)1. - cs2tl;
	sntl = sqrt(sn2tl);
	sbf += cs2tl * sailhcom_1.f[ili - 1];

	bts = angles_1.pi;
	if (ttl + angles_1.tts > (float)90.) {
	    bts = acos(-cstl / (sntl * tants));
	}
	sks += ((bts - angles_1.pi * (float).5) * cstl + sin(bts) * tants * 
		sntl) * sailhcom_1.f[ili - 1];

	bto = angles_1.pi;
	if (ttl + angles_1.tto > (float)90.) {
	    bto = acos(-cstl / (sntl * tanto));
	}
	sko += ((bto - angles_1.pi * (float).5) * cstl + sin(bto) * tanto * 
		sntl) * sailhcom_1.f[ili - 1];

	btran1 = (r__1 = bts - bto, dabs(r__1));
	btran2 = angles_1.pi * (float)2. - bts - bto;

	if (psir <= btran1) {
	    bt1 = psir;
	    bt2 = btran1;
	    bt3 = btran2;
	} else {
	    bt1 = btran1;
	    if (psir <= btran2) {
		bt2 = psir;
		bt3 = btran2;
	    } else {
		bt2 = btran2;
		bt3 = psir;
	    }
	}

	tsin = sn2tl * (float).5 * tants * tanto;
	t1 = cs2tl + tsin * cspsi;
	sw1 += t1 * sailhcom_1.f[ili - 1];

	if (bt2 > (float)0.) {
	    t3 = tsin * (float)2.;
	    if (bts == angles_1.pi || bto == angles_1.pi) {
		t3 = cs2tl / (cos(bts) * cos(bto));
	    }
	    t2 = -bt2 * t1 + sin(bt2) * (t3 + cos(bt1) * cos(bt3) * tsin);
	    sw2 += t2 * sailhcom_1.f[ili - 1];
	}

/* L10: */
    }

/*     Calculate extinction and scattering coefficients */

    sbf = sbf * rtm * sailhcom_1.lai * (float).01;

    att = sailhcom_1.lai * ((float)1. - rtp) + sbf;
    sig = sailhcom_1.lai * rtp + sbf;

    ks = sks * (float).02 * sailhcom_1.lai / angles_1.pi;
    sb = ks * rtp + sbf;
    sf = ks * rtp - sbf;

    ko = sko * (float).02 * sailhcom_1.lai / angles_1.pi;
    uf = ko * rtp - sbf;
    ub = ko * rtp + sbf;

    w = sailhcom_1.lai * (float).01 * (sailhcom_1.roo * (float).01 * sw1 + 
	    rtp * (float)2. * sw2 / angles_1.pi);

/*     Compute optical properties of the layer */

/* Computing 2nd power */
    r__1 = att;
/* Computing 2nd power */
    r__2 = sig;
    m = sqrt(r__1 * r__1 - r__2 * r__2);
    h1 = (att + m) / sig;
    h2 = (float)1. / h1;
    e1 = exp(m);
    e2 = (float)1. / e1;
/* Computing 2nd power */
    r__1 = ks;
/* Computing 2nd power */
    r__2 = m;
    dns = r__1 * r__1 - r__2 * r__2;
/* Computing 2nd power */
    r__1 = ko;
/* Computing 2nd power */
    r__2 = m;
    dno = r__1 * r__1 - r__2 * r__2;
    cs = (sb * (ks - att) - sf * sig) / dns;
    ds = (-sf * (ks + att) - sb * sig) / dns;
    co = (ub * (ko - att) - uf * sig) / dno;
    do__ = (-uf * (ko + att) - ub * sig) / dno;

    rfltrn_1.tss = exp(-ks);
    rfltrn_1.too = exp(-ko);

/*     Treatment of the hotspot-effect */

    if (alf == (float)0.) {

/*        The pure hotspot */

	rfltrn_1.tsstoo = rfltrn_1.tss;
	sumint = (1 - rfltrn_1.tss) / ks;

    } else {

/*        Outside the hotspot */

	fhot = sqrt(ko * ks);

/*        Integrate by exponential Simpson method in 20 steps */
/*        The steps are arranged according to equal partitioning */
/*        of the slope of the joint probability function */

	x1 = (float)0.;
	y1 = (float)0.;
	f1 = (float)1.;
	fint = ((float)1. - exp(-alf)) * (float).05;
	sumint = (float)0.;

	for (istep = 1; istep <= 20; ++istep) {

	    if (istep < 20) {
		x2 = -log((float)1. - istep * fint) / alf;
	    } else {
		x2 = (float)1.;
	    }
	    y2 = -(ko + ks) * x2 + fhot * ((float)1. - exp(-alf * x2)) / alf;
	    f2 = exp(y2);
	    sumint += (f2 - f1) * (x2 - x1) / (y2 - y1);
	    x1 = x2;
	    y1 = y2;
	    f1 = f2;

/* L20: */
	}

	rfltrn_1.tsstoo = f1;

    }

    rsos = w * sumint;

/*     Calculate other reflectances and transmittances */

    dnd = h1 * e1 - h2 * e2;
    rfltrn_1.rdd = (e1 - e2) / dnd;
    rfltrn_1.tdd = (h1 - h2) / dnd;

    rfltrn_1.rsd = cs * (1 - rfltrn_1.tss * rfltrn_1.tdd) - ds * rfltrn_1.rdd;
    rfltrn_1.tsd = ds * (rfltrn_1.tss - rfltrn_1.tdd) - cs * rfltrn_1.tss * 
	    rfltrn_1.rdd;
    rfltrn_1.rdo = co * (1 - rfltrn_1.too * rfltrn_1.tdd) - do__ * 
	    rfltrn_1.rdd;
    rfltrn_1.tdo = do__ * (rfltrn_1.too - rfltrn_1.tdd) - co * rfltrn_1.too * 
	    rfltrn_1.rdd;

    ho = (sf * co + sb * do__) / (ko + ks);
    rsod = ho * (1 - rfltrn_1.tss * rfltrn_1.too) - co * rfltrn_1.tsd * 
	    rfltrn_1.too - do__ * rfltrn_1.rsd;

    rfltrn_1.rso = rsos + rsod;

/*     Return to calling routine */

    return 0;
} /* sailh_ */

