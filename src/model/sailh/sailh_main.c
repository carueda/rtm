/* 
/////////////////////////////////////////////////////////////////////////
//
// sailh
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/


#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>

#include "sailh.h"



///////////////////////////////////////////////////////////////////////////////////////////////////
static 
int usage(char* msg)
{
	if ( msg )
	{
		fprintf(stderr, "\n%s\n", msg);
		fprintf(stderr, "Try `sailh -h' for more information.\n\n");
		return 1;
	}

	fprintf(stdout,
"\n"
"CSTARS sailh 0.2 (%s %s) %s\n"
"\n"
"USAGE\n"
"  sailh [-h] [-n] rfl tns rhos skyl hotspot_size lai cantype ts tv psr can_rfl\n"
"\n"
"	-h            Shows this message and exits\n"
"	-n            Takes tns, rhos, and skyl as such. In this case, all input files\n"
"	              are assumed to be functions evaluated at exactly the same wavelength\n"
"	              abscissas.  By default, these functions are first interpolated with\n"
"	              respect to rfl.\n"
"	rfl           Reflectance function file\n"
"	tns           Transmittance function file\n"
"	rhos          Soil reflectance function file\n"
"	skyl          Skyl function file.\n"
"	hotspot_size  Hotspot size\n"
"	lai           Leaf area index of layer\n"
"	cantype     = 1(planophile)/2(erectophile)/3(plagiophile)/4(extremophile)\n"
"	              5(uniform)/6(spherical)\n"
"	ts            TS\n"
"	tv            TV\n"
"	psr           PSR\n"
"	can_rfl       Generated canopy reflectance function file\n"
"\n"
"EXAMPLE:\n"
"  sailh 1.rfl 1.tns soil.txt skyl.txt 0.00 2.87 3 30 0 0 canopy.rfl\n"
"(provided the files 1.rfl, 1.tns, soil.txt, and skyl.txt exist in the current directory)\n"
"\n"
"SEE ALSO:\n"
"  genskyl, interpolate\n"
"\n",
		       __DATE__, __TIME__,
			   RTM_VERSION_IN_USAGE
	);
	
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	int arg;
	int do_interpolation = 1;
	char* dbname_reflectance;
	char* dbname_rhos;
	char* dbname_transmittance;
	char* dbname_skyl;
	char* dbname_report;
	
	float hotspot_size, lai, ts, tv, psr;
	int can_type;
	
	float fraction[14];

	List reflectance_list;
	List transmittance_list;
	List rhos_list;
	List skyl_list;

	List canopy_reflectante_list;
	long i, size;
	FILE* file;
	
	int status;



	// process options:
	for ( arg = 1; arg < argc && argv[arg][0] == '-'; arg++ )
	{
		if ( strcmp(argv[arg], "-h") == 0 )
		{
			return usage(NULL);
		}
		else if ( strcmp(argv[arg], "-n") == 0 )
		{
			do_interpolation = 0;
		}
		else
		{
			return usage("Invalid option");
		}
	}
	
	
	if ( argc - arg < 11 )
	{
		return usage("Expecting arguments");
	}
	
	
	dbname_reflectance = argv[arg++];
	dbname_transmittance = argv[arg++];
	dbname_rhos = argv[arg++];
	dbname_skyl = argv[arg++];

	sscanf(argv[arg++], "%f", &hotspot_size);
	sscanf(argv[arg++], "%f", &lai);
	sscanf(argv[arg++], "%d", &can_type);
	sscanf(argv[arg++], "%f", &ts);
	sscanf(argv[arg++], "%f", &tv);
	sscanf(argv[arg++], "%f", &psr);
	dbname_report = argv[arg++];


	getLADF(can_type, fraction);
	
	reflectance_list = read_function(dbname_reflectance);
	if ( reflectance_list == NULL )
	{
		fprintf(stderr, "error reading %s\n", dbname_reflectance);
		return 1;
	}
	
	transmittance_list = read_function(dbname_transmittance);
	if ( transmittance_list == NULL )
	{
		fprintf(stderr, "error reading %s\n", dbname_transmittance);
		return 1;
	}
	
	rhos_list = read_function(dbname_rhos);
	if ( rhos_list == NULL )
	{
		fprintf(stderr, "error reading %s\n", dbname_rhos);
		return 1;
	}

	skyl_list = read_function(dbname_skyl);
	if ( skyl_list == NULL )
	{
		fprintf(stderr, "error reading %s\n", dbname_skyl);
		return 1;
	}


	file = fopen (dbname_report, "w");
	if ( file == NULL )
	{
		fprintf(stderr, "Error creating report %s : %s\n", dbname_report, strerror(errno));
		return 2;
	}
	
	
	if ( do_interpolation )
	{
		// Intersect functions (interpolating with respect to reflectance if necessary):
		status = intersect_functions(
			&reflectance_list,
			&transmittance_list,
			&rhos_list,
			&skyl_list
		);
	
		if ( status != 0 )
		{
			fprintf(stderr, "error intersect_functions status = %d\n", status);
			return status;
		}
	}

	// call the main processing routine:
	status = process_sailh_2(
		reflectance_list,
		transmittance_list,
		rhos_list,
		skyl_list,
		hotspot_size,
		lai,
		ts,
		tv,
		psr,
		fraction,
		&canopy_reflectante_list
	);
	
	if ( status != 0 )
	{
		fprintf(stderr, "process_sailh_2 status = %d\n", status);
		return status;
	}
	
	size = list_size(canopy_reflectante_list);
	for ( i = 0; i < size; i++ )
	{
		RPoint* canopy_reflectance_point = (RPoint*) list_elementAt(canopy_reflectante_list, i);
		
		fprintf(file, "%f , %E\n", canopy_reflectance_point->x, canopy_reflectance_point->y);
	}

	fclose(file);
	
	list_destroy(canopy_reflectante_list);
	
	return 0;
	
}

