/* 
/////////////////////////////////////////////////////////////////////////
//
// sailh
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

#ifndef __SAILH_H
#define __SAILH_H

#include "cstars-rtm.h"

#ifndef __FUNC
#include "func.h"
#endif

#include "f2c.h"

#include <stdio.h>


///////////////////////////////////////////////////////////////////////////////
/**
 * Calculates the value of a function 1 on the output of the main sailh routine.
 * This corresponds to the value that the SAILH program is currently computing.
 */
real sailh_function_1
(
	int  print,
	real roo,
	real tau,
	real rsl,
	real hot,
	real lai,
	real ed,
	real tts,
	real azi,
	real ttol,
	real lidf[13]
);


///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Makes all processing with given parameters.
 *
 * Note about canopy_reflectante_list:
 *   If null, a new list is created internally; if not, 
 *   its size is first set to 0. Be sure the element size is sizeof(RPoint).
 */
int process_sailh_2(
	List reflectance_list,
	List transmittance_list,
	List rhos_list,
	List skyl_list,
	float hotspot_size,
	float lai,
	float ts,
	float tv,
	float psr,
	float* lidf,
	List  *canopy_reflectante_list		// resulting function
);


///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the leaf angle ditribution function given a canopy type.
 */
void getLADF(
	int can_type, 		// I- canopy type: 1, 2, 3, 4, 5 (not implemented), and 6
	float fraction[]	// O- distribution function in percentage
);


#endif	/*  __SAILH_H */
