/* 
/////////////////////////////////////////////////////////////////////////
//
// sailh
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/


#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>

#include "sailh.h"



///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Makes all processing with given parameters.
 */
int process_sailh_2(
	List reflectance_list,
	List transmittance_list,
	List rhos_list,
	List skyl_list,
	float hotspot_size,
	float lai,
	float ts,
	float tv,
	float psr,
	float* lidf,
	List  *canopy_reflectante_list		// resulting function
)
{
	long size, i;

	// for testing	
	long report_index;
	char* env;
	
	
	size = list_size(reflectance_list);
	
	if ( size != list_size(transmittance_list) 
	||   size != list_size(rhos_list)
	||   size != list_size(skyl_list)
	)
	{
		fprintf(stderr, "Functions are not conformant\n"
			"reflectance size   = %ld\n"
			"transmittance size = %ld\n"
			"rhos size          = %ld\n"
			"skyl size          = %ld\n",
				size,
				list_size(transmittance_list),
				list_size(rhos_list),
				list_size(skyl_list)
		);
		return 1;
	}
	
	report_index = -1L;
	env = getenv("SAILH_TEST");
	if ( env != NULL )
	{
		sscanf(env, "%ld", &report_index);
	}

	if ( *canopy_reflectante_list == NULL )
	{
		// create resulting function:
		*canopy_reflectante_list = list_create(sizeof(RPoint), size, 0);
	}
	else
	{
		list_setSize(*canopy_reflectante_list, 0);
	}
	
	for ( i = 0; i < size; i++ )
	{
		RPoint* refl_point = (RPoint*) list_elementAt(reflectance_list, i);
		RPoint* tran_point = (RPoint*) list_elementAt(transmittance_list, i);
		RPoint* soil_point = (RPoint*) list_elementAt(rhos_list, i);
		RPoint* skyl_point = (RPoint*) list_elementAt(skyl_list, i);
		RPoint  rcanopy_reflectance_point;
		
		//
		// PENDING
		// refl, tran, and soil are multiplied here by 100. Maybe this should
		// be an option, for example.
		//
		float roo = refl_point->y * 100;
		float tau = tran_point->y * 100;
		float rsl = soil_point->y * 100;
		
		float skyl = skyl_point->y;
		
		real result = sailh_function_1
			(
				report_index == i,
				roo,
				tau,
				rsl,
				hotspot_size,
				lai,
				skyl,
				ts,
				psr,
				tv,
				lidf
			);
			
		rcanopy_reflectance_point.x = refl_point->x;
		rcanopy_reflectance_point.y = result;
		
		list_addElement(*canopy_reflectante_list, &rcanopy_reflectance_point);
			
		if ( report_index == i )
		{
			int j;
			
			
			// new simpler format:	
	
			fprintf(stderr, 
				"%.9f\n" 
				"%.9f\n"
				"%.9f\n"
				"%.9f\n"
				"%.9f\n"
				"%.9f\n"
				"%.9f\n"
				"%.9f\n"
				"%.9f\n",
					roo,
					tau,
					rsl,
					hotspot_size,
					lai,
					skyl,
					ts,
					psr,
					tv
			);
			for ( j = 1; j <= 13; j++ )
			{
				fprintf(stderr, "%.9f\n", lidf[j-1]);
			}
			
			fprintf(stderr, "\n");
			
			fprintf(stderr, "RESULT = %e\n", result);
			fprintf(stderr, "index reported = %ld\n", report_index);
		
		}
	}	

	return 0;
}


