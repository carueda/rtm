/* 
/////////////////////////////////////////////////////////////////////////
//
// sailh inversion
//
// Carlos Rueda - CSTARS
// July 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

   
#include "opt.h"

#include "sailh.h"

#include "symboltable.h"


#include <errno.h>
#include <string.h>
#include <math.h>


static long counter;

#define num_vars 5
        // these 5 variables are:  hotspot_size lai ts tv psr 


static	char* observed_function_filename;
static	char modeled_function_filename[1024];

static int can_type;
	
static List rfl_list;
static List tns_list;
static List rhos_list;
static List skyl_list;

static List canopy_reflectante_list;
static List observed_function_list;
	
static	float fraction[14];


// range of interest:
static	float wl_from = 0.0;
static	float wl_to = 99999.0;
	


/**
 * If idef_filename == NULL, then a direct RMS error is computed;
 * otherwise, an observed_st symbol table is first constructed 
 * to compute an index RMS with the modeled functions.
 */
static char* idef_filename = NULL;

/**
 * meaningful to fun if idef_filename != NULL
 */
static FILE* idef_file = NULL;
static long observed_st;



/////////////////////////////////////////////////////////////////////////
static double fun(OPT_RValue* input)
{
	int status;

	float hotspot_size, lai, ts, tv, psr;
	
	double err;
	
	
	
	hotspot_size = input[0].d;
	lai = input[1].d;
	ts = input[2].d;
	tv = input[3].d;
	psr = input[4].d;
	
	status = process_sailh_2(
		rfl_list,
		tns_list,
		rhos_list,
		skyl_list,
		hotspot_size,
		lai,
		ts,
		tv,
		psr,
		fraction,
		&canopy_reflectante_list
	);

	if ( counter++ % 100 == 0 )
	{
		fputc('.', stdout);
		fflush(stdout);
	}

	if ( status != 0 )
	{
		fprintf(stderr, "status = %d\n", status);
		exit(1);
	}
		

	if ( idef_filename == NULL )
	{
		// get direct RMS
		err = getDirectRMS(observed_function_list, canopy_reflectante_list);
		list_destroy(canopy_reflectante_list);
	}
	else
	{
		// get index RMS
		err = getIndexRMS(observed_st, idef_filename, idef_file, canopy_reflectante_list);
	}

	return err;
}


/////////////////////////////////////////////////////////////////////////
static OPT_RVariableDefinition varDefs[num_vars];

static OPT_RObject opt_robj = 
{
	num_vars,
	varDefs,
	fun,
};
 


/////////////////////////////////////////////////////////////////////////
static 
int write_report(OPT_RValue best_input[], double best_output, char* report_filename)
{
	FILE* report_file;
	
	
	// open for appending:
	report_file = fopen(report_filename, "a");
	if ( report_file == NULL )
	{
		fprintf(stdout, "error opening %s\n", report_filename);
		return 1;
	}
	
	// WRITE ONE LINE OF INFORMATION
	
	// observed function filename:
	fprintf(report_file, "%s ", 
		observed_function_filename
	);
	
	// values:
	fprintf(report_file, "%g %g %g %g %g %d ", 
		best_input[0].d,
		best_input[1].d,
		best_input[2].d,
		best_input[3].d,
		best_input[4].d,
		can_type
	);

	// variable label:
	fprintf(report_file, "%s;%s;%s;%s;%s;%s ", 
		varDefs[0].lower.d < varDefs[0].upper.d ? "hot*" : "hot", 
		varDefs[1].lower.d < varDefs[1].upper.d ? "lai*" : "lai", 
		varDefs[2].lower.d < varDefs[2].upper.d ? "ts*"  : "ts", 
		varDefs[3].lower.d < varDefs[3].upper.d ? "tv*"  : "tv", 
		varDefs[4].lower.d < varDefs[4].upper.d ? "psr*" : "psr", 
		"cantype"
	);
	
	// error:
	fprintf(report_file, "RMSE= %g ", 
		best_output
	);
	
	// error computed on...:
	if ( idef_filename != NULL )
	{
		int i;
		
		fprintf(report_file, "on:");
		
		for ( i = 0; i < st_size(observed_st); i++ )
		{
			ST_Entry entry;
			char* id;

			entry = st_entryAt(observed_st, i);
			id = entry->id;
		
			if ( id[0] == '@' )
			{
				continue;
			}
			
			if ( i > 0 )
				fprintf(report_file, ";");
				
			fprintf(report_file, "%s", id);
		}
		fprintf(report_file, " ");
	}
	else
	{
		fprintf(report_file, "on:range ");
	}

	// range, and number of evaluations:
	fprintf(report_file, "range=%g:%g evals= %ld ", 
		wl_from,
		wl_to,
		counter
	);


	// modeled function filename:
	fprintf(report_file, "%s", 
		modeled_function_filename
	);
	
	
	fprintf(report_file, "\n");
	
	fclose(report_file);
	
	return 0;
}



/////////////////////////////////////////////////////////////////////////
static 
int write_modeled_function(OPT_RValue best_input[], char* modeled_function_filename)
{
	float hotspot_size, lai, ts, tv, psr;
		
	int status;
		
		
		
	fprintf(stdout, "creating %s\n", modeled_function_filename);

	hotspot_size = best_input[0].d;
	lai = best_input[1].d;
	ts = best_input[2].d;
	tv = best_input[3].d;
	psr = best_input[4].d;

	status = process_sailh_2(
		rfl_list,
		tns_list,
		rhos_list,
		skyl_list,
		hotspot_size,
		lai,
		ts,
		tv,
		psr,
		fraction,
		&canopy_reflectante_list
	);

	if ( status != 0 )
	{
		fprintf(stderr, "process_sailh_soil_2 status = %d\n", status);
		return status;
	}

	status = writeFunction(canopy_reflectante_list, modeled_function_filename);
	if ( 0 != status )
	{
		fprintf(stderr, "Error creating function %s : %s\n", modeled_function_filename, strerror(errno));
	}
	return status;
}





///////////////////////////////////////////////////////////////////////////////////////////////////
static 
int usage(char* msg)
{
	if ( msg )
	{
		fprintf(stderr, "\n%s\n", msg);
		fprintf(stderr, "Try `inv_sailh -h' for more information.\n\n");
		return 1;
	}

	fprintf(stdout,
"\n"
"CSTARS inv_sailh 0.1 (%s %s) %s\n"
"\n"
"USAGE\n"
"  inv_sailh [-h ] [-m method] [-i IDEF-file] [-r modeled-rfl] [-R range] \\\n"
"                     rfl tns rhos skyl hotspot_size lai cantype ts tv psr \\\n"
"                     observed_file report-file\n"
"\n"
"	-h                 Shows this message and exits\n"
"	-m method          Search method to be used: exhaustive | binary\n"
"	                   By default, a binary search is performed.\n"
"	-i IDEF-file       Compute error based on indices defined in this file.\n"
"	                   By default, direct reflectance RMS is used.\n"
"	-r modeled-rfl     Name for the the best reflectance function modeled\n"
"	                   By default, it takes the name observed_file.mod.\n"
"	-R range           Range of interest, with the form:  from:to\n"
"	rfl                Reflectance function file\n"
"	tns                Transmittance function file\n"
"	rhos               Soil reflectance function file\n"
"	skyl               Skyl function file\n"
"	hotspot_size       (*) Hotspot size\n"
"	lai                (*) Leaf area index of layer\n"
"	cantype         =  1(planophile)/2(erectophile)/3(plagiophile)/4(extremophile)\n"
"	                   5(uniform)/6(spherical)\n"
"	ts                 (*) TS\n"
"	tv                 (*) TV\n"
"	psr                (*) PSR\n"
"	observed_file      Observed reflectance data\n"
"	report-file        Report filename. A line of info is concatenated to this file.\n"
"\n"
"   (*) This argument can take the following form (with no spaces):  lower,upper,step\n"
"       i.e., three values separated by commas specifying the range of interest for the variable.\n"
"\n"
"EXAMPLE:\n"
"  inv_sailh 1.rfl 1.tns soil.txt skyl.txt 0.007 1,15,.5 3 30 0 0 observed.txt  report1.txt\n"
"\n"
"  In this case the lai variable will be searched; direct reflectance RMS will be used as\n"
"  a measure of error; the resulting function will be observed.txt.mod.\n"
"\n",
		       __DATE__, __TIME__,
			   RTM_VERSION_IN_USAGE
	);
	
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	int arg;
	int do_interpolation = 1;	// NOTE: THERE IS NO USER OPTION TO MODIFY THIS

	char* rfl_filename;
	char* tns_filename;
	char* rhos_filename;
	char* skyl_filename;
	
	int status;

	OPT_RValue best_input[num_vars];
	double best_output;
	
	int method = OPT_BINARY;
	
	char* report_filename;
	

	
	modeled_function_filename[0] = 0;

	// process options:
	for ( arg = 1; arg < argc && argv[arg][0] == '-'; arg++ )
	{
		if ( strcmp(argv[arg], "-h") == 0 )
		{
			return usage(NULL);
		}

		else if ( strcmp(argv[arg], "-m") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting argument to -m option");
				
			++arg;
			if ( strcmp(argv[arg], "exhaustive") == 0 )
				method = OPT_EXHAUSTIVE;
			else if ( strcmp(argv[arg], "binary") == 0 )
				method = OPT_BINARY;
			else
				return usage("Invalid argument to -m option");
		}
		else if ( strcmp(argv[arg], "-i") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting argument to -i option");
				
			++arg;
			idef_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-r") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting argument to -r option");
				
			++arg;
			strcpy(modeled_function_filename, argv[arg]);
		}
		else if ( strcmp(argv[arg], "-R") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting argument to -R option");
				
			++arg;
			if ( 2 != sscanf(argv[arg], "%f:%f", &wl_from, &wl_to) )
				return usage("Invalid argument to -R option");
		}
		else
		{
			return usage("Invalid option");
		}
	}
	
	
	if ( argc - arg < 12 )
	{
		return usage("Expecting arguments");
	}
	
	rfl_filename = argv[arg++];
	tns_filename = argv[arg++];
	rhos_filename = argv[arg++];
	skyl_filename = argv[arg++];
	
	opt_scanVariableDefinition(argv[arg++], &varDefs[0]);
	opt_scanVariableDefinition(argv[arg++], &varDefs[1]);
	
	sscanf(argv[arg++], "%d", &can_type);
	
	opt_scanVariableDefinition(argv[arg++], &varDefs[2]);
	opt_scanVariableDefinition(argv[arg++], &varDefs[3]);
	opt_scanVariableDefinition(argv[arg++], &varDefs[4]);
	
	observed_function_filename = argv[arg++];
	report_filename = argv[arg++];
	

	getLADF(can_type, fraction);
	

	if ( modeled_function_filename[0] == 0 )
	{
		// No -r option
		sprintf(modeled_function_filename, "%s.mod", observed_function_filename);
	}
	

	rfl_list = read_function(rfl_filename);
	if ( rfl_list == NULL )
	{
		fprintf(stderr, "error reading %s\n", rfl_filename);
		return 1;
	}

	tns_list = read_function(tns_filename);
	if ( tns_list == NULL )
	{
		fprintf(stderr, "error reading %s\n", tns_filename);
		return 1;
	}

	rhos_list = read_function(rhos_filename);
	if ( rhos_list == NULL )
	{
		fprintf(stderr, "error reading %s\n", rhos_filename);
		return 1;
	}

	skyl_list = read_function(skyl_filename);
	if ( skyl_list == NULL )
	{
		fprintf(stderr, "error reading %s\n", skyl_filename);
		return 1;
	}


	observed_function_list = read_function(observed_function_filename);
	if ( observed_function_list == NULL )
	{
		fprintf(stderr, "error reading %s\n", observed_function_filename);
		return 1;
	}


	// interpolate
	if ( do_interpolation )
	{
		// Intersect functions (interpolating with respect to reflectance if necessary):
		status = intersect_functions(
			&rfl_list,
			&tns_list,
			&rhos_list,
			&skyl_list
		);
	
		if ( status != 0 )
		{
			fprintf(stderr, "error intersect_functions status = %d\n", status);
			return status;
		}
		
		writeFunction(rfl_list, "RFL");
		writeFunction(tns_list, "TNS");
		writeFunction(rhos_list, "RHOS");
		writeFunction(skyl_list, "SKYL");
	}


	
	// process range to update observed function:
	if ( wl_from != 0.0 || wl_to != 99999.0 )
	{
		// range specified.
		
		List tmp_list;
		
		// update observed_rfl_list:
		tmp_list = function_extract(observed_function_list, wl_from, wl_to);
		list_destroy(observed_function_list);
		observed_function_list = tmp_list;
	}
		
	if ( idef_filename != NULL )
	{
		idef_file = fopen(idef_filename, "r");
		if ( idef_file == NULL )
		{
			fprintf(stderr, "error opening index definition file %s\n", idef_filename);
			return 1;
		}
		
		observed_st = getIndicesFromFunction(idef_filename, idef_file, observed_function_list);
		if ( observed_st < 0L )
		{
			fprintf(stderr, "error getting indices from %s\n", observed_function_filename);
			return 1;
		}
	}
	

	wl_from = ((RPoint*) list_elementAt(observed_function_list, 0))->x;
	wl_to = ((RPoint*) list_elementAt(observed_function_list, list_size(observed_function_list) - 1))->x;
	
	fprintf(stdout, "\nCSTARS inv_sailh - SAILH Inversion\n");
	fprintf(stdout, 
		"Observed range: %g -> %g\n",
			wl_from, wl_to
	);
	fprintf(stdout, "Inverting: ");

	counter = 0;
	status = opt_minimize(&opt_robj, method, best_input, &best_output);
	
	if ( status == 0 )
	{
		write_report(best_input, best_output, report_filename);
		write_modeled_function(best_input, modeled_function_filename);
	}
	else
	{
		fprintf(stderr, "status = %d\n", status);
	}

	if ( idef_filename != NULL )
	{
		fclose(idef_file);
	}

	list_destroy(rhos_list);
	list_destroy(skyl_list);
	
	return status;

}
