/* 
/////////////////////////////////////////////////////////////////////////
//
// sailh
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

/*
 *	sailh_prm: driver to sailh_().
 *	Author: Carlos Rueda
 *	Date:   5/25/01
 */
 
#include "f2c.h"

/* Common Block Declarations */

struct {
    real pi, rd, dg, tts, tto, psi;
} angles_;


struct {
    real lai, li[13], f[13], roo, tau, hot;
} sailhcom_;


struct {
    real tss, too, tsstoo, rdd, tdd, rsd, tsd, rdo, tdo, rso;
} rfltrn_;



/////////////////////////////////////////////////////////////////////////////
int sailh_prm(
	real pi,		// inputs
	real rd,
	real dg,
	real tts,
	real tto,
	real psi,
	real lai,
	real li[13],
	real f[13],
	real roo,
	real tau,
	real hot,
	
	real *tss,		// outputs
	real *too,
	real *tsstoo,
	real *rdd,
	real *tdd,
	real *rsd,
	real *tsd,
	real *rdo,
	real *tdo,
	real *rso
)
{
	extern int sailh_();

	// prepare inputs:
	angles_.pi = pi;
	angles_.rd = rd;
	angles_.dg = dg;
	angles_.tts = tts;
	angles_.tto = tto;
	angles_.psi = psi;
	sailhcom_.lai = lai;
	memcpy(sailhcom_.li, li, sizeof(sailhcom_.li));
	memcpy(sailhcom_.f, f, sizeof(sailhcom_.f));
	sailhcom_.roo = roo;
	sailhcom_.tau = tau;
	sailhcom_.hot = hot;
	
	// call SAILH:
	sailh_();
	
	// take outputs:
	*tss = rfltrn_.tss;
	*too = rfltrn_.too;
	*tsstoo = rfltrn_.tsstoo;
	*rdd = rfltrn_.rdd;
	*tdd = rfltrn_.tdd;
	*rsd = rfltrn_.rsd;
	*tsd = rfltrn_.tsd;
	*rdo = rfltrn_.rdo;
	*tdo = rfltrn_.tdo;
	*rso = rfltrn_.rso;

	return 0;
}

