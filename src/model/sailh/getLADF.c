/* 
/////////////////////////////////////////////////////////////////////////
//
// sailh
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "sailh.h"


#define PI 3.141592654


static float ang[14] = { 0, 10, 20, 30, 40, 50, 60, 70, 80, 82, 84, 86, 88, 90 };


///////////////////////////////////////////////////////////////////////////////////////////////////
void getLADF(
	int can_type, 		// I- canopy type: 1, 2, 3, 4, 5 (not inplemented), and 6
	float fraction[]	// O- distribution function in percentage
)
{
	float check, x1, x2;
	int a;
	
	check=0;
	for (a=1; a<=13; a++)
	{
		if (can_type==1)
			{
			x1=(ang[a-1])*PI/180;
			x2=(ang[a])*PI/180;
			fraction[a]=((2/PI)*((x2)+(0.5*sin(2*x2))))-((2/PI)*((x1)+(0.5*sin(2*x1))));
			check=check+fraction[a];
			//printf ("%f %f %f %f %f\n", x1, x2, ang[a-1], ang[a], fraction[a]);
			}
		if (can_type==2)
			{
			x1=(ang[a-1])*PI/180;
			x2=(ang[a])*PI/180;
			fraction[a]=((2/PI)*((x2)-(0.5*sin(2*x2))))-((2/PI)*((x1)-(0.5*sin(2*x1))));
			check=check+fraction[a];
			//printf ("%f %f %f %f %f\n", x1, x2, ang[a-1], ang[a], fraction[a]);
			}
		if (can_type==3)
			{
			x1=(ang[a-1])*PI/180;
			x2=(ang[a])*PI/180;
			fraction[a]=((2/PI)*((x2)-(0.25*sin(4*x2))))-((2/PI)*((x1)-(0.25*sin(4*x1))));
			check=check+fraction[a];
			//printf ("%f %f %f %f %f\n", x1, x2, ang[a-1], ang[a], fraction[a]);
			}
		if (can_type==4)
			{
			x1=(ang[a-1])*PI/180;
			x2=(ang[a])*PI/180;
			fraction[a]=((2/PI)*((x2)+(0.25*sin(4*x2))))-((2/PI)*((x1)+(0.25*sin(4*x1))));
			check=check+fraction[a];
			//printf ("%f %f %f %f %f\n", x1, x2, ang[a-1], ang[a], fraction[a]);
			}
		if (can_type==5)
			{
			fprintf(stderr, "*********** WARNING: can_type = 5 : Not implemented yet\n");
			return;
			}
		if (can_type==6)
			{
			x1=(ang[a-1])*PI/180;
			x2=(ang[a])*PI/180;
			fraction[a]= (-cos(x2))-(-cos(x1));
			check=check+fraction[a];
			//printf ("%f %f %f %f %f\n", x1, x2, ang[a-1], ang[a], fraction[a]);
			}
	}

	if ( fabs(check - 1.0) > 0.001 )
	{
		fprintf(stderr, 
			"*********** WARNING: |check - 1.0| > 0.001;  check=%f, can_type=%d\n",
			check, can_type
		);
	}

	for (a=1; a<=13; a++)
	{
		fraction[a - 1] = fraction[a] * 100.0;
	}

}

