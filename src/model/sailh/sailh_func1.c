/* 
/////////////////////////////////////////////////////////////////////////
//
// sailh
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

#include "sailh.h"

#include <stdio.h>


    /* Builtin functions */
    double atan();
    integer f_open(), f_rew(), s_rsfe(), do_fio(), e_rsfe(), f_clos(), s_wsfe(), e_wsfe();
    int s_stop();




extern int sailh_prm(
	real pi,		// inputs
	real rd,
	real dg,
	real tts,
	real tto,
	real psi,
	real lai,
	real li[13],
	real f[13],
	real roo,
	real tau,
	real hot,
	
	real *tss,		// outputs
	real *too,
	real *tsstoo,
	real *rdd,
	real *tdd,
	real *rsd,
	real *tsd,
	real *rdo,
	real *tdo,
	real *rso
);





/* Private Block Declarations (carueda) */

static struct {
    real pi, rd, dg, tts, tto, psi;
} xx_angles_;

#define angles_1 xx_angles_

struct sailhcom_1_ {
    real lai, li[13], f[13], roo, tau, hot;
};

#define sailhcom_1 (*(struct sailhcom_1_ *) &xx_sailhcom_)

static struct {
    real tss, too, tsstoo, rdd, tdd, rsd, tsd, rdo, tdo, rso;
} xx_rfltrn_;

#define rfltrn_1 xx_rfltrn_

/* Initialized data */

static struct {
    integer fill_1[1];
    real e_2[13];
    integer fill_3[16];
    } xx_sailhcom_ = { {0}, { (float)5., (float)15., (float)25., (float)35., (
	    float)45., (float)55., (float)65., (float)75., (float)81., (float)
	    83., (float)85., (float)87., (float)89. }
      };


///////////////////////////////////////////////////////////////////////////////
real sailh_function_1
(
	int print,
	real roo,
	real tau,
	real rsl,
	real hot,
	real lai,
	real ed,
	real tts,
	real azi,
	real ttol,
	real lidf[13]
)
{

    real r__1;

    /* Local variables */
    static integer ia;
    static real rn, rsb, tti;


    angles_1.pi = atan((float)1.) * (float)4.;
    angles_1.rd = angles_1.pi / (float)180.;
    angles_1.dg = (float)1. / angles_1.rd;

	
	sailhcom_1.roo = roo;
	sailhcom_1.tau = tau;
	sailhcom_1.hot = hot;
	sailhcom_1.lai = lai;
	
	angles_1.tts = tts,


    tti = ttol;

    angles_1.psi = azi;

    if (tti >= (float)180.) {
	angles_1.psi = (float)180. - azi;
    }
    angles_1.tto = dabs(tti);


    for (ia = 1; ia <= 13; ++ia) {
		sailhcom_1.f[ia - 1] = lidf[ia - 1];

    }


if ( print ) 
{ // DEBUG
int i;
printf("sailh_prm INPUT  %e %e %e %e %e %e %e %e %e %e \n",
	angles_1.pi,		// inputs
	angles_1.rd,
	angles_1.dg,
	angles_1.tts,
	angles_1.tto,
	angles_1.psi,
	sailhcom_1.lai,
//	real li[13],
//	real f[13],
	sailhcom_1.roo,
	sailhcom_1.tau,
	sailhcom_1.hot
);
for (i = 0; i < 13; i++ )
	printf("%e ", sailhcom_1.f[i]);
printf("\n");
for (i = 0; i < 13; i++ )
	printf("%e ", sailhcom_1.li[i]);
printf("\n");
} // DEBUG


    sailh_prm(
	angles_1.pi,		// inputs
	angles_1.rd,
	angles_1.dg,
	angles_1.tts,
	angles_1.tto,
	angles_1.psi,
	sailhcom_1.lai,
	sailhcom_1.li,
	sailhcom_1.f,
	sailhcom_1.roo,
	sailhcom_1.tau,
	sailhcom_1.hot,
	
	&rfltrn_1.tss,		// outputs
	&rfltrn_1.too,
	&rfltrn_1.tsstoo,
	&rfltrn_1.rdd,
	&rfltrn_1.tdd,
	&rfltrn_1.rsd,
	&rfltrn_1.tsd,
	&rfltrn_1.rdo,
	&rfltrn_1.tdo,
	&rfltrn_1.rso
    );

if ( print ) 
{ // DEBUG
printf("sailh_prm OUTPUT %e %e %e %e %e %e %e %e %e %e \n",
	rfltrn_1.tss,		// outputs
	rfltrn_1.too,
	rfltrn_1.tsstoo,
	rfltrn_1.rdd,
	rfltrn_1.tdd,
	rfltrn_1.rsd,
	rfltrn_1.tsd,
	rfltrn_1.rdo,
	rfltrn_1.tdo,
	rfltrn_1.rso
);
}


	rsb = rsl * (float).01;
	rn = (float)1. - rsb * rfltrn_1.rdd;

	rfltrn_1.rso += rsb * (rfltrn_1.tsstoo + (rfltrn_1.tdo * (
		rfltrn_1.tss + rfltrn_1.tsd) + rfltrn_1.too * (rfltrn_1.tsd + 
		rfltrn_1.tss * rsb * rfltrn_1.rdd)) / rn);
	rfltrn_1.rdo += rfltrn_1.tdd * rsb * (rfltrn_1.tdo + rfltrn_1.too) / 
		rn;

	r__1 = (rfltrn_1.rso + rfltrn_1.rdo * ed) / (ed + (float)1.) * 
		(float)100.;

	r__1 = r__1 / 100;
	
	return r__1;
}


