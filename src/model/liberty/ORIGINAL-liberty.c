/* LIBERTY.C
 *
 * A Leaf Incorporating Biochemistry Exhibiting Reflectance
 * and Transmittance Yields
 *
 *
 * Written by: Terence Dawson
 * Department of Geography
 * University of Southampton
 * Highfield
 * Southampton, UK
 *
 * Note: You are welcome to adapt, change and modify this program
 * to your own requirements. Please just drop me a line to let me know
 * of any useful results, modifications or otherwise!
 *
 * Started : 8th March 1995
 * Finished:
 * Last modified: 15th August 1996,
 *                24th September 1996
 *                16th October 1996
 *                28th August 1997
 *
 * References:
 * Dawson, T.P., Curran, P.J. and Plummer, S.E. (1996), 'LIBERTY - Modelling
 * the effects of leaf biochemical concentration on reflectance spectra',
 * Remote Sensing of Environment, Submitted.
 *
 * Melamed, M.T. (1963), 'Optical properties of powders. Part 1. optical
 * absorption coefficients and the absolute value of the diffuse
 * reflectance', Journal of Applied Physics, 34, 560-570.
 *
 * Benford, F. (1946), 'Radiation in a diffusing medium', Journal of the
 * Optical Society of America, 36, 524-537.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>

/* defining all the functions for LIBERTY... */

void startup(void);
void refrac(void);
void para_rad(void);
void vert_rad(void);
void tot_ref(void);
void eval_me(void);
void eval_mi(void);
void leaf_params(void);
void eval_x(void);
void calc_M(void);
void calc_T(void);
void calc_R(void);
void tidyup(void);
void contfunc(void);

/* Defining global variables and parameters... */

double PI;
double N0, N1, alpha, beta, in_angle, vert_r, para_r, refl;
double critical, me, mi, coeff, M, T, x, R, Alt_R,refl,trans,rb,tb,rif,tif;
float D, xu, thick; /* sphere diameter, air gap and thickness parameters */

main()
{
startup();
leaf_params();
tidyup();
}

/*---------------------------------------------------------------------*/
/* Start-up screen and initialisation of variables and parameters etc */

void startup(void)
{
printf("\nThe LIBERTY Leaf Model\n\n");
printf("Version 1.1A\n\n");
printf("\nDeveloped by :\nTerence Dawson\n");
printf("Department of Geography\n");
printf("University of Southampton\n\n");
printf("(c) Terence Dawson, October 1996\n\n");
para_r=0;
vert_r=0;
PI = 180 * (atan(1)/45);
}

/*---------------------------------------------------------------------*/

void tidyup(void)
{
   printf("\nAll files closed - Normal Termination.\n");
   printf("Reflectance data has been written to the file liberty.out\n");
}

/*---------------------------------------------------------------------*/

/* Applying the index of refraction */
void refrac(void)
{
/* average angle of incident light */
in_angle = 59;
/* Index of refraction */
N0 = 1.0;
alpha = in_angle * PI/180;
beta = asin((N0/N1) * sin(alpha));
/* printf("Alpha: %f Beta: %f\n",alpha,beta); */
}

/*---------------------------------------------------------------------*/

/* working out the horizontal (parallel to plane) component of reflected */
/* radiation (Snells Law of Refraction) */
void para_rad(void)
{
	/* printf("Testing coeficients of reflection...\n"); */
	para_r = (tan(alpha - beta))/(tan(alpha + beta));
	/* printf("Parallel: %f",para_r); */
}

/*--------------------------------------------------------------------*/

/* working out the vertical component of reflected radiation...*/
void vert_rad(void)
{
	vert_r = -(sin(alpha-beta))/(sin(alpha+beta));
	/* printf(" Vertical: %f\n",vert_r); */
}

/*--------------------------------------------------------------------*/

/* working out the total reflected radiation...*/
void tot_ref(void)
{
	double plus, dif;

	plus = alpha + beta;
	dif = alpha - beta;

	refl =  0.5 * ( ((sin(dif)*sin(dif))/(sin(plus)*sin(plus))) +
		((tan(dif)*tan(dif))/(tan(plus)*tan(plus))) );
}


/*-------------------------------------------------------------------------*/

/* The evaluation of the regular reflectance for diffuse incident */
/* radiation me for angles of alpha between 0 and PI/2  */
void eval_me(void)
{
   int a;
   double width;

   me = 0;
   width = PI/180;

   for (a = 1; a <= 90; a++) {
	alpha = a * PI/180;
	beta = asin(N0/N1 * sin(alpha));
	tot_ref();
	me = me + (refl * sin(alpha) * cos (alpha) * width);
	}
   me=me*2;
   /* printf("me : %f\n",me); */
}

/*-------------------------------------------------------------------------*/

void eval_mi(void)
{
   int a;
   double mint,width;

   mi = 0;
   mint = 0;
   width = PI/180;
   critical = asin(N0/N1)*180/PI;
   /* printf("Critical angle: %f\n",critical); */
   for (a = 1; a <= (critical); a++) {
	alpha = a * PI/180;
	beta = asin((N0/N1) * sin(alpha));
	tot_ref();
	mint = mint + (refl * sin(alpha) * cos (alpha) * width);
	}
   mi = (1 - (sin(critical*PI/180))*(sin(critical*PI/180))) + (2*mint);
   /* printf("mi : %f\n",mi); */
}


/*-----------------------------------------------------------------------*/

void leaf_params(void)
{
	FILE *infile, *outfile;
	int i;
	float c_factor, w_factor, l_factor, p_factor, element, baseline,a,b,c,d,e,f;
	float k_chloro[500], k_water[500], k_ligcell[500], k_protein[500], ke[500];

	/* reading in pigment absorption coefficients... */

	if ((infile = fopen("pigment.dat","rt")) == NULL) {
	    printf("Could not open pigment absorption file!\n");
	    exit(0);
	    }
	/* Ok so far, Now to read in the absorption data... */
	rewind(infile);
	printf("Reading pigment absorption coefficients...");
	i = 0;
	while (!feof(infile)) {
	     fscanf(infile,"%f",&k_chloro[i]);
	     i=i+1;
	}
	fclose(infile);
	/* Now to set chloro absorption to zero for higher wavelengths */
	for (i = 120; i <= 420; i++) {
	k_chloro[i] = 0;
	}
	printf("done!\n");
	/* similarly for the water absorption coefficients... */

	if ((infile = fopen("water.dat","rt")) == NULL) {
	    printf("Could not open water absorption file!\n");
	    exit(0);
	    }
	/* Ok so far, Now to read in the absorption data... */
	rewind(infile);
	printf("Reading water absorption coefficients...");
	i = 0;
	while (!feof(infile)) {
	     fscanf(infile,"%f",&k_water[i]);
	     i=i+1;
	}
	fclose(infile);
	printf("done!\n");

	/* now to read in trace element absorption coefficients... */

	if ((infile = fopen("albino.dat","rt")) == NULL) {
	    printf("Could not open albino absorption data.\n");
	    exit(0);
	    }
	/* reset ke[i] array... */
	for (i = 0; i <= 420; i++) {
	ke[i] = 0;
	}
	/* Ok so far, Now to read in the absorption data... */
	rewind(infile);
	printf("Reading trace element absorption coefficients...");
	i = 0;
	while (!feof(infile)) {
	     fscanf(infile,"%f",&ke[i]);
	     /* printf("albino: %d %f\n",i,ke[i]); */
	     i=i+1;
	}
	fclose(infile);
	printf("done!\n");


	/* now to read in cellulose and lignin absorption coefficients... */

	if ((infile = fopen("ligcell.dat","rt")) == NULL) {
	    printf("Could not open lignin/cellulose data file!\n");
	    exit(0);
	    }
	/* Ok so far, Now to read in the absorption data... */
	rewind(infile);
	/* first reset k_ligcell[i] array... */
	for (i = 0; i <= 420; i++) { k_ligcell[i] = 0; }
	printf("Reading cellulose and lignin absorption coefficients...");
	i = 0; /* Note: this is because the data starts from 400 nm */
	while (!feof(infile)) {
	     fscanf(infile,"%f",&k_ligcell[i]);
	     /* printf("%d %f\n",(400+(i*5)),k_ligcell[i]); */
	     i=i+1;
	}
	fclose(infile);
	printf("done!\n");

	/* now to read in protein absorption coefficients... */
	if ((infile = fopen("protein.dat","rt")) == NULL) {
	    printf("Could not open protein absorption file!\n");
	    exit(0);
	    }
	/* Ok so far, Now to read in the absorption data... */
	rewind(infile);
	printf("Reading protein absorption coefficients...");
	i = 0; /* Note: this is because the data starts from 400 nm */
	while (!feof(infile)) {
	     fscanf(infile,"%f",&k_protein[i]);
	     i=i+1;
	}
	fclose(infile);
	printf("done!\n");


	/* Open liberty.out for writing data...*/
	if ((outfile = fopen("liberty.out","w")) == NULL) {
	    printf("Could not open liberty.out\n");
	    }
	rewind(outfile);
	/* write the name of the variables to outfile */
	/* These are wavelength W, Infinite reflectance R, */
	/* single leaf reflectance refl and transmittance trans */
	fprintf(outfile,"W R refl trans\n");

/* Just to make life easier, any of the following scanf inputs
can be commented out and variables replaced with fixed value !!!
................................................................*/

	/*** sphere diameter ***/
	printf("\nAverage leaf cell diameter (m-6) (20-100) : ");
	scanf("%f",&D);

	/*** gap length between cells ***/
	printf("Intercellular air space determinant (0.01 - 0.1) : ");
	scanf("%f",&xu);

	/*** thickness of leaf ***/
	printf("Leaf thickness (1 - 10) : ");
	scanf("%f",&thick);

        /*** element factor ***/
	printf("linear (baseline) absorption (0.0004 - 0.001) : ");
	scanf("%f",&baseline);

	/*** albino factor ***/
	printf("Albino leaf (visible) absorption (0 - 10) : ");
	scanf("%f",&element);

	printf("\n");

	/*** chlorophyll concentrations ***/
	printf("Leaf chlorophyll content (mg.m-2) (0 - 600) : ");
	scanf("%f",&c_factor);

	/*** water content ***/
	printf("Leaf water content (g.m-2) (0 - 500) : ");
	scanf("%f",&w_factor);

	/*** lignin and cellulose factor ***/
	printf("Lignin / cellulose content (g.m-2) (10 - 80) : ");
	scanf("%f",&l_factor);

	/*** protein factor ***/
	printf("Nitrogen content (g.m-2) (0 - 2) : ");
	scanf("%f",&p_factor);

/*...............................................................*/



	/* set the following loop for 420 (to 2500nm) or 120 (1000nm) */
	for (i = 0; i <= 420; i++) {
	  /* determining the total absorption coefficient... */
	  coeff = (D * (baseline+(k_chloro[i]*c_factor)+(k_water[i]*w_factor)+(ke[i]*element)+(k_ligcell[i]*l_factor)+(k_protein[i]*p_factor)));
	  /* change of refractive index over wavelength... */

		N1=1.4891-(0.0005*i);
		refrac();
		para_rad();
		vert_rad();
		eval_me();
		eval_mi();

	  calc_M();
	  calc_T();
	  eval_x();
	  calc_R();

	  /* The next bit works out transmittance based upon Benford...*/
	  /* setting up unchanging parameters... */
	  a=(2*x*me)+(x*T)-(x*T*2*x*me);
	  rb=a;
	  tb=sqrt(((R-rb)*(1-(R*rb)))/R);
	  contfunc();
	  //printf("%d %f %f %f\n",(400+(i*5)),R,refl,trans);
	  fprintf(outfile,"%d %f %f %f\n",(400+(i*5)),R,refl,trans);
	  }
	fclose(outfile);
}

/*-----------------------------------------------------------------------*/

	/* Evaluation of M, the total radiation reaching
	the surface after one pass through the sphere */

void calc_M(void)
{
	  M = (2/(coeff * coeff))*(1-(coeff+1)*exp(-coeff));
}

/*-----------------------------------------------------------------------*/

void calc_T(void)
{
	  T = ((1-mi) * M)/(1-(mi * M));
}

/*-----------------------------------------------------------------------*/

void calc_R(void)
{
	double a,b,c,next_R;
	int iterations;

	a = (me * T) + (x * T) - me - T - (x * me * T);
	b = 1 + (x * me * T) - (2 * x * x * me * me * T);
	c = (2 * me * x * x * T) - (x * T) - (2 * x * me);

	R = 0.5;   /* initial guess */
	for (iterations = 1; iterations < 50; iterations++) {
		/* simple iterative method... */
		next_R = -(a*(R*R)+c)/b;
		/* printf("alt_r: %f\n", next_R); */
		R = next_R;
		}


}

/*-----------------------------------------------------------------------*/

void eval_x(void)
{
	x = xu / (1 - (1 - (2*xu)) * T);
}

/*-----------------------------------------------------------------------*/

/* subroutine to determine the value of R and T as a continuous function
   of thickness - Benford, F., 1946, 'Radiation in a diffusing medium',
   Journal of the Optical Society of America, 36, 524-537. */
void contfunc(void)
{
	double fraction,top,bot1,bot2,cur_t,cur_r,prev_t,prev_r;
	int step, whole;

	/* little trick to seperate the fractional part from the real number... */
	whole=thick;
	fraction=thick-whole;

	/* The next bit works out the fractional value */
	/* for the interval between 1 and 2... */
	top=pow(tb,1+fraction)*(pow((pow((1+tb),2)-pow(rb,2)),(1-fraction)));
	bot1=(pow((1+tb),(2*(1-fraction)))-pow(rb,2));
	bot2=(1+((64/3)*fraction)*(fraction-0.5)*(fraction-1)*0.001);
	tif=top/(bot1*bot2);
	rif=(1+pow(rb,2)-pow(tb,2)-sqrt(pow((1+pow(rb,2)-pow(tb,2)),2)-(4*pow(rb,2)*(1-pow(tif,2)))))/(2*rb);

	/* Now to work out for integral thickness greater than 2 ... */
	if (whole >= 2) {
		prev_t=1;
		prev_r=0;
		for (step = 1; step<= (whole-1); step++) {
		   cur_t=(prev_t * tb)/(1-(prev_r*rb));
		   cur_r=prev_r + (((prev_t*prev_t)*rb)/(1-(prev_r*rb)));
		   prev_t=cur_t;
		   prev_r=cur_r;
		   }
	}
	else {
		cur_t=1;
		cur_r=0;
	}
	/* Combine the two results for thickness from 1 to infinity... */
	trans=(cur_t*tif)/(1-(rif*cur_r));
	refl=cur_r+((cur_t*cur_t*rif)/(1-(rif*cur_r)));
}
