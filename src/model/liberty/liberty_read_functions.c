
#include "liberty.h"

#include "rpoint.h"		// RPoint


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>



/////////////////////////////////////////////////////////////////////////////////
/**
 * 
 */
void liberty_read_functions(
	char* protein_filename,		// I- 
	char* pigment_filename,		// I- 
	char* water_filename,		// I- 
	char* albino_filename,		// I- 
	char* ligcell_filename,		// I- 
	float k_chloro[500], 		// O- 
	float k_water[500],			// O- 
	float k_ligcell[500],		// O- 
	float k_protein[500],		// O- 
	float ke[500]				// O- 
)
{
	FILE *infile;
	int i;
//	float a  /*,b,c,d,e,f*/ ;
	

	if ((infile = fopen(pigment_filename,"rt")) == NULL) {
	    printf("Could not open pigment absorption file %s!\n", pigment_filename);
	    exit(0);
	    }
	/* Ok so far, Now to read in the absorption data... */
	rewind(infile);
	//printf("Reading pigment absorption coefficients...");
	i = 0;
	while (!feof(infile)) {
	     fscanf(infile,"%f",&k_chloro[i]);
	     i=i+1;
	}
	fclose(infile);
	
	/* Now to set chloro absorption to zero for higher wavelengths */
	for (i = 120; i <= 420; i++) {
		k_chloro[i] = 0;
	}
	//printf("done!\n");
	
	/* similarly for the water absorption coefficients... */

	if ((infile = fopen(water_filename,"rt")) == NULL) {
	    printf("Could not open water absorption file %s!\n", water_filename);
	    exit(0);
	    }
	/* Ok so far, Now to read in the absorption data... */
	rewind(infile);
	//printf("Reading water absorption coefficients...");
	i = 0;
	while (!feof(infile)) {
	     fscanf(infile,"%f",&k_water[i]);
	     i=i+1;
	}
	fclose(infile);
	//printf("done!\n");

	/* now to read in trace element absorption coefficients... */

	if ((infile = fopen(albino_filename,"rt")) == NULL) {
	    printf("Could not open albino absorption data %s.\n", albino_filename);
	    exit(0);
	    }
	/* reset ke[i] array... */
	for (i = 0; i <= 420; i++) {
		ke[i] = 0;
	}
	/* Ok so far, Now to read in the absorption data... */
	rewind(infile);
	//printf("Reading trace element absorption coefficients...");
	i = 0;
	while (!feof(infile)) {
	     fscanf(infile,"%f",&ke[i]);
	     /* printf("albino: %d %f\n",i,ke[i]); */
	     i=i+1;
	}
	fclose(infile);
	//printf("done!\n");


	/* now to read in cellulose and lignin absorption coefficients... */

	if ((infile = fopen(ligcell_filename,"rt")) == NULL) {
	    printf("Could not open lignin/cellulose data file %s!\n", ligcell_filename);
	    exit(0);
	    }
	/* Ok so far, Now to read in the absorption data... */
	rewind(infile);
	/* first reset k_ligcell[i] array... */
	for (i = 0; i <= 420; i++) 
	{
		k_ligcell[i] = 0; 
	}
	//printf("Reading cellulose and lignin absorption coefficients...");
	i = 0; /* Note: this is because the data starts from 400 nm */
	while (!feof(infile)) {
	     fscanf(infile,"%f",&k_ligcell[i]);
	     /* printf("%d %f\n",(400+(i*5)),k_ligcell[i]); */
	     i=i+1;
	}
	fclose(infile);
	//printf("done!\n");

	/* now to read in protein absorption coefficients... */
	if ((infile = fopen(protein_filename,"rt")) == NULL) {
	    printf("Could not open protein absorption file %s!\n", protein_filename);
	    exit(0);
	    }
	/* Ok so far, Now to read in the absorption data... */
	rewind(infile);
	//printf("Reading protein absorption coefficients...");
	i = 0; /* Note: this is because the data starts from 400 nm */
	while (!feof(infile)) {
	     fscanf(infile,"%f",&k_protein[i]);
	     i=i+1;
	}
	fclose(infile);
	//printf("done!\n");

}
