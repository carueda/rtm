#include "liberty.h"

#include "rpoint.h"		// RPoint


#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>


static 	float k_chloro[500];
static 	float k_water[500];
static 	float k_ligcell[500];
static 	float k_protein[500];
static 	float ke[500];



/////////////////////////////////////////////////////////////////////////
static 
int write_modeled_functions(
	List rfl_list, 
	List tns_list, 
	char* modeled_rfl_filename, 
	char* modeled_tns_filename
)
{
	int status;

	fprintf(stdout, "creating %s\n", modeled_rfl_filename);
	status = writeFunction(rfl_list, modeled_rfl_filename);
	if ( 0 != status )
	{
		fprintf(stderr, "Error creating function %s : %s\n", modeled_rfl_filename, strerror(errno));
		return status;
	}
	
	fprintf(stdout, "creating %s\n", modeled_tns_filename);
	status = writeFunction(tns_list, modeled_tns_filename);
	if ( 0 != status )
	{
		fprintf(stderr, "Error creating function %s : %s\n", modeled_tns_filename, strerror(errno));
	}
	return status;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
static 
int usage(char* msg)
{
	if ( msg )
	{
		fprintf(stderr, "\n%s\n", msg);
		fprintf(stderr, "Try `liberty -h' for more information.\n\n");
		return 1;
	}

	fprintf(stdout, 
"\n"
"CSTARS liberty 0.1 (%s %s) %s\n"
"\n"
"USAGE: \n"
"  liberty D xu thick baseline element c_factor w_factor l_factor p_factor \\\n"
"          protein-file pigment-file water-file albino-file ligcell-file \\\n"
"          rfl-file tns-file\n"
"\n"
"EXAMPLE: \n"
"  liberty 40 .045 1.6 .0004 2 200 100 40 1 \\\n"
"          protein.dat pigment.dat water.dat albino.dat ligcell.dat \\\n"
"          mod.rfl mod.tns\n"
"(provided the files protein.dat, pigment.dat, water.dat, albino.dat, and ligcell.dat\n"
"exist in the current directory)\n"
"\n",
		       __DATE__, __TIME__,
			   RTM_VERSION_IN_USAGE
	);

	return 0;
}



/////////////////////////////////////////////////////////////////////////////////
/**
 *
 */
int main(int argc, char** argv)
{
	int arg;
	float D;
	float xu;
	float thick;
	float baseline;
	float element;
	float c_factor;
	float w_factor;
	float l_factor;
	float p_factor;
	char* protein_filename;
	char* pigment_filename;
	char* water_filename;
	char* albino_filename;
	char* ligcell_filename;
	
	char* modeled_rfl_filename;
	char* modeled_tns_filename;

	List  rfl_list;
	List  tns_list;
	
	int status;
	
	
	// process options:
	for ( arg = 1; arg < argc && argv[arg][0] == '-'; arg++ )
	{
		if ( strcmp(argv[arg], "-h") == 0 )
		{
			return usage(NULL);
		}
		else
		{
			return usage("Invalid option");
		}
	}
	
	if ( argc - arg < 17 )
	{
		return usage("Expecting arguments");
	}
	
	
	arg = 1;
	sscanf(argv[arg++], "%f", &D);
	sscanf(argv[arg++], "%f", &xu);
	sscanf(argv[arg++], "%f", &thick);
	sscanf(argv[arg++], "%f", &baseline);
	sscanf(argv[arg++], "%f", &element);
	sscanf(argv[arg++], "%f", &c_factor);
	sscanf(argv[arg++], "%f", &w_factor);
	sscanf(argv[arg++], "%f", &l_factor);
	sscanf(argv[arg++], "%f", &p_factor);
	protein_filename = argv[arg++];
	pigment_filename = argv[arg++];
	water_filename = argv[arg++];
	albino_filename = argv[arg++];
	ligcell_filename = argv[arg++];
	modeled_rfl_filename = argv[arg++];
	modeled_tns_filename = argv[arg++];

	
	// read functions to liberty:
	liberty_read_functions(
		protein_filename,
		pigment_filename,
		water_filename,
		albino_filename,
		ligcell_filename,
		k_chloro,
		k_water,
		k_ligcell,
		k_protein,
		ke
	);

	status = liberty_prm(
		D,
		xu,
		thick,
		baseline,
		element,
		c_factor,
		w_factor,
		l_factor,
		p_factor,
		k_chloro,
		k_water,
		k_ligcell,
		k_protein,
		ke,
		&rfl_list,
		&tns_list
	);
	if ( status != 0 )
	{
		fprintf(stderr, "status = %d\n", status);
		exit(1);
	}
		
	
	return write_modeled_functions(
		rfl_list, 
		tns_list, 
		modeled_rfl_filename, 
		modeled_tns_filename
	);

}
