#ifndef __LIBERTY_H
#define __LIBERTY_H

#include "cstars-rtm.h"

#ifndef __FUNC_H
#include "func.h"
#endif


/////////////////////////////////////////////////////////////////////////////////
/**
 * 
 */
void liberty_read_functions(
	char* protein_filename,		// I- 
	char* pigment_filename,		// I- 
	char* water_filename,		// I- 
	char* albino_filename,		// I- 
	char* ligcell_filename,		// I- 
	
	float k_chloro[500], 		// O- 
	float k_water[500],			// O- 
	float k_ligcell[500],		// O- 
	float k_protein[500],		// O- 
	float ke[500]				// O- 
);


/////////////////////////////////////////////////////////////////////////////////
/**
 * Using lists.
 */
int liberty_prm(
	float D_,					// I- Average leaf cell diameter (m-6) (20-100)
	float xu_,					// I- Intercellular air space determinant (0.01 - 0.1)
	float thick_,				// I- Leaf thickness (1 - 10)
	float baseline_,			// I- linear (baseline) absorption (0.0004 - 0.001)
	float element_,				// I- Albino leaf (visible) absorption (0 - 10)
	float c_factor_,			// I- Leaf chlorophyll content (mg.m-2) (0 - 600)
	float w_factor_,			// I- Leaf water content (g.m-2) (0 - 500)
	float l_factor_,			// I- Lignin / cellulose content (g.m-2) (10 - 80)
	float p_factor_,			// I- Nitrogen content (g.m-2) (0 - 2)

	float k_chloro[500], 		// I- 
	float k_water[500],			// I- 
	float k_ligcell[500],		// I- 
	float k_protein[500],		// I- 
	float ke[500],				// I- 
	
	List       *refl_list_,		// O- Reflectance function
	List       *tran_list_		// O- Transmittance function
);



#endif 	/*  __LIBERTY_H */
