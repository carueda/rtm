/* 
/////////////////////////////////////////////////////////////////////////
//
// liberty inversion
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

   
#include "opt.h"

#include "liberty.h"

#include "rpoint.h"

#include "symboltable.h"



#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <math.h>


static long counter;

#define num_vars 9
        // these 9 variables are:  D xu thick baseline element c_factor w_factor l_factor p_factor



static 	float k_chloro[500];
static 	float k_water[500];
static 	float k_ligcell[500];
static 	float k_protein[500];
static 	float ke[500];



static	char* observed_rfl_filename;
static	char* observed_tns_filename;
	
static	char modeled_rfl_filename[1024];
static	char modeled_tns_filename[1024];



// range of interest:
static	float wl_from = 0.0;
static	float wl_to = 99999.0;
	


static List observed_rfl_list;
static List observed_tns_list;

static List modeled_rfl_list;
static List modeled_tns_list;
	

/**
 * If idef_filename == NULL, then a direct RMS error is computed;
 * otherwise, observed_rfl_st and observed_tns_st symbol tables are first constructed 
 * to compute an index RMS with the modeled functions.
 */
static char* idef_filename = NULL;

/**
 * meaningful to fun if idef_filename != NULL
 */
static FILE* idef_file = NULL;
static long observed_rfl_st;
static long observed_tns_st;



/////////////////////////////////////////////////////////////////////////
static double fun(OPT_RValue* input)
{
	int status;
	float D;
	float xu;
	float thick;
	float baseline;
	float element;
	float c_factor;
	float w_factor;
	float l_factor;
	float p_factor;

	double err;
	
	
	
	D = input[0].d;
	xu = input[1].d;
	thick = input[2].d;
	baseline = input[3].d;
	element = input[4].d;
	c_factor = input[5].d;
	w_factor = input[6].d;
	l_factor = input[7].d;
	p_factor = input[8].d;
	
	status = liberty_prm(
		D,
		xu,
		thick,
		baseline,
		element,
		c_factor,
		w_factor,
		l_factor,
		p_factor,
		k_chloro,
		k_water,
		k_ligcell,
		k_protein,
		ke,
		&modeled_rfl_list,
		&modeled_tns_list
	);

	if ( counter++ % 100 == 0 )
	{
		fputc('.', stdout);
		fflush(stdout);
	}

	if ( status != 0 )
	{
		fprintf(stderr, "status = %d\n", status);
		exit(1);
	}
		

	if ( idef_filename == NULL )
	{
		// get direct RMS
		err = getDirectRMS(observed_rfl_list, modeled_rfl_list)
		    + getDirectRMS(observed_tns_list, modeled_tns_list)
		;
	}
	else
	{
		// get index RMS
		err = getIndexRMS(observed_rfl_st, idef_filename, idef_file, modeled_rfl_list)
		    + getIndexRMS(observed_tns_st, idef_filename, idef_file, modeled_tns_list)
		;
	}
	list_destroy(modeled_rfl_list);
	list_destroy(modeled_tns_list);

	return err;
}


/////////////////////////////////////////////////////////////////////////
static OPT_RVariableDefinition varDefs[num_vars];

static OPT_RObject opt_robj = 
{
	num_vars,
	varDefs,
	fun,
};
 


/////////////////////////////////////////////////////////////////////////
static 
int write_report(OPT_RValue best_input[], double best_output, char* report_filename)
{
	FILE* report_file;
	
	
	// open for appending:
	report_file = fopen(report_filename, "a");
	if ( report_file == NULL )
	{
		fprintf(stdout, "error opening %s\n", report_filename);
		return 1;
	}

	// WRITE ONE LINE OF INFORMATION
	
	// observed function filenames:
	fprintf(report_file, "%s %s ", 
		observed_rfl_filename,
		observed_tns_filename
	);
	
	// values:
	fprintf(report_file, "%g %g %g %g %g %g %g %g %g ",
		best_input[0].d,
		best_input[1].d,
		best_input[2].d,
		best_input[3].d,
		best_input[4].d,
		best_input[5].d,
		best_input[6].d,
		best_input[7].d,
		best_input[8].d
	);
	
	// variable label:
	fprintf(report_file, "%s;%s;%s;%s;%s;%s;%s;%s;%s ", 
		varDefs[0].lower.d < varDefs[0].upper.d ? "D*"   : "D", 
		varDefs[1].lower.d < varDefs[1].upper.d ? "xu*" : "xu", 
		varDefs[2].lower.d < varDefs[2].upper.d ? "thick*"  : "thick", 
		varDefs[3].lower.d < varDefs[3].upper.d ? "baseline*"  : "baseline",
		varDefs[4].lower.d < varDefs[4].upper.d ? "element*"  : "element",
		varDefs[5].lower.d < varDefs[5].upper.d ? "c_factor*"  : "c_factor",
		varDefs[6].lower.d < varDefs[6].upper.d ? "w_factor*"  : "w_factor",
		varDefs[7].lower.d < varDefs[7].upper.d ? "l_factor*"  : "l_factor",
		varDefs[8].lower.d < varDefs[8].upper.d ? "p_factor*"  : "p_factor"
	);
	

	// error:
	fprintf(report_file, "RMSE= %g ", 
		best_output
	);
	
	// error computed on...:
	if ( idef_filename != NULL )
	{
		int i;
		
		fprintf(report_file, "on:");
		
		for ( i = 0; i < st_size(observed_rfl_st); i++ )
		{
			ST_Entry entry;
			char* id;

			entry = st_entryAt(observed_rfl_st, i);
			id = entry->id;
		
			if ( id[0] == '@' )
			{
				continue;
			}
			
			if ( i > 0 )
				fprintf(report_file, ";");
				
			fprintf(report_file, "%s", id);
		}
		fprintf(report_file, " ");
	}
	else
	{
		fprintf(report_file, "on:range ");
	}


	// range, and number of evaluations:
	fprintf(report_file, "range=%g:%g evals= %ld ", 
		wl_from,
		wl_to,
		counter
	);


	// modeled function filenames:
	fprintf(report_file, "%s %s", 
		modeled_rfl_filename,
		modeled_tns_filename
	);
	
	
	fprintf(report_file, "\n");

	
	fclose(report_file);
	
	return 0;
}



/////////////////////////////////////////////////////////////////////////
static 
int write_modeled_functions(OPT_RValue best_input[], char* modeled_rfl_filename, char* modeled_tns_filename)
{
	float D;
	float xu;
	float thick;
	float baseline;
	float element;
	float c_factor;
	float w_factor;
	float l_factor;
	float p_factor;

	int status;
		
		
	D = best_input[0].d;
	xu = best_input[1].d;
	thick = best_input[2].d;
	baseline = best_input[3].d;
	element = best_input[4].d;
	c_factor = best_input[5].d;
	w_factor = best_input[6].d;
	l_factor = best_input[7].d;
	p_factor = best_input[8].d;
	

	status = liberty_prm(
		D,
		xu,
		thick,
		baseline,
		element,
		c_factor,
		w_factor,
		l_factor,
		p_factor,
		k_chloro,
		k_water,
		k_ligcell,
		k_protein,
		ke,
		&modeled_rfl_list,
		&modeled_tns_list
	);

	if ( status != 0 )
	{
		fprintf(stderr, "liberty_prm status = %d\n", status);
		return status;
	}

	fprintf(stdout, "creating %s\n", modeled_rfl_filename);
	status = writeFunction(modeled_rfl_list, modeled_rfl_filename);
	if ( 0 != status )
	{
		fprintf(stderr, "Error creating function %s : %s\n", modeled_rfl_filename, strerror(errno));
		return status;
	}
	
	fprintf(stdout, "creating %s\n", modeled_tns_filename);
	status = writeFunction(modeled_tns_list, modeled_tns_filename);
	if ( 0 != status )
	{
		fprintf(stderr, "Error creating function %s : %s\n", modeled_tns_filename, strerror(errno));
	}
	return status;
}





///////////////////////////////////////////////////////////////////////////////////////////////////
static 
int usage(char* msg)
{
	if ( msg )
	{
		fprintf(stderr, "\n%s\n", msg);
		fprintf(stderr, "Try `inv_liberty -h' for more information.\n\n");
		return 1;
	}

	fprintf(stdout,
"\n"
"CSTARS inv_liberty 0.1 (%s %s) %s\n"
"\n"
"USAGE\n"
"  inv_liberty [-h ] [-m method] [-i IDEF-file] [-r modeled-rfl] [-t modeled-tns] [-R range] \\\n"
"              D xu thick baseline element c_factor w_factor l_factor p_factor  \\\n"
"              protein-file pigment-file water-file albino-file ligcell-file \\\n"
"              observed-rfl observed-tns report-file\n"
"\n"
"	-h                 Shows this message and exits\n"
"	-m method          Search method to be used: exhaustive | binary\n"
"	                   By default, a binary search is performed.\n"
"	-i IDEF-file       Compute error based on indices defined in this file.\n"
"	                   By default, direct RMS is used.\n"
"	-r modeled-rfl     Name for the best reflectance function modeled.\n"
"	                   By default, it takes the name observed-rfl.mod.\n"
"	-t modeled-tns     Name for the best transmittance function modeled\n"
"	                   By default, it takes the name observed-tns.mod.\n"
"	-R range           Range of interest, with the form:  from:to\n"
"	D                  (*) Average leaf cell diameter (m-6) (20-100)\n"
"	xu                 (*) Intercellular air space determinant (0.01 - 0.1)\n"
"	thick              (*) Leaf thickness (1 - 10)\n"
"	baseline           (*) linear (baseline) absorption (0.0004 - 0.001)\n"
"	element            (*) Albino leaf (visible) absorption (0 - 10)\n"
"	c_factor           (*) Leaf chlorophyll content (mg.m-2) (0 - 600)\n"
"	w_factor           (*) Leaf water content (g.m-2) (0 - 500)\n"
"	l_factor           (*) Lignin / cellulose content (g.m-2) (10 - 80)\n"
"	p_factor           (*) Nitrogen content (g.m-2) (0 - 2)\n"
"	protein-file       Protein filename\n"
"	pigment-file       Pigment filename\n"
"	water-file         Water filename\n"
"	albino-file        Albino filename\n"
"	ligcell-file       Ligcell filename\n"
"	observed-rfl       File with observed reflectance data\n"
"	observed-tns       File with observed transmittance data\n"
"	report-file        Report filename. A line of info is concatenated to this file.\n"
"\n"
"   (*) This argument can take the following form (with no spaces):  lower,upper,step\n"
"       i.e., three values separated by commas specifying the range of interest for the variable.\n"
"\n"
"EXAMPLE:\n"
"  inv_liberty -R 400:800 20,100,5 .01,.1,.01 1,10,.01 .0006 0,4,.5 0.800,5 100 40 1 \\\n"
"              protein.dat pigment.dat water.dat albino.dat ligcell.dat \\\n"
"              1.rfl 1.tns report1.txt\n"
"\n"
"  In this case direct RMS will be used as a measure of error; files 1.rfl.mod, 1.tns.mod, \n"
"  and report1.txt will be generated.\n"
"\n",
		       __DATE__, __TIME__,
			   RTM_VERSION_IN_USAGE
	);
	
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	int arg;
	int status;

	OPT_RValue best_input[num_vars];
	double best_output;
	
	int method = OPT_BINARY;
	
	char* protein_filename;
	char* pigment_filename;
	char* water_filename;
	char* albino_filename;
	char* ligcell_filename;

	char* report_filename;
	
	modeled_rfl_filename[0] = 0;
	modeled_tns_filename[0] = 0;

	wl_from = 0.0;
	wl_to = 99999.0;
	

	// process options:
	for ( arg = 1; arg < argc && argv[arg][0] == '-'; arg++ )
	{
		if ( strcmp(argv[arg], "-h") == 0 )
		{
			return usage(NULL);
		}

		else if ( strcmp(argv[arg], "-m") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting argument to -m option");
				
			++arg;
			if ( strcmp(argv[arg], "exhaustive") == 0 )
				method = OPT_EXHAUSTIVE;
			else if ( strcmp(argv[arg], "binary") == 0 )
				method = OPT_BINARY;
			else
				return usage("Invalid argument to -m option");
		}
		else if ( strcmp(argv[arg], "-i") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting argument to -i option");
				
			++arg;
			idef_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-r") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting argument to -r option");
				
			++arg;
			strcpy(modeled_rfl_filename, argv[arg]);
		}
		else if ( strcmp(argv[arg], "-t") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting argument to -t option");
				
			++arg;
			strcpy(modeled_tns_filename, argv[arg]);
		}
		else if ( strcmp(argv[arg], "-R") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting argument to -R option");
				
			++arg;
			if ( 2 != sscanf(argv[arg], "%f:%f", &wl_from, &wl_to) )
				return usage("Invalid argument to -R option");
		}
		else
		{
			return usage("Invalid option");
		}
	}
	
	
	if ( argc - arg < 17 )
	{
		return usage("Expecting arguments");
	}
	
	opt_scanVariableDefinition(argv[arg++], &varDefs[0]);
	opt_scanVariableDefinition(argv[arg++], &varDefs[1]);
	opt_scanVariableDefinition(argv[arg++], &varDefs[2]);
	opt_scanVariableDefinition(argv[arg++], &varDefs[3]);
	opt_scanVariableDefinition(argv[arg++], &varDefs[4]);
	opt_scanVariableDefinition(argv[arg++], &varDefs[5]);
	opt_scanVariableDefinition(argv[arg++], &varDefs[6]);
	opt_scanVariableDefinition(argv[arg++], &varDefs[7]);
	opt_scanVariableDefinition(argv[arg++], &varDefs[8]);
	

	protein_filename = argv[arg++];
	pigment_filename = argv[arg++];
	water_filename = argv[arg++];
	albino_filename = argv[arg++];
	ligcell_filename = argv[arg++];


	observed_rfl_filename = argv[arg++];
	observed_tns_filename = argv[arg++];
	report_filename = argv[arg++];
	

	// read functions to liberty:
	liberty_read_functions(
		protein_filename,
		pigment_filename,
		water_filename,
		albino_filename,
		ligcell_filename,
		k_chloro,
		k_water,
		k_ligcell,
		k_protein,
		ke
	);



	if ( modeled_rfl_filename[0] == 0 )
	{
		// No -r option
		sprintf(modeled_rfl_filename, "%s.mod", observed_rfl_filename);
	}
	if ( modeled_tns_filename[0] == 0 )
	{
		// No -t option
		sprintf(modeled_tns_filename, "%s.mod", observed_tns_filename);
	}
	

	observed_rfl_list = read_function(observed_rfl_filename);
	if ( observed_rfl_list == NULL )
	{
		fprintf(stderr, "error reading %s\n", observed_rfl_filename);
		return 1;
	}
	
	observed_tns_list = read_function(observed_tns_filename);
	if ( observed_tns_list == NULL )
	{
		fprintf(stderr, "error reading %s\n", observed_tns_filename);
		list_destroy(observed_rfl_list);
		return 1;
	}

	// process range to update observed functions:
	if ( wl_from != 0.0 || wl_to != 99999.0 )
	{
		// range specified.
		
		List tmp_list;
		
		// update observed_rfl_list:
		tmp_list = function_extract(observed_rfl_list, wl_from, wl_to);
		list_destroy(observed_rfl_list);
		observed_rfl_list = tmp_list;

		// update observed_tns_list:
		tmp_list = function_extract(observed_tns_list, wl_from, wl_to);
		list_destroy(observed_tns_list);
		observed_tns_list = tmp_list;
	}
		
	
	if ( idef_filename != NULL )
	{
		idef_file = fopen(idef_filename, "r");
		if ( idef_file == NULL )
		{
			fprintf(stderr, "error opening index definition file %s\n", idef_filename);
			list_destroy(observed_tns_list);
			list_destroy(observed_rfl_list);
			return 1;
		}
		
		observed_rfl_st = getIndicesFromFunction(idef_filename, idef_file, observed_rfl_list);
		if ( observed_rfl_st < 0L )
		{
			fprintf(stderr, "error getting indices from %s\n", observed_rfl_filename);
			list_destroy(observed_tns_list);
			list_destroy(observed_rfl_list);
			return 1;
		}

		observed_tns_st = getIndicesFromFunction(idef_filename, idef_file, observed_tns_list);
		if ( observed_tns_st < 0L )
		{
			fprintf(stderr, "error getting indices from %s\n", observed_tns_filename);
			st_destroy(observed_rfl_st);
			list_destroy(observed_tns_list);
			list_destroy(observed_rfl_list);
			return 1;
		}
	}
	

	wl_from = ((RPoint*) list_elementAt(observed_rfl_list, 0))->x;
	wl_to = ((RPoint*) list_elementAt(observed_rfl_list, list_size(observed_rfl_list) - 1))->x;
	
	fprintf(stdout, "\nCSTARS inv_liberty - Liberty Inversion\n");
	fprintf(stdout, 
		"Observed range: %g -> %g\n",
			wl_from, wl_to
	);
	fprintf(stdout, "Inverting: ");

	counter = 0;
	status = opt_minimize(&opt_robj, method, best_input, &best_output);
	
	if ( status == 0 )
	{
		write_report(best_input, best_output, report_filename);
		write_modeled_functions(best_input, modeled_rfl_filename, modeled_tns_filename);
	}
	else
	{
		fprintf(stderr, "minimization status = %d\n", status);
	}

	if ( idef_filename != NULL )
	{
		fclose(idef_file);
	}

	
	return status;

}
