prospect model
Carlos Rueda                         (see also ../README)
$Id$

04/25/05
	- new option -append-float
	
10/31/02
	- New paramater 'cs' en PROSPECT model:
		int prospect_prm(
			doublereal vai,			// I- Leaf internal structure parameter
			doublereal cab,			// I- Leaf chlorophyll a+b content (micro g.cm-2)
			doublereal cw,			// I- Leaf equivalent water thickness (cm): Cw
			doublereal cm,			// I- Leaf dry matter content (g.cm-2): Cm
			doublereal cs,			// I- (name?)
			List       *refl_list,	// O- Reflectance function
			List       *tran_list	// O- Transmittance function
		);

	- New function to initialize de model:	
		void  prospect_init_coeff_arrays(
			char* refra_filename,
			char* kab_filename,
			char* kw_filename,
			char* km_filename,
			char* ks_filename,
			char* ke_filename
		);

		
4/9/02
	Added option -s. 
	
7/03/01
	Reorganization, but not change in functionality.
	
6/06/01
	New prospect program, new_prospect.c:
	
	> make-prospect
	> prospect 1 30 .01 .01  1300101.rfl 1300101.tns
	> plotfunction 1300101.rfl

	OK.

6/05/01
	prospect_prm ready: Prototype:
	
	int prospect_prm(
		doublereal vai,			// I- Leaf internal structure parameter
		doublereal cab,			// I- Leaf chlorophyll a+b content (micro g.cm-2)
		doublereal cw,			// I- Leaf equivalent water thickness (cm): Cw
		doublereal cm,			// I- Leaf dry matter content (g.cm-2): Cm
		List       *refl_list,	// O- Reflectance function
		List       *tran_list	// O- Transmittance function
	);
	
