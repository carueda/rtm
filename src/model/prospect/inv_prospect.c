/* 
/////////////////////////////////////////////////////////////////////////
//
// prospect inversion
//
// Carlos Rueda - CSTARS
// June 2001
// April 2002 - Option -s added.
//
// 10/31/02  - new parameter 'cs' and initialization of PROSPECT model.
//             See prospect.h
///////////////////////////////////////////////////////////////////////// 
*/

   
#include "opt.h"

#include "symboltable.h"

#include "prospect.h"


#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <math.h>



// For prospect model:
static char* refra_filename = "refra.txt";
static char* kab_filename   = "kab.txt";
static char* kw_filename    = "kw.txt";
static char* km_filename    = "km.txt";
static char* ks_filename    = "ks.txt";
static char* ke_filename    = "ke.txt";




static long counter;

#define num_vars 5
        // these 5 variables are:  N cab cw cm cs


static	char* observed_rfl_filename;
static	char* observed_tns_filename;
	
static	char modeled_rfl_filename[1024];
static	char modeled_tns_filename[1024];



// range of interest:
static	float wl_from = 0.0;
static	float wl_to = 99999.0;
	


static List observed_rfl_list;
static List observed_tns_list;

static List modeled_rfl_list = NULL;  // initialization makes prospect_prm to allocate memory
static List modeled_tns_list = NULL;  // initialization makes prospect_prm to allocate memory
	

/**
 * If idef_filename == NULL, then a direct RMS error is computed;
 * otherwise, observed_rfl_st and observed_tns_st symbol tables are first constructed 
 * to compute an index RMS with the modeled functions.
 */
static char* idef_filename = NULL;

/**
 * meaningful to fun if idef_filename != NULL
 */
static FILE* idef_file = NULL;
static long observed_rfl_st;
static long observed_tns_st;


// 04-08-02
// This info is to weight the output from the model on given bands:
static char* desired_bands_filename = NULL;
static int desired_size;
static BandInfo* desired_bands;
static List desired_rfl_list;  // auxiliary list to fun040802
static List desired_tns_list;  // auxiliary list to fun040802


/////////////////////////////////////////////////////////////////////////
static double fun(OPT_RValue* input)
{
	int status;
	doublereal vai;
	doublereal cab;
	doublereal cw;
	doublereal cm;
	doublereal cs;

	double err;
	
	
	
	vai = input[0].d;
	cab = input[1].d;
	cw = input[2].d;
	cm = input[3].d;
	cs = input[4].d;
	
	if ( modeled_rfl_list != NULL )
	{
		list_setSize(modeled_rfl_list, 0);
		list_setSize(modeled_tns_list, 0);
	}
	status = prospect_prm(
		vai,
		cab,
		cw,
		cm,
		cs,
		&modeled_rfl_list,
		&modeled_tns_list
	);

	if ( status != 0 )
	{
		fprintf(stderr, "status = %d\n", status);
		exit(1);
	}
		
	if ( counter++ % 100 == 0 )
	{
		fputc('.', stdout);
		fflush(stdout);
	}

	// 04-08-02
	if ( desired_bands_filename != NULL )
	{
		List tmp;
		
		list_setSize(desired_rfl_list, 0);
		fun040802(modeled_rfl_list, desired_size, desired_bands, desired_rfl_list);
		tmp = modeled_rfl_list;
		modeled_rfl_list = desired_rfl_list;
		desired_rfl_list = tmp;
		
		list_setSize(desired_tns_list, 0);
		fun040802(modeled_tns_list, desired_size, desired_bands, desired_tns_list);
		tmp = modeled_tns_list;
		modeled_tns_list = desired_tns_list;
		desired_tns_list = tmp;
	}
	

	if ( idef_filename == NULL )
	{
		// get direct RMS
		err = getDirectRMS(observed_rfl_list, modeled_rfl_list)
		    + getDirectRMS(observed_tns_list, modeled_tns_list)
		;
	}
	else
	{
		// get index RMS
		err = getIndexRMS(observed_rfl_st, idef_filename, idef_file, modeled_rfl_list)
		    + getIndexRMS(observed_tns_st, idef_filename, idef_file, modeled_tns_list)
		;
	}
	//list_destroy(modeled_rfl_list);
	//list_destroy(modeled_tns_list);

	return err;
}


/////////////////////////////////////////////////////////////////////////
static OPT_RVariableDefinition varDefs[num_vars];

static OPT_RObject opt_robj = 
{
	num_vars,
	varDefs,
	fun,
};
 


/////////////////////////////////////////////////////////////////////////
static 
int write_report(OPT_RValue best_input[], double best_output, char* report_filename)
{
	FILE* report_file;
	
	
	// open for appending:
	report_file = fopen(report_filename, "a");
	if ( report_file == NULL )
	{
		fprintf(stdout, "error opening %s\n", report_filename);
		return 1;
	}

	// WRITE ONE LINE OF INFORMATION
	
	// observed function filenames:
	fprintf(report_file, "%s %s ", 
		observed_rfl_filename,
		observed_tns_filename
	);
	
	// values:
	fprintf(report_file, "%g %g %g %g %g", 
		best_input[0].d,
		best_input[1].d,
		best_input[2].d,
		best_input[3].d,
		best_input[4].d
	);
	
	// variable label:
	fprintf(report_file, "%s;%s;%s;%s;%s ", 
		varDefs[0].lower.d < varDefs[0].upper.d ? "N*"   : "N", 
		varDefs[1].lower.d < varDefs[1].upper.d ? "cab*" : "cab", 
		varDefs[2].lower.d < varDefs[2].upper.d ? "cw*"  : "cw", 
		varDefs[3].lower.d < varDefs[3].upper.d ? "cm*"  : "cm",
		varDefs[4].lower.d < varDefs[4].upper.d ? "cs*"  : "cs"
	);
	

	// error:
	fprintf(report_file, "RMSE= %g ", 
		best_output
	);
	
	// error computed on...:
	if ( idef_filename != NULL )
	{
		int i;
		
		fprintf(report_file, "on:");
		
		for ( i = 0; i < st_size(observed_rfl_st); i++ )
		{
			ST_Entry entry;
			char* id;

			entry = st_entryAt(observed_rfl_st, i);
			id = entry->id;
		
			if ( id[0] == '@' )
			{
				continue;
			}
			
			if ( i > 0 )
				fprintf(report_file, ";");
				
			fprintf(report_file, "%s", id);
		}
		fprintf(report_file, " ");
	}
	else
	{
		fprintf(report_file, "on:range ");
	}


	// range, and number of evaluations:
	fprintf(report_file, "range=%g:%g evals= %ld ", 
		wl_from,
		wl_to,
		counter
	);


	// modeled function filenames:
	fprintf(report_file, "%s %s", 
		modeled_rfl_filename,
		modeled_tns_filename
	);
	
	
	fprintf(report_file, "\n");

	
	fclose(report_file);
	
	return 0;
}



/////////////////////////////////////////////////////////////////////////
static 
int write_modeled_functions(OPT_RValue best_input[], char* modeled_rfl_filename, char* modeled_tns_filename)
{
	doublereal vai;
	doublereal cab;
	doublereal cw;
	doublereal cm;
	doublereal cs;

	int status;
		
		
	vai = best_input[0].d;
	cab = best_input[1].d;
	cw = best_input[2].d;
	cm = best_input[3].d;
	cs = best_input[4].d;

	if ( modeled_rfl_list != NULL )
	{
		list_setSize(modeled_rfl_list, 0);
		list_setSize(modeled_tns_list, 0);
	}
	status = prospect_prm(
		vai,
		cab,
		cw,
		cm,
		cs,
		&modeled_rfl_list,
		&modeled_tns_list
	);

	if ( status != 0 )
	{
		fprintf(stderr, "prospect_prm status = %d\n", status);
		return status;
	}

	// 04-08-02
	if ( desired_bands_filename != NULL )
	{
		List tmp;
		
		list_setSize(desired_rfl_list, 0);
		fun040802(modeled_rfl_list, desired_size, desired_bands, desired_rfl_list);
		tmp = modeled_rfl_list;
		modeled_rfl_list = desired_rfl_list;
		desired_rfl_list = tmp;
		
		list_setSize(desired_tns_list, 0);
		fun040802(modeled_tns_list, desired_size, desired_bands, desired_tns_list);
		tmp = modeled_tns_list;
		modeled_tns_list = desired_tns_list;
		desired_tns_list = tmp;
	}
	
	fprintf(stdout, "creating %s\n", modeled_rfl_filename);
	status = writeFunction(modeled_rfl_list, modeled_rfl_filename);
	if ( 0 != status )
	{
		fprintf(stderr, "Error creating function %s : %s\n", modeled_rfl_filename, strerror(errno));
		return status;
	}
	
	fprintf(stdout, "creating %s\n", modeled_tns_filename);
	status = writeFunction(modeled_tns_list, modeled_tns_filename);
	if ( 0 != status )
	{
		fprintf(stderr, "Error creating function %s : %s\n", modeled_tns_filename, strerror(errno));
	}
	return status;
}





///////////////////////////////////////////////////////////////////////////////////////////////////
static 
int usage(char* msg)
{
	if ( msg )
	{
		fprintf(stderr, "\n%s\n", msg);
		fprintf(stderr, "Try `inv_prospect -h' for more information.\n\n");
		return 1;
	}

	fprintf(stdout,
"\n"
"CSTARS inv_prospect 0.3 (%s %s) %s\n"
"\n"
"USAGE\n"
"  inv_prospect -h\n"
"	    Shows this message and exits\n"
"\n"
"  inv_prospect [options] N cab cw cm cs  observed-rfl observed-tns report-file\n"
"\n"
"options\n"
"	See ``prospect -h'' for options: -refra -kab -kw -km -ks -ke -s\n"
"	-m method        Search method to be used: exhaustive | binary\n"
"	                 By default, binary.\n"
"	-i IDEF-file     Compute error based on indices defined in this file.\n"
"	                 By default, direct RMS is used.\n"
"	-r modeled-rfl   Name for the best reflectance function modeled.\n"
"	                 By default, it takes the name observed-rfl.mod.\n"
"	-t modeled-tns   Name for the best transmittance function modeled\n"
"	                 By default, it takes the name observed-tns.mod.\n"
"	-R range         Range of interest, with the form:  from:to\n"
"\n"
"Required parameters:\n"
"	N                (*) Leaf internal structure parameter\n"
"	cab              (*) Leaf chlorophyll a+b content (micro g.cm-2)\n"
"	cw               (*) Leaf equivalent water thickness (cm)\n"
"	cm               (*) Leaf dry matter content (g.cm-2)\n"
"	cs               (*) (new parameter for PROSPECT)\n"
"	observed-rfl     File with observed reflectance data\n"
"	observed-tns     File with observed transmittance data\n"
"	report-file      Report filename. A line of info is concatenated to this file.\n"
"\n"
"   (*) This argument can take the following form (with no spaces):  lower,upper,step\n"
"       i.e., three values separated by commas specifying the range of interest for the variable.\n"
"\n"
"EXAMPLE:\n"
"  inv_prospect -R 400:800 .5,2,.05 10,90,.5 0,.05,.001 0,.01,.001 0,2,1 1.rfl 1.tns report1.txt\n"
"\n"
"  In this case all variables will be searched to minimize error in the range 400 to 800;\n"
"  direct RMS will be used as a measure of error; files 1.rfl.mod, 1.tns.mod, and report1.txt\n"
"  will be generated.\n"
"\n",
		       __DATE__, __TIME__,
			   RTM_VERSION_IN_USAGE
	);
	
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	int arg;
	int status;

	OPT_RValue best_input[num_vars];
	double best_output;
	
	int method = OPT_BINARY;

	char* report_filename;
	
	modeled_rfl_filename[0] = 0;
	modeled_tns_filename[0] = 0;

	wl_from = 0.0;
	wl_to = 99999.0;
	

	// process options:
	for ( arg = 1; arg < argc && argv[arg][0] == '-'; arg++ )
	{
		if ( strcmp(argv[arg], "-h") == 0 )
		{
			return usage(NULL);
		}

		else if ( strcmp(argv[arg], "-m") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting argument to -m option");
				
			++arg;
			if ( strcmp(argv[arg], "exhaustive") == 0 )
				method = OPT_EXHAUSTIVE;
			else if ( strcmp(argv[arg], "binary") == 0 )
				method = OPT_BINARY;
			else
				return usage("Invalid argument to -m option");
		}
		else if ( strcmp(argv[arg], "-i") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting argument to -i option");
				
			++arg;
			idef_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-r") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting argument to -r option");
				
			++arg;
			strcpy(modeled_rfl_filename, argv[arg]);
		}
		else if ( strcmp(argv[arg], "-t") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting argument to -t option");
				
			++arg;
			strcpy(modeled_tns_filename, argv[arg]);
		}
		else if ( strcmp(argv[arg], "-R") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting argument to -R option");
				
			++arg;
			if ( 2 != sscanf(argv[arg], "%f:%f", &wl_from, &wl_to) )
				return usage("Invalid argument to -R option");
		}
		else if ( strcmp(argv[arg], "-refra") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -refra expects a filename");
			}
			refra_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-kab") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -ab expects a filename");
			}
			kab_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-kw") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -kw expects a filename");
			}
			kw_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-km") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -km expects a filename");
			}
			km_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-ks") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -ks expects a filename");
			}
			ks_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-ke") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -ke expects a filename");
			}
			ke_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-s") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting argument to -s option");
				
			++arg;
			desired_bands_filename = argv[arg];
		}
		else
		{
			return usage("Invalid option");
		}
	}
	
	
	if ( argc - arg < 8 )
	{
		return usage("Expecting arguments");
	}
	
	opt_scanVariableDefinition(argv[arg++], &varDefs[0]);
	opt_scanVariableDefinition(argv[arg++], &varDefs[1]);
	opt_scanVariableDefinition(argv[arg++], &varDefs[2]);
	opt_scanVariableDefinition(argv[arg++], &varDefs[3]);
	opt_scanVariableDefinition(argv[arg++], &varDefs[4]);
	
	observed_rfl_filename = argv[arg++];
	observed_tns_filename = argv[arg++];
	report_filename = argv[arg++];
	

	
	
	// PROSPECT model initialization:
	prospect_init_coeff_arrays(
		refra_filename,
		kab_filename,
		kw_filename,
		km_filename,
		ks_filename,
		ke_filename
	);
	

	if ( modeled_rfl_filename[0] == 0 )
	{
		// No -r option
		sprintf(modeled_rfl_filename, "%s.mod", observed_rfl_filename);
	}
	if ( modeled_tns_filename[0] == 0 )
	{
		// No -t option
		sprintf(modeled_tns_filename, "%s.mod", observed_tns_filename);
	}
	
	// 04-08-02
	if ( desired_bands_filename != NULL )
	{
		desired_bands = readBandsInfo(desired_bands_filename, &desired_size);
		if ( desired_bands == NULL )
		{
			fprintf(stderr, "Error reading %s\n", desired_bands_filename);
			return 10;
		}
		
		// prepare work lists:
		desired_rfl_list = list_create(sizeof(RPoint), 2000, 1000);
		desired_tns_list = list_create(sizeof(RPoint), 2000, 1000);
	}

	observed_rfl_list = read_function(observed_rfl_filename);
	if ( observed_rfl_list == NULL )
	{
		fprintf(stderr, "error reading %s\n", observed_rfl_filename);
		return 1;
	}
	
	observed_tns_list = read_function(observed_tns_filename);
	if ( observed_tns_list == NULL )
	{
		fprintf(stderr, "error reading %s\n", observed_tns_filename);
		list_destroy(observed_rfl_list);
		return 1;
	}

	// process range to update observed functions:
	if ( wl_from != 0.0 || wl_to != 99999.0 )
	{
		// range specified.
		
		List tmp_list;
		
		// update observed_rfl_list:
		tmp_list = function_extract(observed_rfl_list, wl_from, wl_to);
		list_destroy(observed_rfl_list);
		observed_rfl_list = tmp_list;

		// update observed_tns_list:
		tmp_list = function_extract(observed_tns_list, wl_from, wl_to);
		list_destroy(observed_tns_list);
		observed_tns_list = tmp_list;
	}
		
	
	if ( idef_filename != NULL )
	{
		idef_file = fopen(idef_filename, "r");
		if ( idef_file == NULL )
		{
			fprintf(stderr, "error opening index definition file %s\n", idef_filename);
			list_destroy(observed_tns_list);
			list_destroy(observed_rfl_list);
			return 1;
		}
		
		observed_rfl_st = getIndicesFromFunction(idef_filename, idef_file, observed_rfl_list);
		if ( observed_rfl_st < 0L )
		{
			fprintf(stderr, "error getting indices from %s\n", observed_rfl_filename);
			list_destroy(observed_tns_list);
			list_destroy(observed_rfl_list);
			return 1;
		}

		observed_tns_st = getIndicesFromFunction(idef_filename, idef_file, observed_tns_list);
		if ( observed_tns_st < 0L )
		{
			fprintf(stderr, "error getting indices from %s\n", observed_tns_filename);
			st_destroy(observed_rfl_st);
			list_destroy(observed_tns_list);
			list_destroy(observed_rfl_list);
			return 1;
		}
	}
	

	wl_from = ((RPoint*) list_elementAt(observed_rfl_list, 0))->x;
	wl_to = ((RPoint*) list_elementAt(observed_rfl_list, list_size(observed_rfl_list) - 1))->x;
	
	fprintf(stdout, "\nCSTARS inv_prospect - Prospect Inversion\n");
	fprintf(stdout, 
		"Observed range: %g -> %g\n",
			wl_from, wl_to
	);
	fprintf(stdout, "Inverting: ");

	counter = 0;
	status = opt_minimize(&opt_robj, method, best_input, &best_output);
	
	if ( status == 0 )
	{
		write_report(best_input, best_output, report_filename);
		write_modeled_functions(best_input, modeled_rfl_filename, modeled_tns_filename);
	}
	else
	{
		fprintf(stderr, "minimization status = %d\n", status);
	}

	if ( idef_filename != NULL )
	{
		fclose(idef_file);
	}

	// 04-08-02
	if ( desired_bands_filename != NULL )
	{
		freeBandsInfo(desired_bands, desired_size);
		list_destroy(desired_rfl_list);
		list_destroy(desired_tns_list);
	}
	
	return status;

}
