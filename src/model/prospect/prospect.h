/* 
/////////////////////////////////////////////////////////////////////////
//
// prospect
//
// Carlos Rueda - CSTARS
// June 2001
// April 2002 - New prospect_prm_wl
// 10/30/02  - new parameter 'cs' and function prospect_init_coeff_arrays()
//
///////////////////////////////////////////////////////////////////////// 
*/

#ifndef __PROSPECT_H
#define __PROSPECT_H

#include "cstars-rtm.h"

#ifndef __LIST_H
#include "list.h"
#endif

#include "f2c.h"

#include "func.h"
//#include "sailh_soil.h"

#include <stdio.h>


#ifndef DATA_DIR
#define DATA_DIR "."
#endif
#define _datadir(file) DATA_DIR "/" file


////////////////////////////////////////////////////////////////////////
/**
 * 10/31/02.
 * This function must be called before prospect_prm in order to set the
 * coeff arrays used internally by the model.
 * Each file is assumed to contain 421 lines, each with the format:
 *    wavelength <separator> val
 * where <separator> is blank, comma, or tab.
 * All wavelengths will be confronted to expected working wavelengths.
 */
void  prospect_init_coeff_arrays(
	char* refra_filename,
	char* kab_filename,
	char* kw_filename,
	char* km_filename,
	char* ks_filename,
	char* ke_filename
);




///////////////////////////////////////////////////////////////////////////////////
/**
 * The PROSPECT model.
 *
 * Note about refl_list and tran_list:
 *   If null, new lists are created internally; if not, just adds point to them; 
 *   be sure the element size is sizeof(RPoint).
 */
int prospect_prm(
	doublereal vai,			// I- Leaf internal structure parameter
	doublereal cab,			// I- Leaf chlorophyll a+b content (micro g.cm-2)
	doublereal cw,			// I- Leaf equivalent water thickness (cm): Cw
	doublereal cm,			// I- Leaf dry matter content (g.cm-2): Cm
	doublereal cs,			// I- "new parameter related to brown pigments" (Pablo)
	List       *refl_list,	// IO- Reflectance function
	List       *tran_list	// IO- Transmittance function
);



#endif	/*  __PROSPECT_H */
