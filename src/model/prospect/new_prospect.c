/* 
 * prospect main program
 * Carlos Rueda-Velasquez
 * $Id$
 * 06/15/05 - _datadir used to look for some required data files
 * 04/25/05 - append_float
 * 04/00/02 - Option -s added.
 * Original version: June 2001
 */

#include "prospect.h"

#include <malloc.h>
#include <string.h>
#include <errno.h>

static char* refra_filename = _datadir("refra.txt");
static char* kab_filename   = _datadir("kab.txt");
static char* kw_filename    = _datadir("kw.txt");
static char* km_filename    = _datadir("km.txt");
static char* ks_filename    = _datadir("ks.txt");
static char* ke_filename    = _datadir("ke.txt");




///////////////////////////////////////////////////////////////////////////////////////////////////
static 
int usage(char* msg)
{
	if ( msg )
	{
		fprintf(stderr, "\n%s\n", msg);
		fprintf(stderr, "Try `prospect -h' for more information.\n\n");
		return 1;
	}

	fprintf(stdout,
"\n"
"CSTARS prospect 0.6 (%s %s) %s\n"
"\n"
"USAGE\n"
"  prospect -h\n"
"	    Shows this message and exits\n"
"\n"
"  prospect [options]  N cab cw cm cs\n"
"\n"
"options\n"
"	-refra refra_file  I- refra coefficients(*)  By default:\n"
"	                        %s\n"
"	-kab kab_file      I- kab coefficients(*)  By default:\n"
"	                        %s\n"
"	-kw kw_file        I- kw coefficients(*)  By default:\n"
"	                        %s\n"
"	-km km_file        I- km coefficients(*)  By default:\n"
"	                        %s\n"
"	-ks ks_file        I- ks coefficients(*)  By default:\n"
"	                        %s\n"
"	-ke ke_file        I- ke coefficients(*)  By default:\n"
"	                        %s\n"
"	-rfl rfl_file      O- Generated reflectance function file. (**)\n"
"	-tns tns_file      O- Generated transmittance function file. (**)\n"
"	-append-float     Append values to output files in Float32 data type\n"
"\n"
"	  (*) 421 coeffs expected.  Each line with the format:\n"
"	           wavelength <separator> value \n"
"	      where <separator> is blank, comma, or tab.\n"
"	      All wavelengths will be confronted to expected working wavelengths.\n"
"\n"
"	  (**) An automatic name based on given parameteres is given by default.\n"
"\n"
"	-s bands.spec  I- Desired bands (with weighting windows).\n"
"	                  See below for format description.\n"
"	                  By default, direct output from the model is used.\n"
"\n"
"Required parameters:\n"
"	N              I- Leaf internal structure parameter\n"
"	cab            I- Leaf chlorophyll a+b content (micro g.cm-2)\n"
"	cw             I- Leaf equivalent water thickness (cm)\n"
"	cm             I- Leaf dry matter content (g.cm-2)\n"
"	cs             I- (new parameter related to brown pigments)\n"
"\n"
"EXAMPLE:\n"
"  prospect 1 30 .01 .01 0\n"
"     generates:\n"
"         PROSPECT_N1_cab30_cw0.01_cm0.01_cs0.rfl\n"
"         PROSPECT_N1_cab30_cw0.01_cm0.01_cs0.tns\n"
"\n"
"bands.spec format description:\n"
" (integer)                                   | (number of bands) For each band:\n"
" (string) (string) (integer) (float) (float) |    (bandname) (don't care) (response size) (center) (bandwidth)\n"
"                                             |    the following, (response size) times:\n"
" (integer) (float) (float)                   |        (bandnumber) (wavelength) (weight)\n"
"\n",
		       __DATE__, __TIME__,
			   RTM_VERSION_IN_USAGE,
			   refra_filename,
			   kab_filename,
			   kw_filename,
			   km_filename,
			   ks_filename,
			   ke_filename
	);
	
	return 0;
}


///////////////////////////////////////////////////////////////////////// 
int main(int argc, char** argv)
{
	int arg;
	doublereal vai;
	doublereal cab;
	doublereal cw;
	doublereal cm;
	doublereal cs;


	char       base_filename[1024];
	char       refl_filename[1024];
	char       tran_filename[1024];
	List       refl_list = NULL;   // initialization makes prospect_prm to allocate memory
	List       tran_list = NULL;   // initialization makes prospect_prm to allocate memory
	int status;
	
	// 04-08-02
	char* desired_bands_filename = NULL;

	// append values to output files?
	int append_float = 0;
	
	*refl_filename = 0;
	*tran_filename = 0;


	// process options:
	for ( arg = 1; arg < argc && argv[arg][0] == '-'; arg++ )
	{
		if ( strcmp(argv[arg], "-h") == 0 )
		{
			return usage(NULL);
		}
		else if ( strcmp(argv[arg], "-refra") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -refra expects a filename");
			}
			refra_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-kab") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -ab expects a filename");
			}
			kab_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-kw") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -kw expects a filename");
			}
			kw_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-km") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -km expects a filename");
			}
			km_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-ks") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -ks expects a filename");
			}
			ks_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-ke") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -ke expects a filename");
			}
			ke_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-s") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -s expects a band specification file");
			}
			desired_bands_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-rfl") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -rfl expects a filename");
			}
			strncpy(refl_filename, argv[arg], sizeof(refl_filename) -1);
		}
		else if ( strcmp(argv[arg], "-tns") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -tns expects a filename");
			}
			strncpy(tran_filename, argv[arg], sizeof(tran_filename) -1);
		}
		else if ( strcmp(argv[arg], "-append-float") == 0 )
		{
			append_float = 1;
		}
		else
		{
			return usage("Invalid option");
		}
	}
	
	if ( argc - arg < 5 )
	{
		return usage("Expecting arguments");
	}
	
	
	sscanf(argv[arg++], "%lf", &vai);
	sscanf(argv[arg++], "%lf", &cab);
	sscanf(argv[arg++], "%lf", &cw);
	sscanf(argv[arg++], "%lf", &cm);
	sscanf(argv[arg++], "%lf", &cs);
	
	sprintf(base_filename, "PROSPECT_N%g_cab%g_cw%g_cm%g_cs%g",
		vai, cab, cw, cm, cs
	);
	
	if ( *refl_filename == 0 )
		sprintf(refl_filename, "%s.rfl", base_filename);
	if ( *tran_filename == 0 )
		sprintf(tran_filename, "%s.tns", base_filename);

	
	// initialization:
	prospect_init_coeff_arrays(
		refra_filename,
		kab_filename,
		kw_filename,
		km_filename,
		ks_filename,
		ke_filename
	);

	status = prospect_prm(
		vai,
		cab,
		cw,
		cm,
		cs,
		&refl_list,
		&tran_list
	);
	if ( status != 0 )
	{
		fprintf(stderr, "status = %d\n", status);
		return status;
	}
	
	// 04-08-02
	if ( desired_bands_filename != NULL )
	{
		List desired_refl_list;
		List desired_tran_list;
		int desired_size;
		BandInfo* desired_bands = readBandsInfo(desired_bands_filename, &desired_size);
		if ( desired_bands == NULL )
		{
			return 10;
		}
		
		desired_refl_list = fun040802(refl_list, desired_size, desired_bands, NULL);
		desired_tran_list = fun040802(tran_list, desired_size, desired_bands, NULL);
		
		list_destroy(refl_list);
		list_destroy(tran_list);
		
		refl_list = desired_refl_list;
		tran_list = desired_tran_list;
		
		freeBandsInfo(desired_bands, desired_size);
	}
	
	
	if ( append_float ) {
		long size, i;
		
		FILE* refl_file = fopen(refl_filename, "ab");
		if ( refl_file == NULL ) {
			fprintf(stderr, "Error opening %s : %s\n", refl_filename, strerror(errno));
			return 2;
		}
		FILE* tran_file = fopen(tran_filename, "ab");
		if ( tran_file == NULL ) {
			fprintf(stderr, "Error opening %s : %s\n", tran_filename, strerror(errno));
			return 2;
		}
		size = list_size(refl_list);
		for ( i = 0; i < size; i++ ) {
			RPoint* pr = (RPoint*) list_elementAt(refl_list, i);
			RPoint* pt = (RPoint*) list_elementAt(tran_list, i);
			float valr = pr->y; 
			float valt = pt->y; 
			fwrite(&valr, sizeof(valr), 1, refl_file);
			fwrite(&valt, sizeof(valt), 1, tran_file);
		}
		fclose(refl_file);
		fclose(tran_file);
	}
	else {
		status = writeFunction(refl_list, refl_filename);
		if ( status != 0 ) {
			fprintf(stderr, "Error writing reflectance; status = %d\n", status);
			return status;
		}
	
		status = writeFunction(tran_list, tran_filename);
		if ( status != 0 ) {
			fprintf(stderr, "Error writing transmitance; status = %d\n", status);
			return status;
		}
	}
	
	list_destroy(refl_list);
	list_destroy(tran_list);
	
	return 0;
}



