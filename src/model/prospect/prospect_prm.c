/* 
 * prospect_prm function
 * Carlos Rueda-Velasquez
 * $Id$
 *
 * April 2002: updated to allocate output lists if NULL
 *
 * 10/31/02:
 *	New parameter cs and new coeff array initialization.
 *  New function prospect_init_coeff_arrays to initialized all
 *	coeff arrays reading them from files.
 *
 * Original version: June 2001
 */

/* ////////////////////////////////////////////////////////////////////////
 *	
 *	C Version by: f2c & Carlos Rueda (Jun/5/2001)
 *	
 * //////////////////////////////////////////////////////////////////////// */

#include "prospect.h"

//#include <stdlib.h>
#include <math.h>
#include <ctype.h>


static int initialized = 0;

// arrays taken out from valeur_()

// wavelengths:
static doublereal long0[421] = { 400.,405.,410.,415.,420.,425.,430.,435.,
	    440.,445.,450.,455.,460.,465.,470.,475.,480.,485.,490.,495.,500.,
	    505.,510.,515.,520.,525.,530.,535.,540.,545.,550.,555.,560.,565.,
	    570.,575.,580.,585.,590.,595.,600.,605.,610.,615.,620.,625.,630.,
	    635.,640.,645.,650.,655.,660.,665.,670.,675.,680.,685.,690.,695.,
	    700.,705.,710.,715.,720.,725.,730.,735.,740.,745.,750.,755.,760.,
	    765.,770.,775.,780.,785.,790.,795.,800.,805.,810.,815.,820.,825.,
	    830.,835.,840.,845.,850.,855.,860.,865.,870.,875.,880.,885.,890.,
	    895.,900.,905.,910.,915.,920.,925.,930.,935.,940.,945.,950.,955.,
	    960.,965.,970.,975.,980.,985.,990.,995.,1e3,1005.,1010.,1015.,
	    1020.,1025.,1030.,1035.,1040.,1045.,1050.,1055.,1060.,1065.,1070.,
	    1075.,1080.,1085.,1090.,1095.,1100.,1105.,1110.,1115.,1120.,1125.,
	    1130.,1135.,1140.,1145.,1150.,1155.,1160.,1165.,1170.,1175.,1180.,
	    1185.,1190.,1195.,1200.,1205.,1210.,1215.,1220.,1225.,1230.,1235.,
	    1240.,1245.,1250.,1255.,1260.,1265.,1270.,1275.,1280.,1285.,1290.,
	    1295.,1300.,1305.,1310.,1315.,1320.,1325.,1330.,1335.,1340.,1345.,
	    1350.,1355.,1360.,1365.,1370.,1375.,1380.,1385.,1390.,1395.,1400.,
	    1405.,1410.,1415.,1420.,1425.,1430.,1435.,1440.,1445.,1450.,1455.,
	    1460.,1465.,1470.,1475.,1480.,1485.,1490.,1495.,1500.,1505.,1510.,
	    1515.,1520.,1525.,1530.,1535.,1540.,1545.,1550.,1555.,1560.,1565.,
	    1570.,1575.,1580.,1585.,1590.,1595.,1600.,1605.,1610.,1615.,1620.,
	    1625.,1630.,1635.,1640.,1645.,1650.,1655.,1660.,1665.,1670.,1675.,
	    1680.,1685.,1690.,1695.,1700.,1705.,1710.,1715.,1720.,1725.,1730.,
	    1735.,1740.,1745.,1750.,1755.,1760.,1765.,1770.,1775.,1780.,1785.,
	    1790.,1795.,1800.,1805.,1810.,1815.,1820.,1825.,1830.,1835.,1840.,
	    1845.,1850.,1855.,1860.,1865.,1870.,1875.,1880.,1885.,1890.,1895.,
	    1900.,1905.,1910.,1915.,1920.,1925.,1930.,1935.,1940.,1945.,1950.,
	    1955.,1960.,1965.,1970.,1975.,1980.,1985.,1990.,1995.,2e3,2005.,
	    2010.,2015.,2020.,2025.,2030.,2035.,2040.,2045.,2050.,2055.,2060.,
	    2065.,2070.,2075.,2080.,2085.,2090.,2095.,2100.,2105.,2110.,2115.,
	    2120.,2125.,2130.,2135.,2140.,2145.,2150.,2155.,2160.,2165.,2170.,
	    2175.,2180.,2185.,2190.,2195.,2200.,2205.,2210.,2215.,2220.,2225.,
	    2230.,2235.,2240.,2245.,2250.,2255.,2260.,2265.,2270.,2275.,2280.,
	    2285.,2290.,2295.,2300.,2305.,2310.,2315.,2320.,2325.,2330.,2335.,
	    2340.,2345.,2350.,2355.,2360.,2365.,2370.,2375.,2380.,2385.,2390.,
	    2395.,2400.,2405.,2410.,2415.,2420.,2425.,2430.,2435.,2440.,2445.,
	    2450.,2455.,2460.,2465.,2470.,2475.,2480.,2485.,2490.,2495.,2500. 
	    };

// next arrays are to be filled in by reading from corresponding files:
static doublereal refra0[421];
static doublereal ke0_leaf__[421];
static doublereal kab0_leaf__[421];
static doublereal kw0_leaf__[421];
static doublereal km0_leaf__[421];
static doublereal ks0_leaf__[421];




////////////////////////////////////////////////////////////////////////
// Reads in 421 values from a file.
// Each line assumed to have (at least) two values:
//    wl <separator> val
// <separator> = blank, comma, tab.
static void read_values(char* filename, doublereal* array)
{
	int i, lineno;
	char line[1024];
	
	
	FILE* file = fopen(filename, "r");
	if ( file == NULL )
	{
		fprintf(stderr, "Cannot open %s\n", filename);
		exit(100);
	}
	
	i = 0;
	for ( lineno = 1; i < 421; lineno++ )
	{
		doublereal wavelength, val;
		char* tok;
		
		if ( NULL == fgets(line, sizeof(line) - 1, file) )
		{
			fprintf(stderr, "Unexpected end of file %s (line %d)\n", filename, lineno);
			exit(100);
		}
		
		// skip spaces:
		for ( tok = line; isspace(*tok); tok++ )
			;

		// see if it's a comment or blank line:
		if ( *tok == '#' || *tok == '\n' || *tok == 0 )
			continue;
		
		
		if ( 2 != sscanf(line, "%lf%*[,\t ]%lf", &wavelength, &val) )
		{
			fprintf(stderr, "Cannot read two values from %s (line %d)\n", 
				filename, lineno);
			exit(101);
		}
		
		if ( abs(long0[i] - wavelength) > 1.0e-5 )
		{
			fprintf(stderr, "Unexpected wavelength %g in %s (line %d)\n", 
				wavelength, filename, lineno);
			exit(102);
		}
		
		array[i++] = val;
	}
}



////////////////////////////////////////////////////////////////////////
void  prospect_init_coeff_arrays(
	char* refra_filename,
	char* kab_filename,
	char* kw_filename,
	char* km_filename,
	char* ks_filename,
	char* ke_filename
)
{
	read_values(refra_filename, refra0);
	read_values(kab_filename, kab0_leaf__);
	read_values(kw_filename, kw0_leaf__);
	read_values(km_filename, km0_leaf__);
	read_values(ks_filename, ks0_leaf__);
	read_values(ke_filename, ke0_leaf__);
	
	initialized = 1;
}




/* Common Block Declarations */

struct {
    doublereal n_leaf__, vai, k_leaf__;
} leafin_;

#define leafin_1 leafin_

struct {
    doublereal refl, tran;
} leafout_;

#define leafout_1 leafout_

struct {
    doublereal refra[421], ke_leaf__[421], kab_leaf__[421], kw_leaf__[421],
	    km_leaf__[421],
		ks_leaf__[421]; // <- 10/30/02 new member
} dat_;

#define dat_1 dat_

struct {
    doublereal long__[421];
} wave_;

#define wave_1 wave_

struct {
    doublereal inex__;
} nagout_;

#define nagout_1 nagout_

struct {
    doublereal teta;
} tauin_;

#define tauin_1 tauin_

struct {
    doublereal tau;
} tauout_;

#define tauout_1 tauout_

/* Table of constant values */

static doublereal c_b31 = -1.;

/*     ****************************************************************** */
/*     Jacquemoud S., Bacour C., Poilve H., Frangi J.-P. (1999), */
/*     Comparison of four radiative transfer models to simulate plant */
/*     canopies reflectance - Direct and inverse mode, Remote Sens. */
/*     Environ., submitted. */
/*     Jacquemoud S., Ustin S.L., Verdebout J., Schmuck G., Andreoli G., */
/*     Hosgood B. (1996), Estimating leaf biochemistry using the PROSPECT */
/*     leaf optical properties model, Remote Sens. Environ., 56:194-202 */
/*     Jacquemoud S., Baret F. (1990), PROSPECT: a model of leaf optical */
/*     properties spectra, Remote Sens. Environ., 34:75-91. */
/*     ****************************************************************** */

/*     Stéphane JACQUEMOUD */

/*     Université Paris 7 */
/*     Laboratoire Environnement et Développement */
/*     Case postale 7071 */
/*     2 place Jussieu */
/*     75251 Paris Cedex 05 */
/*     tel: (33)-1-44-27-60-47 */
/*     fax: (33)-1-44-27-81-46 */
/*     E-mail: jacquemo@ccr.jussieu.fr */
/*     www: http://www.sigu7.jussieu.fr/comm/led1.htm */

/*     version 3.01 (5 May 1998) */


/*     ****************************************************************** */
int prospect_prm(
	doublereal vai,			// I- Leaf internal structure parameter
	doublereal cab,			// I- Leaf chlorophyll a+b content (micro g.cm-2)
	doublereal cw,			// I- Leaf equivalent water thickness (cm)
	doublereal cm,			// I- Leaf dry matter content (g.cm-2)
	doublereal cs,			// I- 10/30/02 
	List       *refl_list,	// IO- Reflectance function
	List       *tran_list	// IO- Transmittance function
)
{

    /* Local variables */
    extern /* Subroutine */ int leaf_();
    static integer i__;
    extern /* Subroutine */ int valeur_();

	leafin_1.vai = vai;


	if ( !initialized )
	{
		fprintf(stderr, "prospect_init_coeff_arrays not called yet!");
		exit(39);
	}

    valeur_();

	if ( *refl_list == NULL ) // 04-09-02
	{
		*refl_list = list_create(sizeof(RPoint), 2000, 1000);
		if ( *refl_list == NULL )
		{
			return 1;
		}
	}
	
	if ( *tran_list == NULL ) // 04-09-02
	{
		*tran_list = list_create(sizeof(RPoint), 2000, 1000);
		if ( *tran_list == NULL )
		{
			return 1;
		}
	}
    
    for (i__ = 1; i__ <= 421; ++i__) 
    {
    	RPoint refl_point;
    	RPoint tran_point;
    	
	leafin_1.n_leaf__ = dat_1.refra[i__ - 1];
	
	//// code from original fortran code corresponding to:
	//// k_leaf=ke_leaf[i]+(cab*kab_leaf[i]+cw*kw_leaf[i]+cm*km_leaf[i])/vai;
//	leafin_1.k_leaf__ = dat_1.ke_leaf__[i__ - 1] + (cab * 
//		dat_1.kab_leaf__[i__ - 1] + cw * dat_1.kw_leaf__[i__ - 1] + 
//		cm * dat_1.km_leaf__[i__ - 1]) / leafin_1.vai;

	//  10/30/02 new code including the new parameter 'cs' corresponding to:
	//// k_leaf=ke_leaf[i]+(cab*kab_leaf[i]+cw*kw_leaf[i]+cm*km_leaf[i]+cs*ks_leaf[i])/vai;
	leafin_1.k_leaf__ = dat_1.ke_leaf__[i__ - 1] + (cab * 
		dat_1.kab_leaf__[i__ - 1] + cw * dat_1.kw_leaf__[i__ - 1] +
		cs * dat_1.ks_leaf__[i__ - 1] /*  <- 10/30/02 new term */ +
		cm * dat_1.km_leaf__[i__ - 1]) / leafin_1.vai;

		
	leaf_();

	refl_point.x = (float) wave_1.long__[i__ - 1];
	tran_point.x = (float) wave_1.long__[i__ - 1];

	refl_point.y = (float) leafout_1.refl;
	tran_point.y = (float) leafout_1.tran;
	
	list_addElement(*refl_list, &refl_point);
	list_addElement(*tran_list, &tran_point);
    }
    
    
    	return 0;
}

/*     ******************************************************************** */
/*     leaf : the Prospect model */
/*     ******************************************************************** */
/* Subroutine */ int leaf_()
{
    /* System generated locals */
    doublereal d__1, d__2, d__3, d__4;

    /* Builtin functions */
    double exp(), sqrt(), pow_dd();

    /* Local variables */
    static doublereal alfa, beta, delta, s1, t1, t2, s2, s3, x1, x2, x3, x4, 
	    x5, x6, ra, ta;
    extern /* Subroutine */ int s13aaf_leaf__();
    static doublereal va, vb, rr, tt;
    extern /* Subroutine */ int tav_();

/*     ****************************************************************** */
/*     reflectance and transmittance of one layer */
/*     ****************************************************************** */
/*     Allen W.A., Gausman H.W., Richardson A.J., Thomas J.R. (1969), */
/*     Interaction of isotropic ligth with a compact plant leaf, J. Opt. */
/*     Soc. Am., 59(10):1376-1379. */
/*     Jacquemoud S., Baret F. (1990), Prospect: a model of leaf optical */
/*     properties spectra, Remote Sens. Environ., 34:75-91. */
/*     ****************************************************************** */
    if (leafin_1.k_leaf__ <= 0.) {
	leafin_1.k_leaf__ = 1.;
    } else {
	s13aaf_leaf__();
/* Computing 2nd power */
	d__1 = leafin_1.k_leaf__;
	leafin_1.k_leaf__ = (1. - leafin_1.k_leaf__) * exp(-leafin_1.k_leaf__)
		 + d__1 * d__1 * nagout_1.inex__;
    }
    tauin_1.teta = 90.;
    tav_();
    t1 = tauout_1.tau;
    tauin_1.teta = 60.;
    tav_();
    t2 = tauout_1.tau;
    x1 = 1. - t1;
/* Computing 2nd power */
    d__1 = t1;
/* Computing 2nd power */
    d__2 = leafin_1.k_leaf__;
/* Computing 2nd power */
    d__3 = leafin_1.n_leaf__;
    x2 = d__1 * d__1 * (d__2 * d__2) * (d__3 * d__3 - t1);
/* Computing 2nd power */
    d__1 = t1;
/* Computing 2nd power */
    d__2 = leafin_1.n_leaf__;
    x3 = d__1 * d__1 * leafin_1.k_leaf__ * (d__2 * d__2);
/* Computing 4th power */
    d__1 = leafin_1.n_leaf__, d__1 *= d__1;
/* Computing 2nd power */
    d__2 = leafin_1.k_leaf__;
/* Computing 2nd power */
    d__4 = leafin_1.n_leaf__;
/* Computing 2nd power */
    d__3 = d__4 * d__4 - t1;
    x4 = d__1 * d__1 - d__2 * d__2 * (d__3 * d__3);
    x5 = t2 / t1;
    x6 = x5 * (t1 - 1.) + 1. - t2;
    rr = x1 + x2 / x4;
    tt = x3 / x4;
    ra = x5 * rr + x6;
    ta = x5 * tt;
/*     ****************************************************************** */
/*     reflectance and transmittance of N layers */
/*     ****************************************************************** */
/*     Stokes G.G. (1862), On the intensity of the light reflected from */
/*     or transmitted through a pile of plates, Proc. Roy. Soc. Lond., */
/*     11:545-556. */
/*     ****************************************************************** */
/* Computing 2nd power */
    d__2 = tt;
/* Computing 2nd power */
    d__3 = rr;
/* Computing 2nd power */
    d__1 = d__2 * d__2 - d__3 * d__3 - 1.;
/* Computing 2nd power */
    d__4 = rr;
    delta = d__1 * d__1 - d__4 * d__4 * 4.;
/* Computing 2nd power */
    d__1 = rr;
/* Computing 2nd power */
    d__2 = tt;
    alfa = (d__1 * d__1 + 1. - d__2 * d__2 + sqrt(delta)) / (rr * 2.);
/* Computing 2nd power */
    d__1 = rr;
/* Computing 2nd power */
    d__2 = tt;
    beta = (d__1 * d__1 + 1. - d__2 * d__2 - sqrt(delta)) / (rr * 2.);
/* Computing 2nd power */
    d__1 = rr;
/* Computing 2nd power */
    d__2 = tt;
    va = (d__1 * d__1 + 1. - d__2 * d__2 + sqrt(delta)) / (rr * 2.);
    vb = sqrt(beta * (alfa - rr) / (alfa * (beta - rr)));
    d__1 = leafin_1.vai - 1.;
    d__2 = -(leafin_1.vai - 1.);
    d__3 = leafin_1.vai - 1.;
    d__4 = -(leafin_1.vai - 1.);
    s1 = ra * (va * pow_dd(&vb, &d__1) - pow_dd(&va, &c_b31) * pow_dd(&vb, &
	    d__2)) + (ta * tt - ra * rr) * (pow_dd(&vb, &d__3) - pow_dd(&vb, &
	    d__4));
    s2 = ta * (va - pow_dd(&va, &c_b31));
    d__1 = leafin_1.vai - 1.;
    d__2 = -(leafin_1.vai - 1.);
    d__3 = leafin_1.vai - 1.;
    d__4 = -(leafin_1.vai - 1.);
    s3 = va * pow_dd(&vb, &d__1) - pow_dd(&va, &c_b31) * pow_dd(&vb, &d__2) - 
	    rr * (pow_dd(&vb, &d__3) - pow_dd(&vb, &d__4));
    leafout_1.refl = s1 / s3;
    leafout_1.tran = s2 / s3;
    return 0;
} /* leaf_ */

/*     ******************************************************************** */
/*     s13aaf_leaf : exponential integral int(exp(-t)/t,t=x...inf) */
/*     ******************************************************************** */
/* Subroutine */ int s13aaf_leaf__()
{
    /* Builtin functions */
    double log(), exp();

    /* Local variables */
    static doublereal xx, yy;

    if (leafin_1.k_leaf__ > 4.) {
	goto L10;
    }
    xx = leafin_1.k_leaf__ * .5 - 1.;
    yy = (((((((((((((((xx * -3.60311230482612224e-13 + 
	    3.46348526554087424e-12) * xx - 2.99627399604128973e-11) * xx + 
	    2.57747807106988589e-10) * xx - 2.09330568435488303e-9) * xx + 
	    1.59501329936987818e-8) * xx - 1.13717900285428895e-7) * xx + 
	    7.55292885309152956e-7) * xx - 4.64980751480619431e-6) * xx + 
	    2.63830365675408129e-5) * xx - 1.37089870978830576e-4) * xx + 
	    6.476865037281034e-4) * xx - .00276060141343627983) * xx + 
	    .0105306034687449505) * xx - .0357191348753631956) * xx + 
	    .107774527938978692) * xx - .296997075145080963;
    yy = (yy * xx + .864664716763387311) * xx + .742047691268006429;
    nagout_1.inex__ = yy - log(leafin_1.k_leaf__);
    goto L30;
L10:
    if (leafin_1.k_leaf__ >= 85.) {
	goto L20;
    }
    xx = 14.5 / (leafin_1.k_leaf__ + 3.25) - 1.;
    yy = (((((((((((((((xx * -1.62806570868460749e-12 - 
	    8.95400579318284288e-13) * xx - 4.08352702838151578e-12) * xx - 
	    1.45132988248537498e-11) * xx - 8.35086918940757852e-11) * xx - 
	    2.13638678953766289e-10) * xx - 1.1030243146706977e-9) * xx - 
	    3.67128915633455484e-9) * xx - 1.66980544304104726e-8) * xx - 
	    6.11774386401295125e-8) * xx - 2.70306163610271497e-7) * xx - 
	    1.05565006992891261e-6) * xx - 4.72090467203711484e-6) * xx - 
	    1.95076375089955937e-5) * xx - 9.16450482931221453e-5) * xx - 
	    4.05892130452128677e-4) * xx - .00214213055000334718;
    yy = ((yy * xx - .0106374875116569657) * xx - .0850699154984571871) * xx 
	    + .923755307807784058;
    nagout_1.inex__ = exp(-leafin_1.k_leaf__) * yy / leafin_1.k_leaf__;
    goto L30;
L20:
    nagout_1.inex__ = 0.;
    goto L30;
L30:
    return 0;
} /* s13aaf_leaf__ */

/*     ****************************************************************** */
/*     evaluation of tav for any solid angle */
/*     ****************************************************************** */
/*     Stern F. (1964), Transmission of isotropic radiation across an */
/*     interface between two dielectrics, Appl. Opt., 3(1):111-113. */
/*     Allen W.A. (1973), Transmission of isotropic light across a */
/*     dielectric surface in two and three dimensions, J. Opt. Soc. Am., */
/*     63(6):664-666. */
/*     ****************************************************************** */
/* Subroutine */ int tav_()
{
    /* System generated locals */
    doublereal d__1, d__2, d__3, d__4, d__5, d__6;

    /* Builtin functions */
    double sin(), sqrt(), log();

    /* Local variables */
    static doublereal b0, b1, b2, r2, rd, ds, ax, bx, rm, rp, tp, ts, tetard, 
	    tp1, tp2, tp3, tp4, tp5;

    rd = .017453292519943295;
    tetard = tauin_1.teta * rd;
/* Computing 2nd power */
    d__1 = leafin_1.n_leaf__;
    r2 = d__1 * d__1;
    rp = r2 + 1.;
    rm = r2 - 1.;
/* Computing 2nd power */
    d__1 = leafin_1.n_leaf__ + 1.;
    ax = d__1 * d__1 / 2.;
/* Computing 2nd power */
    d__1 = r2 - 1.;
    bx = -(d__1 * d__1) / 4.;
    ds = sin(tetard);
    if (tauin_1.teta == 0.) {
/* Computing 2nd power */
	d__1 = leafin_1.n_leaf__ + 1.;
	tauout_1.tau = leafin_1.n_leaf__ * 4. / (d__1 * d__1);
    } else {
	if (tauin_1.teta == 90.) {
	    b1 = 0.;
	} else {
/* Computing 2nd power */
	    d__2 = ds;
/* Computing 2nd power */
	    d__1 = d__2 * d__2 - rp / 2.;
	    b1 = sqrt(d__1 * d__1 + bx);
	}
/* Computing 2nd power */
	d__1 = ds;
	b2 = d__1 * d__1 - rp / 2.;
	b0 = b1 - b2;
/* Computing 2nd power */
	d__1 = bx;
/* Computing 3rd power */
	d__2 = b0;
/* Computing 2nd power */
	d__3 = bx;
/* Computing 3rd power */
	d__4 = ax;
	ts = d__1 * d__1 / (d__2 * (d__2 * d__2) * 6.) + bx / b0 - b0 / 2. - (
		d__3 * d__3 / (d__4 * (d__4 * d__4) * 6.) + bx / ax - ax / 2.)
		;
/* Computing 2nd power */
	d__1 = rp;
	tp1 = r2 * -2. * (b0 - ax) / (d__1 * d__1);
/* Computing 2nd power */
	d__1 = rm;
	tp2 = r2 * -2. * rp * log(b0 / ax) / (d__1 * d__1);
	tp3 = r2 * (1. / b0 - 1. / ax) / 2.;
/* Computing 2nd power */
	d__1 = r2;
/* Computing 2nd power */
	d__2 = r2;
/* Computing 2nd power */
	d__3 = rm;
/* Computing 2nd power */
	d__4 = rm;
/* Computing 3rd power */
	d__5 = rp;
/* Computing 2nd power */
	d__6 = rm;
	tp4 = d__1 * d__1 * 16. * (d__2 * d__2 + 1.) * log((rp * 2. * b0 - 
		d__3 * d__3) / (rp * 2. * ax - d__4 * d__4)) / (d__5 * (d__5 *
		 d__5) * (d__6 * d__6));
/* Computing 3rd power */
	d__1 = r2;
/* Computing 2nd power */
	d__2 = rm;
/* Computing 2nd power */
	d__3 = rm;
/* Computing 3rd power */
	d__4 = rp;
	tp5 = d__1 * (d__1 * d__1) * 16. * (1. / (rp * 2. * b0 - d__2 * d__2) 
		- 1. / (rp * 2. * ax - d__3 * d__3)) / (d__4 * (d__4 * d__4));
	tp = tp1 + tp2 + tp3 + tp4 + tp5;
/* Computing 2nd power */
	d__1 = ds;
	tauout_1.tau = (ts + tp) / (d__1 * d__1 * 2.);
    }
    return 0;
} /* tav_ */

/*     ******************************************************************** */
/*     valeur : wavelength (long), refractive index (refra), absorption */
/*     coefficient of an albino leaf (ke), specific absorption coefficient */
/*     of chlorophyll a+b (kab), water (kw), dry matter (km) */
/*     ******************************************************************** */
/* Subroutine */ int valeur_()
{
	
    static integer i__;

    for (i__ = 1; i__ <= 421; ++i__) {
	wave_1.long__[i__ - 1] = long0[i__ - 1];
	dat_1.refra[i__ - 1] = refra0[i__ - 1];
	dat_1.ke_leaf__[i__ - 1] = ke0_leaf__[i__ - 1];
	dat_1.kab_leaf__[i__ - 1] = kab0_leaf__[i__ - 1];
	dat_1.kw_leaf__[i__ - 1] = kw0_leaf__[i__ - 1];
	dat_1.km_leaf__[i__ - 1] = km0_leaf__[i__ - 1];
	dat_1.ks_leaf__[i__ - 1] = ks0_leaf__[i__ - 1]; // <- 10/30/02
    }
    return 0;
} /* valeur_ */

