/* 
/////////////////////////////////////////////////////////////////////////
//
// infi
//
// Carlos Rueda & David Riano - CSTARS
// Feb 2004 -infi produced.
//
///////////////////////////////////////////////////////////////////////// 
*/


#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>

#include "process_infi.h"

/////////////////////////////////
/* Infinitive models from Zarco-Tejada et al. IEEE TGRS v39 n7 july 2001*/

real infi1(int print, float roo, float tau ) 
{
	real result;
	
	if ( print ) 
	{ // DEBUG
		printf("infi_prm INPUT  %e %e \n",
			// inputs
			roo,
			tau
		);

		printf("%e %e", roo,tau);
		printf("\n");
	} // DEBUG

	result=(roo*0.01/(1-tau*tau*0.0001));
	return result;
}


real infi3(int print, float roo, float tau) 
{

	real result;
	
	if ( print ) 
	{ // DEBUG
		printf("infi3_prm INPUT  %e %e \n",
			// inputs
			roo,
			tau
		);

		printf("%e %e", roo,tau);
		printf("\n");
	} // DEBUG

	result=(1-sqrt(1-(roo+tau)*0.01))/(1+sqrt(1-(roo+tau)*0.01));
	return result;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Makes all processing with given parameters.
 */
int process_infi (
	List reflectance_list,
	List transmittance_list,
	int model,                                           // infinitive reflectance model:  1 (Lillestaeter),  2 (Yamada&Fujimura),  3 (Hapke)
	List  *canopy_reflectante_list		// resulting function
)
{
	long size, i;
	real (*infi)(int print, float roo, float tau) = NULL;

	// for testing	
	long report_index;
	char* env;
	
	switch ( model ) {
		case 1: 
			infi = infi1; 
			break;
		case 2: 
			fprintf(stderr, "Sorry, model 2 not implemented yet\n");
			exit(1);
			break;
		case 3: 
			infi = infi3; 
			break;
		default: 
			fprintf(stderr, "Which model?  given %d\n", model);
			exit(1);
			break;
	}			

	
	size = list_size(reflectance_list);
	
	if ( size != list_size(transmittance_list) )
	{
		fprintf(stderr, "Functions are not conformant\n"
			"reflectance size   = %ld\n"
			"transmittance size = %ld\n",
				size,
				list_size(transmittance_list)
		);
		return 1;
	}
	
	report_index = -1L;
	env = getenv("infi_TEST");
	if ( env != NULL )
	{
		sscanf(env, "%ld", &report_index);
	}

	if ( *canopy_reflectante_list == NULL )
	{
		// create resulting function:
		*canopy_reflectante_list = list_create(sizeof(RPoint), size, 0);
	}
	else
	{
		list_setSize(*canopy_reflectante_list, 0);
	}
	
	for ( i = 0; i < size; i++ )
	{
		RPoint* refl_point = (RPoint*) list_elementAt(reflectance_list, i);
		RPoint* tran_point = (RPoint*) list_elementAt(transmittance_list, i);
		RPoint  rcanopy_reflectance_point;
		
		//
		// PENDING
		// refl, tran, and soil are multiplied here by 100. Maybe this should
		// be an option, for example.
		//
		float roo = refl_point->y * 100;
		float tau = tran_point->y * 100;
		
		real result = infi(report_index == i, roo, tau);
			
		rcanopy_reflectance_point.x = refl_point->x;
		rcanopy_reflectance_point.y = result;
		
		list_addElement(*canopy_reflectante_list, &rcanopy_reflectance_point);
			
		if ( report_index == i )
		{
			
			
			// new simpler format:	
	
			fprintf(stderr, 
				"%.9f\n" 
				"%.9f\n",
					roo,
					tau
			);
						
			fprintf(stderr, "\n");
			
			fprintf(stderr, "RESULT = %e\n", result);
			fprintf(stderr, "index reported = %ld\n", report_index);
		
		}
	}	

	return 0;
}

