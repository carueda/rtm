/* 
/////////////////////////////////////////////////////////////////////////
//
// prospect_infi
//
// Carlos Rueda & David Riano - CSTARS
// Feb 2004 -infi produced.
//
///////////////////////////////////////////////////////////////////////// 
*/


#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>

#include "prospect_infi.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
int prospect_infi_prm(
	doublereal vai,
	doublereal cab,
	doublereal cw,
	doublereal cm,
	doublereal cs,			// I- "new parameter related to brown pigments" (Pablo)

	int model,                          // infinitive reflectance model:  1 (Lillestaeter),  2 (Yamada&Fujimura),  3 (Hapke)

	List       *canopy_reflectante_list		// resulting function
)
{
	

	List reflectance_list = NULL;   // initialization makes prospect_prm to allocate memory
	List transmittance_list = NULL; // initialization makes prospect_prm to allocate memory
	
	int status;


	status = prospect_prm(
		vai,
		cab,
		cw,
		cm,
		cs,
		&reflectance_list,
		&transmittance_list
	);
	
	if ( status != 0 )
	{
		fprintf(stderr, "prospect_prm status = %d\n", status);
		fprintf(stderr, "vai, cab, cw, cm = %f %f %f %f \n", vai, cab, cw, cm);
		return status;
	}
	
	// call the main processing routine:
	status = process_infi(
		reflectance_list,
		transmittance_list,
		model,
		canopy_reflectante_list		// resulting function
	);

	list_destroy(reflectance_list);
	list_destroy(transmittance_list);
	
	return status;
}

