prospect_infi1
--------------
Carlos Rueda                         (see also ../README)

10/31/02
	 - new parameter 'cs' and initialization of PROSPECT model.
             See prospect.h
			 
4/9/02
	Added option -s. 

7/03/01
	Reorganization, but not change in functionality.
	
3/04/03
	David Riano
	New prospect_infi1 program to integrate prospect and infinitive1 reflectance model
	described in Zarco-Tejada et al. IEEE TGRS v39 n7 july 2001