/* 
/////////////////////////////////////////////////////////////////////////
//
// infi
//
// Carlos Rueda & David Riano - CSTARS
// Feb 2004 -infi produced.
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

#ifndef __infi_H
#define __infi_H


#ifndef __FUNC
#include "func.h"
#endif

#include "f2c.h"

#include <stdio.h>



///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Makes all processing with given parameters.
 *
 * Note about canopy_reflectante_list:
 *   If null, a new list is created internally; if not, 
 *   its size is first set to 0. Be sure the element size is sizeof(RPoint).
 */
int process_infi(
	List reflectance_list,
	List transmittance_list,
	int model,                                            // infinitive reflectance model:  1 (Lillestaeter),  2 (Yamada&Fujimura),  3 (Hapke)
	List  *canopy_reflectante_list		// resulting function
);

#endif	/*  __infi_H */
