/* 
/////////////////////////////////////////////////////////////////////////
//
// prospect_infi
//
// Carlos Rueda & David Riano - CSTARS
// 06/15/05 - _datadir used to look for some required data files
// Feb 2004 -infi produced.
// April 2002 - Option -s added.
// 10/31/02  - new parameter 'cs' and initialization of PROSPECT model.
//             See prospect.h
//
///////////////////////////////////////////////////////////////////////// 
*/


#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>

#include "prospect_infi.h"


// For prospect model:
static char* refra_filename = _datadir("refra.txt");
static char* kab_filename   = _datadir("kab.txt");
static char* kw_filename    = _datadir("kw.txt");
static char* km_filename    = _datadir("km.txt");
static char* ks_filename    = _datadir("ks.txt");
static char* ke_filename    = _datadir("ke.txt");



///////////////////////////////////////////////////////////////////////////////////////////////////
static 
int usage(char* msg)
{
	if ( msg )
	{
		fprintf(stderr, "\n%s\n", msg);
		fprintf(stderr, "Try `prospect_infi -h' for more information.\n\n");
		return 1;
	}

	fprintf(stdout,
"\n"
"CSTARS prospect_infi 0.1 (%s %s) %s\n"
"\n"
"USAGE\n"
"  prospect_infi -h\n"
"	    Shows this message and exits\n"
"\n"
"USAGE\n"
"  prospect_infi [options] N cab cw cm cs model canopy_rfl\n"
"\n"
"options\n"
"	See ``prospect -h'' for options: -refra -kab -kw -km -ks -ke -s\n"
"\n"
"Required parameters:\n"
"	See ``prospect -h'' for paramaters: N cab cw cm cs\n"
"	model             I-  1 (Lillestaeter),  2 (Yamada&Fujimura)- NOT IMPLEMENTED,  3 (Hapke)\n"
"	canopy_rfl      O- Generated reflectance function file\n"
"\n"
"EXAMPLE:\n"
"  prospect_infi 1 30 .01 .01 0 1 canopy.rfl\n"
"\n"
"NOTE:\n"
"  Infinitive models from Zarco-Tejada et al. IEEE TGRS v39 n7 july 2001\n"
"\n",
		       __DATE__, __TIME__,
			   RTM_VERSION_IN_USAGE
	);
	
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	int arg;
	doublereal vai;			// I- Leaf internal structure parameter
	doublereal cab;			// I- Leaf chlorophyll a+b content (micro g.cm-2)
	doublereal cw;			// I- Leaf equivalent water thickness (cm): Cw
	doublereal cm;			// I- Leaf dry matter content (g.cm-2): Cm
	doublereal cs;
	int model;                             // 1 (Lillestaeter),  2 (Yamada&Fujimura),  3 (Hapke)

	char* dbname_report;
		
	List canopy_reflectante_list = NULL;   // initialization makes prospect_prm to allocate memory;
		// resulting function
	
	FILE* file;
	long i, size;
	
	int status;
	
	// 04-08-02
	char* desired_bands_filename = NULL;

	// process options:
	for ( arg = 1; arg < argc && argv[arg][0] == '-'; arg++ )
	{
		if ( strcmp(argv[arg], "-h") == 0 )
		{
			return usage(NULL);
		}
		else if ( strcmp(argv[arg], "-refra") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -refra expects a filename");
			}
			refra_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-kab") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -ab expects a filename");
			}
			kab_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-kw") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -kw expects a filename");
			}
			kw_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-km") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -km expects a filename");
			}
			km_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-ks") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -ks expects a filename");
			}
			ks_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-ke") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -ke expects a filename");
			}
			ke_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-s") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -s expects a band specification file");
			}
			desired_bands_filename = argv[arg];
		}
		else
		{
			return usage("Invalid option");
		}
	}
	
	if ( argc - arg < 7 )
	{
		return usage("Expecting arguments");
	}
	
	sscanf(argv[arg++], "%lf", &vai);
	sscanf(argv[arg++], "%lf", &cab);
	sscanf(argv[arg++], "%lf", &cw);
	sscanf(argv[arg++], "%lf", &cm);
	sscanf(argv[arg++], "%lf", &cs);
	sscanf(argv[arg++], "%d", &model);
	dbname_report = argv[arg++];

	// PROSPECT model initialization:
	prospect_init_coeff_arrays(
		refra_filename,
		kab_filename,
		kw_filename,
		km_filename,
		ks_filename,
		ke_filename
	);


	status = prospect_infi_prm(
		vai,
		cab,
		cw,
		cm,
		cs,
		model,
		&canopy_reflectante_list
	);

	if ( status != 0 )
	{
		fprintf(stderr, "prospect_infi_prm status = %d\n", status);
		return status;
	}

	// 04-08-02
	if ( desired_bands_filename != NULL )
	{
		List desired_refl_list;
		int desired_size;
		BandInfo* desired_bands = readBandsInfo(desired_bands_filename, &desired_size);
		if ( desired_bands == NULL )
		{
			return 10;
		}
		
		desired_refl_list = fun040802(canopy_reflectante_list, desired_size, desired_bands, NULL);
		
		list_destroy(canopy_reflectante_list);
		
		canopy_reflectante_list = desired_refl_list;
		
		freeBandsInfo(desired_bands, desired_size);
	}
	
	file = fopen (dbname_report, "w");
	if ( file == NULL )
	{
		fprintf(stderr, "Error creating report %s : %s\n", dbname_report, strerror(errno));
		return 2;
	}

	size = list_size(canopy_reflectante_list);
	for ( i = 0; i < size; i++ )
	{
		RPoint* canopy_reflectance_point = (RPoint*) list_elementAt(canopy_reflectante_list, i);
		
		fprintf(file, "%f , %E\n", canopy_reflectance_point->x, canopy_reflectance_point->y);
	}

	fclose(file);
	
	list_destroy(canopy_reflectante_list);
	
	return 0;
	
	
}

