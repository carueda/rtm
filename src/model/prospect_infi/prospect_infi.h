/* 
/////////////////////////////////////////////////////////////////////////
//
// prospect_infi
//
// Carlos Rueda & David Riano- CSTARS
// Feb 2004 -infi produced.
// June 2003
//
// 10/31/02  - new parameter 'cs'. See prospect.h
//
///////////////////////////////////////////////////////////////////////// 
*/

#ifndef __PROSPECT_infi_H
#define __PROSPECT_infi_H

#include "cstars-rtm.h"

#ifndef __LIST_H
#include "list.h"
#endif

#include "f2c.h"

#include "prospect.h"

#include "process_infi.h"

#include <stdio.h>


///////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * The PROSPECT-infi Model
 *
 * Note about canopy_reflectante_list:
 *   If null, a new list is created internally; if not, 
 *   its size is first set to 0. Be sure the element size is sizeof(RPoint).
 */
int prospect_infi_prm(
	doublereal vai,			// I- Leaf internal structure parameter
	doublereal cab,			// I- Leaf chlorophyll a+b content (micro g.cm-2)
	doublereal cw,			// I- Leaf equivalent water thickness (cm): Cw
	doublereal cm,			// I- Leaf dry matter content (g.cm-2): Cm
	doublereal cs,			// I- "new parameter related to brown pigments" (Pablo)

	int model,                          // infinitive reflectance model:  1 (Lillestaeter),  2 (Yamada&Fujimura),  3 (Hapke)

	List       *canopy_reflectante_list		// IO- resulting function
);



#endif	/*  __PROSPECT_infi_H */
