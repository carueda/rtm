/*
/////////////////////////////////////////////////////////////////////////
//
// geosail_prm_lists - "main" service routine.
//
// Carlos Rueda - CSTARS
// August 2001
//
//  #define SHOW_INPUTS to show inputs before processing.
//
/////////////////////////////////////////////////////////////////////////
*/

#include "geosail.h"


///////////////////////////////////////////////////////////////////////////////////
/**
 * Helper to tranfer data from a list to an array.
 */
static void transfer_from_list(List src, int src_from, int len, real* dest, int dest_from)
{
    int i;
    for ( i = 0; i < len; i++ )
    {
        RPoint* p = (RPoint*) list_elementAt(src, src_from + i);
        dest[dest_from + i] = p->y;
    }
}

///////////////////////////////////////////////////////////////////////////////////
/**
 * Helper to tranfer data from an array to a list.
 * The points are added to list. Abscissas are taken from ref_list;
 * Ordinates are taken from array.
 */
static void add_points(real* src, int src_from, int len, List dest, List ref_list, int dest_from)
{
    int i;
    for ( i = 0; i < len; i++ )
    {
        // reference point:
        RPoint rp = *((RPoint*) list_elementAt(ref_list, dest_from + i));

        rp.y = src[src_from + i];
        list_addElement(dest, &rp);
    }
}

#ifdef SHOW_INPUTS
///////////////////////////////////////////////////////////////////////////////////
static void show_input(
    integer    nlai,          // I- CANOPY LAYERS (NLAI)
    integer    ncomp,         // I- NUMBER OF CANOPY COMPONENTS (NCOMP)
    real       li[9],         // I- 9 LEAF INCLINATION ANGLE INTERVALS (LI) -- LI(9)
    real       lang[90],      // I- ncomp*9 LEAF ANGLE DISTRIBUTIONS
    List*      rfl_list,      // I- ncomp SPECTRAL REFLECTANCES
    List*      tns_list,      // I- ncomp SPECTRAL TRANSMITTANCES
    List       rhos_list,     // I- SOIL SPECTRAL REFLECTANC
    real       tts,           // I- THE SOLAR ZENITH ANGLE (TTS)
    real       lai[75],       // I- ncomp*nlai CROWN LAIS (LAI)   -- LAI(15,5)  in FORTRAN
    char*      cshp,         // I- CROWN SHAPE (CSHP) "CYLI" or "CONE"
    real       chw,           // I- HEIGHT TO WIDTH RATIO OF THE CROWNS (CHW)
    real       ccover         // I- CANOPY COVERAGE (CCOVER)
)
{
    int i, j, k;
    List list;


    printf("nlai = %d\n", nlai);
    printf("ncomp = %d\n", ncomp);
    for ( i = 0; i < 9; i++ )
    {
        printf("li = %f\n", li[i]);
    }
    for ( i = 0; i < ncomp; i++ )
    {
        for ( j = 0; j < 9; j++ )
        {
            printf("lang = %f\n", lang[i*9 +j]);
        }
    }
    for ( i = 0; i < ncomp; i++ )
    {
        list = rfl_list[i];
        printf("rfl[%i] = ", i);
        for ( j = 0; j < list_size(list); j++ )
        {
            RPoint* p = (RPoint*) list_elementAt(list, j);
            printf("%f ", p->y);
        }
        printf("\n");

        list = tns_list[i];
        printf("tns[%i] = ", i);
        for ( j = 0; j < list_size(list); j++ )
        {
            RPoint* p = (RPoint*) list_elementAt(list, j);
            printf("%f ", p->y);
        }
        printf("\n");
    }
    list = rhos_list;
    printf("rhos = ");
    for ( j = 0; j < list_size(list); j++ )
    {
        RPoint* p = (RPoint*) list_elementAt(list, j);
        printf("%f ", p->y);
    }
    printf("\n");
    printf("tts = %f\n", tts);
    // lai...
    printf("lai =");
    for ( i = 1; i <= nlai; i++ )
    {
        for (k = 1; k <= ncomp; ++k)
        {
            printf("%f ", lai[i + k * 15 - 16]);
        }

    }
    printf("\n");
    printf("cshp = %s\n", cshp);
    printf("chw = %f\n", chw);
    printf("ccover = %f\n", ccover);
}
#endif // SHOW_INPUTS




///////////////////////////////////////////////////////////////////////////////////
int geosail_prm_lists(
    integer    nlai,          // I- CANOPY LAYERS (NLAI)
    integer    ncomp,         // I- NUMBER OF CANOPY COMPONENTS (NCOMP)
    real       li[9],         // I- 9 LEAF INCLINATION ANGLE INTERVALS (LI) -- LI(9)
    real       lang[90],      // I- ncomp*9 LEAF ANGLE DISTRIBUTIONS
    List*      rfl_list,      // I- ncomp SPECTRAL REFLECTANCES
    List*      tns_list,      // I- ncomp SPECTRAL TRANSMITTANCES
    List       rhos_list,     // I- SOIL SPECTRAL REFLECTANC
    real       tts,           // I- THE SOLAR ZENITH ANGLE (TTS)
    real       lai[75],       // I- ncomp*nlai CROWN LAIS (LAI)   -- LAI(15,5)  in FORTRAN
    char*      cshp,         // I- CROWN SHAPE (CSHP) "CYLI" or "CONE"
    real       chw,           // I- HEIGHT TO WIDTH RATIO OF THE CROWNS (CHW)
    real       ccover,        // I- CANOPY COVERAGE (CCOVER)

    // OUTPUTS
    List       *result_rfl_list,    // O-  reflectance function
    List       *result_frac_list   // O-  instantaneous fraction of radiation absorved
)
{
    long size, i;
    long completed;    // number of wavelengths completed

#ifdef SHOW_INPUTS
    printf("showing inputs...\n");
    show_input(
        nlai,
        ncomp,
        li,
        lang,
        rfl_list,
        tns_list,
        rhos_list,
        tts,
        lai,
        cshp,         // I- CROWN SHAPE (CSHP) "CYLI" or "CONE"
        chw,
        ccover
    );
#endif // SHOW_INPUTS

    // take length from the first rfl function:
    size = list_size(rfl_list[0]);

    // check that all ncomp rfl and tns functions are the same length:
    for ( i = 0; i < ncomp; i++ )
    {
        long sz = list_size(rfl_list[i]);
        if (sz != size )
        {
            fprintf(stderr, " %ld-reflectance function length %ld != %ld\n",
                i, sz, size
            );
            return 1;
        }
        sz = list_size(tns_list[i]);
        if (sz != size )
        {
            fprintf(stderr, " %ld-transmittance function length %ld != %ld\n",
                i, sz, size
            );
            return 1;
        }
    }
    // check soil length:
    if ( list_size(rhos_list) != size )
    {
        fprintf(stderr, " soil reflectance function length %ld != %ld\n",
            list_size(rhos_list), size
        );
        return 1;
    }

    // create resulting functions:
    if ( (*result_rfl_list = list_create(sizeof(RPoint), size, 0)) == NULL
    ||   (*result_frac_list = list_create(sizeof(RPoint), size, 0)) == NULL )
    {
        fprintf(stderr, "No memory to allocate resulting lists (%ld points)\n", size);
        return 1;
    }

    // We have `size' wavelengths but nlam_ must be <= 10 to
    // make a proper call to geosail_prm.

    printf("%ld points to process:\n", size);
    completed = 0;

    while ( completed < size )
    {
        // info to geosail_prm:
        integer    nlam_;        // I- NUMBER OF WAVELENGTH BANDS (NLAM)
        real       refl_[150];   // I- SPECTRAL REFLECTANCE (REFL)       -- REFL(10,15) in FORTRAN
        real       trm_[150];    // I- SPECTRAL TRANSMITTANCE (TRM)      -- TRM(10,15) in FORTRAN
        real       rsoil_[10];   // I- SOIL SPECTRAL REFLECTANCE (RSOIL) -- RSOIL(10) in FORTRAN
        real       result_refl[10];     // O- Resulting reflectance function
        real       result_frac[10];     // O- Resulting instantaneous fraction of
        int        status;

        //printf("%ld points completed\n", completed);

        nlam_ = size - completed;
        if ( nlam_ > 10 )
        {
            nlam_ = 10;
        }

        //printf("transfering %d points to basic service...\n", nlam_);

        // transfer nlam_ data to accomodate input to geosail_prm:

        // soil reflectance:
        transfer_from_list(rhos_list, completed, nlam_, rsoil_, 0);

        // reflectances and transmittances:
        for ( i = 0; i < ncomp; i++ )
        {
            transfer_from_list(rfl_list[i], completed, nlam_, refl_, i * 10);
            transfer_from_list(tns_list[i], completed, nlam_, trm_, i * 10);
        }

        // call the basic service:
        status = geosail_prm(
            nlam_,
            nlai,
            ncomp,
            li,
            lang,
            refl_,
            trm_,
            rsoil_,
            tts,
            lai,
            cshp,
            chw,
            &ccover,
            result_refl,
            result_frac
        );

        if ( status != 0 )
        {
            fprintf(stderr, "geosail_prm status = %d\n", status);
            return status;
        }

        // transfer from result to output lists:
        add_points(result_refl, 0, nlam_, *result_rfl_list, rfl_list[0], completed);
        add_points(result_frac, 0, nlam_, *result_frac_list, rfl_list[0], completed);

        // updated number of wavelentgths processed:
        completed += nlam_;
    }

    //printf("%ld points completed\n", completed);

    return 0;

}

