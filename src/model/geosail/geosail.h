/*
/////////////////////////////////////////////////////////////////////////
//
// geosail.h
//
// Carlos Rueda - CSTARS
// August 2001
//
/////////////////////////////////////////////////////////////////////////
*/

#ifndef __GEOSAIL_H
#define __GEOSAIL_H

#include "cstars-rtm.h"
#include "f2c.h"


#ifndef __FUNC_H
#include "func.h"
#endif


#include <stdio.h>



///////////////////////////////////////////////////////////////////////////////////
/**
 * This is the complete service to run the GeoSail model,
 * working on function lists.
 * This routine is the one called directly from the main program "new_geosail.c"
 */
int geosail_prm_lists(
    integer    nlai,          // I- CANOPY LAYERS (NLAI)
    integer    ncomp,         // I- NUMBER OF CANOPY COMPONENTS (NCOMP)
    real       li[9],         // I- 9 LEAF INCLINATION ANGLE INTERVALS (LI) -- LI(9)
    real       lang[90],      // I- ncomp*9 LEAF ANGLE DISTRIBUTIONS
    List*      rfl_list,      // I- ncomp SPECTRAL REFLECTANCES
    List*      tns_list,      // I- ncomp SPECTRAL TRANSMITTANCES
    List       rhos_list,     // I- SOIL SPECTRAL REFLECTANC
    real       tts,           // I- THE SOLAR ZENITH ANGLE (TTS)
    real       lai[75],       // I- ncomp*nlai CROWN LAIS (LAI)   -- LAI(15,5)  in FORTRAN
    char*      cshp,         // I- CROWN SHAPE (CSHP) "CYLI" or "CONE"
    real       chw,           // I- HEIGHT TO WIDTH RATIO OF THE CROWNS (CHW)
    real       ccover,        // I- CANOPY COVERAGE (CCOVER)

    // OUTPUTS
    List       *result_rfl_list,   // O-  reflectance function
    List       *result_frac_list   // O-  instantaneous fraction of radiation absorved
);


///////////////////////////////////////////////////////////////////////////////////
/**
 * This is the basic service to run the GeoSail model.
 * This is called from the complete service geosail_prm_lists().
 *
 * Note that it only accepts ONE canopy cover (parameter ccovers_) to produce
 * only one reflectance function (and one fraction of radiation absorved
 * function).
 *
 * Some limitations come from the original FORTRAN version (that we don't want
 * to change at all):
 *
 *    NUMBER OF BANDS, nlam_, MUST BE <= 10
 *    NUMBER OF LAYERS, nlai_, MUST BE <= 15
 *    NUMBER OF COMPONENTS, ncomp_, MUST BE <= 15
 *
 */
int geosail_prm(
    integer    nlam_,        // I- NUMBER OF WAVELENGTH BANDS (NLAM)
    integer    nlai_,        // I- CANOPY LAYERS (NLAI)
    integer    ncomp_,       // I- NUMBER OF CANOPY COMPONENTS (NCOMP)
    real       li_[9],       // I- LEAF INCLINATION ANGLE INTERVALS (LI) -- LI(9)
    real       lang_[90],    // I- LEAF ANGLE DISTRIBUTION (LANG)    -- LANG(9,10) in FORTRAN
    real       refl_[150],   // I- SPECTRAL REFLECTANCE (REFL)       -- REFL(10,15) in FORTRAN
    real       trm_[150],    // I- SPECTRAL TRANSMITTANCE (TRM)      -- TRM(10,15) in FORTRAN
    real       rsoil_[10],   // I- SOIL SPECTRAL REFLECTANCE (RSOIL) -- RSOIL(10) in FORTRAN
    real       tts_,         // I- THE SOLAR ZENITH ANGLE (TTS)
    real       lai_[75],     // I- CROWN LAI (LAI)                   -- LAI(15,5)  in FORTRAN
    char*      cshp_,       // I- CROWN SHAPE (CSHP)
    real       chw_,         // I- HEIGHT TO WIDTH RATIO OF THE CROWNS (CHW)
    real*      ccovers_,     // I- CANOPY COVERAGE (CCOVER)
                             //    ONLY ONE VALUE ASSUMED

    // OUTPUTS
    //  Only one output "group" corresponding to ONE canopy coverage.
    //
    real       result_refl[10],     // O- Resulting reflectance function
    real       result_frac[10]      // O- Resulting instantaneous fraction of
                                    //    radiation absorved
);



///////////////////////////////////////////////////////////////////////////////////
/**
 * Initial conversion from fortran: take parameters from arguments
 * and write to stdout.
 */
int geosail_pre_prm(
    char*      fileout,
    integer    nlam_,        // I- NUMBER OF WAVELENGTH BANDS (NLAM)
    integer    nlai_,        // I- CANOPY LAYERS (NLAI)
    integer    ncomp_,       // I- NUMBER OF CANOPY COMPONENTS (NCOMP)
    real       li_[9],       // I- LEAF INCLINATION ANGLE INTERVALS (LI)
    real       lang_[90],	// I- LEAF ANGLE DISTRIBUTION (LANG)
    real       refl_[150],   // I- SPECTRAL REFLECTANCE (REFL)
    real       trm_[150],    // I- SPECTRAL TRANSMITTANCE (TRM)
    real       rsoil_[10],	// I- SOIL SPECTRAL REFLECTANCE (RSOIL)
    real       tts_,         // I- THE SOLAR ZENITH ANGLE (TTS)
    real       lai_[75],     // I- CROWN LAI (LAI)
    char*      cshp_,        // I- CROWN SHAPE (CSHP)
    real       chw_,         // I- HEIGHT TO WIDTH RATIO OF THE CROWNS (CHW)
    integer    nval_,        // I- NUMBER OF DIFFERENT CANOPY COVERAGES TO CALCULATE (NVAL)
    real*      ccovers_       // I- CANOPY COVERAGE (CCOVER)  ***NOTE:  nval values
);

///////////////////////////////////////////////////////////////////////////////////
int read_input(
    char*      fileout,
    integer    *nlam_,        // I- NUMBER OF WAVELENGTH BANDS (NLAM)
    integer    *nlai_,        // I- CANOPY LAYERS (NLAI)
    integer    *ncomp_,       // I- NUMBER OF CANOPY COMPONENTS (NCOMP)
    real       li_[9],       // I- LEAF INCLINATION ANGLE INTERVALS (LI)
    real       lang_[90],	// I- LEAF ANGLE DISTRIBUTION (LANG)
    real       refl_[150],   // I- SPECTRAL REFLECTANCE (REFL)
    real       trm_[150],    // I- SPECTRAL TRANSMITTANCE (TRM)
    real       rsoil_[10],	// I- SOIL SPECTRAL REFLECTANCE (RSOIL)
    real       *tts_,         // I- THE SOLAR ZENITH ANGLE (TTS)
    real       lai_[75],     // I- CROWN LAI (LAI)
    char*      cshp_,        // I- CROWN SHAPE (CSHP)
    real       *chw_,         // I- HEIGHT TO WIDTH RATIO OF THE CROWNS (CHW)
    integer    *nval_,        // I- NUMBER OF DIFFERENT CANOPY COVERAGES TO CALCULATE (NVAL)
    real*      *ccovers_       // I- CANOPY COVERAGE (CCOVER)  ***NOTE:  nval values
);

///////////////////////////////////////////////////////////////////////////////////
int write_input(
    char*      fileout,
    integer    nlam_,        // I- NUMBER OF WAVELENGTH BANDS (NLAM)
    integer    nlai_,        // I- CANOPY LAYERS (NLAI)
    integer    ncomp_,       // I- NUMBER OF CANOPY COMPONENTS (NCOMP)
    real       li_[9],       // I- LEAF INCLINATION ANGLE INTERVALS (LI)
    real       lang_[90],    // I- LEAF ANGLE DISTRIBUTION (LANG)
    real       refl_[150],   // I- SPECTRAL REFLECTANCE (REFL)
    real       trm_[150],    // I- SPECTRAL TRANSMITTANCE (TRM)
    real       rsoil_[10],   // I- SOIL SPECTRAL REFLECTANCE (RSOIL)
    real       tts_,         // I- THE SOLAR ZENITH ANGLE (TTS)
    real       lai_[75],     // I- CROWN LAI (LAI)
    char*      cshp_,       // I- CROWN SHAPE (CSHP)
    real       chw_,         // I- HEIGHT TO WIDTH RATIO OF THE CROWNS (CHW)
    integer    nval_,        // I- NUMBER OF DIFFERENT CANOPY COVERAGES TO CALCULATE (NVAL)
    real*      ccovers_      // I- CANOPY COVERAGE (CCOVER)  ***NOTE:  nval values
);



///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets the LADF for an array of angles according to a canopy type.
 * This routine is a generalization of getLADF in ../sailh/
 * BUT doesn't multiply by 100.
 */
void getLADF2(
    int can_type,        // I- canopy type: 1, 2, 3, 4, 5 (not implemented), and 6
    int numAngles,       // I- number of angles including 0 and 90.
    float ang[],        // I- Angles. This must of the form [0, ..., 90]
    float fract[]       // O- distribution function in percentage
);



#endif    /*  __GEOSAIL_H */
