/*
/////////////////////////////////////////////////////////////////////////
//
// getLADF2
//
//      To calculate the fraction for an array of angles.
//      according to a given canopy type.
//
// Carlos Rueda - CSTARS
// August 2001
//
/////////////////////////////////////////////////////////////////////////
*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "geosail.h"


#define PI 3.141592654


//////////////////////////////////////////////////////////////////////////////////////////
/**
 * Gets a LADF for a set of angles according to a canopy type.
 */
void getLADF2(
    int can_type,        // I- canopy type: 1, 2, 3, 4, 5 (not implemented), and 6
    int numAngles,       // I- number of angles including 0 and 90.
    float ang[],        // I- Angles. This must of the form [0, ..., 90]
    float fract[]       // O- distribution function in percentage
)
{
    float fraction[128];
    float check, x1, x2;
    int a;

    check=0;
    for (a=1; a<=numAngles - 1; a++)
    {
        if (can_type==1)
            {
            x1=(ang[a-1])*PI/180;
            x2=(ang[a])*PI/180;
            fraction[a]=((2/PI)*((x2)+(0.5*sin(2*x2))))-((2/PI)*((x1)+(0.5*sin(2*x1))));
            check=check+fraction[a];
            //printf ("%f %f %f %f %f\n", x1, x2, ang[a-1], ang[a], fraction[a]);
            }
        if (can_type==2)
            {
            x1=(ang[a-1])*PI/180;
            x2=(ang[a])*PI/180;
            fraction[a]=((2/PI)*((x2)-(0.5*sin(2*x2))))-((2/PI)*((x1)-(0.5*sin(2*x1))));
            check=check+fraction[a];
            //printf ("%f %f %f %f %f\n", x1, x2, ang[a-1], ang[a], fraction[a]);
            }
        if (can_type==3)
            {
            x1=(ang[a-1])*PI/180;
            x2=(ang[a])*PI/180;
            fraction[a]=((2/PI)*((x2)-(0.25*sin(4*x2))))-((2/PI)*((x1)-(0.25*sin(4*x1))));
            check=check+fraction[a];
            //printf ("%f %f %f %f %f\n", x1, x2, ang[a-1], ang[a], fraction[a]);
            }
        if (can_type==4)
            {
            x1=(ang[a-1])*PI/180;
            x2=(ang[a])*PI/180;
            fraction[a]=((2/PI)*((x2)+(0.25*sin(4*x2))))-((2/PI)*((x1)+(0.25*sin(4*x1))));
            check=check+fraction[a];
            //printf ("%f %f %f %f %f\n", x1, x2, ang[a-1], ang[a], fraction[a]);
            }
        if (can_type==5)
            {
            fprintf(stderr, "*********** WARNING: can_type = 5 : Not implemented yet\n");
            return;
            }
        if (can_type==6)
            {
            x1=(ang[a-1])*PI/180;
            x2=(ang[a])*PI/180;
            fraction[a]= (-cos(x2))-(-cos(x1));
            check=check+fraction[a];
            //printf ("%f %f %f %f %f\n", x1, x2, ang[a-1], ang[a], fraction[a]);
            }
    }

    if ( fabs(check - 1.0) > 0.001 )
    {
        fprintf(stderr,
            "*********** WARNING: |check - 1.0| > 0.001;  check=%f, can_type=%d\n",
            check, can_type
        );
    }

    for (a=1; a<=numAngles-1; a++)
    {
        fract[a - 1] = fraction[a];
    }

}

/////////////////////////////
#ifdef INCLUDE_TEST_MAIN

#define MAX_ANGLES 1024

/////////////////////////////////////////////////////////////////////////
/**
 * A test.
 */
int main(int argc, char** argv)
{
                    // must go from 0 to 90
//    float li[] = { 0., 15., 25., 35., 45., 55., 65., 75., 90.};
//    real li[] = { 0, 10, 20, 30, 40, 50, 60, 70, 80, 82, 84, 86, 88, 90 };
    float li[MAX_ANGLES];

    float fraction[MAX_ANGLES];
    int numAngles;
    int can_type, i;
    float sum;

    if ( argc == 1 )
    {
        printf(
            "USAGE: getladf <angles> ...\n"
            "(max angles %d)\n"
            "\n"
            "EXAMPLE: getladf 0 15 25 35 45 55 65 75 90\n"
            , MAX_ANGLES
        );
        return 0;
    }

    numAngles = argc - 1;

    for ( i = 0; i < numAngles; i++ )
    {
        sscanf(argv[i + 1], "%f", &li[i]);
    }

    fprintf(stdout, "numAngles = %d:\n    Angles: ", numAngles);
    for ( i = 0; i < numAngles; i++ )
    {
        fprintf(stdout, "%8g ", li[i]);
    }
    fprintf(stdout, "\n");

    fprintf(stdout, "num Fracs = %d:\n", numAngles - 1);
    for ( can_type = 1; can_type <= 6; can_type++ )
    {
        if (can_type == 5)
        {
            //fprintf(stdout, "can_type=%d: NOT IMPLEMENTED\n", can_type);
            continue;
        }

        getLADFgeosail(
            can_type,
            numAngles,
            li,
            fraction
        );

        fprintf(stdout, "can_type=%d: %8s ", can_type, "");
        sum = 0.;
        for ( i = 0; i < numAngles-1; i++ )
        {
            fprintf(stdout, "%8f ", fraction[i]);
            sum = sum + fraction[i];
        }
        fprintf(stdout, "= %f\n", sum);
    }

    return 0;
}

/////////////////////////////
#endif //INCLUDE_TEST_MAIN

