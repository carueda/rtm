/*
/////////////////////////////////////////////////////////////////////////
//
// geosail
//
// Carlos Rueda - CSTARS
// August 2001
//
/////////////////////////////////////////////////////////////////////////
*/

#include "geosail.h"

// Maximum number for ncomp
#define MAX_NCOMP 2

// Maximum number for nlai
#define MAX_NLAI 2


///////////////////////////////////////////////////////////////////////////////////////////////////
static
int usage(char* msg)
{
    if ( msg )
    {
        fprintf(stderr, "\n%s\n", msg);
        fprintf(stderr, "Try geosail -h' for more information.\n\n");
        return 1;
    }

    fprintf(stdout,
"\n"
"CSTARS geosail 0.2 (%s %s) %s\n"
"\n"
"USAGES\n"
"  geosail -h\n"
"                 Shows this message and exits\n"
"\n"
"  geosail nlai ncomp cantype... rfl... tns... rhos tts lai... cshp chw ccover can_rfl can_frc\n"
"     nlai        Number of canopy layers (max %d)\n"
"     ncomp       Number of canopy components (max %d)\n"
"     cantype...  ncomp canopy types to generate leaf angle distribution values\n"
"                 Each type = 1(planophile)/2(erectophile)/3(plagiophile)/4(extremophile)\n"
"                             5(uniform)/6(spherical) -- type 5 not implemented\n"
"     rfl...      ncomp reflectance function files\n"
"     tns...      ncomp transmittance function files\n"
"     rhos        Soil reflectance function file\n"
"     tts         Solar zenith angle\n"
"     lai...      ncomp*nlai leaf area index values\n"
"     cshp        Crown shape = \"CYLI\" | \"CONE\"\n"
"     chw         Height-to-width ratio of crowns\n"
"     ccover      Canopy coverage\n"
"     can_rfl     Generated canopy reflectance function file\n"
"     can_frc     Generated instantaneous fraction of radiation absorved function file\n"
"\n"
"EXAMPLE:\n"
"geosail 1 2 \\\n"
"    1 \\\n"
"    6 \\\n"
"    comp1.rfl \\\n"
"    comp2.rfl \\\n"
"    comp1.tns \\\n"
"    comp2.tns \\\n"
"    soil.rfl \\\n"
"    42.27 \\\n"
"    4.90  0.1 \\\n"
"    CYLI \\\n"
"    3.5 \\\n"
"    1.0 \\\n"
"    can.rfl can.frc\n"
"(provided the input files exist in the current directory)\n"
"\n"
"All input functions are assumed to be evaluated at the same wavelengths.\n"
"\n"
"SEE ALSO:\n"
"   interpolate, intersect_abscissas\n",
               __DATE__, __TIME__,
			   RTM_VERSION_IN_USAGE,
               MAX_NLAI, MAX_NCOMP
    );


    return 0;
}



/////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{
    integer    nlai;          // I- CANOPY LAYERS (NLAI)
    integer    ncomp;         // I- NUMBER OF CANOPY COMPONENTS (NCOMP)
    real       li[9] =        // I- 9 LEAF INCLINATION ANGLE INTERVALS (LI) -- LI(9)
               {    15., 25., 35., 45., 55., 65., 75., 85., 90 };

    real       angles[10] =   // Only used to generate LADFs: NOTE that is the same
                              // as li[] but beggining with zero
               { 0, 15., 25., 35., 45., 55., 65., 75., 85., 90 };

    real       lang[90];      // I- ncomp*9 LEAF ANGLE DISTRIBUTIONS
    List       rfl_lists[MAX_NCOMP];      // I- ncomp SPECTRAL REFLECTANCES
    List       tns_lists[MAX_NCOMP];      // I- ncomp SPECTRAL TRANSMITTANCES
    List       rhos_list;     // I- SOIL SPECTRAL REFLECTANC
    real       tts;           // I- THE SOLAR ZENITH ANGLE (TTS)
    real       lai[75];       // I- ncomp*nlai CROWN LAIS (LAI)   -- LAI(15,5)  in FORTRAN
    char*      cshp;         // I- CROWN SHAPE (CSHP) "CYLI" or "CONE"
    real       chw;           // I- HEIGHT TO WIDTH RATIO OF THE CROWNS (CHW)
    real       ccover;        // I- CANOPY COVERAGE (CCOVER)

    // OUTPUTS
    List       result_rfl_list;   // O-  reflectance function
    List       result_frc_list;   // O-  instantaneous fraction of radiation absorved

    char*     can_rfl_filename;
    char*     can_frc_filename;

    int status, arg, i, k;
    char* filename;


    ////////////////////////////////////////////////////////////////////////////
    //      READ COMMAND LINE
    ////////////////////////////////////////////////////////////////////////////


    // process options:
    for ( arg = 1; arg < argc && argv[arg][0] == '-'; arg++ )
    {
        if ( strcmp(argv[arg], "-h") == 0 )
        {
            return usage(NULL);
        }
        else
        {
            return usage("Invalid option");
        }
    }

    printf("READING INPUTS...\n");

    // nlai
    if ( arg >= argc )  return usage("Expecting nlai");
    sscanf(argv[arg++], "%d", &nlai);
    if ( nlai < 1 || nlai > MAX_NLAI ) return usage("nlai invalid");
    //printf("nlai = %d\n", nlai);

    // ncomp
    if ( arg >= argc )  return usage("Expecting ncomp");
    sscanf(argv[arg++], "%d", &ncomp);
    if ( ncomp < 1 || ncomp > MAX_NCOMP ) return usage("ncomp invalid");
    //printf("ncomp = %d\n", ncomp);

    // lang...
    for (k = 0; k < ncomp; ++k)
    {
        int can_type;
        int numAngles = 10;

        if ( arg >= argc )  return usage("Expecting canopy type");
        sscanf(argv[arg++], "%d", &can_type);

        // generate corresponding LADF
        getLADF2(
            can_type,
            numAngles,
            angles,
            &lang[k * 10]
        );
    }

    // rfl...
    for ( i = 0; i < ncomp; i++ )
    {
        if ( arg >= argc )  return usage("Expecting input reflectante filename");
        filename = argv[arg++];
        rfl_lists[i] = read_function(filename);
        if ( rfl_lists[i] == NULL )
        {
            fprintf(stderr, "error reading %s\n", filename);
            return 1;
        }
        //printf("refl = %s, %ld points\n", filename, list_size(rfl_lists[i]));

    }

    // tns...
    for ( i = 0; i < ncomp; i++ )
    {
        if ( arg >= argc )  return usage("Expecting input transmittance filename");
        filename = argv[arg++];
        tns_lists[i] = read_function(filename);
        if ( tns_lists[i] == NULL )
        {
            fprintf(stderr, "error reading %s\n", filename);
            return 1;
        }
        //printf("tns = %s, %ld points\n", filename, list_size(tns_lists[i]));
    }

    if ( arg >= argc )  return usage("Expecting soil reflectance filename");
    filename = argv[arg++];
    rhos_list = read_function(filename);
    if ( rhos_list == NULL )
    {
        fprintf(stderr, "error reading %s\n", filename);
        return 1;
    }
    //printf("soil = %s, %ld points\n", filename, list_size(rhos_list));

    // tts
    if ( arg >= argc )  return usage("Expecting tts");
    sscanf(argv[arg++], "%f", &tts);
    //printf("tts = %f\n", tts);

    // lai...
    for ( i = 1; i <= nlai; i++ )
    {
        for (k = 1; k <= ncomp; ++k)
        {
            if ( arg >= argc )  return usage("Expecting lai value");
            sscanf(argv[arg++], "%f", &lai[i + k * 15 - 16]);
            //printf("lai = %f\n", lai[i + k * 15 - 16]);
        }

    }

    // cshp
    if ( arg >= argc )  return usage("Expecting cshp");
    cshp = argv[arg++];
    //printf("cshp = %s\n", cshp);

    // chw
    if ( arg >= argc )  return usage("Expecting chw");
    sscanf(argv[arg++], "%f", &chw);
    //printf("chw = %f\n", chw);

    // ccover
    if ( arg >= argc )  return usage("Expecting ccover");
    sscanf(argv[arg++], "%f", &ccover);
    //printf("ccover = %f\n", ccover);

    // can_rfl
    if ( arg >= argc )  return usage("Expecting can_rfl filename");
    can_rfl_filename = argv[arg++];
    //printf("can_rfl_filename = %s\n", can_rfl_filename);

    // can_frc
    if ( arg >= argc )  return usage("Expecting can_frc filename");
    can_frc_filename = argv[arg++];
    //printf("can_frc_filename = %s\n", can_frc_filename);


    printf("PROCESSING...\n");

    ////////////////////////////////////////////////////////////////////////////
    //      CALL THE MAIN SERVICE
    ////////////////////////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////////////////////////
    status = geosail_prm_lists(
            nlai,
            ncomp,
            li,
            lang,
            rfl_lists,
            tns_lists,
            rhos_list,
            tts,
            lai,
            cshp,
            chw,
            ccover,
            &result_rfl_list,
            &result_frc_list
    );

    if ( status != 0 )
    {
        fprintf(stderr, "geosail_prm_lists status =  %d\n", status);
        return status;
    }

    ////////////////////////////////////////////////////////////////////////////
    //      WRITE RESULTING FUNCTIONS
    ////////////////////////////////////////////////////////////////////////////

    status = writeFunction(result_rfl_list, can_rfl_filename);
    if ( status != 0 )
    {
        fprintf(stderr, "Error writing reflectance; status = %d\n", status);
        return status;
    }

    status = writeFunction(result_frc_list, can_frc_filename);
    if ( status != 0 )
    {
        fprintf(stderr, "Error writing ifra; status = %d\n", status);
        return status;
    }

    return 0;
}


