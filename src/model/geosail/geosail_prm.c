/* 
/////////////////////////////////////////////////////////////////////////
//
// geosail_prm
//
// Carlos Rueda - CSTARS
// August 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

/* ////////////////////////////////////////////////////////////////////////
 *	
 *	C Version by: f2c & Carlos Rueda (Aug/9/2001)
 *	
 * //////////////////////////////////////////////////////////////////////// */

#include "geosail.h"


/* Common Block Declarations */

union {
    struct {
	real lai[75]	/* was [15][5] */, li[9], f[675]	/* was [15][9]
		[5] */, refl[150]	/* was [10][15] */, trm[150]	/* 
		was [10][15] */, rsoil[10], emo, eso, tts, tto, psi;
    } _1;
    struct {
	real lai[75]	/* was [15][5] */, li[9], f[675]	/* was [15][9]
		[5] */, roo[150]	/* was [10][15] */, tau[150]	/* 
		was [10][15] */, roos[10], emo, eso, tts, tto, psi;
    } _2;
} sailin_;

#define sailin_1 (sailin_._1)
#define sailin_2 (sailin_._2)

struct {
    real bts[9], bto[9], flux[48]	/* was [3][16] */;
} sailot_;

#define sailot_1 sailot_

/* Table of constant values */

static integer c__1 = 1;
static real c_b153 = (float)180.;
static integer c__6 = 6;

#define lai (sailin_1.lai)
#define li (sailin_1.li)
#define f (sailin_1.f)
#define refl (sailin_1.refl)
#define trm (sailin_1.trm)
#define rsoil (sailin_1.rsoil)
#define emo (sailin_1.emo)
#define eso (sailin_1.eso)
#define tts (sailin_1.tts)
#define tto (sailin_1.tto)
#define psi (sailin_1.psi)
#define flux (sailot_1.flux)


///////////////////////////////////////////////////////////////////////////////////
// To copy from parameters to common struct
static void copy(
	real       li_[9],       // I- LEAF INCLINATION ANGLE INTERVALS (LI)
	real       refl_[150],   // I- SPECTRAL REFLECTANCE (REFL)
	real       trm_[150],    // I- SPECTRAL TRANSMITTANCE (TRM)
	real       rsoil_[10],	 // I- SOIL SPECTRAL REFLECTANCE (RSOIL)
	real       tts_,         // I- THE SOLAR ZENITH ANGLE (TTS)
	real       lai_[74]      // I- CROWN LAI (rsoil_LAI)
)
{
    int i;
    
    for ( i = 0; i < 9; i++ )
    {
        li[i] = li_[i];
    }
    for ( i = 0; i < 150; i++ )
    {
        refl[i] = refl_[i];
        trm[i] = trm_[i];
    }
    for ( i = 0; i < 10; i++ )
    {
        rsoil[i] = rsoil_[i];
    }
    tts = tts_;
    for ( i = 0; i < 74; i++ )
    {
        lai[i] = lai_[i];
    }
}


///////////////////////////////////////////////////////////////////////////////////
int geosail_prm(
	integer    nlam_,        // I- NUMBER OF WAVELENGTH BANDS (NLAM)
	integer    nlai_,        // I- CANOPY LAYERS (NLAI)
	integer    ncomp_,       // I- NUMBER OF CANOPY COMPONENTS (NCOMP)
	real       li_[9],       // I- LEAF INCLINATION ANGLE INTERVALS (LI)
	real       lang_[90],	 // I- LEAF ANGLE DISTRIBUTION (LANG)
	real       refl_[150],   // I- SPECTRAL REFLECTANCE (REFL)
	real       trm_[150],    // I- SPECTRAL TRANSMITTANCE (TRM)
	real       rsoil_[10],	 // I- SOIL SPECTRAL REFLECTANCE (RSOIL)
	real       tts_,         // I- THE SOLAR ZENITH ANGLE (TTS)
	real       lai_[74],     // I- CROWN LAI (LAI)
	char*      cshp_,       // I- CROWN SHAPE (CSHP)
	real       chw_,         // I- HEIGHT TO WIDTH RATIO OF THE CROWNS (CHW)
	real*      ccovers_,     // I- CANOPY COVERAGE (CCOVER)
                             //    ONLY ONE VALUE ASSUMED
    
    // OUTPUTS
    //  Only one output "group" corresponding to ONE canopy coverage and
    //  ONE canopy component.
    //
    real       result_refl[150],		// O- Resulting reflectance function
    real       result_frac[150]		// O- Resulting instantaneous fraction of
                                    //    radiation absorved
)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    integer s_wsfe(), e_wsfe(), s_rsfe(), do_fio(), e_rsfe(), f_open(), 
	    s_rsle(), do_lio(), e_rsle(), s_cmp();
    /* Subroutine */ int s_stop();

    /* Local variables */
    static real clai;
    static real tlai;
    extern /* Subroutine */ int sail_();
    static real gsfr[10];
    static integer i__, j, k;
    static real chref[10], crefl[10], gsref[10], ctran[10];
    static integer i1;
    static real r0;
    static integer ic;
    static real ccover;
    extern /* Subroutine */ int geocone_(), geocyli_();


    //////////////////////////////////////////
    // Value fixed:
	const integer    nval_ = 1;    // ONLY ONE COVERAGE TO CALCULATE
        // NOTE: the code below makes loops using nval_: no attempt was
        // made to modify this.


    //////////////////////////////////////////////////////////////
    // copy to common struct
    copy(
       li_,
       refl_,
       trm_,
       rsoil_,
       tts_,
       lai_
    );

    i__1 = ncomp_;
    for (j = 1; j <= i__1; ++j) {
        i__2 = nlam_;
        for (i__ = 1; i__ <= i__2; ++i__) {
            if (refl[i__ + j * 10 - 11] + trm[i__ + j * 10 - 11] >= (float)1.)
                 {
                     trm[i__ + j * 10 - 11] = (float).9995 - refl[i__ + j * 10 - 
                        11];
            }
            //fprintf(stdout, "%d  %d  %g  %g\n", j, i__, refl[i__ + j * 10 - 11], trm[i__ + j * 10 - 11]);
        }
    }

    tto = (float)0.;
    psi = (float)0.;
    eso = (float)1.;
    emo = 1 - eso;
    
    clai = (float)0.;
    i__2 = ncomp_;
    for (k = 1; k <= i__2; ++k) {
        i__1 = nlai_;
        for (i1 = 1; i1 <= i__1; ++i1) {
            lai[i1 + k * 15 - 16] = lai_[i1 + k * 15 - 16];
            clai += lai[i1 + k * 15 - 16];
        }
    }

    i__2 = nlai_;
    for (j = 1; j <= i__2; ++j) {
        i__1 = ncomp_;
        for (k = 1; k <= i__1; ++k) {
            for (i__ = 1; i__ <= 9; ++i__) {
                f[j + (i__ + k * 9) * 15 - 151] = lang_[i__ + k * 9 - 10];
            }
        }
    }
    
/*
    fprintf(stdout, "SAIL MODEL OUTPUT\n");
    fprintf(stdout, "TOTAL CROWN LAI:  %g\n", clai);
    fprintf(stdout, "BAND  REFL  TRANS    HEMI REFL\n");
*/

    i__2 = nlam_;
    for (i__ = 1; i__ <= i__2; ++i__) {
        sail_(&nlai_, &ncomp_, &i__, &r0);
        crefl[i__ - 1] = r0;
        chref[i__ - 1] = flux[0];
        ctran[i__ - 1] = flux[(nlai_ + 1) * 3 - 2] + flux[(nlai_ + 1) * 3 - 1];
    
        //fprintf(stdout, "%d   %g  %g  %g\n", i__, crefl[i__ - 1], ctran[i__ - 1]
        //, chref[i__ - 1]);
    }

    //fprintf(stdout, "GEOSAIL OUTPUT\n");

/*    if (s_cmp(cshp_, "CYLI", (ftnlen)4, (ftnlen)4) == 0) {
        fprintf(stdout, "CYLINDRICAL CROWNS      HEIGHT/WIDTH= %g\n", chw_);
    }
    else if (s_cmp(cshp_, "CONE", (ftnlen)4, (ftnlen)4) == 0) {
        fprintf(stdout, "CONIC CROWNS     HEIGHT/WIDTH= %g\n", chw_);
    }

    fprintf(stdout, "CANOPY COVER   TOTAL LAI   BAND     REFL   FRAC ABS\n");
*/
    i__2 = nval_;
    for (ic = 1; ic <= i__2; ++ic) {
        ccover = ccovers_[ic - 1];	// <<<<<<<<<<
        tlai = ccover * clai;
        if (s_cmp(cshp_, "CYLI", (ftnlen)4, (ftnlen)4) == 0) {
            i__1 = nlam_;
            for (i__ = 1; i__ <= i__1; ++i__) {
                geocyli_(&chw_, &ccover, &tts, &crefl[i__ - 1], &ctran[i__ - 1]
                    , &chref[i__ - 1], &rsoil[i__ - 1], &gsref[i__ - 1], &
                    gsfr[i__ - 1]);
        /* L500: */
            }
        }
        else if (s_cmp(cshp_, "CONE", (ftnlen)4, (ftnlen)4) == 0) {
            i__1 = nlam_;
            for (i__ = 1; i__ <= i__1; ++i__) {
                geocone_(&chw_, &ccover, &tts, &crefl[i__ - 1], &ctran[i__ - 1]
                    , &chref[i__ - 1], &rsoil[i__ - 1], &gsref[i__ - 1], &
                    gsfr[i__ - 1]);
            }
        }
        i__1 = nlam_;
        for (i__ = 1; i__ <= i__1; ++i__) 
        {
            /*
            fprintf(stdout, "%6g  %6g  %6d  %6g   %6g\n", 
                ccover,
                tlai,
                i__,
                gsref[i__ - 1],
                gsfr[i__ - 1]
            );
            */
            result_refl[i__ -1] =  gsref[i__ - 1];
            result_frac[i__ -1] =  gsfr[i__ - 1];
        }
    }
    
    
    // s_stop("", (ftnlen)0);
  
    return 0;
}

#undef lai
#undef li
#undef f
#undef refl
#undef trm
#undef rsoil
#undef emo
#undef eso
#undef tts
#undef tto
#undef psi
#undef flux


/* Subroutine */ int geocyli_(chw, ccover, sza, rc, tc, rch, rsoil, rsc, gsfr)
real *chw, *ccover, *sza, *rc, *tc, *rch, *rsoil, *rsc, *gsfr;
{
    /* System generated locals */
    doublereal d__1, d__2;

    /* Builtin functions */
    double tan(), pow_dd();

    /* Local variables */
    static real rdeg, rssh, sfrac, e1;
    static integer id;
    static real difint, ilsoil, eta;


/*  THIS SUBROUTINE CALCULATES THE REFLECTANCE AND ABSORPTION */
/*    FOR A SCENE CONSISTING OF UNIFORM CYLINDERS RANDOMLY */
/*    DISTRIBUTED OVER A PLANE */
/*  INPUT VARIABLES: */
/*    CHW - CANOPY CROWN HEIGHT TO WIDTH RATIO */
/*    CCOVER - FRACTION OF CANOPY COVER */
/*    SZA - SOLAR ZENITH ANGLE */
/*    RC - NADIR VIEW REFLECTANCE OF ILLUMINATED CROWN (FROM SAIL) */
/*    TC - TRANSMITTANCE THROUGH CROWN (FROM SAIL) */
/*    RCH - HEMISPHERIC REFLECTANCE OF ILLUMINATED CROWN (FROM SAIL) */
/*    RSOIL - BACKGROUND REFLECTANCE */
/*  OUTPUT VARIABLES: */
/*    RSC - SCENE REFLECTANCE */
/*    GSFR - FRACTION OF RADIATION ABSORBED BY CANOPY, IF INPUT */
/*      VARIABLES ARE FOR PAR WAVELENGTHS, THIS IS FAPAR */

/*  CALCULATE ETA, THE RATIO OF CANOPY AREA TO SHADOW AREA FOR AN */
/*    INDIVIDUAL CROWN */
/*  USING ETA CALCULATE THE FRACTION OF THE BACKGROUND THAT IS SHADOWED */
/*    (SFRAC) AND ILLUMINATED (ILSOIL).  ALSO CALCULATE THE REFLECTANCE */
/*    OF THE SHADOWED BACKGROUND (RSSH) */

    eta = *chw * tan(*sza / 180);
    d__1 = (doublereal) ((float)1. - *ccover);
    d__2 = (doublereal) (eta + (float)1.);
    sfrac = (float)1. - *ccover - pow_dd(&d__1, &d__2);
    ilsoil = (float)1. - *ccover - sfrac;
    rssh = *tc * *rsoil;

/*  CALCULATE THE SCENE REFLECTANCE (RSC) AS THE SUM OF THE COMPONENT */
/*    REFLECTANCES (ILLUMINATED CROWN, ILLUMINATED BACKGROUND, AND */
/*    SHADOWED BACKGROUND) WEIGHTED BY THEIR FRACTIONS IN THE */
/*    SCENE. */

    *rsc = *ccover * *rc + sfrac * rssh + ilsoil * *rsoil;

/*  NOW CALCULATE THE FRACTION OF RADIATION ABSORBED BY CANOPY */
/*    FIRST, CALCULATE DIFFUSE INTERCEPTANCE (DIFINT) BY TAKING */
/*    THE AVERAGE OF THE INTERCEPTANCE BETWEEN 0 AND 90 DEGREES */

    difint = (float)0.;
    for (id = 1; id <= 10; ++id) {
	rdeg = (id - (float)1.) * (float)10.;
	if (rdeg == (float)90.) {
	    rdeg = (float)89.9999;
	}
	e1 = *chw * tan(rdeg / 180);
	d__1 = (doublereal) ((float)1. - *ccover);
	d__2 = (doublereal) (e1 + (float)1.);
	difint += (float)1. - pow_dd(&d__1, &d__2);
/* L30: */
    }
    difint /= (float)10.;

/*  CALCUATE THE FRACTION OF RADIATION ABSORBED BY CANOPY */
/*    BY ADDING UP ALL THE RADIATION STREAMS INTO AND OUT */
/*    OF THE TREE CROWNS. */

    *gsfr = ((float)1. - *tc) * (*ccover + sfrac + ilsoil * *rsoil * difint + 
	    sfrac * rssh * difint) + *ccover * (rssh - *rch);
    if (*gsfr > (float)1.) {
	*gsfr = (float)1.;
    }

    return 0;
} /* geocyli_ */

/* Subroutine */ int geocone_(chw, ccover, sza, rc, tc, rch, rsoil, rsc, gsfr)
real *chw, *ccover, *sza, *rc, *tc, *rch, *rsoil, *rsc, *gsfr;
{
    /* System generated locals */
    doublereal d__1, d__2;

    /* Builtin functions */
    double atan(), tan(), acos(), pow_dd();

    /* Local variables */
    static real beta, rdeg, fcsh, rcsh, rssh, caspa, sfrac, e1;
    static integer id;
    static real pi, difint, ilsoil, eta;

    pi = (float)3.14159;

/*  THIS SUBROUTINE CALCULATES THE REFLECTANCE AND ABSORPTION */
/*    FOR A SCENE CONSISTING OF UNIFORM CONES RANDOMLY */
/*    DISTRIBUTED OVER A PLANE */

/* ***************** */
/*  NOTE: IF THE SOLAR ZENITH ANGLE IS LESS THAN THE CONE ASPECT ANGLE */
/*    THIS SUBROUTINE WILL NOT WORK.  THAT CONDITION PRODUCES NO SHADOWS */
/*    IN THE SCENE */
/* ***************** */

/*  INPUT VARIABLES: */
/*    CHW - CANOPY CROWN HEIGHT TO WIDTH RATIO */
/*    CCOVER - FRACTION OF CANOPY COVER */
/*    SZA - SOLAR ZENITH ANGLE */
/*    RC - NADIR VIEW REFLECTANCE OF ILLUMINATED CROWN (FROM SAIL) */
/*    TC - TRANSMITTANCE THROUGH CROWN (FROM SAIL) */
/*    RCH - HEMISPHERIC REFLECTANCE OF ILLUMINATED CROWN (FROM SAIL) */
/*    RSOIL - BACKGROUND REFLECTANCE */
/*  OUTPUT VARIABLES: */
/*    RSC - SCENE REFLECTANCE */
/*    GSFR - FRACTION OF RADIATION ABSORBED BY CANOPY, IF INPUT */
/*      VARIABLES ARE FOR PAR WAVELENGTHS, THIS IS FAPAR */

/*  CALCULATE ETA, THE RATIO OF CANOPY AREA TO SHADOW AREA FOR AN */
/*    INDIVIDUAL CROWN */
/*    CASPA IS THE CONE ASPECT ANGLE */
/*    BETA IS THE ANGLE DESCIBING THE LOCATION OF THE SHADOW EDGE ON CONE */
/*    FCSH IS THE FRACTION OF THE CROWN IN SHADOW */
/*    RCSH IS THE REFLECTANCE OF THE SHADOWED CROWN, IT IS THE PRODUCT */
/*      OF THE CROWN REFLECTANCE AND CROWN TRANSMITTANCE */
/*  USING ETA CALCULATE THE FRACTION OF THE BACKGROUND THAT IS SHADOWED */
/*    (SFRAC) AND ILLUMINATED (ILSOIL).  ALSO CALCULATE THE REFLECTANCE */
/*    OF THE SHADOWED BACKGROUND (RSSH) AS THE PRODUCT OF THE CROWN */
/*    TRANSMITTANCE AND SOIL REFLECTANCE. */

    caspa = atan((float)1. / (*chw * (float)2.));
    beta = acos(tan(caspa) / tan(*sza / 180));
    eta = (tan(beta) - beta) / pi;
    fcsh = beta / pi;
    d__1 = (doublereal) ((float)1. - *ccover);
    d__2 = (doublereal) (eta + (float)1.);
    sfrac = (float)1. - *ccover - pow_dd(&d__1, &d__2);
    ilsoil = (float)1. - *ccover - sfrac;
    rcsh = *tc * *rc;
    rssh = *tc * *rsoil;

/*  CALCULATE THE SCENE REFLECTANCE (RSC) AS THE SUM OF THE COMPONENT */
/*    REFLECTANCES (ILLUMINATED CROWN, ILLUMINATED BACKGROUND, */
/*    SHADOWED CROWN, AND SHADOWED BACKGROUND) WEIGHTED BY THEIR */
/*    FRACTIONS IN THE SCENE. */

    *rsc = *ccover * ((float)1. - fcsh) * *rc + *ccover * fcsh * rcsh + sfrac 
	    * rssh + ilsoil * *rsoil;

/*  NOW CALCULATE THE FRACTION OF RADIATION ABSORBED BY CANOPY */
/*    FIRST, CALCULATE DIFFUSE INTERCEPTANCE (DIFINT) BY TAKING */
/*    THE AVERAGE OF THE INTERCEPTANCE BETWEEN 0 AND 90 DEGREES */

    difint = (float)0.;
    for (id = 1; id <= 10; ++id) {
	rdeg = (id - (float)1.) * (float)10.;
	if (rdeg == (float)90.) {
	    rdeg = (float)89.9999;
	}
	e1 = *chw * tan(rdeg / 180);
	d__1 = (doublereal) ((float)1. - *ccover);
	d__2 = (doublereal) (e1 + (float)1.);
	difint += (float)1. - pow_dd(&d__1, &d__2);
/* L30: */
    }
    difint /= (float)10.;

/*  CALCUATE THE FRACTION OF RADIATION ABSORBED BY CANOPY */
/*    BY ADDING UP ALL THE RADIATION STREAMS INTO AND OUT */
/*    OF THE TREE CROWNS. */

    *gsfr = ((float)1. - *tc) * (*ccover + sfrac + ilsoil * *rsoil * difint + 
	    sfrac * rssh * difint) + *ccover * (rssh - *rch);
    if (*gsfr > (float)1.) {
	*gsfr = (float)1.;
    }

    return 0;
} /* geocone_ */
#define lai (sailin_2.lai)
#define li (sailin_2.li)
#define f (sailin_2.f)
#define roo (sailin_2.roo)
#define tau (sailin_2.tau)
#define roos (sailin_2.roos)
#define emo (sailin_2.emo)
#define eso (sailin_2.eso)
#define tts (sailin_2.tts)
#define tto (sailin_2.tto)
#define psi (sailin_2.psi)
#define bts (sailot_1.bts)
#define bto (sailot_1.bto)
#define flux (sailot_1.flux)

/* Subroutine */ int sail_(n, nc, mm, r__)
integer *n, *nc, *mm;
real *r__;
{
    /* Format strings */
    static char fmt_2500[] = "(///,\002EIGENVALUES NOT UNIQUE IN LAYER\002,1\
x,i2)";

    /* System generated locals */
    integer i__1, i__2;
    real r__1, r__2;

    /* Builtin functions */
    double atan(), cos(), tan(), sin(), acos(), sqrt(), exp();
    integer s_wsfe(), do_fio(), e_wsfe();

    /* Local variables */
    static real pili, pils, psir, ttlr, a, b, c__, g[75]	/* was [15][5]
	     */;
    static integer i__, j;
    static real m[15], s, w[15], y, spili, a0, b0, c0, costl[9], tanto, a2, 
	    b2, sintl[9], tants;
    extern /* Subroutine */ int mmult_();
    static real t1, t2, s2, x2, cos2tl[9], bf, sin2tl;
    static integer ic;
    static real mb[9]	/* was [3][3] */, ek, em, gk, gm, sb, mi[9]	/* 
	    was [3][3] */, ub[15], sf, ko[75]	/* was [15][5] */, uf[15], pi,
	     ks[75]	/* was [15][5] */, wh[45]	/* was [9][5] */;
    static integer il;
    static real mt[144]	/* was [3][3][16] */, tm, ep, gp, realbs, xs, wv[45]	
	    /* was [9][5] */, problo, cospsi, bt1, bt2, bt3, xi1[15], xi2[15],
	     wv1, wv2, cbo, cbs, amm, ekl, eml, emm, apm, epm, koi, sig[15], 
	    esm, ksi, att[15], tko[15], tks[15], ttl;
    static integer lay;
    static real rtp[5], epl, bti1, bti2, pil0;

    /* Fortran I/O blocks */
    static cilist io___164 = { 0, 6, 0, fmt_2500, 0 };


/* NAME */
/* ---- */
/* SUBROUTINE SAIL */

/* HISTORY */
/* ------- */
/* W. VERHOFF        BUNNIK,NETHERLANDS                   ORIGINAL CODE */
/* J A HOLLIDAY       LEMSCO                  1/25/83           ? */
/* L ALEXANDER        LEMSCO            4/83     MODIFIED TO ACCOUNT FOR */
/*                                               CANOPY LAYERS WITH */
/*                                               UNIQUE LAI AND LIDF */
/*                                      5/83     MODIFIED TO ACCOUNT FOR */
/*                                               DIFFERENT COMPONENTS IN */
/*                                               EACH LAYER. */
/* K. HUEMMRICH      UMD                         MODIFIED TO OUTPUT */
/*                                               VALUES FOR 3 FLUX */
/*                                               STREAMS AND TO ACCEPT */
/*                                               ARRAYS FOR COMPONENT */
/*                                               REFLECTANCE AND */
/*                                               TRANSMITTANCE AND SOIL */
/*                                               REFLECTANCE */

/* REFERENCES */
/* ---------- */
/* N.J.J. BUNNIK.  THE MULTISPECTRAL REFLECTANCE OF SHORTWAVE */
/*        RADIATION BY AGRICULSTURAL CROPS IN RELATION WITH */
/*        THEIR MORPHOLOGICAL AND OPTICAL PROPERTIES.  DEPT. */
/*        OF THEORETICAL PRODUCTION ECOLOGY, AGRIC UNIV., */
/*        WAGENINGEN, THE NETHERLANDS. MEDEDELINGEN LANDBOUW- */
/*        HOGESCHOOL, 78-1, (1978) 175 PP. */

/* N.J.J. BUNNIK; W. VERHOF.  THE SPECTRAL DIRECTIONAL REFLECTANCE OF */
/*        AGRICULTURAL CROPS. NIWARS PUBLIC. NO. 23, OCT. 1974 */

/* CHANCE AND CANTU.  A STUDY OF PLANT CANOPY REFLECTANCE MODELS. */
/*        UNPUBLISHED PAPER FROM PAN ANERICAN UNIVERSITY, 1975. */

/* PURPOSE */
/* ------- */
/* CALCULATES CANOPY REFLECTANCE USING MODIFIED SUITS MODEL */

/* EXCEPTIONS */
/* ---------- */
/*     PROGRAM WILL END WITHOUT CALCULATING ANY REFLECTANCES IF THE */
/*     3 EIGENVALUES (M, -M, AND KS) ARE NOT UNIQUE */

/* PARAMETERS */
/* ---------- */

/*     REAL F(I,J,K) - PERCENT OF CANOPY AT EACH LEAF INCLINATION ANGLE */
/*                     EACH LAYER OF THE CANOPY,I, AND FOR EACH COMPONENT */
/*                     IN EACH LAYER, K. */

/*     REAL ROO(K)  - RHO LEAF REFLECTANCE FOR EACH CANOPY COMPONENT */

/*     REAL TAU(K)  - TRANSMITTANCE FOR EACH CANOPY COMPONENT */

/*     REAL ROOS - RHO SOIL REFLECTANCE */

/*     REAL EMO  - FRACTION OF SKY IRRADIANCE */

/*     REAL ESO  - 1-EMO */

/*     REAL TTS  - SUN ZENITH ANGLE */

/*     REAL PSI  - RELATIVE AZIMUTH ANGLE BETWEEN SUN AND LEAF */

/*     REAL TTO  - VIEW ZENITH ANGLE */


/* EXTERNAL REFERENCES */
/* -------- ---------- */

/*       MMULT FORTRAN */

/* PARAMETERS */
/* ---------- */

/*  INTEGER N,NC */
/*      N=NUMBER OF LAYERS,  NC=NUMBER OF COMPONENTS IN EACH LAYER */


/* LOCAL DECLARATIONS */
/* ----- ------------ */

/*         AVERAGE OF ROO AND TAU--USED IN CALCULATIONS */

/*             FOR EACH LAYER: */
/*             EXTINCTION COEFFICIENT FOR RADIANCE FROM SUN */
/*             ALSO EIGENVALUE TO MATRIX SOLUTION FOR FLUX VECTOR */

/*            EXTINCTION COEFFICIENT FOR RADIANCE TO OBSERVER */
/*            FOR EACH LAYER */

/*            KS FOR LEAF ANGLE IL, LAYER LAY, & COMPONENT IC */

/*            KO FOR LEAF ANGLE IL, LAYER LAY, & COMPONENT IC */

/*            TOTAL KS, KO FOR EACH LAYER */

/*             EIGENVALUE OF MATRIX SOLUTION FOR FLUX VECTOR, */
/*             (E+,E-,ES) FOR EACH LAYER */

/*            PART OF MATRIX USED TO SOLVE FOR FLUX VECTOR */
/*            THAT IS DEPENDENT ON BOUNDARY CONDITIONS */

/*             FUNCTION USED IN CALCULATING SOLUTION FOR */
/*             REFLECTANCE */

/*            UPWARD ABSORPTION COEFFICIENT FOR EACH LAYER */

/*             SCATTERING COEFFICIENT */

/*            BACK-, FORWARD SCATTER COEFFICIENT OF DIRECT FLUX */

/*            WEIGHTING FACTORS IN EQUATION FOR REFLECTANCE */

/*        SUM OVER ALL LEAF INCLINATION ANGLES OF F(IL,LAY,IC)*COSTL(IL) */

/*         USED IN CALCULATION OF COEFFICIENTS */

/*         HORIZONTAL AND VERTICAL WEIGHTING FACTOR FOR BIDIRECT. FLUX */

/*         FLUX VECTOR AT BOTTOM OF EACH LAYER */

/*          REFLECTANCE FROM THE I-TH LAYER AND FROM THE SOIL, THE SUM OF */
/*           ALL PREVIOUS PILI, THE PROBABILITY THAT THE PILI */
/*            IS SEEN BY THE OBSERVER FROM LAYER I , & THE REFLECTANCE */

/*       COEFFICIENTS IN THE EQUATIONS FOR THE COMPONENTS OF THE */
/*       REFLECTANCE (FOR EACH LAYER) */



/* UNITS */
/* ----- */
/*    15 --   ERROR MESSAGES */

/* PROCEDURE */
/* --------- */

/* CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
/*       FUNCTIONS                   C */
/* CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
/* CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
/* GET COMMON VARIABLES IN CORRECT FORM  C */
/* CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
    pi = c_b153 * atan((float)1.) / 45;
    psir = psi * atan((float)1.) / 45;
    cospsi = cos(psir);
    tants = tan(tts * atan((float)1.) / 45);
    tanto = tan(tto * atan((float)1.) / 45);
    i__1 = *nc;
    for (ic = 1; ic <= i__1; ++ic) {
	rtp[ic - 1] = (roo[*mm + ic * 10 - 11] + tau[*mm + ic * 10 - 11]) / 2;
/* L5: */
    }
/* CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
/*                                                              C */
/*   CALCULATIONS DONE FOR 9 LEAF INCLINATION ANGLES           C */
/*                                                              C */
/* CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
    for (il = 1; il <= 9; ++il) {
	ttl = li[il - 1];
	ttlr = ttl * atan((float)1.) / 45;
	sintl[il - 1] = sin(ttlr);
	costl[il - 1] = cos(ttlr);
/* Computing 2nd power */
	r__1 = sintl[il - 1];
	sin2tl = r__1 * r__1;
/* Computing 2nd power */
	r__1 = costl[il - 1];
	cos2tl[il - 1] = r__1 * r__1;
/*       CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
/*       C                                                  C */
/*       C   EXCLUDE THE AZIMUTH AND ZENITH ANGLES FOR      C */
/*       C   WHICH REFLECTANCE AND/OR DETECTION IS NOT      C */
/*       C   PHYSICALLY POSSIBLE:                           C */
/*       C                                                  C */
/*       CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
	bts[il - 1] = pi;
	bto[il - 1] = pi;
	if (ttl < (float)90.) {
	    goto L10;
	}
	bts[il - 1] = pi / 2;
	bto[il - 1] = pi / 2;
	goto L20;
/*       CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
/*       C  IF THERE IS AN OBLIQUE ZENITH ANGLE BETWEEN THE  C */
/*       C  SUN AND THE LEAF, THEN THE AZIMUTH ANGLES OF THE C */
/*       C  LEAVES AT THAT INCLINATION MUST BE BETWEEN BTS   C */
/*       C  AND BTO IN ORDER TO HAVE THEIR REFLECTANCE DE-   C */
/*       C  TECTED BY THE OBSERVER                           C */
/*       CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
L10:
	if (ttl + tts > (float)90.) {
	    bts[il - 1] = acos(-costl[il - 1] / (sintl[il - 1] * tants));
	}
	if (ttl + tto > (float)90.) {
	    bto[il - 1] = acos(-costl[il - 1] / (sintl[il - 1] * tanto));
	}
L20:
	bti1 = (r__1 = bts[il - 1] - bto[il - 1], dabs(r__1));
	bti2 = pi * 2 - bts[il - 1] - bto[il - 1];
	if (psir >= bti1) {
	    goto L30;
	}
	bt1 = psir;
	bt2 = bti1;
	bt3 = bti2;
	goto L50;
L30:
	bt1 = bti1;
	if (psir >= bti2) {
	    goto L40;
	}
	bt2 = psir;
	bt3 = bti2;
	goto L50;
L40:
	bt2 = bti2;
	bt3 = psir;
/*      CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
/*      C  CALCULATE TERMS IN SUM THAT MAKES UP BIDIREC-   C */
/*      C  TIONAL WEIGHTING FACTOR (W)                     C */
/*      CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
L50:
	i__1 = *nc;
	for (ic = 1; ic <= i__1; ++ic) {
	    t1 = pi * roo[*mm + ic * 10 - 11] - bt2 * rtp[ic - 1] * 2;
	    t2 = rtp[ic - 1] * 2 * sin(bt2);
	    s = sin2tl * tants * tanto;
	    realbs = bts[il - 1];
	    cbs = cos(realbs);
	    cbo = cos(bto[il - 1]);
	    y = cbs * cbo;
	    wh[il + ic * 9 - 10] = t1 * 2 * cos2tl[il - 1];
	    wv2 = t2 * 2 * s;
	    if (ttl != (float)90.) {
		wv2 = t2 * 2 * cos2tl[il - 1] / y;
	    }
	    wv1 = (t1 * cospsi + t2 * cos(bt1) * cos(bt3)) * s;
	    wv[il + ic * 9 - 10] = wv1 + wv2;
/* L55: */
	}
/* L100: */
    }
/* CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
/*   INITIALIZE SUMS OVER LEAF INCLINATION ANGLES  C */
/* CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	w[i__ - 1] = (float)0.;
	i__2 = *nc;
	for (j = 1; j <= i__2; ++j) {
	    ks[i__ + j * 15 - 16] = (float)0.;
	    ko[i__ + j * 15 - 16] = (float)0.;
	    g[i__ + j * 15 - 16] = (float)0.;
/* L111: */
	}
/* L110: */
    }
    mt[0] = (float)1.;
    mt[4] = (float)1.;
    mt[8] = (float)1.;
    mt[3] = (float)0.;
    mt[6] = (float)0.;
    mt[1] = (float)0.;
    mt[7] = (float)0.;
    mt[2] = (float)0.;
    mt[5] = (float)0.;
    i__1 = *n;
    for (lay = 1; lay <= i__1; ++lay) {
/*      CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
/*      C                                                    C */
/*      C              CALCULATE W,KS,KO, AND G              C */
/*      C                                                    C */
/*      CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
	for (il = 1; il <= 9; ++il) {
	    ksi = 2 / pi * ((bts[il - 1] - pi / 2) * costl[il - 1] + sin(bts[
		    il - 1]) * tants * sintl[il - 1]);
	    koi = 2 / pi * ((bto[il - 1] - pi / 2) * costl[il - 1] + sin(bto[
		    il - 1]) * tanto * sintl[il - 1]);
	    i__2 = *nc;
	    for (ic = 1; ic <= i__2; ++ic) {
		w[lay - 1] += (wh[il + ic * 9 - 10] + wv[il + ic * 9 - 10]) * 
			lai[lay + ic * 15 - 16] * f[lay + (il + ic * 9) * 15 
			- 151] / (pi * 2);
		ks[lay + ic * 15 - 16] += ksi * lai[lay + ic * 15 - 16] * f[
			lay + (il + ic * 9) * 15 - 151];
		ko[lay + ic * 15 - 16] += koi * lai[lay + ic * 15 - 16] * f[
			lay + (il + ic * 9) * 15 - 151];
		g[lay + ic * 15 - 16] += f[lay + (il + ic * 9) * 15 - 151] * 
			cos2tl[il - 1];
/* L135: */
	    }
/* L130: */
	}
/*       CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
/*       C                                                      C */
/*       C  CALCULATE ATT,SIG,SB,SF,UB,UF,M,XI1,AND XI2         C */
/*       C                                                      C */
/*       CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
	sig[lay - 1] = (float)0.;
	att[lay - 1] = (float)0.;
	ub[lay - 1] = (float)0.;
	uf[lay - 1] = (float)0.;
	sb = (float)0.;
	sf = (float)0.;
	tks[lay - 1] = (float)0.;
	tko[lay - 1] = (float)0.;
	i__2 = *nc;
	for (ic = 1; ic <= i__2; ++ic) {
	    bf = (roo[*mm + ic * 10 - 11] - tau[*mm + ic * 10 - 11]) / 2 * 
		    lai[lay + ic * 15 - 16] * g[lay + ic * 15 - 16];
	    att[lay - 1] = att[lay - 1] + (1 - rtp[ic - 1]) * lai[lay + ic * 
		    15 - 16] + bf;
	    sig[lay - 1] = sig[lay - 1] + rtp[ic - 1] * lai[lay + ic * 15 - 
		    16] + bf;
	    sb = sb + ks[lay + ic * 15 - 16] * rtp[ic - 1] + bf;
	    sf = sf + ks[lay + ic * 15 - 16] * rtp[ic - 1] - bf;
	    ub[lay - 1] = ub[lay - 1] + ko[lay + ic * 15 - 16] * rtp[ic - 1] 
		    + bf;
	    uf[lay - 1] = uf[lay - 1] + ko[lay + ic * 15 - 16] * rtp[ic - 1] 
		    - bf;
	    tks[lay - 1] += ks[lay + ic * 15 - 16];
	    tko[lay - 1] += ko[lay + ic * 15 - 16];
/* L140: */
	}
/* Computing 2nd power */
	r__1 = att[lay - 1];
/* Computing 2nd power */
	r__2 = sig[lay - 1];
	m[lay - 1] = sqrt(r__1 * r__1 - r__2 * r__2);
	if (m[lay - 1] == (float)0. || m[lay - 1] == (r__1 = tks[lay - 1], 
		dabs(r__1))) {
	    goto L900;
	}
/* Computing 2nd power */
	r__1 = m[lay - 1];
/* Computing 2nd power */
	r__2 = tks[lay - 1];
	xi1[lay - 1] = (sf * sig[lay - 1] - sb * (tks[lay - 1] - att[lay - 1])
		) / (r__1 * r__1 - r__2 * r__2);
/* Computing 2nd power */
	r__1 = m[lay - 1];
/* Computing 2nd power */
	r__2 = tks[lay - 1];
	xi2[lay - 1] = (sb * sig[lay - 1] + sf * (tks[lay - 1] + att[lay - 1])
		) / (r__1 * r__1 - r__2 * r__2);
/*  www */
/*            WRITE (16,5556) XI1(LAY),XI2(LAY),UB(LAY),UF(LAY),SB,SF */
/*  www>> */
/* 5556        FORMAT (10X,'XI1=',E11.5,2X,'XI2=',E11.5,2X,'UB=',E11.5, */
/*    @               2X,'UF=',E11.5,2X,'SB=',E11.5,2X,'SF=',E11.5,//) */
/*        CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
/*        C                                                         C */
/*        C   THIS SECTION CALCULATES THE COEFFICIENTS IN THE EQUA- C */
/*        C   TION FOR THE FLUX VECTOR (E+,E-,ES)                   C */
/*        C                                                         C */
/*        CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
	x2 = xi2[lay - 1] * sig[lay - 1];
/* Computing 2nd power */
	r__1 = sig[lay - 1];
	s2 = r__1 * r__1;
	xs = xi1[lay - 1] * s2;
	epl = exp(-m[lay - 1]);
	eml = exp(m[lay - 1]);
	ekl = exp(-tks[lay - 1]);
	amm = att[lay - 1] - m[lay - 1];
	apm = att[lay - 1] + m[lay - 1];
	tm = m[lay - 1] * 2;
/*        CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
/*        C                                                      C */
/*        C  CALCULATE THE INVERSE MATRIX (MI), AND M-BAR (MB)   C */
/*        C  THEN MULTIPLY MATRIX MT BY NEW MI (FOR THIS LAYER)  C */
/*        C  AND THEN MULTIPLY MATRIX MT BY NEW MB (FOR NEXT     C */
/*        C  LAYER)--MT HOLDS THE LARGE MATRIX PRODUCT AS GIVEN  C */
/*        C  IN THE CHANCE AND CANTU REFERENCE.                  C */
/*        C                                                      C */
/*        CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
	mi[0] = -s2 / tm;
	mi[3] = sig[lay - 1] * apm / tm;
	mi[6] = (xs - x2 * apm) / tm;
	mi[1] = s2 / tm;
	mi[4] = -sig[lay - 1] * amm / tm;
	mi[7] = (x2 * amm - xs) / tm;
	mi[2] = (float)0.;
	mi[5] = (float)0.;
	mi[8] = (float)1.;
	mmult_(mi, mt, &lay, &lay, &c__6);
	mb[0] = epl / apm;
	mb[3] = eml / amm;
	mb[6] = xi1[lay - 1] * ekl;
	mb[1] = epl / sig[lay - 1];
	mb[4] = eml / sig[lay - 1];
	mb[7] = xi2[lay - 1] * ekl;
	mb[2] = (float)0.;
	mb[5] = (float)0.;
	mb[8] = ekl;
	i__2 = lay + 1;
	mmult_(mb, mt, &lay, &i__2, &c__6);
/* L120: */
    }
    b0 = emo;
    c0 = eso;
    a0 = -b0 * (mt[((*n + 1) * 3 + 2) * 3 - 12] - roos[*mm - 1] * (mt[((*n + 
	    1) * 3 + 2) * 3 - 11] + mt[((*n + 1) * 3 + 2) * 3 - 10]));
    a0 -= c0 * (mt[((*n + 1) * 3 + 3) * 3 - 12] - roos[*mm - 1] * (mt[((*n + 
	    1) * 3 + 3) * 3 - 11] + mt[((*n + 1) * 3 + 3) * 3 - 10]));
    a0 /= mt[((*n + 1) * 3 + 1) * 3 - 12] - roos[*mm - 1] * (mt[((*n + 1) * 3 
	    + 1) * 3 - 11] + mt[((*n + 1) * 3 + 1) * 3 - 10]);
    flux[0] = a0;
    flux[1] = b0;
    flux[2] = c0;
    spili = (float)0.;
    problo = (float)1.;
    i__1 = *n;
    for (lay = 1; lay <= i__1; ++lay) {
	a = a0 * mt[(lay * 3 + 1) * 3 - 12] + b0 * mt[(lay * 3 + 2) * 3 - 12] 
		+ c0 * mt[(lay * 3 + 3) * 3 - 12];
	b = a0 * mt[(lay * 3 + 1) * 3 - 11] + b0 * mt[(lay * 3 + 2) * 3 - 11] 
		+ c0 * mt[(lay * 3 + 3) * 3 - 11];
	c__ = a0 * mt[(lay * 3 + 1) * 3 - 10] + b0 * mt[(lay * 3 + 2) * 3 - 
		10] + c0 * mt[(lay * 3 + 3) * 3 - 10];
/*       CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
/*       C                                            C */
/*       C  CALCULATE THE FLUX VECTOR FOR LAST LAYER  C */
/*       C  E+=EPM;  E-=EMM;  ES=ESM.  SIGNS ARE      C */
/*       C  CHANGED ON EXPONENTIALS SINCE X IS NEG.   C */
/*       C  DOWNWARD, BUT LAI ASSUMES X IS POS.       C */
/*       C                                            C */
/*       CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
	epl = exp(-m[lay - 1]);
	eml = exp(m[lay - 1]);
	ekl = exp(-tks[lay - 1]);
	amm = att[lay - 1] - m[lay - 1];
	apm = att[lay - 1] + m[lay - 1];
	epm = a * epl / apm + b * eml / amm + c__ * xi1[lay - 1] * ekl;
	emm = a * epl / sig[lay - 1] + b * eml / sig[lay - 1] + c__ * xi2[lay 
		- 1] * ekl;
	esm = c__ * ekl;
	flux[(lay + 1) * 3 - 3] = epm;
	flux[(lay + 1) * 3 - 2] = emm;
	flux[(lay + 1) * 3 - 1] = esm;
	a2 = a / apm;
	b2 = b / amm;
/*       CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
/*       C                                           C */
/*       C CALCULATE THE REFLECTANCE OF AN INFINITE- C */
/*       C  SIMAL INCREMENT OF LAYER LAY             C */
/*       C                                           C */
/*       CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
	r__1 = tko[lay - 1] + m[lay - 1];
	ep = (1 - exp(-r__1)) / r__1;
	r__1 = tko[lay - 1] - m[lay - 1];
	em = (1 - exp(-r__1)) / r__1;
	r__1 = tko[lay - 1] + tks[lay - 1];
	ek = (1 - exp(-r__1)) / r__1;
	gp = (a * ep / apm + b * em / amm + c__ * xi1[lay - 1] * ek) * uf[lay 
		- 1];
	gm = (a * ep / sig[lay - 1] + b * em / sig[lay - 1] + c__ * xi2[lay - 
		1] * ek) * ub[lay - 1];
	gk = c__ * ek * w[lay - 1];
	pili = (gp + gm + gk) * problo;
	problo *= exp(-tko[lay - 1]);
	spili += pili;
/* L200: */
    }
    pils = roos[*mm - 1] * (esm + emm) * problo;
    t1 = esm * problo;
    t2 = emm * problo;
    pil0 = spili + pils;
    *r__ = pil0;
    goto L999;
/*  www */
L900:
    s_wsfe(&io___164);
    do_fio(&c__1, (char *)&lay, (ftnlen)sizeof(integer));
    e_wsfe();
/*  www>> */
/*  www */
/*     WRITE (6,2600) TKS(LAY),M(LAY) */
/*  www>> */
/* 2600 FORMAT ('KS=',E11.5,3X,'M=',E11.5) */
L999:
    return 0;
} /* sail_ */

#undef lai
#undef li
#undef f
#undef roo
#undef tau
#undef roos
#undef emo
#undef eso
#undef tts
#undef tto
#undef psi
#undef bts
#undef bto
#undef flux


/* Subroutine */ int mmult_(m1, m2, im1, im2, n)
real *m1, *m2;
integer *im1, *im2, *n;
{
    static real hold[9]	/* was [3][3] */;
    static integer iii, jjj, kkk;

/*     MULTIPLIES TWO MATRICES:  M1 IS A 3X3XN MATRIX WHERE THE LAST */
/*      DIMENSION DENOTES A SUBSCRIPT (=IM) */
/*     CALLED BY SAIL SUBROUTINE */

    /* Parameter adjustments */
    m1 -= 4;
    m2 -= 13;

    /* Function Body */
    for (iii = 1; iii <= 3; ++iii) {
	for (jjj = 1; jjj <= 3; ++jjj) {
	    hold[iii + jjj * 3 - 4] = (float)0.;
/* L6: */
	}
/* L5: */
    }
    for (iii = 1; iii <= 3; ++iii) {
	for (jjj = 1; jjj <= 3; ++jjj) {
	    for (kkk = 1; kkk <= 3; ++kkk) {
		hold[iii + jjj * 3 - 4] += m1[iii + kkk * 3] * m2[kkk + (jjj 
			+ *im1 * 3) * 3];
/* L30: */
	    }
/* L20: */
	}
/* L10: */
    }
    for (iii = 1; iii <= 3; ++iii) {
	for (jjj = 1; jjj <= 3; ++jjj) {
	    m2[iii + (jjj + *im2 * 3) * 3] = hold[iii + jjj * 3 - 4];
/* L50: */
	}
/* L40: */
    }
    return 0;
} /* mmult_ */

