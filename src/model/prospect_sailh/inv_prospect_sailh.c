/*
/////////////////////////////////////////////////////////////////////////
//
// prospect_sailh inversion
//
// Carlos Rueda - CSTARS
//
// 04/14/03  - new option '-t' to keep track on a number of best solutions.
//
// 10/31/02  - new parameter 'cs' and initialization of PROSPECT model.
//             See prospect.h, prospect_sailh.h 
//
// April 2002 - Option -s added.
//
// June 2001
//
/////////////////////////////////////////////////////////////////////////
*/


#include <stdlib.h>     /* getenv */

#include "opt.h"

#include "prospect_sailh.h"

#include "symboltable.h"


#include <errno.h>
#include <string.h>
#include <math.h>


// For prospect model:
static char* refra_filename = "refra.txt";
static char* kab_filename   = "kab.txt";
static char* kw_filename    = "kw.txt";
static char* km_filename    = "km.txt";
static char* ks_filename    = "ks.txt";
static char* ke_filename    = "ke.txt";



/////////////////////////////////////////////////////////////////////////
#ifdef MONITOR_ERROR


static int monitor_var = -1;
static FILE* monitor_file = NULL;

static void setup_monitor_error()
{
    char* v = getenv("MONITOR_ERROR_VARIABLE");
    char* f = getenv("MONITOR_ERROR_FUNCTION");
    if ( v != NULL && f != NULL )
    {
        sscanf(v, "%d", &monitor_var);
        monitor_file = fopen(f, "w");
    }
}

static void monitor_error(OPT_RValue* input, double err)
{
    if ( monitor_file == NULL )
    {
        return;
    }

    fprintf(monitor_file, "%f , %f\n", input[monitor_var].d, err);
}

static void end_monitor_error()
{
    if ( monitor_file != NULL )
    {
        fclose(monitor_file);
    }
}


#endif // ifdef MONITOR_ERROR
/////////////////////////////////////////////////////////////////////////




static long counter;

#define num_vars 10
        // these 10 variables are:  N cab cw cm cs  hotspot_size lai ts tv psr


static  char* observed_function_filename;
static  char modeled_function_filename[1024];

static int can_type;

static List rhos_list;
static List skyl_list;

static List canopy_reflectante_list = NULL;  // initialization makes prospect_sailh_prm to allocate memory
static List observed_function_list;


// range of interest:
static  float wl_from = 0.0;
static  float wl_to = 99999.0;



/**
 * If idef_filename == NULL, then a direct RMS error is computed;
 * otherwise, an observed_st symbol table is first constructed
 * to compute an index RMS with the modeled functions.
 */
static char* idef_filename = NULL;

/**
 * meaningful to fun if idef_filename != NULL
 */
static FILE* idef_file = NULL;
static long observed_st;


// 04-08-02
// This info is to weight the output from the model on given bands:
static char* desired_bands_filename = NULL;
static int desired_size;
static BandInfo* desired_bands;
static List desired_rfl_list;  // auxiliary list to fun040802



// 04/16/03
static int no_track = 1;
static int median = 0;


// 04/14/03
static int csv = 0;

/////////////////////////////////////////////////////////////////////////
static double fun(OPT_RValue* input)
{
    int status;
    doublereal vai;
    doublereal cab;
    doublereal cw;
    doublereal cm;
    doublereal cs;

    float hotspot_size, lai, ts, tv, psr;

    double err;



    vai = input[0].d;
    cab = input[1].d;
    cw = input[2].d;
    cm = input[3].d;
    cs = input[4].d;
    hotspot_size = input[5].d;
    lai = input[6].d;
    ts = input[7].d;
    tv = input[8].d;
    psr = input[9].d;

    status = prospect_sailh_prm(
        vai,
        cab,
        cw,
        cm,
		cs,
        &rhos_list,
        &skyl_list,
        hotspot_size,
        lai,
        can_type,
        ts,
        tv,
        psr,
        &canopy_reflectante_list
    );

    if ( status != 0 )
    {
        fprintf(stderr, "status = %d\n", status);
        exit(1);
    }

    if ( counter++ % 100 == 0 )
    {
        fputc('.', stdout);
        fflush(stdout);
    }

	// 04-08-02
	if ( desired_bands_filename != NULL )
	{
		List tmp;
		
		list_setSize(desired_rfl_list, 0);
		fun040802(canopy_reflectante_list, desired_size, desired_bands, desired_rfl_list);
		tmp = canopy_reflectante_list;
		canopy_reflectante_list = desired_rfl_list;
		desired_rfl_list = tmp;
	}

    if ( idef_filename == NULL )
    {
        // get direct RMS
        err = getDirectRMS(observed_function_list, canopy_reflectante_list);
		
        //list_destroy(canopy_reflectante_list);
    }
    else
    {
        // get index RMS
        err = getIndexRMS(observed_st, idef_filename, idef_file, canopy_reflectante_list);
    }


#ifdef MONITOR_ERROR
    monitor_error(input, err);
#endif // ifdef MONITOR_ERROR


	// 04/14/03
	if ( no_track > 1 )
		opt_track(input, err);

    return err;
}


/////////////////////////////////////////////////////////////////////////
static OPT_RVariableDefinition varDefs[num_vars];

static OPT_RObject opt_robj =
{
    num_vars,
    varDefs,
    fun,
};



/////////////////////////////////////////////////////////////////////////
static
int write_report(OPT_RValue best_input[], double best_output, char* report_filename)
{
	char septr = csv ? ',' : ' '; 

    FILE* report_file;


    // open for appending:
    report_file = fopen(report_filename, "a");
    if ( report_file == NULL )
    {
        fprintf(stdout, "error opening %s\n", report_filename);
        return 1;
    }

    // WRITE ONE LINE OF INFORMATION

    // observed function filename:
    fprintf(report_file, "%s%c",
        observed_function_filename,  septr
    );

    // values:
    fprintf(report_file, "%g%c%g%c%g%c%g%c%g%c%g%c%g%c%g%c%g%c%g%c%d%c",
        best_input[0].d, septr,
        best_input[1].d, septr,
        best_input[2].d, septr,
        best_input[3].d, septr,
        best_input[4].d, septr,
        best_input[5].d, septr,
        best_input[6].d, septr,
        best_input[7].d, septr,
        best_input[8].d, septr,
        best_input[9].d, septr,
        can_type,        septr
    );

    // variable label:
    fprintf(report_file, "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s%c",
        varDefs[0].lower.d < varDefs[0].upper.d ? "N*"   : "N",
        varDefs[1].lower.d < varDefs[1].upper.d ? "cab*" : "cab",
        varDefs[2].lower.d < varDefs[2].upper.d ? "cw*"  : "cw",
        varDefs[3].lower.d < varDefs[3].upper.d ? "cm*"  : "cm",
        varDefs[4].lower.d < varDefs[4].upper.d ? "cs*"  : "cs",
        varDefs[5].lower.d < varDefs[5].upper.d ? "hot*" : "hot",
        varDefs[6].lower.d < varDefs[6].upper.d ? "lai*" : "lai",
        varDefs[7].lower.d < varDefs[7].upper.d ? "ts*"  : "ts",
        varDefs[8].lower.d < varDefs[8].upper.d ? "tv*"  : "tv",
        varDefs[9].lower.d < varDefs[9].upper.d ? "psr*" : "psr",
        "cantype", septr
    );

    // error:
    fprintf(report_file, "RMSE=%c%g%c",
        septr, best_output, septr
    );

    // error computed on...:
    if ( idef_filename != NULL )
    {
        int i;

        fprintf(report_file, "on:");

        for ( i = 0; i < st_size(observed_st); i++ )
        {
            ST_Entry entry;
            char* id;

            entry = st_entryAt(observed_st, i);
            id = entry->id;

            if ( id[0] == '@' )
            {
                continue;
            }

            if ( i > 0 )
                fprintf(report_file, ";");

            fprintf(report_file, "%s", id);
        }
        fprintf(report_file, "%c", septr);
    }
    else
    {
        fprintf(report_file, "on:range%c", septr);
    }

    // range, and number of evaluations:
    fprintf(report_file, "range=%g:%g%cevals=%c%ld%c",
        wl_from,
        wl_to,   septr,  septr,
        counter, septr
    );


    // modeled function filename:
    fprintf(report_file, "%s",
        modeled_function_filename
    );


    fprintf(report_file, "\n");

    fclose(report_file);

    return 0;
}


/////////////////////////////////////////////////////////////////////////
static
int write_best_solutions(char* report_filename, int median)
{
	int pos, no_best, status;
	OPT_RValue input[num_vars];
	double output;
	no_best = opt_tracker_getNoBest();
	
	if ( median )
	{
		pos = (no_best-1) / 2;
		output = opt_tracker_getBest(pos, input);
		status = write_report(input, output, report_filename);
		return status;
	}
	
	// else, report all:
	for ( pos = 0; pos < no_best; pos++ )
	{
		output = opt_tracker_getBest(pos, input);
		status = write_report(input, output, report_filename);
		if ( status )
			return status;
	}
	return 0;
}



/////////////////////////////////////////////////////////////////////////
static
int write_modeled_function(OPT_RValue best_input[], char* modeled_function_filename)
{
    doublereal vai;
    doublereal cab;
    doublereal cw;
    doublereal cm;
    doublereal cs;

    float hotspot_size, lai, ts, tv, psr;

    int status;



    fprintf(stdout, "creating %s\n", modeled_function_filename);

    vai = best_input[0].d;
    cab = best_input[1].d;
    cw = best_input[2].d;
    cm = best_input[3].d;
    cs = best_input[4].d;
    hotspot_size = best_input[5].d;
    lai = best_input[6].d;
    ts = best_input[7].d;
    tv = best_input[8].d;
    psr = best_input[9].d;

    status = prospect_sailh_prm(
        vai,
        cab,
        cw,
        cm,
		cs,
        &rhos_list,
        &skyl_list,
        hotspot_size,
        lai,
        can_type,
        ts,
        tv,
        psr,
        &canopy_reflectante_list
    );

    if ( status != 0 )
    {
        fprintf(stderr, "prospect_sailh_prm status = %d\n", status);
        return status;
    }

	// 04-08-02
	if ( desired_bands_filename != NULL )
	{
		List tmp;
		
		list_setSize(desired_rfl_list, 0);
		fun040802(canopy_reflectante_list, desired_size, desired_bands, desired_rfl_list);
		tmp = canopy_reflectante_list;
		canopy_reflectante_list = desired_rfl_list;
		desired_rfl_list = tmp;
	}
	
    status = writeFunction(canopy_reflectante_list, modeled_function_filename);
    if ( 0 != status )
    {
        fprintf(stderr, "Error creating function %s : %s\n", modeled_function_filename, strerror(errno));
    }
    return status;
}





///////////////////////////////////////////////////////////////////////////////////////////////////
static
int usage(char* msg)
{
    if ( msg )
    {
        fprintf(stderr, "\n%s\n", msg);
        fprintf(stderr, "Try `inv_prospect_sailh -h' for more information.\n\n");
        return 1;
    }

    fprintf(stdout,
"\n"
"CSTARS inv_prospect_sailh 0.4 (%s %s) %s\n"
"\n"
"USAGE\n"
"  inv_prospect_sailh -h\n"
"	    Shows this message and exits\n"
"\n"
"  inv_prospect_sailh [options] N cab cw cm cs \\\n"
"                     rhos_file skyl_file hotspot_size lai cantype ts tv psr \\\n"
"                     observed_file report-file\n"
"\n"
"options\n"
"   See ``prospect -h'' for options: -refra -kab -kw -km -ks -ke -s\n"
"   -m method          Search method to be used: exhaustive | binary | golden\n"
"                      By default, binary.\n"
"   -t number          Number of best solutions to be tracked. All of these will be reported\n"
"                      unless -median is given.   By default, 1. \n"
"   -median            Only the median element from the best solution will be reported.\n"
"   -i IDEF-file       Compute error based on indices defined in this file.\n"
"                      By default, direct reflectance RMS is used.\n"
"   -r modeled-rfl     Name for the the best reflectance function modeled\n"
"                      By default, it takes the name observed_file.mod.\n"
"   -R range           Range of interest, with the form:  from:to\n"
"   -csv               Report file is written in a comma-separated-values format\n"
"\n"
"Required parameters:\n"
"   N                  (*) Leaf internal structure parameter\n"
"   cab                (*) Leaf chlorophyll a+b content (micro g.cm-2)\n"
"   cw                 (*) Leaf equivalent water thickness (cm)\n"
"   cm                 (*) Leaf dry matter content (g.cm-2)\n"
"   cs                 (*) (new parameter for PROSPECT)\n"
"   rhos_file              Soil reflectance file\n"
"   skyl_file              Skyl file\n"
"   hotspot_size       (*) Hotspot size\n"
"   lai                (*) Leaf area index of layer\n"
"   cantype         =  1(planophile)/2(erectophile)/3(plagiophile)/4(extremophile)\n"
"                      5(uniform)/6(spherical)\n"
"   ts                 (*) TS\n"
"   tv                 (*) TV\n"
"   psr                (*) PSR\n"
"   observed_file      Observed reflectance data\n"
"   report-file        Report filename. A line of info is concatenated to this file.\n"
"\n"
"   (*) This argument can take the following form (with no spaces):  lower,upper,step\n"
"       i.e., three values separated by commas specifying the range of interest for the variable.\n"
"\n"
"EXAMPLE:\n"
"  inv_prospect_sailh 1.5 10,90,.5 .015 .005 0 \\\n"
"                     t0477f06_soil.norm skyl.txt 0.007 1,15,.5 3 30 0 0 \\\n"
"                     observed.txt  report1.txt\n"
"\n"
"  In this case the cab and lai variables will be searched; direct reflectance RMS will be used as\n"
"  a measure of error; the resulting function will be observed.txt.mod.\n"
"\n",
               __DATE__, __TIME__,
			   RTM_VERSION_IN_USAGE
    );

    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
    int arg;
    char* rhos_filename;
    char* skyl_filename;

    int status;

    OPT_RValue best_input[num_vars];
    double best_output;

    int method = OPT_BINARY;

    char* report_filename;



    modeled_function_filename[0] = 0;

    // process options:
    for ( arg = 1; arg < argc && argv[arg][0] == '-'; arg++ )
    {
        if ( strcmp(argv[arg], "-h") == 0 )
        {
            return usage(NULL);
        }

        else if ( strcmp(argv[arg], "-m") == 0 )
        {
            if ( arg == argc - 1 )
                return usage("Expecting argument to -m option");

            ++arg;
            if ( strcmp(argv[arg], "exhaustive") == 0 )
                method = OPT_EXHAUSTIVE;
            else if ( strcmp(argv[arg], "binary") == 0 )
                method = OPT_BINARY;
            else if ( strcmp(argv[arg], "golden") == 0 )
                method = OPT_GOLDEN;
            else
                return usage("Invalid argument to -m option");
        }
        else if ( strcmp(argv[arg], "-i") == 0 )
        {
            if ( arg == argc - 1 )
                return usage("Expecting argument to -i option");

            ++arg;
            idef_filename = argv[arg];
        }
        else if ( strcmp(argv[arg], "-r") == 0 )
        {
            if ( arg == argc - 1 )
                return usage("Expecting argument to -r option");

            ++arg;
            strcpy(modeled_function_filename, argv[arg]);
        }
        else if ( strcmp(argv[arg], "-R") == 0 )
        {
            if ( arg == argc - 1 )
                return usage("Expecting argument to -R option");

            ++arg;
            if ( 2 != sscanf(argv[arg], "%f:%f", &wl_from, &wl_to) )
                return usage("Invalid argument to -R option");
        }
		else if ( strcmp(argv[arg], "-refra") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -refra expects a filename");
			}
			refra_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-kab") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -ab expects a filename");
			}
			kab_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-kw") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -kw expects a filename");
			}
			kw_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-km") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -km expects a filename");
			}
			km_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-ks") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -ks expects a filename");
			}
			ks_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-ke") == 0 )
		{
			if ( ++arg >= argc )
			{
				return usage("Option -ke expects a filename");
			}
			ke_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-s") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting argument to -s option");
				
			++arg;
			desired_bands_filename = argv[arg];
		}
		else if ( strcmp(argv[arg], "-t") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting integer argument to -t option");
				
			++arg;
			no_track = atoi(argv[arg]);
		}
		else if ( strcmp(argv[arg], "-median") == 0 )
		{
			median = 1;
		}
		else if ( strcmp(argv[arg], "-csv") == 0 )
		{
			csv = 1;
		}
        else
        {
            return usage("Invalid option");
        }
    }


    if ( argc - arg < 15 )
    {
        return usage("Expecting arguments");
    }

    opt_scanVariableDefinition(argv[arg++], &varDefs[0]);
    opt_scanVariableDefinition(argv[arg++], &varDefs[1]);
    opt_scanVariableDefinition(argv[arg++], &varDefs[2]);
    opt_scanVariableDefinition(argv[arg++], &varDefs[3]);
    opt_scanVariableDefinition(argv[arg++], &varDefs[4]);

    rhos_filename = argv[arg++];
    skyl_filename = argv[arg++];

    opt_scanVariableDefinition(argv[arg++], &varDefs[5]);
    opt_scanVariableDefinition(argv[arg++], &varDefs[6]);

    sscanf(argv[arg++], "%d", &can_type);

    opt_scanVariableDefinition(argv[arg++], &varDefs[7]);
    opt_scanVariableDefinition(argv[arg++], &varDefs[8]);
    opt_scanVariableDefinition(argv[arg++], &varDefs[9]);

    observed_function_filename = argv[arg++];
    report_filename = argv[arg++];


	
	
	// PROSPECT model initialization:
	prospect_init_coeff_arrays(
		refra_filename,
		kab_filename,
		kw_filename,
		km_filename,
		ks_filename,
		ke_filename
	);
	
	

    if ( modeled_function_filename[0] == 0 )
    {
        // No -r option
        sprintf(modeled_function_filename, "%s.mod", observed_function_filename);
    }

	// 04-08-02
	if ( desired_bands_filename != NULL )
	{
		desired_bands = readBandsInfo(desired_bands_filename, &desired_size);
		if ( desired_bands == NULL )
		{
			fprintf(stderr, "Error reading %s\n", desired_bands_filename);
			return 10;
		}
		
		// prepare work lists:
		desired_rfl_list = list_create(sizeof(RPoint), 2000, 1000);
	}

    rhos_list = read_function(rhos_filename);
    if ( rhos_list == NULL )
    {
        fprintf(stderr, "error reading %s\n", rhos_filename);
        return 1;
    }

    skyl_list = read_function(skyl_filename);
    if ( skyl_list == NULL )
    {
        fprintf(stderr, "error reading %s\n", skyl_filename);
        return 1;
    }


    observed_function_list = read_function(observed_function_filename);
    if ( observed_function_list == NULL )
    {
        fprintf(stderr, "error reading %s\n", observed_function_filename);
        return 1;
    }

    // process range to update observed function:
    if ( wl_from != 0.0 || wl_to != 99999.0 )
    {
        // range specified.

        List tmp_list;

        // update observed_rfl_list:
        tmp_list = function_extract(observed_function_list, wl_from, wl_to);
        list_destroy(observed_function_list);
        observed_function_list = tmp_list;
    }

    if ( idef_filename != NULL )
    {
        idef_file = fopen(idef_filename, "r");
        if ( idef_file == NULL )
        {
            fprintf(stderr, "error opening index definition file %s\n", idef_filename);
            return 1;
        }

        observed_st = getIndicesFromFunction(idef_filename, idef_file, observed_function_list);
        if ( observed_st < 0L )
        {
            fprintf(stderr, "error getting indices from %s\n", observed_function_filename);
            return 1;
        }
    }


    wl_from = ((RPoint*) list_elementAt(observed_function_list, 0))->x;
    wl_to = ((RPoint*) list_elementAt(observed_function_list, list_size(observed_function_list) - 1))->x;

    fprintf(stdout, "\nCSTARS inv_prospect_sailh - Prospect-SAILH Inversion\n");
    fprintf(stdout,
        "Observed range: %g -> %g\n",
            wl_from, wl_to
    );
    fprintf(stdout, "Inverting: ");

/////////////////////////////////////////////////////////////////////////
#ifdef MONITOR_ERROR
    setup_monitor_error();
#endif // ifdef MONITOR_ERROR
/////////////////////////////////////////////////////////////////////////


	if ( no_track > 1 )
	{
		if ( (status = opt_tracker_init(num_vars, no_track)) != 0 )
		{
			fprintf(stderr, "tracker initialization returned = %d\n", status);
			return status;
		}
	}


    counter = 0;
    status = opt_minimize(&opt_robj, method, best_input, &best_output);

    if ( status == 0 )
    {
	if ( no_track > 1 )
	{
		write_best_solutions(report_filename, median);
	}
	else
	{
		write_report(best_input, best_output, report_filename);
	}
	
        write_modeled_function(best_input, modeled_function_filename);
    }
    else
    {
        fprintf(stderr, "status = %d\n", status);
    }

    
    opt_tracker_end();

    
    if ( idef_filename != NULL )
    {
        fclose(idef_file);
    }

    list_destroy(rhos_list);
    list_destroy(skyl_list);

    /////////////////////////////////////////////////////////////////////////
#ifdef MONITOR_ERROR
    end_monitor_error();
#endif // ifdef MONITOR_ERROR
/////////////////////////////////////////////////////////////////////////


	// 04-08-02
	if ( desired_bands_filename != NULL )
	{
		freeBandsInfo(desired_bands, desired_size);
		list_destroy(desired_rfl_list);
	}

	list_destroy(canopy_reflectante_list);
	
    return status;

}
