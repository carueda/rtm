prospect_sailh model
Carlos Rueda                         (see also ../README)
$Id$

04/06/05
	- new option -append-float
	
	
10/31/02
	 - new parameter 'cs' and initialization of PROSPECT model.
             See prospect.h
			 
4/9/02
	Added option -s. 

7/03/01
	Reorganization, but not change in functionality.
	
6/07/01
	New prospect_sailh program to integrate the prospect and sailh models

	USAGE
	  prospect_sailh vai cab cw cm  rhos_file skyl_file hotspot_size lai cantype ts tv psr report_file
	  
	OK