/* 
/////////////////////////////////////////////////////////////////////////
//
// prospect_sailh
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/


#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>

#include "prospect_sailh.h"



///////////////////////////////////////////////////////////////////////////////////////////////////
int prospect_sailh_prm(
	doublereal vai,
	doublereal cab,
	doublereal cw,
	doublereal cm,
	doublereal cs,			// I- "new parameter related to brown pigments" (Pablo)

	List       *rhos_list,
	List       *skyl_list,
	float      hotspot_size, 
	float      lai, 
	int        can_type,
	float      ts, 
	float      tv, 
	float      psr,
	List       *canopy_reflectante_list		// resulting function
)
{
	
	
	float fraction[14];

	List reflectance_list = NULL;   // initialization makes prospect_prm to allocate memory
	List transmittance_list = NULL; // initialization makes prospect_prm to allocate memory
	
	int status;


	status = prospect_prm(
		vai,
		cab,
		cw,
		cm,
		cs,
		&reflectance_list,
		&transmittance_list
	);
	
	if ( status != 0 )
	{
		fprintf(stderr, "prospect_prm status = %d\n", status);
		fprintf(stderr, "vai, cab, cw, cm = %f %f %f %f \n", vai, cab, cw, cm);
		return status;
	}


	getLADF(can_type, fraction);
	

	// Intersect functions (interpolating with respect to reflectance if necessary):
	status = intersect_functions(
		&reflectance_list,
		&transmittance_list,
		rhos_list,
		skyl_list
	);
	
	if ( status != 0 )
	{
		fprintf(stderr, "error intersect_functions status = %d\n", status);
		return status;
	}


	// call the main processing routine:
	status = process_sailh_2(
		reflectance_list,
		transmittance_list,
		*rhos_list,
		*skyl_list,
		hotspot_size,
		lai,
		ts,
		tv,
		psr,
		fraction,
		canopy_reflectante_list		// resulting function
	);

	list_destroy(reflectance_list);
	list_destroy(transmittance_list);
	
	return status;
}

