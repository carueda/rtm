/* 
/////////////////////////////////////////////////////////////////////////
//
// prospect_sailh
//
// Carlos Rueda - CSTARS
// June 2001
//
// 10/31/02  - new parameter 'cs'. See prospect.h
//
///////////////////////////////////////////////////////////////////////// 
*/

#ifndef __PROSPECT_SAILH_H
#define __PROSPECT_SAILH_H

#include "cstars-rtm.h"

#ifndef __LIST_H
#include "list.h"
#endif

#include "f2c.h"

#include "prospect.h"
#include "sailh.h"

#include <stdio.h>


///////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * The PROSPECT-SAILH Model
 *
 * Note about canopy_reflectante_list:
 *   If null, a new list is created internally; if not, 
 *   its size is first set to 0. Be sure the element size is sizeof(RPoint).
 */
int prospect_sailh_prm(
	doublereal vai,			// I- Leaf internal structure parameter
	doublereal cab,			// I- Leaf chlorophyll a+b content (micro g.cm-2)
	doublereal cw,			// I- Leaf equivalent water thickness (cm): Cw
	doublereal cm,			// I- Leaf dry matter content (g.cm-2): Cm
	doublereal cs,			// I- "new parameter related to brown pigments" (Pablo)

	List       *rhos_list,	// IO- Soil reflectance function (UPDATED)
	List       *skyl_list,	// IO- Skyl function (UPDATED)
	float      hotspot_size, 
	float      lai, 
	int        can_type,
	float      ts, 
	float      tv, 
	float      psr,
	List       *canopy_reflectante_list		// IO- resulting function
);



#endif	/*  __PROSPECT_SAILH_H */
