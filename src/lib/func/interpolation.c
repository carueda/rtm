/* 
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

#include "func.h"

#include <assert.h>
#include <math.h>


///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Given a range [x_from, x_to], this routine finds the indexes i_from and i_to
 * in the list such that list(i_from).x <= x_from <= x_to <= list(i_to).x .
 * Calls findIndexes.
 *
 * NEVER TESTED!!!! (as of 040802)
 *
 * Precondition: [x_from, x_to] is in the domain of the function (list).
 */
void findRangeIndexes(float x_from, float x_to, List list, long *i_from, long *i_to)
{
	long from_1, from_2;
	long to_1, to_2;
	
	findIndexes(x_from, list, &from_1, &from_2);
	findIndexes(x_to, list, &to_1, &to_2);
	
	*i_from = from_1;
	*i_to = to_2;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Finds the indexes i1 and i2 in the list such that
 * list(i1).x <= x <= list(i2).x .
 * Makes a binary search.
 *
 * Precondition: x is in the domain of the function.
 */
void findIndexes(float x, List list, long *i1, long *i2)
//
// Strategy: Binary search.
//
{
	RPoint* p;
	RPoint* q;
	long inf, sup;
	long mid;
	
	*i1 = -1L;
	
	inf = 0;
	sup = list_size(list) - 1;
	
	while ( inf < sup )
	{
		mid = (inf + sup) / 2;
		
		p = (RPoint*) list_elementAt(list, mid);
		q = (RPoint*) list_elementAt(list, mid + 1);

		if ( p->x <= x && x <= q->x )
		{
			if ( p->x == x )
			{
				*i1 = *i2 = mid;
			}
			else if ( p->x == x )
			{
				*i1 = *i2 = mid + 1;
			}
			else
			{
				*i1 = mid;
				*i2 = mid + 1;
			}
			
			return;
		}
		else if ( x < p->x )
		{
			sup = mid;
		}
		else 
		{
			assert(q->x < x);
			
			inf = mid;
		}
	}
	
	assert(0 && "cannot be reached");
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * (Pablo Zarco-Tejada)
 */
static void interpolate (float xp1, float yp1, float xp2, float yp2, float xc, float *ycp)
{
	*ycp = yp2 - (((yp2-yp1)*(xp2-xc))/(xp2-xp1));
}


///////////////////////////////////////////////////////////////////////////////////////////////////
float calculate_interpolation(float x, List in_list)
{
	long i1, i2;
	RPoint* p1;
	RPoint* p2;
	float y;
	
	findIndexes(x, in_list, &i1, &i2);
	p1 = (RPoint*) list_elementAt(in_list, i1);
	if ( i1 == i2 )
	{
		y = p1->y;
	}
	else
	{
		p2 = (RPoint*) list_elementAt(in_list, i2);
		interpolate(p1->x, p1->y, p2->x, p2->y, x, &y);
	}
	
	return y;
}

