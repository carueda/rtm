/* 
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

#include "func.h"

#include <assert.h>



///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Precondition: x_from <= x_to
 */
List function_extract(List function_list, float x_from, float x_to)
{
	long index_from, index_to;
	long size, i;
	RPoint* p;
	RPoint* q;
	List extracted_list;

	assert(x_from <= x_to);
		
	size = list_size(function_list);

	p = (RPoint*) list_elementAt(function_list, 0);
	q = (RPoint*) list_elementAt(function_list, size - 1);

	index_from = 0;
	if ( p->x <= x_from && x_from <= q->x )
	{
		long i1, i2;
		findIndexes(x_from, function_list, &i1, &i2);
		index_from = i1;
	}
	
	index_to = size - 1;
	if ( p->x <= x_to && x_to <= q->x )
	{
		long i1, i2;
		findIndexes(x_to, function_list, &i1, &i2);
		index_to = i2;
	}
	
	size = index_to - index_from + 1;

	extracted_list = list_create(sizeof(RPoint), size, 0);
	for ( i = 0; i < size; i ++ )
	{	
		p = (RPoint*) list_elementAt(function_list, index_from + i);
		list_addElement(extracted_list, p);
	}
	
	return extracted_list;
}

