/* 
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

#include "func.h"

#include "symboltable.h"
#include "see.h"


/////////////////////////////////////////////////////////////////////////
double getIndexRMS(
	long reference_st,				// reference symbol table
	char* idef_filename,			// IDEF filename
	FILE* idef_file,				// IDEF file already open
	List function_list				// function to extract indices from
)
{
	double rms;
	long function_st;
	int i, size;
	
	// get symbol table from the other function:
	function_st = getIndicesFromFunction(idef_filename, idef_file, function_list);
	if ( function_st < 0L )
	{
		fprintf(stderr, "error getting indices from modeled function\n");
		exit(1);
	}
	size = st_size(reference_st);

	// now compare indices to compute RMS:
	rms = 0.0;
	for ( i = 0; i < size; i++ )
	{
		ST_Entry reference_entry;
		ST_Entry function_entry;
		char* id;
		double dif;

		reference_entry = st_entryAt(reference_st, i);
		id = reference_entry->id;
		
		if ( id[0] == '@' )
		{
			continue;
		}
		
		function_entry = st_find(function_st, id);
		if ( function_entry == NULL )
		{
			fprintf(stderr, "warning: %s not found in function symbol table!\n", id);
			continue;
		}

		dif = *((TYPE2*) reference_entry->value) - *((TYPE2*) function_entry->value);
		
		rms += dif * dif;
	}
	
	st_destroy(function_st);
	
	return rms;
}

