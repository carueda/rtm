/* 
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// Carlos Rueda - CSTARS
// June-July 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

#include "func.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
int intersect_functions(
	List *reference_list,     // IO- Reference function
	List *one_list,           // IO- function 1
	List *two_list,           // IO- function 2
	List *three_list          // IO- function 3
)
{
	List new_reference_list;
	List new_one_list;
	List new_two_list;
	List new_three_list;
	long new_reference_size;
	int status;
	long i, ref_index_from, ref_index_to;
	

	// First, find the range to apply to all functions, with respect to reference:
	ref_index_from = 0;
	ref_index_to = list_size(*reference_list) - 1;
	
	status = update_indexes_interpolation(*reference_list, &ref_index_from, &ref_index_to, *one_list);
	if ( status != 0 )
		return status;
		
	status = update_indexes_interpolation(*reference_list, &ref_index_from, &ref_index_to, *two_list);
	if ( status != 0 )
		return status;
		
	status = update_indexes_interpolation(*reference_list, &ref_index_from, &ref_index_to, *three_list);
	if ( status != 0 )
		return status;
		
		
	// Now, interpolate:
	
	new_one_list = interpolate_function(
		*reference_list, ref_index_from, ref_index_to, *one_list
	);
	if ( new_one_list == NULL )
		return 10;

	new_two_list = interpolate_function(
		*reference_list, ref_index_from, ref_index_to, *two_list
	);
	if ( new_two_list == NULL )
		return 11;

	new_three_list = interpolate_function(
		*reference_list, ref_index_from, ref_index_to, *three_list
	);
	if ( new_three_list == NULL )
		return 12;
	
	// update reference
	new_reference_size = ref_index_to - ref_index_from + 1;
	new_reference_list = list_create(sizeof(RPoint), new_reference_size, 0);
	for ( i = ref_index_from; i <= ref_index_to; i++ )
	{
		RPoint* ref_point = (RPoint*) list_elementAt(*reference_list, i);
		list_addElement(new_reference_list, ref_point);
	}
	
	// finally, update all parameters:
	
	list_destroy(*reference_list);
	*reference_list = new_reference_list;

	list_destroy(*one_list);
	*one_list = new_one_list;

	list_destroy(*two_list);
	*two_list = new_two_list;

	list_destroy(*three_list);
	*three_list = new_three_list;
	

	return 0;
}


