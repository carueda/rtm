/* 
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

#include "func.h"

#include "see.h"
#include "symboltable.h"


static	FILE*	idef_file;

static	List function_list;

// to check if a ``@...'' identifier is valid (i.e., falls in the function domain):
static TYPE2 min_x;
static TYPE2 max_x;




//////////////////////////////////////////////////////////////////////
static
int getNextChar(SEE_Source source)
{
	return fgetc(idef_file);
}

static RSEE_Source source = {getNextChar};


/* the current symbol table */
static long st;


//////////////////////////////////////////////////////////////////////
static
int checkAssignment(SEE_ExpressionEvaluator see, char* id, int type)
{
	ST_Entry entry = st_find(st, id);
	if ( entry == NULL )
	{
		TYPE2 d = 0;
		entry = st_createEntry(id, type, &d, sizeof(d));
		st_addEntry(st, entry);
	}
	else
	{
		// OK
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////
static
int doAssignment(SEE_ExpressionEvaluator see, char* id, SEE_Expression expr)
{
	ST_Entry entry = st_find(st, id);
	if ( entry == NULL )
	{
		entry = st_createEntry(id, expr->type, &expr->value, sizeof(expr->value));
		st_addEntry(st, entry);
	}
	else
	{
		entry->type = expr->type;
		memcpy(entry->value, &expr->value, sizeof(expr->value));
		st_assignValue(entry, &expr->value, sizeof(expr->value));
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////
static
int simpleCheck(SEE_ExpressionEvaluator see, char* id)
{
	ST_Entry entry = st_find(st, id);
	if ( entry != NULL )
	{
		return entry->type;
	}
	else
	{
		TYPE2 wl = -1.0;
		int count = sscanf(id, "@%f", &wl);
		if ( count != 1 )
		{
			return -1;	// id doesn't have the form ``@wavelength''
		}
		
		if ( wl < min_x || wl > max_x )
		{
			return -1;	// wavelength undefined in function
		}
		
		return TYPE2_CODE;
	}
}


//////////////////////////////////////////////////////////////////////
static
SEE_Expression simpleEvaluate(SEE_ExpressionEvaluator see, char* id)
{
	ST_Entry entry = st_find(st, id);
	if ( entry != NULL )
	{
		return see_CreateTYPE2Expression(see, *((TYPE2*) entry->value));
	}
	else
	{
		TYPE2 x = -1.0;
		TYPE2 value;
		int count;
		
		
		count = sscanf(id, "@%f", &x);
		
		if ( count != 1 )
		{
			return NULL;	// id doesn't have the form ``@wavelength''
		}
		
		if ( x < min_x || x > max_x )
		{
			return NULL;	// f(x) undefined
		}
		
		value = calculate_interpolation(x, function_list);

		entry = st_createEntry(id, TYPE2_CODE, &value, sizeof(value));
		st_addEntry(st, entry);
		
		return see_CreateTYPE2Expression(see, value);
	}
}


/////////////////////////////////////////////////////////////////////////
long getIndicesFromFunction(
	char* idef_filename,
	FILE* idef_file_,
	List function_list_
)
{
	SEE_ExpressionEvaluator see;
	int type;
	RPoint* point;

	
	idef_file = idef_file_;

	function_list = function_list_;
	point = (RPoint*) list_elementAt(function_list, 0);
	min_x = point->x;
	point = (RPoint*) list_elementAt(function_list, list_size(function_list) - 1);
	max_x = point->x;
	

	/* init the symbol table */
	st = st_create();

	rewind(idef_file);

	see = see_Create(checkAssignment, doAssignment, simpleCheck, simpleEvaluate);
	type = see_checkAssignmentList(see, &source, 1);

	if ( type < 0 )
	{
		fprintf(stderr, "%s:%s\n", idef_filename, see_getMsgError());
		see_Destroy(see);
		st_destroyIncludingEntries(st);
		st = -1L;
	}
	else
	{
//printf("Evaluating idef file...\n");
		
		rewind(idef_file);
		type = see_doAssignmentList(see, &source);
		
		see_Destroy(see);
	}

	return st;
}

