/*
/////////////////////////////////////////////////////////////////////////
//
// function utilities - calculate_fl_peak
//
// Carlos Rueda - CSTARS
// Sept 2001
//
/////////////////////////////////////////////////////////////////////////
*/


#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>

#include "func.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
static int writeOuput(
	char* out_filename,
	char* ref_filename,
	float startwl,
	float endwl,
	int nbands,
	long ref_index_from,
	long ref_index_to,
	float deviation,
	List ref_list, List out_list, char* filename
)
{
	long size, i;
	FILE* file = fopen (filename, "w");

	if ( file == NULL )
	{
		return 1; // Error creating output file
	}

	fprintf(file,
"# this file   : %s\n"
"# ref_function: %s\n"
"# start-wl    : %g   at index: %ld\n"
"# end-wl      : %g   at index: %ld\n"
"# nbands      : %d\n"
"# RMSE        : %g\n"
"\n",
		out_filename,
		ref_filename,
		startwl, ref_index_from,
		endwl, ref_index_to,
		nbands,
		deviation
	);


	fprintf(file, "%-10s  %10s  %10s  %12s\n",
		"#waveln", "orig", "smooth", "diff"
	);
	fprintf(file, "%-10s  %10s  %10s  %12s\n",
		"-------", "----", "------", "----"
	);

	size = list_size(ref_list);
	for ( i = 0; i < size; i++ )
	{
		RPoint* p = (RPoint*) list_elementAt(ref_list, i);
		RPoint* q = (RPoint*) list_elementAt(out_list, i);
		float d = p->y - q->y;
		fprintf(file, "%-10.1f  %10f  %10f  %12e\n",
			p->x, p->y, q->y, d
		);
	}
	fclose(file);
	return 0;
}



///////////////////////////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	char* ref_filename;
	float startwl, endwl;
	int nbands;
	char  out_filename[256];

	List  ref_list;
	List  out_list;
	long ref_index_from;
	long ref_index_to;
	long i1, i2;
	float deviation;
	int arg, status;
	int processed;


	if ( argc < 5 )
	{
		fprintf(stderr,
"\n"
"CSTARS calculate_fl_peak 0.3 (%s %s)\n"
"Calculates the fluoroscense peak of a list of functions.\n"
"\n"
"USAGE\n"
"  calculate_fl_peak start-wl end-wl nbands ref_function ...\n"
"\n"
"	start-wl          Starting wavelength\n"
"	end-wl            Ending wavelength\n"
"	nbands            Number of bands\n"
"	ref_function ...  Reference functions to be smoothed\n"
"\n"
"EXAMPLE:\n"
"  calculate_fl_peak 683 689 5 1.rfl 2.rfl\n"
"  Generates two output files 1.rfl.683_689_5.flpeak and\n"
"  2.rfl.683_689_5.flpeak.\n",
			   __DATE__, __TIME__
		);
		return 1;
	}

	sscanf(argv[1], "%f", &startwl);
	sscanf(argv[2], "%f", &endwl);
	sscanf(argv[3], "%d", &nbands);

	processed = 0;
	for ( arg = 4; arg < argc; arg++ )
	{
		char hour[256];
		char* ptr;
		ref_filename = argv[arg];
		sscanf(ref_filename, "%[^.]", hour);
		while ( (ptr = strchr(hour, '_')) != NULL )
		{
			*ptr = '.';
		}

		ref_list = read_function(ref_filename);
		if ( ref_list == NULL )
		{
			fprintf(stderr, "Error reading reference function %s\n", ref_filename);
			continue;
		}
		//printf("reference function %s has %ld points\n", ref_filename, list_size(ref_list));

		findIndexes(startwl, ref_list, &i1, &i2);
		ref_index_from = i1;

		findIndexes(endwl, ref_list, &i1, &i2);
		ref_index_to = i2;

		//printf("smoothing from index %ld to index %ld\n", ref_index_from, ref_index_to);

		out_list = smooth_function(
			ref_list,
			ref_index_from,
			ref_index_to,
			nbands,
			&deviation
		);

		if ( out_list == NULL )
		{
			fprintf(stderr, "Error smoothing %s\n", ref_filename);
			continue;
		}

		sprintf(out_filename, "%s.%g_%g_%d.flpeak",
			ref_filename, startwl, endwl, nbands
		);

		//printf("writing output file %s\n", out_filename);

		status = writeOuput(
			out_filename,
			ref_filename,
			startwl,
			endwl,
			nbands,
			ref_index_from,
			ref_index_to,
			deviation,
			ref_list, out_list, out_filename
		);
		if ( status != 0 )
		{
				fprintf(stderr, "Error writing.  status = %d\n", status);
			continue;
		}

		fprintf(stdout, "%-8s %f\n", hour, deviation);

		processed++;
	}

	//printf("%d file(s) processed successfully.\n", processed);

	return processed > 0 ? 0 : 1;
}


