/* 
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/


#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>

#include "func.h"



///////////////////////////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	char* dbname_ref_function;
	char* dbname_function_in;
	char* dbname_function_out;
	
	List  ref_list;
	List  in_list;
	List  out_list;
	
	
	if ( argc < 4 )
	{
		fprintf(stderr,
"\n"
"CSTARS interpolate 0.1 (%s %s)\n"
"interpolate interpolates a function according to a reference function\n"
"such that it's evaluated at the same abscissas.\n"
"\n"
"USAGE\n"
"  interpolate reference_function function_in function_out\n"
"\n"
"	reference_function      Reference function\n"
"	function_in             Function file to be interpolated\n"
"	function_out            Resulting interpolated function file\n"
"\n"
"EXAMPLE:\n"
"  interpolate 1.rfl soil.txt interpolated_soil.txt\n"
"\n",
		       __DATE__, __TIME__
		);
		return 1;
	}
	
	dbname_ref_function = argv[1];
	dbname_function_in = argv[2];
	dbname_function_out = argv[3];


	ref_list = read_function(dbname_ref_function);
	if ( ref_list == NULL )
	{
		fprintf(stderr, "Error reading reference function\n");
		return 1;
	}
	printf("reference function has %ld points\n", list_size(ref_list));
	
	in_list = read_function(dbname_function_in);
	if ( in_list == NULL )
	{
		fprintf(stderr, "Error reading function in\n");
		return 1;
	}
	printf("function to normalize has %ld points\n", list_size(in_list));
	

	printf("interpolating...\n");

	out_list = interpolate_function(ref_list, 0, list_size(ref_list) - 1, in_list);
	if ( out_list == NULL )
	{
		printf("Error interpolating\n");
		return 1;
	}
	
	printf("writing %ld points of interpolated function %s\n", list_size(out_list), dbname_function_out);

	return writeFunction(out_list, dbname_function_out);
}

