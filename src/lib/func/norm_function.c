/*
/////////////////////////////////////////////////////////////////////////
//
// function utilities - norm_function
//
// Carlos Rueda - CSTARS
// November 2001
//
/////////////////////////////////////////////////////////////////////////
*/


#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>

#include "func.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
static int writeOuput(
	char* out_filename,
	char* ref_filename,
	float startwl,
	float endwl,
	int nbands,
	long ref_index_from,
	long ref_index_to,
	float deviation,
	List ref_list, List out_list, char* filename
)
{
	long size, i;
	FILE* file = fopen (filename, "w");

	if ( file == NULL )
	{
		return 1; // Error creating output file
	}

	fprintf(file,
"# this file   : %s\n"
"# ref_function: %s\n"
"# start-wl    : %g   at index: %ld\n"
"# end-wl      : %g   at index: %ld\n"
"# nbands      : %d\n"
"# RMSE        : %g\n"
"\n",
		out_filename,
		ref_filename,
		startwl, ref_index_from,
		endwl, ref_index_to,
		nbands,
		deviation
	);


	fprintf(file, "%-10s  %10s  %10s  %12s\n",
		"#waveln", "orig", "smooth", "diff"
	);
	fprintf(file, "%-10s  %10s  %10s  %12s\n",
		"-------", "----", "------", "----"
	);

	size = list_size(ref_list);
	for ( i = 0; i < size; i++ )
	{
		RPoint* p = (RPoint*) list_elementAt(ref_list, i);
		RPoint* q = (RPoint*) list_elementAt(out_list, i);
		float d = p->y - q->y;
		fprintf(file, "%-10.1f  %10f  %10f  %12e\n",
			p->x, p->y, q->y, d
		);
	}
	fclose(file);
	return 0;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
static
int usage(char* msg)
{
	if ( msg )
	{
		fprintf(stderr, "\n%s\n", msg);
		fprintf(stderr, "Try `norm_function -h' for more information.\n\n");
		return 1;
	}

	fprintf(stdout,
"\n"
"CSTARS norm_function 0.1 (%s %s)\n"
"Normalizes a function f[n]:  norm_function(f[n]) = f[n]/f[0]\n"
"\n"
"USAGE\n"
"  norm_function [-cols x y] in_function out_function\n"
"\n"
"   -cols x y         Columns to take as x and y in in_function\n"
"                     First column is zero (0).\n"
"   in_function       Function to be normalized\n"
"   out_function      Resulting function\n"
"\n"
"EXAMPLE:\n"
"  norm_function -cols 0 8 func1.txt func2.txt\n"
"  Generates func2.txt by normalizing func1.txt where columns 0 and 8\n"
"  are taken as abscissas and ordinates respectively.\n",
			   __DATE__, __TIME__
	);
	return 0;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	char* in_filename;
	char* out_filename;

	List  list;
	int colx = 0;
	int coly = 1;
	int arg;
	long size, i, midpoint;
	RPoint* p;
	float denominator;


	// process options:
	for ( arg = 1; arg < argc && argv[arg][0] == '-'; arg++ )
	{
		if ( strcmp(argv[arg], "-h") == 0 )
		{
			return usage(NULL);
		}
		else if ( strcmp(argv[arg], "-cols") == 0 )
		{
			if ( arg >= argc - 2 )
				return usage("Expecting argument to -cols option");

			sscanf(argv[++arg], "%d", &colx);
			sscanf(argv[++arg], "%d", &coly);
		}
		else
		{
			return usage("Invalid option");
		}
	}

	if ( arg > argc - 1 )
	{
		return usage("Expecting arguments");
	}

	in_filename = argv[arg++];
	out_filename = argv[arg++];

	list = read_function_cols(in_filename, colx, coly);
	if ( list == NULL )
	{
		fprintf(stderr, "Error reading input function %s\n", in_filename);
		return 1;
	}

	size = list_size(list);
	midpoint = size / 2;
	p = (RPoint*) list_elementAt(list, midpoint);
	denominator = p->y;
	if ( denominator != 0.0 )
	{

		for ( i = 0; i < size; i++ )
		{
			p = (RPoint*) list_elementAt(list, i);
			p->y = (p->y - denominator) / denominator;
		}
	}

	return writeFunction(list, out_filename);
}


