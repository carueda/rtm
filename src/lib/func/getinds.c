/*
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// getinds - tool to extract calculated indices from a set of functions files.
//
// Based on valindices
//
// Carlos Rueda - CSTARS
// 10/18/01
//
/////////////////////////////////////////////////////////////////////////
*/


#include "func.h"

#include "symboltable.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <math.h>


///////////////////////////////////////////////////////////////////////////////////////////////////
static
int usage(char* msg)
{
	if ( msg )
	{
		fprintf(stderr, "\n%s\n", msg);
		fprintf(stderr, "Try `getinds -h' for more information.\n\n");
		return 1;
	}

	fprintf(stdout,
"\n"
"CSTARS getinds 0.2 (%s %s)\n"
"Extracts calculated indices from a set of functions files.\n"
"\n"
"USAGE\n"
"  getinds [-o outfile] IDEF-file function_file ...\n"
"\n"
"	outfile         Output file. If not given, a bare result is written to stdout.\n"
"	IDEF-file       Index definition file\n"
"	function_file   Function file\n"
"\n"
"EXAMPLE:\n"
"  getinds -o output.txt WI.idef 1.rfl 2.rfl 3.rfl\n"
"    Gets the water index for the given reflectance functions\n"
"    (assuming WI.idef contains: ``WI = @858.5 / @1240;'').\n"
"    A report file is written out to output.txt.\n"
"\n",
			   __DATE__, __TIME__
		);
		return 0;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	char* function_filename;
	char* out_filename = NULL;
	char* idef_filename;
	List function_list;
	FILE* idef_file;
	FILE* out_file = NULL;
	long indices_st;
	int files_tot, files_ok;
	int arg, k;
	int header_put;
	int max_filename_length = 0;
	const int ID_LEN = 8;
	ST_Entry ste;

	// process options:
	for ( arg = 1; arg < argc && argv[arg][0] == '-'; arg++ )
	{
		if ( strcmp(argv[arg], "-h") == 0 )
		{
			return usage(NULL);
		}
		else if ( strcmp(argv[arg], "-o") == 0 )
		{
			if ( arg == argc - 1 )
				return usage("Expecting argument to -o option");

			++arg;
			out_filename = argv[arg];
		}
		else
		{
			return usage("Invalid option");
		}
	}


	if ( argc - arg < 2 )
	{
		return usage("Expecting arguments");
	}


	idef_filename = argv[arg++];

	idef_file = fopen(idef_filename, "r");
	if ( idef_file == NULL )
	{
		fprintf(stderr, "error opening index definition file %s\n", idef_filename);
		return 1;
	}

	if ( out_filename != NULL )
	{
		// check that out_filename doesn't exist (to prevent overwriting a function file):
		if ( NULL != (out_file = fopen(out_filename, "r")) )
		{
			fprintf(stderr, "Output file %s exists. I don't want to overwrite it!\n", out_filename);
			fclose(out_file);
			return 1;
		}

		out_file = fopen(out_filename, "w");
		if ( out_file == NULL )
		{
			fclose(idef_file);
			fprintf(stderr, "error creating output file %s\n", out_filename);
			return 1;
		}

		// get max_filename_length to help formatting the output file
		max_filename_length = 0;
		for ( arg = 3; arg < argc; arg++ )
		{
			int len;
			function_filename = argv[arg];
			len = strlen(function_filename);
			if ( max_filename_length < len )
			{
				max_filename_length = len;
			}
		}


		fprintf(out_file, "# Extraction of indices defined in %s\n", idef_filename);
	}

	files_tot = files_ok = 0;
	header_put = 0;
	for ( ; arg < argc; arg++ )
	{
		files_tot++;

		function_filename = argv[arg];

		function_list = read_function(function_filename);
		if ( function_list == NULL )
		{
			fprintf(stderr, "error reading %s\n", function_filename);
			continue;
		}

		fseek(idef_file, 0L, SEEK_SET);
		indices_st = getIndicesFromFunction(idef_filename, idef_file, function_list);
		if ( indices_st < 0L )
		{
			fprintf(stderr, "error getting indices from %s\n", function_filename);
			fclose(idef_file);
			list_destroy(function_list);
			continue;
		}

		if ( out_filename != NULL )
		{
			if ( !header_put )
			{
				fprintf(out_file, "# %*s  ", max_filename_length -2, "FILENAME");
				for ( k = 0; k < st_size(indices_st); k++ )
				{
					ste = st_entryAt(indices_st, k);
					fprintf(out_file, " %*s", ID_LEN, ste->id);
				}
				fprintf(out_file, "\n");
				header_put = 1;
			}

			fprintf(out_file, "%*s  ", max_filename_length, function_filename);
		}

		if ( out_filename != NULL )
		{
			for ( k = 0; k < st_size(indices_st); k++ )
			{
				ste = st_entryAt(indices_st, k);

				fprintf(out_file, " %*f", ID_LEN, *((float*)ste->value));
			}
			fprintf(out_file, "\n");
		}
		else
		{
			for ( k = 0; k < st_size(indices_st); k++ )
			{
				ste = st_entryAt(indices_st, k);
				fprintf(stdout, " %f", *((float*)ste->value));
			}
			fprintf(stdout, "\n");
		}


		files_ok++;

		st_destroyIncludingEntries(indices_st);
		list_destroy(function_list);
	}

	if ( out_filename != NULL )
	{
		fclose(out_file);
		fprintf(stdout,
			"\nProcessed %d file(s) successfully from a total of %d.\n",
			files_ok, files_tot
		);
	}

	fclose(idef_file);


	return 0;
}
