/*
/////////////////////////////////////////////////////////////////////////
//
// func - Miscellaneous services on univariate functions.
//
// Carlos Rueda - CSTARS
// June-July 2001
// Sept 2001 - Added smooth_function.
// April 2002 - Added fun040802, readBandsInfo, freeBandsInfo
//
/////////////////////////////////////////////////////////////////////////
*/

#ifndef __FUNC_H
#define __FUNC_H

#ifndef __RPOINT_H
#include "rpoint.h"
#endif

#ifndef __LIST_H
#include "list.h"
#endif

#include <stdio.h>

//////////////////////////////////////////////////////////////////////
// Information for a specific band.
typedef struct BandInfo
{
	float center;      // the center of the band
	float bandwidth;   // the band goes from center-bandwidth/2 to center+bandwidth/2 
	
	// response (this works like a weighting window)
	int response_size;           // length of following arrays
	float* response_wavelengths; // array of wavelengths. center must lie within
	float* response_weights;     // weights to a function
	
} BandInfo;

//////////////////////////////////////////////////////////////////////
/**
 * Maps a given function on desired bands. 
 *
 * Returns the resulting list.
 */
List fun040802(
	List in_list,              // the given function
	int desired_size,          // length of next array
	BandInfo* desired_bands,   // array of desired bands.
	List out_list              // output list.
	                           // If null, a new list is created internally;
							   // if not, just adds point to it; be sure this
							   // list element size is sizeof(RPoint).
							   // This list is returned.
);

/////////////////////////////////////////////////////////////////////////
/**
 * Reads a band-info file.
 * Allocates and returns an array of BandInfo elements.
 * See freeBandsInfo().
 */
BandInfo* readBandsInfo(char* bands_filename, int *size);


/////////////////////////////////////////////////////////////////////////
/**
 * Frees the memory allocated for an array of DandInfo elements.
 * See readBandsInfo().
 */
void freeBandsInfo(BandInfo* bands, int size);

/////////////////////////////////////////////////////////////////////////
/**
 * Gets an index-based RMS between a reference symbol table (that contains indices
 * from one function), and the same indices extracted from another function.
 */
double getIndexRMS(
	long reference_st,				// reference symbol table
	char* idef_filename,			// IDEF file name (only to report messages)
	FILE* idef_file_,				// IDEF file already open
	List function_list				// function to extract indices from
);


/////////////////////////////////////////////////////////////////////////
/**
 * Gets a symbol table with symbols from a idef-file.
 */
long getIndicesFromFunction(
	char* idef_filename,		// IDEF file name (only to report messages)
	FILE* idef_file_,			// IDEF file already open
	List function_list			// the function
);



///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Intersect functions with respect to a reference function (interpolating if necessary).
 */
int intersect_functions(
	List *ref_list,           // IO- Reference function
	List *one_list,           // IO- function 1
	List *two_list,           // IO- function 2
	List *three_list          // IO- function 3
);


///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Update the range of indexes in ref_list such that in_list can be interpolated with respect to
 * this range.
 */
int update_indexes_interpolation(
	List ref_list,
	long *ref_index_from,
	long *ref_index_to,
	List in_list
);


/////////////////////////////////////////////////////////////////////////
/**
 * Gets the direct RMS between the two functions.
 * The function_list is first interpolated with respect to reference_function_list.
 */
double getDirectRMS(
	List reference_function_list, 	// reference function
	List function_list				// function to be interpolated
);


///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns a list of points from a file.
 * The expected format of the text file is: one point per line with the form:
 *     abscissa <separator> ordinate
 * <separator> can be ',', ' ', or '\t'
 */
List read_function(char* filename);

///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns a list of points from a file.
 *
 * The expected format of the text file is: one point per line with the form:
 *
 *     col0 <separator> col1 <separator> col2 ... <separator> coln
 *
 * <separator> can be ',', ' ', or '\t'
 *
 * filename - To read values from
 * colx - Column to read x values
 * coly - Column to read y values
 */
List read_function_cols(char* filename, int colx, int coly);

///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Writes a function (list) to a file.
 * Return 0 iff OK.
 */
int writeFunction(List list, char* filename);


///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Returns the value of the function in_list in the x abscissa by means of interpolation.
 * It's assumed that x falls within in_list.
 */
float calculate_interpolation(float x, List in_list);



///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Makes an interpolated in_list function sampled at the reference abscissas in ref_list
 * indicated by its indexes.
 * If:
 *	min_abscissa(in_list) > abscissa(ref_list, ref_index_from)
 * OR
 *	max_abscissa(in_list) < abscissa(ref_list, ref_index_to)
 * then this routine returns NULL;
 */
List interpolate_function(
	List ref_list, 			// reference function
	long ref_index_from, 	// first index in reference
	long ref_index_to,  	// last index in reference
	List in_list			// function to be interpolated
);



///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Like interpolate_function, but the two functions are intersected.
 * If the intersection is empty, this routine returns NULL;
 */
List intersect_abscissas(List ref_list, List in_list, List *new_ref_list);


///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Finds the indexes i1 and i2 in the list such that
 * list(i1).x <= x <= list(i2).x .
 * Makes a binary search.
 *
 * Precondition: x is in the domain of the function.
 */
void findIndexes(float x, List list, long *i1, long *i2);


///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Given a range [x_from, x_to], this routine finds the indexes i_from and i_to
 * in the list such that list(i_from).x <= x_from <= x_to <= list(i_to).x .
 * Calls findIndexes.
 *
 * NEVER TESTED!!!! (as of 040802)
 *
 * Precondition: [x_from, x_to] is in the domain of the function (list).
 */
void findRangeIndexes(float x_from, float x_to, List list, long *i_from, long *i_to);

///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Extracts an interval from a function.
 *
 * Precondition: x_from <= x_to
 */
List function_extract(List function_list, float x_from, float x_to);



///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Smooths a function in a given range and returns the resulting function.
 * Uses polynomial interpolation. See interp.h
 */
List smooth_function(
	List ref_list,         // reference function list
	long ref_index_from,  // starting index in ref_list
	long ref_index_to,    // ending index in ref_list
	int  number_bands,    // Number of bands to make interpolation
	float *deviation     // RMSE in smoothed region
);



#endif	/*  __FUNC_H */
