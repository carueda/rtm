/* 
/////////////////////////////////////////////////////////////////////////
//
// genskyl
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/


#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>

#include "func.h"



///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 */
static List gen_skyl(List reflectance_list)
{
	List skyl_list;
	
	long i, reflectance_size;
	
	skyl_list = list_create(sizeof(RPoint), 2000, 1000);
	if ( skyl_list == NULL )
	{
		return NULL;
	}

	reflectance_size = list_size(reflectance_list);
	for ( i = 0; i < reflectance_size; i++ )
	{
		RPoint* wl_refl;
		RPoint wl_skyl;
		float lambda_m;		// = lambda / 1000  (micrometers)
		
		wl_refl = (RPoint*) list_elementAt(reflectance_list, i);
		wl_skyl.x = wl_refl->x;
		lambda_m = wl_skyl.x / 1000;
		
		wl_skyl.y = 1.3273*lambda_m*lambda_m - 1.7829*lambda_m + 0.6944;
        	
		list_addElement(skyl_list, &wl_skyl);
	}
	
	return skyl_list;
}




///////////////////////////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	char* dbname_ref_function;
	char* dbname_function_out;
	
	List  ref_list;
	List  out_list;
	
	
	if ( argc < 3 )
	{
		fprintf(stderr,
"\n"
"CSTARS genskyl 0.1 (%s %s)\n"
"genskyl generates a skyl function evaluated at the same abscissas as\n"
"those of a reference function.\n"
"\n"
"USAGE\n"
"  genskyl reference_function function_out\n"
"\n"
"	reference_function      Reference function file\n"
"	function_out            Resulting skyl function file\n"
"\n"
"EXAMPLE:\n"
"  genskyl 1.rfl skyl.txt\n"
"\n",
		       __DATE__, __TIME__
		);
		return 1;
	}
	
	dbname_ref_function = argv[1];
	dbname_function_out = argv[2];


	ref_list = read_function(dbname_ref_function);
	if ( ref_list == NULL )
	{
		fprintf(stderr, "Error reading reference function\n");
		return 1;
	}
	printf("reference function has %ld points\n", list_size(ref_list));
	

	printf("generating...\n");

	out_list = gen_skyl(ref_list);
	
	printf("writing %ld points of skyl function %s\n", list_size(out_list), dbname_function_out);

	return writeFunction(out_list, dbname_function_out);
}



