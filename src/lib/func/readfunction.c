/*
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// Carlos Rueda - CSTARS
//
// June 2001:
//      List read_function(char* filename)
// November 2001:
//      List read_function_cols(char* filename, int colx, coly)
//
/////////////////////////////////////////////////////////////////////////
*/


#include "func.h"

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>



///////////////////////////////////////////////////////////////////////////////////////////////////
List read_function_cols(char* filename, int colx, int coly)
{
	int colmax = colx > coly ? colx : coly;
	List list;
	FILE* file = fopen (filename, "r");

	if ( file == NULL )
	{
		return NULL;	// error opening file
	}

	list = list_create(sizeof(RPoint), 2000, 1000);
	if ( list == NULL )
	{
		fclose(file);
		return NULL;	// error: no memory
	}

	while ( ! feof(file) )
	{
		RPoint p;
		char line[1024];
		char* tok;
		int col;
		int vals_put;

		if ( NULL == fgets(line, sizeof(line) -1, file) )
			break;

		tok = strtok(line, " \t,");

		// see if it's a comment or blank line:
		if ( tok == NULL || *tok == '#' || *tok == '\n' || *tok == 0 )
		{
			continue;
		}

		vals_put = 0;
		for ( col = 0; tok != NULL && col <= colmax; col++ )
		{
			float val;

			if ( 1 != sscanf(tok, "%f", &val) )
			{
				break;
			}

			if ( col == colx )
			{
				p.x = val;
				vals_put++;
			}

			// no ``else'' to allow for colx == coly
			if ( col == coly )
			{
				p.y = val;
				vals_put++;
			}

			tok = strtok(NULL, " \t,");
		}

		if ( vals_put == 2 )
		{
			list_addElement(list, &p);
		}
	}

	fclose(file);

	return list;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
List read_function(char* filename)
{
	List list;
	FILE* file = fopen (filename, "r");

	if ( file == NULL )
	{
		return NULL;	// error opening file
	}

	list = list_create(sizeof(RPoint), 2000, 1000);
	if ( list == NULL )
	{
		fclose(file);
		return NULL;	// error: no memory
	}

	while ( ! feof(file) )
	{
		RPoint p;
			char line[1024];
			char* tok;

			if ( NULL == fgets(line, sizeof(line) -1, file) )
			break;

		// skip spaces:
		for ( tok = line; isspace(*tok); tok++ )
			;

		// see if it's a comment or blank line:
		if ( *tok == '#' || *tok == '\n' || *tok == 0 )
			continue;

		tok = strtok(tok, " \t,");
		if ( tok == NULL
		||   1 != sscanf(tok, "%f", &p.x) )
			continue;

		tok = strtok(NULL, " \t,");
		if ( tok == NULL
		||   1 != sscanf(tok, "%f", &p.y) )
			continue;

		list_addElement(list, &p);
	}

	fclose(file);

	return list;
}

