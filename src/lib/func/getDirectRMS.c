/* 
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

#include "func.h"

#include <stdio.h>

/////////////////////////////////////////////////////////////////////////
double getDirectRMS(
	List reference_function_list, 	// reference function
	List function_list				// function to be interpolated
)
{
	int status;
	List new_function_list;
	double rms;
	long i, size;
	

	// First, find the range to apply to function_list, with respect to reference_function_list:
	long index_from = 0;
	long index_to = list_size(reference_function_list) - 1;
	
	status = update_indexes_interpolation(reference_function_list, &index_from, &index_to, function_list);
	if ( status != 0 )
	{
		fprintf(stderr, "update_indexes_interpolation status = %d\n", status);
		exit(1);
	}

	// Now, interpolate function_list:
	
	new_function_list = interpolate_function(
		reference_function_list, index_from, index_to, function_list
	);
	if ( new_function_list == NULL )
	{
		fprintf(stderr, "error in interpolate_function\n");
		exit(2);
	}
	
	// now compute RMS:
	rms = 0.0;
	size = list_size(new_function_list);
	for ( i = 0; i < size; i++ )
	{
		RPoint* obs_point = (RPoint*) list_elementAt(reference_function_list, index_from + i);
		RPoint* mod_point = (RPoint*) list_elementAt(new_function_list, i);
		double dif = obs_point->y - mod_point->y;
		
		rms += dif * dif;
	}
	
	list_destroy(new_function_list);
	
	return rms;
}

