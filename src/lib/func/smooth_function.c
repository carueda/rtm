/*
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// Carlos Rueda - CSTARS
// Sept 2001
//
/////////////////////////////////////////////////////////////////////////
*/


#include "func.h"
#include "interp.h"

#include <stdio.h>
#include <math.h>


#define MAX_N  64


///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *
 */
List smooth_function(
	List ref_list,
	long ref_index_from,
	long ref_index_to,
	int  number_bands,
	float *deviation
)
{
	long    i, ref_size;
	int    n;
	RPoint* point;
	float  xa[MAX_N];
	float  ya[MAX_N];
	int    k;
	List    out_list;
	float error;


	n = 2 * number_bands;
	if ( n > MAX_N )
	{
		fprintf(stderr, "number of bands too large: %d\n", number_bands);
		return NULL;
	}

	ref_size = list_size(ref_list);

	// create new function: (it's the same size as input)
	out_list = list_create(sizeof(RPoint), ref_size, 0);

	// make first a complete copy from ref_list to out_list:
	for ( i = 0; i < ref_size; i++ )
	{
		point = (RPoint*) list_elementAt(ref_list, i);
		list_addElement(out_list, point);
	}

	// prepare arrays to make interpolation:
	k = 0;
	for ( i = ref_index_from - number_bands; i < ref_index_from; i++ )
	{
		point = (RPoint*) list_elementAt(ref_list, i);
		xa[k] = point->x;
		ya[k] = point->y;
		k++;
	}
	for ( i = ref_index_to + 1; i <= ref_index_to + number_bands; i++ )
	{
		point = (RPoint*) list_elementAt(ref_list, i);
		xa[k] = point->x;
		ya[k] = point->y;
		k++;
	}

	// now, replace y values in out_list by using polynomial interpolation
	// and cummulating differences:
	error =0.;
	for ( i = ref_index_from; i <= ref_index_to; i++ )
	{
		float y;  // original y
		float dy;
		float tmp;
		int status;

		point = (RPoint*) list_elementAt(out_list, i);
		y = point->y;
		status = interp_polint(
			xa,
			ya,
			n,
			point->x,
			&point->y,
			&dy
		);

		if ( status != 0 )
		{
			fprintf(stderr, "interp_polint status = %d\n", status);
			break;
		}

		tmp = point->y - y;
		error += tmp*tmp;

	}

	if ( deviation != NULL )
	{
		*deviation = sqrt(error);
	}

	return out_list;
}

