/* 
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

#include "func.h"

#include <stdio.h>


///////////////////////////////////////////////////////////////////////////////////////////////////
int writeFunction(List list, char* filename)
{
	long size, i;
	FILE* file = fopen (filename, "w");
	
	if ( file == NULL )
	{
		return 1; // Error creating output file
	}

	size = list_size(list);
	for ( i = 0; i < size; i++ )
	{
		RPoint* p = (RPoint*) list_elementAt(list, i);
		fprintf(file, "%f , %f\n", p->x, p->y);
	}
	fclose(file);
	return 0;
}


