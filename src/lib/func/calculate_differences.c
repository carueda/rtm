/*
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// calculate_differences - tool to calculate differences on functions files.
//
// Based on getinds
//
// Carlos Rueda - CSTARS
// 10/19/01
//
// 01/29/02 -> New option to choose between "function minus reference" or
//             vice versa to make differences.
//
/////////////////////////////////////////////////////////////////////////
*/


#include "func.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>


// pattern to extract date and time:
static const char* PATTERN_BEGIN = "Spectrum saved: ";          // to strstr
static const char* PATTERN       = "Spectrum saved: %s at %s";  // to sscanf

/////////////////////////////////////////////////////////////////////////////////////////
/*
 * Returns 1 iff OK.
 */
static
int extract_time(char* function_filename, char* date, char* time)
{
	FILE* file = fopen(function_filename, "r");
	if ( file != NULL )
	{
		char line[4*1024];
		while ( fgets(line, sizeof(line), file) != NULL )
		{
			char* ptr = strstr(line, PATTERN_BEGIN);
			if ( ptr != NULL )
			{
				if ( 2 == sscanf(ptr, PATTERN, date, time) )
				{
					fclose(file);
					return 1; // OK: date/time extracted
				}
			}
		}
		fclose(file);
	}
	strcpy(date, "??");
	strcpy(time, "??");
	return 0; // No date/time
}

/////////////////////////////////////////////////////////////////////
typedef struct
{
	List	function;
	char*	name;
	char	date[64];
	char	time[64];
	int		date_time_ok;

} ItemFunction;


///////////////////////////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	char* out_filename;
	FILE* out_file;
	ItemFunction* functions;
	int expected_num_functions, num_functions;
	int arg, k, size, i;
	char* function_filename;
	const int COL_LEN = 10;
	int r_minus_f = 0;  // reference minus function?



	if ( argc < 4 )
	{
		fprintf(stderr,
"\n"
"CSTARS calculate_differences 0.2 (%s %s)\n"
"Calculates differences on functions files\n"
"\n"
"USAGE\n"
"  calculate_differences [-r_f] outfile reference_file function_file ...\n"
"\n"
"	-r_f                Differences will be \"reference minus function\"\n"
"	                    By default: \"function minus reference\"\n"
"	outfile             Output file\n"
"	reference_file      Reference function file\n"
"	function_file...    Function files\n"
"\n"
"EXAMPLE:\n"
"  calculate_differences diffs.txt 0.rfl 1.rfl 2.rfl\n"
"    Creates output.txt with differences of 1.rfl and\n"
"    2.rfl with respect to 0.rfl."
"\n",
			   __DATE__, __TIME__
		);
		return 1;
	}

	arg = 1;

	if ( strcmp(argv[arg], "-r_f") == 0 )
	{
		r_minus_f = 1;
		arg++;
	}

	out_filename = argv[arg++];

	// check out_filename doesn't exist (to prevent overwriting a function file):
	if ( NULL != (out_file = fopen(out_filename, "r")) )
	{
		fprintf(stderr, "Output file %s exists. I don't want to overwrite it!\n", out_filename);
		fclose(out_file);
		return 1;
	}

	out_file = fopen(out_filename, "w");
	if ( out_file == NULL )
	{
		fprintf(stderr, "error creating output file %s\n", out_filename);
		return 1;
	}

	// remaining arguments are functions to process including reference:
	expected_num_functions = argc - arg;

	functions = (ItemFunction*) malloc(expected_num_functions * sizeof(ItemFunction));
	if ( functions == NULL )
	{
		fclose(out_file);
		fprintf(stderr, "Not enough memory for %d functions\n", expected_num_functions);
		return 1;
	}

	// load function files:
	num_functions = 0;
	for ( ; arg < argc; arg++ )
	{
		function_filename = argv[arg];

		functions[num_functions].function = read_function(function_filename);
		if ( functions[num_functions].function == NULL )
		{
			fprintf(stderr, "error reading %s\n", function_filename);
			continue;
		}
		functions[num_functions].name = function_filename;

		functions[num_functions].date_time_ok =
			extract_time(function_filename,
				functions[num_functions].date, functions[num_functions].time
			)
		;

		num_functions++;
	}

	// write header:
	fprintf(out_file, "# Function Differences Report File\n");
	fprintf(out_file, "#  COD - FILENAME DATE TIME\n");
	for ( k = 0; k < num_functions; k++ )
	{
		fprintf(out_file, "#  %03d - %s %s %s", k,
			functions[k].name, functions[k].date, functions[k].time
		);
		if ( k == 0 )
		{
			fprintf(out_file, "  <-- Reference");
		}
		fprintf(out_file, "\n");
	}
	fprintf(out_file, "#\n");

	if ( r_minus_f )
	{
		fprintf(out_file, "# Differences: reference minus function\n");
	}
	else
	{
		fprintf(out_file, "# Differences: function minus reference\n");
	}
	fprintf(out_file, "#\n");

	fprintf(out_file,
		"# The function column header is its TIME if successfully extracted,\n"
		"# or its COD (if TIME is ??).\n"
	);
	fprintf(out_file, "#\n");

	// wavelength columns header:
	fprintf(out_file, "# %8s ", "wavelen");

	// column header
	for ( k = 1; k < num_functions; k++ )
	{
		char col_header[128];
		if ( functions[k].date_time_ok )
		{
			strcpy(col_header, functions[k].time);
		}
		else
		{
			// show the function code:
			sprintf(col_header, "%03d", k);
		}

		fprintf(out_file, "%*s ", COL_LEN, col_header);
	}
	fprintf(out_file, "\n");

	// write differences:
	size = list_size(functions[0].function);
	for ( i = 0; i < size; i++ )
	{
		// reference point:
		RPoint* ref_point = list_elementAt(functions[0].function, i);

		// wavelength:
		fprintf(out_file, "  %8.2f ", ref_point->x);

		// differences for functions k >= 1
		for ( k = 1; k < num_functions; k++ )
		{
			RPoint* point = list_elementAt(functions[k].function, i);
			float diff;
			if ( r_minus_f )
			{
				diff = ref_point->y - point->y;
			}
			else
			{
				diff = point->y - ref_point->y;
			}

			fprintf(out_file, "%*f ", COL_LEN, diff);
		}
		fprintf(out_file, "\n");
	}

	fclose(out_file);

	fprintf(stdout,
		"\nProcessed %d file(s) out of %d.\n",
		num_functions, expected_num_functions
	);

	return 0;
}
