/* 
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/


#include "func.h"

#include <stdio.h>
/*
#include <errno.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>
*/


///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * 
 */
List interpolate_function(
	List ref_list, 
	long ref_index_from, 
	long ref_index_to, 
	List in_list
)
{
	long    i, in_size, new_in_size;
	RPoint* ref_point;
	RPoint* in_point;
	List    out_list;
	

	// check starting abscissas:	
	ref_point = (RPoint*) list_elementAt(ref_list, ref_index_from);
	in_point = (RPoint*) list_elementAt(in_list, 0);
	if ( in_point->x > ref_point->x )
		return NULL;
	
	// check ending abscissas:	
	in_size = list_size(in_list);
	
	ref_point = (RPoint*) list_elementAt(ref_list, ref_index_to);
	in_point = (RPoint*) list_elementAt(in_list, in_size - 1);
	if ( in_point->x < ref_point->x )
		return NULL;

	// create new function:
	new_in_size = ref_index_to - ref_index_from + 1;
	out_list = list_create(sizeof(RPoint), new_in_size, 0);
	for ( i = ref_index_from; i <= ref_index_to; i++ )
	{
		RPoint out_point;
		ref_point = (RPoint*) list_elementAt(ref_list, i);
		out_point = *ref_point;
		out_point.y = calculate_interpolation(out_point.x, in_list);
		
		list_addElement(out_list, &out_point);
	}
	
	return out_list;
}

