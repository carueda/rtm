/*
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// Carlos Rueda - CSTARS
//
// April 2002:
//      BandInfo* readBandsInfo(char* bands_filename, int *size)
//      void freeBandsInfo(BandInfo* bands, int size)
//
/////////////////////////////////////////////////////////////////////////
*/

#include "func.h"

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


///////////////////////////////////////////////////////////////////////////////////////////////////
BandInfo* readBandsInfo(char* bands_filename, int *size)
{
	int i, j;
	BandInfo* bands;
	FILE* file = fopen(bands_filename, "r");
	if ( file == NULL )
	{
		fprintf(stderr, "Cannot open %s\n", bands_filename);
		return NULL;
	}
	
	// get size:
	*size = -1;
	fscanf(file, "%d", size);
	if ( *size < 0 )
	{
		fprintf(stderr, "Error reading size (%d)\n", *size);
		return NULL;
	}
	
	bands = (BandInfo*) malloc(*size * sizeof(BandInfo));
	if ( bands == NULL )
	{
		fprintf(stderr, "Not enough memory to %d bands\n", *size);
		fclose(file);
		return NULL;
	}
	
	for ( i = 0; i < *size; i++ )
	{
		char bandname[1024];
		
		fscanf(file, "%s%*s%d%f%f", 
			bandname,
			&bands[i].response_size,
			&bands[i].center,
			&bands[i].bandwidth
		);

/*fprintf(stdout, "bandname=%s response_size=%d center=%f bandwidth=%f\n", 
				bandname,  bands[i].response_size, bands[i].center, bands[i].bandwidth
		);
*/		
		bands[i].response_wavelengths = (float*) malloc(bands[i].response_size * sizeof(float));
		bands[i].response_weights     = (float*) malloc(bands[i].response_size * sizeof(float));
		if ( bands[i].response_wavelengths == NULL 
		||   bands[i].response_weights == NULL )
		{
			fprintf(stderr, "Not enough memory for response. band=%s (response_size=%d)\n", 
				bandname,  bands[i].response_size
			);
			fclose(file);
			return NULL;
		}
		
		for ( j = 0; j < bands[i].response_size; j++ )
		{
			bands[i].response_wavelengths[j] = -1.0;
			bands[i].response_weights[j] = -1.0;
			fscanf(file, "%*d%f%f",
				&bands[i].response_wavelengths[j],
				&bands[i].response_weights[j]
			);
		}
	}
	return bands;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void freeBandsInfo(BandInfo* bands, int size)
{
	int i;
	
	for ( i = 0; i < size; i++ )
	{
		free(bands[i].response_wavelengths);
		free(bands[i].response_weights);
	}
	free(bands);
}


