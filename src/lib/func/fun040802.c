/* 
/////////////////////////////////////////////////////////////////////////
//
// function utilities
// fun040802
//
// Carlos Rueda - CSTARS
// April 2002
//
///////////////////////////////////////////////////////////////////////// 
*/

#include "func.h"

#include <assert.h>
#include <math.h>

///////////////////////////////////////////////////////////////////////////////////////////////////
List fun040802(
	List in_list,
	int desired_size,
	BandInfo* desired_bands,
	List out_list
)
{
	long i, r;
	if ( out_list == NULL )
	{
		out_list = list_create(sizeof(RPoint), desired_size, 0);
		if ( out_list == NULL )
		{
			fprintf(stderr, "Cannot create list, desired size=%d\n", desired_size);
			return NULL;
		}
	}
	else if ( in_list == out_list )
	{
		fprintf(stderr, "fun040802: in_list cannot be equal to out_list!\n");
		return NULL;
	}
	
	for ( i = 0; i < desired_size; i++ )
	{
		BandInfo band_info = desired_bands[i];
		float bw2 = band_info.bandwidth / 2.0f;
		float from_wl = band_info.center - bw2;
		float to_wl = band_info.center + bw2;
		RPoint p;
		float sum_weights = 0.0;
		float sum_y = 0.0;
		
//fprintf(stdout, "bw2=%f from_wl=%f to_wl=%f\n",bw2, from_wl, to_wl);
//fprintf(stdout, "response_size=%d\n", band_info.response_size);
		
		for ( r = 0; r < band_info.response_size; r++ )
		{
			float in_y;
			float response_wl = band_info.response_wavelengths[r];
			float response_wg;
//fprintf(stdout, "response_wl=%f\n", response_wl);
			if ( response_wl < from_wl || response_wl > to_wl )
			{
				continue;
			}
			
			in_y = calculate_interpolation(response_wl, in_list);
			response_wg = band_info.response_weights[r];
			
//fprintf(stdout, "in_y=%f wg=%f\n", in_y, response_wg);
			
			sum_y += in_y * response_wg;
			sum_weights += response_wg;
		}
		
		// we have our point:
		p.x = band_info.center;
		p.y = sum_y / sum_weights;
		list_addElement(out_list, &p);
	}
	
	return out_list;

}

