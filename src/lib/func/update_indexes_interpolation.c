/* 
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/


#include "func.h"



///////////////////////////////////////////////////////////////////////////////////////////////////
int update_indexes_interpolation(
	List ref_list, 
	long *ref_index_from,
	long *ref_index_to,
	List in_list
)
{
	RPoint* in_point;

	in_point = (RPoint*) list_elementAt(in_list, 0);
	for ( ; *ref_index_from <= *ref_index_to; (*ref_index_from)++ )
	{
		RPoint* ref_point = (RPoint*) list_elementAt(ref_list, *ref_index_from);
		
		if ( in_point->x <= ref_point->x )
			break;
	}
	if ( *ref_index_from > *ref_index_to )
		return -1;
	
	in_point = (RPoint*) list_elementAt(in_list, list_size(in_list) - 1);
	for ( ; *ref_index_from <= *ref_index_to; (*ref_index_to)-- )
	{
		RPoint* ref_point = (RPoint*) list_elementAt(ref_list, *ref_index_to);
		
		if ( in_point->x >= ref_point->x )
			break;
	}
	if ( *ref_index_from > *ref_index_to )
		return -1;
	
	return 0;
}


