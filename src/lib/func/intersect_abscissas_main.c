/* 
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/


#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>

#include "func.h"



///////////////////////////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	char* dbname_ref_function;
	char* dbname_function_in;
	char* dbname_function_out;
	char* dbname_new_ref_function;
	
	List  ref_list;
	List  in_list;
	List  out_list;
	List  new_ref_list;
	
	
	if ( argc < 5 )
	{
		fprintf(stderr,
"\n"
"CSTARS intersect_abscissas 0.1 (%s %s)\n"
"Like interpolate, but the two input functions are first intersected.\n"
"\n"
"USAGE\n"
"  intersect_abscissas reference_function function_in function_out new_reference_function\n"
"\n"
"	reference_function      Reference function file\n"
"	function_in             Function file to be interpolated\n"
"	function_out            Resulting interpolated function file\n"
"	new_reference_function  New reference function file\n"
"\n"
"EXAMPLE:\n"
"  intersect_abscissas 1.rfl skyl.txt skylout.txt new1.rfl\n"
"\n",
		       __DATE__, __TIME__
		);
		return 1;
	}
	
	dbname_ref_function = argv[1];
	dbname_function_in = argv[2];
	dbname_function_out = argv[3];
	dbname_new_ref_function = argv[4];


	ref_list = read_function(dbname_ref_function);
	if ( ref_list == NULL )
	{
		fprintf(stderr, "Error reading reference function\n");
		return 1;
	}
	printf("reference function has %ld points\n", list_size(ref_list));
	
	in_list = read_function(dbname_function_in);
	if ( in_list == NULL )
	{
		fprintf(stderr, "Error reading function in\n");
		return 1;
	}
	printf("function to interpolate has %ld points\n", list_size(in_list));
	

	printf("intersecting...\n");

	out_list = intersect_abscissas(ref_list, in_list, &new_ref_list);
	
	printf("writing %ld points of interpolated function %s\n", list_size(out_list), dbname_function_out);
	writeFunction(out_list, dbname_function_out);

	printf("writing %ld points of new reference function %s\n", list_size(new_ref_list), dbname_new_ref_function);
	return writeFunction(new_ref_list, dbname_new_ref_function);
}

