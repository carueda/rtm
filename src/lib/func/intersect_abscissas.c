/* 
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

#include "func.h"

#include <stdio.h>
/*
#include <errno.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>
*/

///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Like normalize_abscissas, but the two functions are intersected.
 * If the intersection is empty, this routine returns NULL;
 */
List intersect_abscissas(List ref_list, List in_list, List *new_ref_list)
{
	long    i, ref_size, in_size;
	long	ref_index_from, ref_index_to, new_ref_size;
	RPoint* ref_point;
	RPoint* in_point;
	List    out_list;
	

	ref_size = list_size(ref_list);
	in_size = list_size(in_list);

	// Find from where:
	in_point = (RPoint*) list_elementAt(in_list, 0);

	
	for ( ref_index_from = 0; ref_index_from < ref_size; ref_index_from++ )
	{
		ref_point = (RPoint*) list_elementAt(ref_list, ref_index_from);
		
		if ( in_point->x <= ref_point->x )
		{
			break;
		}
	}
	if ( ref_index_from >= ref_size )
	{
		return NULL;
	}
	
	// find last index in ref:
	
	ref_point = (RPoint*) list_elementAt(ref_list, ref_size - 1);
	in_point = (RPoint*) list_elementAt(in_list, in_size - 1);
	for ( ref_index_to = ref_size - 1; ref_index_to >= ref_index_from; ref_index_to-- )
	{
		ref_point = (RPoint*) list_elementAt(ref_list, ref_index_to);
		
		if ( in_point->x >= ref_point->x )
		{
			break;
		}
	}
	if ( ref_index_to < ref_index_from )
	{
		return NULL;
	}


	new_ref_size = ref_index_to - ref_index_from + 1;

	// create new interpolated function:
	out_list = list_create(sizeof(RPoint), new_ref_size, 0);
	
	// create new reference function:
	*new_ref_list = list_create(sizeof(RPoint), new_ref_size, 0);
	
	for ( i = ref_index_from; i <= ref_index_to; i++ )
	{
		RPoint out_point;
		ref_point = (RPoint*) list_elementAt(ref_list, i);
		
		list_addElement(*new_ref_list, ref_point);

		out_point = *ref_point;
		out_point.y = calculate_interpolation(out_point.x, in_list);
		
		list_addElement(out_list, &out_point);
	}
	
	return out_list;
}

