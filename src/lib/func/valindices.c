/* 
/////////////////////////////////////////////////////////////////////////
//
// function utilities
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

   
#include "func.h"

#include "opt.h"

#include "symboltable.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <math.h>



///////////////////////////////////////////////////////////////////////////////////////////////////
static	char* showType(int type)
{
	return "float";
}

///////////////////////////////////////////////////////////////////////////////////////////////////
static	char* showValue(int type, void* value)
{
	static char s[64];
	sprintf(s, "%f", *((float*) value));
	return s;
}



///////////////////////////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	char* function_filename;
	char* idef_filename;
	List function_list;
	FILE* idef_file;
	long indices_st;
	

	
	if ( argc < 3 )
	{
		fprintf(stderr,
"\n"
"CSTARS valindices 0.1 (%s %s)\n"
"This program shows index values computed on a reflectance function.\n"
"\n"
"USAGE\n"
"  valindices IDEF-file function_file\n"
"\n"
"	IDEF-file       Index definition file\n"
"	function_file   Function file\n"
"\n"
"EXAMPLE:\n"
"  valindices inversion.idef canopy_reflectante.txt\n"
"\n",
		       __DATE__, __TIME__
		);
		return 1;
	}
	idef_filename = argv[1];
	function_filename = argv[2];
	

	function_list = read_function(function_filename);
	if ( function_list == NULL )
	{
		fprintf(stderr, "error reading %s\n", function_filename);
		return 1;
	}

	idef_file = fopen(idef_filename, "r");
	if ( idef_file == NULL )
	{
		fprintf(stderr, "error opening index definition file %s\n", idef_filename);
		list_destroy(function_list);
		return 1;
	}
		
	indices_st = getIndicesFromFunction(idef_filename, idef_file, function_list);
	if ( indices_st < 0L )
	{
		fprintf(stderr, "error getting indices from %s\n", function_filename);
		fclose(idef_file);
		list_destroy(function_list);
		return 1;
	}
		
	st_show(indices_st, showType, showValue, stdout);

	fclose(idef_file);
	list_destroy(function_list);

	return 0;		
}
