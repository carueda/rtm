/* **************************************************************************
**
** A Simple Symbol Table
** symboltable.h - Public declarations.
** Author: Carlos Rueda
** Date:   04/27/01
**
** **************************************************************************/
#ifndef __SYMBOLTABLE_H
#define __SYMBOLTABLE_H


#include <stdio.h>		/* FILE */


/* maximum length in characteres for an identifier */
#define ST_ID_MAX_LEN		128

/* maximum length in characteres for a value */
#define ST_VAL_MAX_LEN		256



//////////////////////////////////////////////////////////////////////
typedef struct
{
	/* the identifier */
	char	id[ST_ID_MAX_LEN];
	
	/* the type;  you use this field as you want */
	int	type;
	
	/* buffer to store a value; you must cast this field to use it */
	char	value[ST_VAL_MAX_LEN];

} RST_Entry, *ST_Entry;


//////////////////////////////////////////////////////////////////////
/**
 * Creates a new symbol table.
 * Returns the handle, -1 if error.
 */
long st_create(void);

//////////////////////////////////////////////////////////////////////
/**
 * Adds an entry to a symbol table
 */
void st_addEntry(long stid, ST_Entry entry);

//////////////////////////////////////////////////////////////////////
/**
 * Creates an entry.
 * Returns the entry.
 */
ST_Entry st_createEntry(char* id, int type, void* value, int value_len);


//////////////////////////////////////////////////////////////////////
/**
 * Assigns a value to an entry.
 */
void st_assignValue(ST_Entry entry, void* value, int value_len);

//////////////////////////////////////////////////////////////////////
/**
 * Finds an entry with a given id.
 * Returns the entry, or NULL if id not found.
 */
ST_Entry st_find(long stid, char* id);


//////////////////////////////////////////////////////////////////////
/**
 * Gets the number of entries of a symbol table.
 */
int st_size(long stid);

//////////////////////////////////////////////////////////////////////
/**
 * Gets an entry.
 * Returns the entry, or NULL if index is invalid.
 */
ST_Entry st_entryAt(long stid, int index);


//////////////////////////////////////////////////////////////////////
/**
 * Shows a symbol table.
 */
void st_show(
	long stid, 
	char* (showType)(int type),
	char* (showValue)(int type, void* value),
	FILE* out
);


//////////////////////////////////////////////////////////////////////
/**
 * Destroys a symbol table, BUT NOT its entries
 */
void st_destroy(long stid);

//////////////////////////////////////////////////////////////////////
/**
 * Destroys a symbol table AND its entries
 */
void st_destroyIncludingEntries(long stid);



#endif	/* SYMBOLTABLE */
