/*
	A Simple Symbol Table
*/

#include "symboltable.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//////////////////////////////////////////////////////////////////////
typedef struct RSymbolTable
{
	ST_Entry* entries;
	int size;
	int capacity;

} RSymbolTable, *SymbolTable;


//////////////////////////////////////////////////////////////////////
/**
 * Creates a new symbol table.
 * Returns the handle.
 */
long st_create(void)
{
	SymbolTable st = (SymbolTable) malloc(sizeof(RSymbolTable));
	if ( st == NULL )
	{
		return -1L;
	}
	st->capacity = 100;
	st->entries = (ST_Entry*) malloc(st->capacity * sizeof(ST_Entry));
	if ( st->entries == NULL )
	{
		free(st);
		return -1L;
	}
	st->size = 0;
	
	return (long) st;
}

//////////////////////////////////////////////////////////////////////
/**
 * Adds an entry to a symbol table
 */
void st_addEntry(long stid, ST_Entry entry)
{
	SymbolTable st = (SymbolTable) stid;
	if ( st->size == st->capacity )
	{
		int new_capacity = st->capacity + 100;
		ST_Entry* tmp = (ST_Entry*) 
			realloc(st->entries, (new_capacity * sizeof(ST_Entry)))
		;
		if ( tmp == NULL )
		{
			fprintf(stderr, "No memory in reallocation!\n");
			exit(1);
		}
		st->entries = tmp;
		st->capacity = new_capacity;
	}
	st->entries[st->size++] = entry;
}


//////////////////////////////////////////////////////////////////////
/**
 * Creates an entry.
 * Returns the entry.
 */
ST_Entry st_createEntry(char* id, int type, void* value, int value_len)
{
	ST_Entry entry = (ST_Entry) malloc(sizeof(RST_Entry));
	if ( entry == NULL )
		return NULL;
		
	strncpy(entry->id, id, sizeof(entry->id) - 1);
	entry->type = type;
	st_assignValue(entry, value, value_len);
	
	return entry;
}

//////////////////////////////////////////////////////////////////////
/**
 * Assigns a value to an entry.
 */
void st_assignValue(ST_Entry entry, void* value, int value_len)
{
	int len = value_len;
		
	if ( len > sizeof(entry->value) )
		len = sizeof(entry->value);
		
	memcpy(entry->value, value, len);
}


//////////////////////////////////////////////////////////////////////
/**
 * Finds an entry with a given id.
 * Returns the entry, or NULL if id not found.
 */
ST_Entry st_find(long stid, char* id)
{
	SymbolTable st = (SymbolTable) stid;
	int i;
	
	for ( i = 0; i < st->size; i++ )
	{
		ST_Entry entry = st->entries[i];
		if ( strcmp(entry->id, id) == 0 )
		{
			return entry;
		}
	}
	
	return NULL;
}


//////////////////////////////////////////////////////////////////////
/**
 * Gets an entry.
 * Returns the entry, or NULL if index is invalid.
 */
ST_Entry st_entryAt(long stid, int index)
{
	SymbolTable st = (SymbolTable) stid;
	
	if ( 0 <= index && index < st->size )
	{
		return  st->entries[index];
	}
	
	return NULL;
}


//////////////////////////////////////////////////////////////////////
/**
 * Gets the number of entries of a symbol table.
 */
int st_size(long stid)
{
	SymbolTable st = (SymbolTable) stid;
	
	return st->size;
}



//////////////////////////////////////////////////////////////////////
/**
 * Shows a symbol table.
 */
void st_show(
	long stid, 
	char* (showType)(int type),
	char* (showValue)(int type, void* value),
	FILE* out
)
{
	SymbolTable st = (SymbolTable) stid;
	int i;

	fprintf(out, "%-10s %7s %10s\n", "ID",         "TYPE",    "VALUE");
	fprintf(out, "%-10s %7s %10s\n", "----------", "-------", "----------");
	
	for ( i = 0; i < st->size; i++ )
	{
		ST_Entry entry = st->entries[i];
		fprintf(out, "%-10s ", entry->id);
		fprintf(out, "%7s ", showType(entry->type));
		fprintf(out, "%10s ", showValue(entry->type, entry->value));
		fprintf(out, "\n");
	}
}


//////////////////////////////////////////////////////////////////////
/**
 * Destroys a symbol table, BUT NOT its entries
 */
void st_destroy(long stid)
{
	SymbolTable st = (SymbolTable) stid;
	free(st->entries);
}

//////////////////////////////////////////////////////////////////////
/**
 * Destroys a symbol table AND its entries
 */
void st_destroyIncludingEntries(long stid)
{
	SymbolTable st = (SymbolTable) stid;
	int i;
	
	for ( i = 0; i < st->size; i++ )
	{
		ST_Entry entry = st->entries[i];
		free(entry);
	}

	free(st->entries);
}


