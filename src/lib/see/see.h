/* **************************************************************************
**
** SEE - A Simple Expression Evaluator
** see.h - Public declarations.
** Author: Carlos Rueda
** Date:   05/04/01
**
** **************************************************************************/
#ifndef __SEE_H
#define __SEE_H


/****************************************************************************
	DESCRIPCION

	This module offers the service of evaluating arithmetic expressions
	that can contain identifiers.

	A SEE_ExpressionEvaluator object is created with the following given
	functions:

		- A ``source'' object, i.e., a provider of characters
		
		- A checker for simple identifiers

		- An evaluator for simple identifiers

		- A checker for assignments

		- An executor for assignments

	Each expression is modeled as an object of the SEE_Expression class.
	An SEE_Expression has a type and a value.

	The grammar is as follows:

	  Syntax:
	  	asiglist ::= ( asig )*
	  
	  	asig ::= ID "=" expr ";"
	  	
		expr ::= expr2 ( ("<"|">"|"<="|">="|"=="|"!=") expr2 )*

		expr2 ::= term ( ("+" | "-") term )*

		term ::= term2 ( ("*" | "/") term2 )*

		term2 ::= fact ( "^" expr )*

		fact ::= literal
		       | ID
			   | "(" expr ")"

		literal ::= TYPE1_LITERAL
		          | TYPE2_LITERAL

	  Lexical rules:
	 	TYPE1_LITERAL   ::= DIGIT+
		TYPE2_LITERAL ::= (DIGIT | ".")+
		DIGIT   ::= 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
	  	ID ::= <any other token delimited by space, operator or parenthesis>

	example.c shows a simple usage of this service.

****************************************************************************/


#define TYPE1_CODE	2101
#define TYPE2_CODE	2102

typedef long		TYPE1;
typedef float		TYPE2;


//////////////////////////////////////////////////////////////////////
/* A value */
typedef union
{
	TYPE1	i;
	TYPE2	f;
	
} RSEE_Value;


////////////////////////////////////////////////////////////////////////////
/**
 * An expression.
 */
typedef struct RSEE_Expression
{
	/** The type of this expression: TYPE1_CODE, TYPE2_CODE. */
	int type;

	/** The value of this expression. */
	RSEE_Value value;
	
	/** 
	 * The next expression.
	 * DO NOT USE THIS FIELD!
	 */
	struct RSEE_Expression* next;

} RSEE_Expression, *SEE_Expression;



////////////////////////////////////////////////////////////////////////////
/**
 * A ``source'' object
 */
typedef struct RSEE_Source
{
	////////////////////////////////////////////////////////////////////
	/**
	 * Gets the next character.
	 *
	 * @param source	
	 * @return	The next character or -1 if end-of-source.
	 */
	int (*getNextChar)(struct RSEE_Source* source);

} RSEE_Source, *SEE_Source;




////////////////////////////////////////////////////////////////////////////
/**
 * An expression evaluator
 */
typedef struct RSEE_ExpressionEvaluator
{
	////////////////////////////////////////////////////////////////////
	/**
	 * Checks an assignment
	 *
	 * @param see	
	 * @param id	The ID that receives the assignment.
	 * @param type	The value type to be assigned.
	 * @return		0 if OK; -1 if error.
	 */
	int (*checkAssignment)(struct RSEE_ExpressionEvaluator* see, char* id, int type);

	////////////////////////////////////////////////////////////////////
	/**
	 * Does an assignment
	 *
	 * @param see	
	 * @param id	The ID that receives the assignment.
	 * @param expr	The value to be assigned.
	 * @return		0 if OK; -1 if error.
	 */
	int (*doAssignment)(struct RSEE_ExpressionEvaluator* see, char* id, SEE_Expression expr);

	////////////////////////////////////////////////////////////////////
	/**
	 * Checks an id.
	 *
	 * @param see	
	 * @param id	
	 * @return	The type of the expression or -1 if error.
	 */
	int (*check)(struct RSEE_ExpressionEvaluator* see, char* id);

	////////////////////////////////////////////////////////////////////
	/**
	 * Evaluates an id.
	 *
	 * @param see	
	 * @param id	
	 * @return	The resulting expression or NULL if error.
	 */
	SEE_Expression (*evaluate)(struct RSEE_ExpressionEvaluator* see, char* id);


	/** The link. */
	void* link;

	/** 
	 * Expressions associated to this evaluator. 
	 * DO NOT USE THIS FIELD!
	 */
	SEE_Expression exprs;

} RSEE_ExpressionEvaluator, *SEE_ExpressionEvaluator;


////////////////////////////////////////////////////////////////////////////
/**
 * Creates an expression evaluator.
 */
SEE_ExpressionEvaluator see_Create (
	int (*checkAssignment)(struct RSEE_ExpressionEvaluator* see, char* id, int type),
	int (*doAssignment)(struct RSEE_ExpressionEvaluator* see, char* id, SEE_Expression expr),
	int (*check)(SEE_ExpressionEvaluator see, char* id),
	SEE_Expression (*evaluate)(SEE_ExpressionEvaluator see, char* id)
) ;


////////////////////////////////////////////////////////////////////////////
/**
 * Checks an expression. Returns the resulting expression type,
 * or -1 if an error occurs (call see_GetMsgError()).
 * Be sure the source cursor is in the position you want.
 */
int see_Check(
	SEE_ExpressionEvaluator see,	/* I The evaluator object */
	SEE_Source source,				/* I The source containing the expression */
	int withEND						/* I END must be present? */
);

////////////////////////////////////////////////////////////////////////////
/**
 * Evaluates an expression. Returns the resulting expression type and value,
 * or -1 if an error occurs (call see_GetMsgError()).
 * Be sure the source cursor is in the position you want.
 */
int see_Evaluate(
	SEE_ExpressionEvaluator see,	/* I The evaluator object */
	SEE_Source source,				/* I The source containing the expression */
	RSEE_Value *result				/* O The resulting value */
);


////////////////////////////////////////////////////////////////////////////
/**
 * Checks an assignment. 
 * Returns the resulting expression type,
 * or -1 if an error occurs (call see_GetMsgError()).
 * Be sure the source cursor is in the position you want.
 */
int see_CheckAssignment(
	SEE_ExpressionEvaluator see,	/* I The evaluator object */
	SEE_Source source,				/* I The source containing the assignment */
	int withEND						/* I END must be present? */
);

////////////////////////////////////////////////////////////////////////////
/**
 * Evaluates an assignment. 
 * Returns the resulting expression type and value,
 * or -1 if an error occurs (call see_GetMsgError()).
 * Be sure the source cursor is in the position you want.
 */
int see_doAssignment(
	SEE_ExpressionEvaluator see,	/* I The evaluator object */
	SEE_Source source				/* I The source containing the assignment */
);

////////////////////////////////////////////////////////////////////////////
/**
 * Check a list of assignments. 
 * Returns the resulting expression type and value of the last assignment,
 * or -1 if an error occurs (call see_GetMsgError()).
 * Be sure the source cursor is in the position you want.
 */
int see_checkAssignmentList(
	SEE_ExpressionEvaluator see,	/* I The evaluator object */
	SEE_Source source,				/* I The source containing the assignment */
	int withEND						/* I END must be present? */
);

////////////////////////////////////////////////////////////////////////////
/**
 * Does a list of assignments. 
 * Returns the resulting expression type and value of the last assignment,
 * or -1 if an error occurs (call see_GetMsgError()).
 * Be sure the source cursor is in the position you want.
 */
int see_doAssignmentList(
	SEE_ExpressionEvaluator see,	/* I The evaluator object */
	SEE_Source source				/* I The source containing the assignment */
);


////////////////////////////////////////////////////////////////////////////
/**
 * Installs the link.
 */
#define	see_PutLink(see,link) (see)->link = (link)

////////////////////////////////////////////////////////////////////////////
/**
 * Gets the link
 */
#define see_GetLink(see)  ((see)->link)


////////////////////////////////////////////////////////////////////////////
/**
 * Creates a TYPE1 expression.
 *
 * There is no direct destructor: see_Destroy destroys all associated 
 * expressions.
 */
SEE_Expression see_CreateTYPE1Expression(SEE_ExpressionEvaluator see, TYPE1 n);

////////////////////////////////////////////////////////////////////////////
/**
 * Creates a TYPE2 expression.
 *
 * There is no direct destructor: see_Destroy destroys all associated 
 * expressions.
 */
SEE_Expression see_CreateTYPE2Expression(SEE_ExpressionEvaluator see, TYPE2 f);


////////////////////////////////////////////////////////////////////////////
/**
 * Gets the type of an expression.
 */
#define see_GetType(se) \
	(se)->type

////////////////////////////////////////////////////////////////////////////
/**
 * Gets the value of a TYPE1 expression.
 */
#define see_GetTYPE1Value(se) \
	(se)->value.i

////////////////////////////////////////////////////////////////////////////
/**
 * Gets the value of a TYPE2 expression.
 */
#define see_GetTYPE2Value(se) \
	(se)->value.f

////////////////////////////////////////////////////////////////////////////
/**
 * Gets a verbal description (read-only) of the last error ocurred.
 */
char* see_getMsgError();

////////////////////////////////////////////////////////////////////////////
/**
 * Destroys an expression evaluator.
 * All associated expressions dynamically created are released.
 */
int see_Destroy ( SEE_ExpressionEvaluator see ) ;


#endif	// #ifndef __SEE_H
