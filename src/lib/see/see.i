/* **************************************************************************
**
** SEE - A Simple Expression Evaluator
** see.i - Private declarations.
** Author: Carlos Rueda
** Date:   04/24/01
**
** **************************************************************************/
#ifndef __SEE_I
#define __SEE_I

#include "see.h"


#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define SEE_MSG_ERROR_SIZE	255

extern char SEE_msgError[SEE_MSG_ERROR_SIZE + 1];



/***************************************************************************
	LEXICAL ANALIZER
***************************************************************************/

#define NOTHING				-1
#define EOS				'\0'
#define NUM				256
#define ID				257
#define END				258
#define TYPE1_LITERAL		259
#define TYPE2_LITERAL		260

#define LE		270
#define GE		271
#define EQ		272
#define NE		273

#define BSIZE	128

////////////////////////////////////////////////////////////////////////////
/**
 * A lexical analizer.
 */
typedef struct
{
	/** The source to take characters from. */
	SEE_Source source;
	
	/** The current character */
	int t;

	/** Number of current line. */
	int lineno;

	/** Number of current column. */
	int colno;

	/** Last lexeme found. */
	char lexeme[BSIZE];

	/** Last value. */
	union
	{
		TYPE1	i;
		TYPE2	f;
	} value;

} RSEE_LexicalAnalizer, *SEE_LexicalAnalizer;

////////////////////////////////////////////////////////////////////////////
/**
 * Creates a lexical analizer.
 */
SEE_LexicalAnalizer SEE_CreateLexicalAnalizer(SEE_Source source);

////////////////////////////////////////////////////////////////////////////
/**
 * Gets the next token from the source.
 */
int SEE_getNextToken(SEE_LexicalAnalizer lex);

////////////////////////////////////////////////////////////////////////////
/**
 * Gets the name of a token.
 * If withLexeme != 0 and the token isn't the same lexeme,
 * the current lexeme is included, e.g.
 *	TYPE1_LITERAL (2121)
 *	ID (algunid)
 *	'+'
 */
char* SEE_getTokenName(SEE_LexicalAnalizer lex, int t, int withLexeme);

////////////////////////////////////////////////////////////////////////////
/**
 * Destroys a lexical analizer.
 */
int SEE_DestroyLexicalAnalizer(SEE_LexicalAnalizer zal);



/***************************************************************************
	PARSER
***************************************************************************/


////////////////////////////////////////////////////////////////////////////
/**
 * A parser.
 */
typedef struct
{
	/** The lexical analizer that provides tokens. */
	SEE_LexicalAnalizer zal;

	/** Token that guides the parsing. */
	int lookahead;

	/**
	 * The evaluator that resolves simple expressions (ID).
	 */
	SEE_ExpressionEvaluator see;

} RSEE_Parser, *SEE_Parser;

////////////////////////////////////////////////////////////////////////////
/**
 * Creates a parser.
 */
SEE_Parser SEE_CreateParser(SEE_LexicalAnalizer zal, SEE_ExpressionEvaluator see);

////////////////////////////////////////////////////////////////////////////
/**
 * 
 */
int SEE_AcceptEND(SEE_Parser parser);


////////////////////////////////////////////////////////////////////////////
/**
 * 
 */
int SEE_CheckAssignment(SEE_Parser parser);

////////////////////////////////////////////////////////////////////////////
/**
 * 
 */
int SEE_doAssignment(SEE_Parser parser);

////////////////////////////////////////////////////////////////////////////
/**
 * 
 */
int SEE_checkAssignmentList(SEE_Parser parser);

////////////////////////////////////////////////////////////////////////////
/**
 * 
 */
int SEE_doAssignmentList(SEE_Parser parser);


////////////////////////////////////////////////////////////////////////////
/**
 * Checks and gets the resulting expression type.
 */
int SEE_CheckExpression(SEE_Parser zas);

////////////////////////////////////////////////////////////////////////////
/**
 * Parses and gets the resulting expression.
 */
SEE_Expression SEE_EvaluateExpression(SEE_Parser zas);

////////////////////////////////////////////////////////////////////////////
/**
 * Destroys a parser.
 */
int SEE_DestroyParser(SEE_Parser zas);



#endif
