/* **************************************************************************
**
** SEE - A Simple Expression Evaluator
** Author: Carlos Rueda
** Date:   04/24/01
**
** **************************************************************************/
#include "see.i"


////////////////////////////////////////////////////////////////////////////
/**
 * Wrapper
 */
int SEE_doAssignment(SEE_ExpressionEvaluator see, char* id, SEE_Expression expr)
{
	if ( see != NULL && see->doAssignment != NULL )
	{
		return see->doAssignment(see, id, expr);
	}

	return -2;
}



