/* **************************************************************************
**
** SEE - A Simple Expression Evaluator
** Author: Carlos Rueda
** Date:   05/04/01
**
** **************************************************************************/
#include "see.i"


char SEE_msgError[SEE_MSG_ERROR_SIZE + 1];


////////////////////////////////////////////////////////////////////////////
char* see_getMsgError()
{
	return SEE_msgError;
}

////////////////////////////////////////////////////////////////////////////
static
SEE_Expression SEE_CreateTYPE1Expression(TYPE1 n)
{
	SEE_Expression e = (SEE_Expression) malloc(sizeof(RSEE_Expression));
	e->type = TYPE1_CODE;
	e->value.i = n;
	e->next = NULL;
	return e;
}

////////////////////////////////////////////////////////////////////////////
static
SEE_Expression SEE_CreateTYPE2Expression(TYPE2 d)
{
	SEE_Expression e = (SEE_Expression) malloc(sizeof(RSEE_Expression));
	e->type = TYPE2_CODE;
	e->value.f = d;
	e->next = NULL;
	return e;
}


////////////////////////////////////////////////////////////////////////////
/**
 * Free all associated expressions.
 */
static
void	SEE_FreeExpressions(SEE_ExpressionEvaluator see)
{
	SEE_Expression e1 = see->exprs;
	while ( e1 != NULL )
	{
		SEE_Expression e2 = e1->next;
		free(e1);
		e1 = e2;
	}
	see->exprs = NULL;
}

////////////////////////////////////////////////////////////////////////////
SEE_ExpressionEvaluator	see_Create (
	int (*checkAssignment)(struct RSEE_ExpressionEvaluator* see, char* id, int type),
	int (*doAssignment)(struct RSEE_ExpressionEvaluator* see, char* id, SEE_Expression expr),
	int (*check)(SEE_ExpressionEvaluator see, char* id),
	SEE_Expression (*evaluate)(SEE_ExpressionEvaluator see, char* id)
)
{
	SEE_ExpressionEvaluator see = 
		(SEE_ExpressionEvaluator) malloc(sizeof(RSEE_ExpressionEvaluator));
	if ( see == NULL )
		return NULL;

	see->checkAssignment = checkAssignment;
	see->doAssignment = doAssignment;
	see->check = check;
	see->evaluate = evaluate;
	see->exprs = NULL;
	see->link = NULL;
	
	return see;
}


////////////////////////////////////////////////////////////////////////////
int see_checkAssignmentList(SEE_ExpressionEvaluator see, SEE_Source source, int withEND)
{
	SEE_LexicalAnalizer lexer = SEE_CreateLexicalAnalizer(source);
	SEE_Parser parser = SEE_CreateParser(lexer, see);

	int type = SEE_checkAssignmentList(parser);

	if ( type >= 0 && withEND && SEE_AcceptEND(parser) != 0 )
		type = -1;

	SEE_DestroyParser(parser);
	SEE_DestroyLexicalAnalizer(lexer);
	
	SEE_FreeExpressions(see);

	return type;
}

////////////////////////////////////////////////////////////////////////////
int see_doAssignmentList(SEE_ExpressionEvaluator see, SEE_Source source)
{
	SEE_LexicalAnalizer lexer = SEE_CreateLexicalAnalizer(source);
	SEE_Parser parser = SEE_CreateParser(lexer, see);

	int type = SEE_doAssignmentList(parser);

	SEE_DestroyParser(parser);
	SEE_DestroyLexicalAnalizer(lexer);
	
	SEE_FreeExpressions(see);

	return type;
}


////////////////////////////////////////////////////////////////////////////
int see_CheckAssignment(SEE_ExpressionEvaluator see, SEE_Source source, int withEND)
{
	SEE_LexicalAnalizer lexer = SEE_CreateLexicalAnalizer(source);
	SEE_Parser parser = SEE_CreateParser(lexer, see);

	int type = SEE_CheckAssignment(parser);

	if ( type >= 0 && withEND && SEE_AcceptEND(parser) != 0 )
		type = -1;

	SEE_DestroyParser(parser);
	SEE_DestroyLexicalAnalizer(lexer);
	
	SEE_FreeExpressions(see);
	
	return type;
}


////////////////////////////////////////////////////////////////////////////
int see_doAssignment(SEE_ExpressionEvaluator see, SEE_Source source)
{
	SEE_LexicalAnalizer lexer = SEE_CreateLexicalAnalizer(source);
	SEE_Parser parser = SEE_CreateParser(lexer, see);

	int type = SEE_doAssignment(parser);

	SEE_DestroyParser(parser);
	SEE_DestroyLexicalAnalizer(lexer);
	
	SEE_FreeExpressions(see);

	return type;
}


////////////////////////////////////////////////////////////////////////////
int see_Check(SEE_ExpressionEvaluator see, SEE_Source source, int withEND)
{
	SEE_LexicalAnalizer lexer = SEE_CreateLexicalAnalizer(source);
	SEE_Parser parser = SEE_CreateParser(lexer, see);

	int type = SEE_CheckExpression(parser);

	if ( type >= 0 && withEND && SEE_AcceptEND(parser) != 0 )
		type = -1;

	SEE_DestroyParser(parser);
	SEE_DestroyLexicalAnalizer(lexer);
	
	SEE_FreeExpressions(see);

	return type;
}

////////////////////////////////////////////////////////////////////////////
int see_Evaluate(SEE_ExpressionEvaluator see, SEE_Source source, RSEE_Value *result)
{
	int type;
	SEE_LexicalAnalizer lexer = SEE_CreateLexicalAnalizer(source);

	SEE_Parser parser = SEE_CreateParser(lexer, see);

	SEE_Expression se = SEE_EvaluateExpression(parser);

	if ( se == NULL )
	{
		type = -1;
	}
	else
	{
		type = se->type;
		if ( result != NULL )
		{
			*result = se->value;
		}
	}
	
	SEE_DestroyParser(parser);
	SEE_DestroyLexicalAnalizer(lexer);
	SEE_FreeExpressions(see);
	return type;
}

////////////////////////////////////////////////////////////////////////////
int	see_Destroy ( SEE_ExpressionEvaluator see )
{
	SEE_FreeExpressions(see);

	free(see);
	return 0;
}

////////////////////////////////////////////////////////////////////////////
SEE_Expression see_CreateTYPE1Expression(SEE_ExpressionEvaluator see, TYPE1 n)
{
	SEE_Expression se = SEE_CreateTYPE1Expression(n);

	se->next = see->exprs;
	see->exprs = se;
	return se;
}

////////////////////////////////////////////////////////////////////////////
SEE_Expression see_CreateTYPE2Expression(SEE_ExpressionEvaluator see, TYPE2 d)
{
	SEE_Expression se = SEE_CreateTYPE2Expression(d);

	se->next = see->exprs;
	see->exprs = se;
	return se;
}


