/*
** SEE - A Simple Expression Evaluator
** A demo program
** Author: Carlos Rueda
** $Id$
*/

#include "see.h"
#include "symboltable.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


static char source_string[32 * 1024];
static int len;
static int cursor;

//////////////////////////////////////////////////////////////////////
static
int getNextChar(SEE_Source source)
{
	if ( cursor >= len )
		return -1;
	else
		return source_string[cursor++];
}

static RSEE_Source source = {getNextChar};


/* the symbol table */
static long st;


//////////////////////////////////////////////////////////////////////
static
void initSymbolTable()
{
	TYPE1 v1;
	TYPE2 v2;
	
	st = st_create();
	if ( 0 )
	{
		// for testing
		st_addEntry(st, st_createEntry("n", TYPE1_CODE, (v1 = 2121,   &v1), sizeof(v1) ));
		st_addEntry(st, st_createEntry("x", TYPE2_CODE, (v2 = 123.45, &v2), sizeof(v2) ));
		st_addEntry(st, st_createEntry("m", TYPE1_CODE, (v1 = 326,    &v1), sizeof(v1) ));
	}
}

//////////////////////////////////////////////////////////////////////
static
void destroySymbolTable()
{
	st_destroyIncludingEntries(st);
}


//////////////////////////////////////////////////////////////////////
static
int checkAssignment(SEE_ExpressionEvaluator see, char* id, int type)
{
	ST_Entry entry = st_find(st, id);
	if ( entry == NULL )
	{
		if ( type == TYPE1_CODE )
		{
			TYPE1 v1 = 0;
			entry = st_createEntry(id, type, &v1, sizeof(v1));
		}
		else
		{
			TYPE2 v2 = 0.0;
			entry = st_createEntry(id, type, &v2, sizeof(v2));
		}
		st_addEntry(st, entry);
	}
	else
	{
		if ( type != entry->type )
			return -1;		/* different types */
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////
static
int doAssignment(SEE_ExpressionEvaluator see, char* id, SEE_Expression expr)
{
	ST_Entry entry = st_find(st, id);
	if ( entry == NULL )
	{
		entry = st_createEntry(id, expr->type, &expr->value, sizeof(expr->value));
		st_addEntry(st, entry);
	}
	else
	{
		entry->type = expr->type;
		memcpy(entry->value, &expr->value, sizeof(expr->value));
		st_assignValue(entry, &expr->value, sizeof(expr->value));
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////
static
int simpleCheck(SEE_ExpressionEvaluator see, char* id)
{
	ST_Entry entry = st_find(st, id);
	if ( entry != NULL )
	{
		return entry->type;
	}

	return -1;
}

//////////////////////////////////////////////////////////////////////
static
char* showType(int type)
{
	if ( type == TYPE1_CODE )
		return "TYPE1";
	else
		return "TYPE2";
}

//////////////////////////////////////////////////////////////////////
static
char* showValue(int type, void* value)
{
static char s[64];
	if ( type == TYPE1_CODE )
	{
		sprintf(s, "%ld", *((TYPE1*) value));
	}
	else
	{
		sprintf(s, "%f", *((TYPE2*) value));
	}
	return s;
}

//////////////////////////////////////////////////////////////////////
static
void showSymbolTable(void)
{
	st_show(st, showType, showValue, stdout);
}

//////////////////////////////////////////////////////////////////////
static
SEE_Expression simpleEvaluate(SEE_ExpressionEvaluator see, char* id)
{
	ST_Entry entry = st_find(st, id);
	if ( entry != NULL )
	{
		if ( entry->type == TYPE1_CODE )
			return see_CreateTYPE1Expression(see, *((TYPE1*) entry->value));
		else
			return see_CreateTYPE2Expression(see, *((TYPE2*) entry->value));
	}

	return NULL;
}


//////////////////////////////////////////////////////////////////////
static
void usage()
{
	printf(
"see - A Simple Expression Evaluator\n"
"USAGE: see [option] source\n"
" Option:\n"
"	-l	list of assignments\n"
"	-a	assignment\n"
" by default, an expression is expected.\n"
"	-fmtint    printf format for integer result\n"
"	-fmtreal   printf format for real result\n"
"\n"
"Examples:\n"
" see \"43.5 / 2\"                   # an expression\n"
" see -l \"y = 21 ; z = 2 * y ;\"    # a list of assignments\n"
" see -a \"z = 12 + 9 ;\"            # an assigment\n"
" see \"21 <= 200\"                  # another expression\n"
	);
	//showSymbolTable();
}

//////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{
	int arg;
	char* option = "-e";
	SEE_ExpressionEvaluator see;
	int type = -1;
	RSEE_Value result;
	char* fmtint = "%ld\n";
	char* fmtreal = "%g\n";


	initSymbolTable();
	
	if ( argc < 2 ) {
		usage();
		return 1;
	}
	
    for ( arg = 1; arg < argc && argv[arg][0] == '-'; arg++ ) {
		if ( strcmp(argv[arg], "-l") == 0
		||   strcmp(argv[arg], "-a") == 0 
		||   strcmp(argv[arg], "-e") == 0 ) {
			option = argv[arg++];
		}
		else if ( strcmp(argv[arg], "-fmtint") == 0 ) {
			if ( ++arg >= argc )
				usage();
			fmtint = argv[arg];
		}
		else if ( strcmp(argv[arg], "-fmtreal") == 0 ) {
			if ( ++arg >= argc )
				usage();
			fmtreal= argv[arg];
		}
	}
	if ( arg >= argc )
	{
		usage();
		return 1;
	}
    
    source_string[0] = 0;
    while ( arg < argc )
    {
        strcat(source_string, argv[arg++]);
        if ( arg < argc )
            strcat(source_string, " ");
    }
    
	len = strlen(source_string);

	see = see_Create(checkAssignment, doAssignment, simpleCheck, simpleEvaluate);

	cursor = 0;
	
	if ( strcmp(option, "-l") == 0 )
	{
		//printf("Checking assignment list...\n");
		type = see_checkAssignmentList(see, &source, 1);
	}
	else if ( strcmp(option, "-a") == 0 )
	{
		//printf("Checking assignment...\n");
		type = see_CheckAssignment(see, &source, 1);
	}
	else if ( strcmp(option, "-e") == 0 )
	{
		//printf("Checking expression...\n");
		type = see_Check(see, &source, 1);
	}
	
	if ( type < 0 )
	{
        fprintf(stdout, "\"\n%s\n\"\n", source_string);
		fprintf(stdout, "%s\n", see_getMsgError());
		goto finish;
	}

	cursor = 0;

	if ( strcmp(option, "-l") == 0 )
	{
		//printf("Doing assignment list...\n");
		type = see_doAssignmentList(see, &source);
		showSymbolTable();
	}
	else if ( strcmp(option, "-a") == 0 )
	{
		//printf("Doing assignment...\n");
		type = see_doAssignment(see, &source);
		showSymbolTable();
	}
	else if ( strcmp(option, "-e") == 0 )
	{
		//printf("Evaluating expression...\n");
		type = see_Evaluate(see, &source, &result);
		if ( type == -1 )
		{
			fprintf(stdout, "expr: \"%s\"\n", source_string);
			fprintf(stdout, "%s\n", see_getMsgError());
		}
		else
		{
			switch ( type )
			{
			case TYPE1_CODE:
				printf(fmtint, result.i);
				break;
			case TYPE2_CODE:
				printf(fmtreal, result.f);
				break;
			}
			printf("\n");
		}
	}
	
finish:
	see_Destroy(see);
	destroySymbolTable();

	return 0;
}

