/* **************************************************************************
**
** SEE - A Simple Expression Evaluator
** Author: Carlos Rueda
** Date:     04/25/01
** Update:   05/15/02 - New ^ operator (right-associative). See term2.
**
** **************************************************************************/
#include "see.i"

#include <stdarg.h>
#include <setjmp.h>
#include <math.h>


// To error handling
static jmp_buf jmpbuf;

// Evaluating? If false, only checking
static int evaluating;

static void accept(SEE_Parser parser, int t);
static SEE_Expression operate(int op, SEE_Expression e1, SEE_Expression e2);

static int asiglist();
static int asig();
static SEE_Expression expr();
static SEE_Expression expr2();
static SEE_Expression term();
static SEE_Expression term2();
static SEE_Expression fact();


static SEE_Parser parser;


////////////////////////////////////////////////////////////////////////////
/**
 * 
 */
int SEE_AcceptEND(SEE_Parser parser_)
{
	int r;

	parser = parser_;
	r = setjmp(jmpbuf);
	if (r == 0)
	{
		accept(parser, END);

		return 0;
	}
	
	return -1;
}


////////////////////////////////////////////////////////////////////////////
/**
 * 
 */
int SEE_CheckAssignment(SEE_Parser parser_)
{
	int r;
	int status = -2;

	parser = parser_;
	evaluating = 0;
	r = setjmp(jmpbuf);
	if (r == 0)
	{
		status = asig();
	}
	
	return status;
}

////////////////////////////////////////////////////////////////////////////
/**
 * 
 */
int SEE_checkAssignmentList(SEE_Parser parser_)
{
	int r;
	int status = -2;

	parser = parser_;
	evaluating = 0;
	r = setjmp(jmpbuf);
	if (r == 0)
	{
		status = asiglist();
	}
	
	return status;
}

////////////////////////////////////////////////////////////////////////////
/**
 * 
 */
int SEE_doAssignmentList(SEE_Parser parser_)
{
	int r;
	int status = -2;

	parser = parser_;
	evaluating = 1;
	r = setjmp(jmpbuf);
	if (r == 0)
	{
		status = asiglist();
	}
	
	return status;
}

////////////////////////////////////////////////////////////////////////////
/**
 * 
 */
int SEE_doAssignment(SEE_Parser parser_)
{
	int r;
	int status = -2;

	parser = parser_;
	evaluating = 1;
	r = setjmp(jmpbuf);
	if (r == 0)
	{
		status = asig();
	}
	
	return status;
}


////////////////////////////////////////////////////////////////////////////
/**
 * Chequea y obtiene el type de la expresi�n resultante.
 */
int SEE_CheckExpression(SEE_Parser parser_)
{
	int r;

	parser = parser_;
	evaluating = 0;
	r = setjmp(jmpbuf);
	if (r == 0)
	{
		// primera entrada:
		SEE_Expression se = expr();
		return se->type;
	}
	else
	{
		// hubo alg�n error:
		return -1;
	}
}

////////////////////////////////////////////////////////////////////////////
/**
 * Analiza y obtiene la expresi�n resultante.
 */
SEE_Expression SEE_EvaluateExpression(SEE_Parser parser_)
{
	int r;

	parser = parser_;
	evaluating = 1;
	r = setjmp(jmpbuf);
	if (r == 0)
	{
		// primera entrada:
		SEE_Expression zexpr = expr();
		return zexpr;
	}
	else
	{
		// hubo alg�n error:
		return NULL;
	}
}



////////////////////////////////////////////////////////////////////////////
/**
 * Manejador de error durante el analisis.
 * Toma la informacion dada anteponiendo la linea y columna en donde
 * va el analizador lexico. Y hace un longjmp a SEE_Analizar.
 */
static
void error(SEE_Parser parser, char* fmt, ...)
{
	char	buf[1024];
	va_list args;

	va_start(args, fmt);
	vsprintf(buf, fmt, args);
	va_end(args);

	sprintf(SEE_msgError,
		"(%d,%d) %s\n", parser->zal->lineno, parser->zal->colno, buf)
	;
	longjmp(jmpbuf, 1);
}



////////////////////////////////////////////////////////////////////////////
SEE_Parser SEE_CreateParser(SEE_LexicalAnalizer zal, SEE_ExpressionEvaluator see)
{
	SEE_Parser parser = (SEE_Parser) malloc(sizeof(RSEE_Parser));
	if ( parser == NULL )
		return NULL;

	parser->zal = zal;
	parser->lookahead = SEE_getNextToken(parser->zal);
	parser->see = see;

	return parser;
}

////////////////////////////////////////////////////////////////////////////
int SEE_DestroyParser(SEE_Parser parser)
{
	free(parser);
	return 0;
}


////////////////////////////////////////////////////////////////////////////
static
void accept(SEE_Parser parser, int t)
{
	if ( parser->lookahead == t )
	{
		parser->lookahead = SEE_getNextToken(parser->zal);
	}
	else
	{
		error(parser, "``%s'' encountered.  ``%s'' would be fine.",
			SEE_getTokenName(parser->zal, parser->lookahead, 1),
			SEE_getTokenName(parser->zal, t, 0)
		);
	}
}


////////////////////////////////////////////////////////////////////////////
static
SEE_Expression operate(int op, SEE_Expression e1, SEE_Expression e2)
{
	switch ( op )
	{
	case '+':
		if ( e1->type == TYPE2_CODE || e2->type == TYPE2_CODE )
		{
			if ( evaluating )
			{
				e1->value.f =
					(e1->type == TYPE2_CODE ? e1->value.f : (double)e1->value.i)
					+
					(e2->type == TYPE2_CODE ? e2->value.f : (double)e2->value.i)
				;
			}
			e1->type = TYPE2_CODE;
		}
		else
		{
			if ( evaluating )
			{
				e1->value.i += e2->value.i;
			}
		}
		break;

	case '-':
		if ( e1->type == TYPE2_CODE || e2->type == TYPE2_CODE )
		{
			if ( evaluating )
			{
				e1->value.f =
					(e1->type == TYPE2_CODE ? e1->value.f : (double)e1->value.i)
					-
					(e2->type == TYPE2_CODE ? e2->value.f : (double)e2->value.i)
				;
			}
			e1->type = TYPE2_CODE;
		}
		else
		{
			if ( evaluating )
			{
				e1->value.i -= e2->value.i;
			}
		}
		break;

	case '*':
		if ( e1->type == TYPE2_CODE || e2->type == TYPE2_CODE )
		{
			if ( evaluating )
			{
				e1->value.f =
					(e1->type == TYPE2_CODE ? e1->value.f : (double)e1->value.i)
					*
					(e2->type == TYPE2_CODE ? e2->value.f : (double)e2->value.i)
				;
			}
			e1->type = TYPE2_CODE;
		}
		else
		{
			if ( evaluating )
			{
				e1->value.i *= e2->value.i;
			}
		}
		break;

	case '/':
		if ( evaluating )
		{
			if ( ( e2->type == TYPE2_CODE && e2->value.f == 0.0 )
			||   ( e2->type == TYPE1_CODE && e2->value.i == 0L )
			)
			{
				error(parser, "Division by zero");
			}
		}

		if ( e1->type == TYPE2_CODE || e2->type == TYPE2_CODE )
		{
			if ( evaluating )
			{
				e1->value.f =
					(e1->type == TYPE2_CODE ? e1->value.f : (double)e1->value.i)
					/
					(e2->type == TYPE2_CODE ? e2->value.f : (double)e2->value.i)
				;
			}
			e1->type = TYPE2_CODE;
		}
		else
		{
			if ( evaluating )
			{
				e1->value.i /= e2->value.i;
			}
		}
		break;

	case '^':
		if ( evaluating )
		{
			double x = (e1->type == TYPE2_CODE ? e1->value.f : (double)e1->value.i);
			double y = (e2->type == TYPE2_CODE ? e2->value.f : (double)e2->value.i);
			e1->value.f = pow(x, y);
		}
		e1->type = TYPE2_CODE;
		break;

	case '<':
		if ( e1->type == TYPE2_CODE || e2->type == TYPE2_CODE )
		{
			if ( evaluating )
			{
				e1->value.i =
					(e1->type == TYPE2_CODE ? e1->value.f : (double)e1->value.i)
					<
					(e2->type == TYPE2_CODE ? e2->value.f : (double)e2->value.i)
				;
			}
			
		}
		else
		{
			if ( evaluating )
			{
				e1->value.i = e1->value.i < e2->value.i;
			}
		}
        e1->type = TYPE1_CODE;
		break;
	case '>':
		if ( e1->type == TYPE2_CODE || e2->type == TYPE2_CODE )
		{
			if ( evaluating )
			{
				e1->value.i =
					(e1->type == TYPE2_CODE ? e1->value.f : (double)e1->value.i)
					>
					(e2->type == TYPE2_CODE ? e2->value.f : (double)e2->value.i)
				;
			}
			
		}
		else
		{
			if ( evaluating )
			{
				e1->value.i = e1->value.i > e2->value.i;
			}
		}
        e1->type = TYPE1_CODE;
		break;
	case LE:
		if ( e1->type == TYPE2_CODE || e2->type == TYPE2_CODE )
		{
			if ( evaluating )
			{
				e1->value.i =
					(e1->type == TYPE2_CODE ? e1->value.f : (double)e1->value.i)
					<=
					(e2->type == TYPE2_CODE ? e2->value.f : (double)e2->value.i)
				;
			}
			
		}
		else
		{
			if ( evaluating )
			{
				e1->value.i = e1->value.i <= e2->value.i;
			}
		}
        e1->type = TYPE1_CODE;
		break;
	case GE:
		if ( e1->type == TYPE2_CODE || e2->type == TYPE2_CODE )
		{
			if ( evaluating )
			{
				e1->value.i =
					(e1->type == TYPE2_CODE ? e1->value.f : (double)e1->value.i)
					>=
					(e2->type == TYPE2_CODE ? e2->value.f : (double)e2->value.i)
				;
			}
			
		}
		else
		{
			if ( evaluating )
			{
				e1->value.i = e1->value.i >= e2->value.i;
			}
		}
        e1->type = TYPE1_CODE;
		break;
	case EQ:
		if ( e1->type == TYPE2_CODE || e2->type == TYPE2_CODE )
		{
			if ( evaluating )
			{
				e1->value.i =
					(e1->type == TYPE2_CODE ? e1->value.f : (double)e1->value.i)
					==
					(e2->type == TYPE2_CODE ? e2->value.f : (double)e2->value.i)
				;
			}
			
		}
		else
		{
			if ( evaluating )
			{
				e1->value.i = e1->value.i == e2->value.i;
			}
		}
        e1->type = TYPE1_CODE;
		break;
	case NE:
		if ( e1->type == TYPE2_CODE || e2->type == TYPE2_CODE )
		{
			if ( evaluating )
			{
				e1->value.i =
					(e1->type == TYPE2_CODE ? e1->value.f : (double)e1->value.i)
					!=
					(e2->type == TYPE2_CODE ? e2->value.f : (double)e2->value.i)
				;
			}
			
		}
		else
		{
			if ( evaluating )
			{
				e1->value.i = e1->value.i != e2->value.i;
			}
		}
        e1->type = TYPE1_CODE;
		break;
	}

	return e1;
}


////////////////////////////////////////////////////////////////////////////
static
SEE_Expression unary_operate(int op, SEE_Expression e1)
{
	switch ( op )
	{
	case '-':
		if ( evaluating )
		{
            if ( e1->type == TYPE2_CODE)
				e1->value.f = -e1->value.f;
            else
                e1->value.i = -e1->value.i;
		}
	case '+':
        // nothing to do ;-)
        break;
    }
}

////////////////////////////////////////////////////////////////////////////
/*
  	asiglist ::= ( asig )*
*/
static
int asiglist()
{
	int status;
		
	while ( parser->lookahead != END )
	{
		status = asig();
		if ( status < 0 )
			return status;
	}
	
	return 0;
}


////////////////////////////////////////////////////////////////////////////
/*
  	asig ::= ID "=" expr ";"
*/
static
int asig()
{
	SEE_Expression se;
	char id[BSIZE];
		
	if ( parser->lookahead == ID )
	{
		strcpy(id, parser->zal->lexeme);
	}

	accept(parser, ID);
	accept(parser, '=');
	se = expr();
	accept(parser, ';');
	if ( evaluating )
		return parser->see->doAssignment(parser->see, id, se);
	else
		return parser->see->checkAssignment(parser->see, id, se->type);
}

////////////////////////////////////////////////////////////////////////////
/*
	expr ::= expr2 ( ("<"|">"|"<="|">="|"=="|"!=") expr2 )*
*/
static
SEE_Expression expr()
{
	SEE_Expression e1 = expr2();
	SEE_Expression e2;

	while ( parser->lookahead == '<' || parser->lookahead == '>' ||
	        parser->lookahead == LE || parser->lookahead == GE  ||
	        parser->lookahead == EQ || parser->lookahead == NE )
	{
		int op = parser->lookahead;

		accept(parser, parser->lookahead);

		e2 = expr2();

		e1 = operate(op, e1, e2);
	}

	return e1;
}

////////////////////////////////////////////////////////////////////////////
/*
	expr2 ::= term ( ("+" | "-") term )*
*/
static
SEE_Expression expr2()
{
	SEE_Expression e1 = term();
	SEE_Expression e2;

	while ( parser->lookahead == '+' || parser->lookahead == '-' )
	{
		int op = parser->lookahead;

		accept(parser, parser->lookahead);

		e2 = term();

		e1 = operate(op, e1, e2);
	}

	return e1;
}

////////////////////////////////////////////////////////////////////////////
/*
	term ::= term2 ( ("*" | "/") term2 )*
*/
static
SEE_Expression term()
{
	SEE_Expression e1 = term2();
	SEE_Expression e2;

	while ( parser->lookahead == '*' || parser->lookahead == '/' )
	{
		int op = parser->lookahead;

		accept(parser, parser->lookahead);

		e2 = term2();

		e1 = operate(op, e1, e2);
	}

	return e1;
}

////////////////////////////////////////////////////////////////////////////
/*
	term2 ::= fact ( "^" expr )*
*/
static
SEE_Expression term2()
{
	SEE_Expression e1 = fact();
	SEE_Expression e2;

	while ( parser->lookahead == '^' )
	{
		int op = parser->lookahead;

		accept(parser, parser->lookahead);

		e2 = expr();

		e1 = operate(op, e1, e2);
	}

	return e1;
}



////////////////////////////////////////////////////////////////////////////
/*
	fact ::= literal
	       | ID
	       | "(" expr ")"
	       | "-" fact
*/
static
SEE_Expression fact()
{
	SEE_Expression e;

	if ( parser->lookahead == TYPE1_LITERAL )
	{
		long n = parser->zal->value.i;
		accept(parser, parser->lookahead);
		e = see_CreateTYPE1Expression(parser->see, n);
	}
	else if ( parser->lookahead == TYPE2_LITERAL )
	{
		double d = parser->zal->value.f;
		accept(parser, parser->lookahead);
		e = see_CreateTYPE2Expression(parser->see, d);
	}
	else if ( parser->lookahead == ID )
	{
		char id[BSIZE];
		strcpy(id, parser->zal->lexeme);
		accept(parser, parser->lookahead);

		if ( evaluating )
		{
			e = parser->see->evaluate(parser->see, id);
			if ( e == NULL )
			{
				error(parser, "``%s'' undefined", id);
			}
		}
		else
		{
			int type = parser->see->check(parser->see, id);
			if ( type == -1 )
				e = NULL;
			else if ( type == TYPE1_CODE )
				e = see_CreateTYPE1Expression(parser->see, 0L);
			else
				e = see_CreateTYPE2Expression(parser->see, 0.0);

			if ( e == NULL )
			{
				error(parser, "``%s'' undefined", id);
			}
		}
	}
	else if ( parser->lookahead == '-' || parser->lookahead == '+' )
	{
        int op = parser->lookahead;
		accept(parser, parser->lookahead);
		e = fact();
        e = unary_operate(op, e);
	}
	else
	{
		accept(parser, '(');
		e = expr();
		accept(parser, ')');
	}

	return e;
}




