/* **************************************************************************
**
** SEE - A Simple Expression Evaluator
** Author: Carlos Rueda
** Date:   04/25/01
**
** **************************************************************************/
#include "see.i"



////////////////////////////////////////////////////////////////////////////
SEE_LexicalAnalizer SEE_CreateLexicalAnalizer(SEE_Source source)
{
	SEE_LexicalAnalizer lexer = (SEE_LexicalAnalizer) malloc(sizeof(RSEE_LexicalAnalizer));
	if ( lexer == NULL )
		return NULL;

	lexer->source = source;
	lexer->lineno = 1;
	lexer->colno = 1;
	lexer->t = lexer->source->getNextChar(lexer->source);

	return lexer;
}

////////////////////////////////////////////////////////////////////////////
int SEE_getNextToken(SEE_LexicalAnalizer lexer)
{
	while(1)
	{
		if ( lexer->t == -1 )
			return END;
			
		if ( lexer->t == ' ' || lexer->t == '\t' )
		{
			lexer->colno++;
			lexer->t = lexer->source->getNextChar(lexer->source);
			continue;
		}
		
		if ( lexer->t == '\n' )
		{
			lexer->lineno++;
			lexer->colno = 1;
			lexer->t = lexer->source->getNextChar(lexer->source);
			continue;
		}

		if ( isdigit(lexer->t) )
		{
			int b = 0;
			while ( isdigit(lexer->t) || lexer->t == '.' )
			{
				if ( b < BSIZE )
				{
					lexer->lexeme[b++] = lexer->t;
				}
				lexer->t = lexer->source->getNextChar(lexer->source);
				lexer->colno++;
			}
		
			if ( b >= BSIZE )
			{
				// lexeme too big, truncated to BSIZE -1 characters
				b = BSIZE -1;
			}
			lexer->lexeme[b] = EOS;

			if ( strchr(lexer->lexeme, '.') != NULL )
			{
				// TYPE2 number
				TYPE2 val2 = 0.0;
				sscanf(lexer->lexeme, "%f", &val2);
				lexer->value.f = val2;
				return TYPE2_LITERAL;
			}
			else
			{
				// TYPE1 number
				TYPE1 val1 = 0;
				sscanf(lexer->lexeme, "%ld", &val1);
				lexer->value.i = val1;
				return TYPE1_LITERAL;
			}
		}
		
		if ( isalpha(lexer->t) || lexer->t == '_' || lexer->t == '$' || lexer->t == '@'  )
		{
			int b = 0;
			while ( isalnum(lexer->t) 
				|| lexer->t == '.' || lexer->t == '_' || lexer->t == '$' || lexer->t == '@' )
			{
				if ( b < BSIZE )
				{
					lexer->lexeme[b++] = lexer->t;
				}
				lexer->t = lexer->source->getNextChar(lexer->source);
				lexer->colno++;
			}
			if ( b >= BSIZE )
			{
				// el lexeme fue demasiado grande: s�lo se han tenido
				// en cuenta los primeros BSIZE -1 caracteres:
				b = BSIZE -1;
			}
			lexer->lexeme[b] = EOS;

			return ID;
		}
		else if ( lexer->t == '<' || lexer->t == '>'
		||        lexer->t == '=' || lexer->t == '!' )
		{
            int t = lexer->t;
			lexer->t = lexer->source->getNextChar(lexer->source);
            lexer->colno++;
            if ( lexer->t == '=' )
            {
                lexer->t = lexer->source->getNextChar(lexer->source);
                lexer->colno++;
                return t == '<' ? LE : 
                       t == '>' ? GE :
                       t == '=' ? EQ :
                       NE
                ;
            }
            return t;
		}
		else if ( lexer->t == '#' )
		{
			/* comment */
			/* discard the rest of the line */
			do
			{
				lexer->t = lexer->source->getNextChar(lexer->source);
				lexer->colno++;
			} while ( lexer->t != -1 && lexer->t != '\n' ) ;
		}
		else if ( lexer->t == '\r' )
		{
			lexer->t = lexer->source->getNextChar(lexer->source);
			continue;
		}
		else
		{
			int t = lexer->t; 
			lexer->t = lexer->source->getNextChar(lexer->source);
			lexer->colno++;
			return t;
		}
	}
}

////////////////////////////////////////////////////////////////////////////
int SEE_DestroyLexicalAnalizer(SEE_LexicalAnalizer lexer)
{
	free(lexer);
	return 0;
}


////////////////////////////////////////////////////////////////////////////
char* SEE_getTokenName(SEE_LexicalAnalizer lexer, int t, int withLexeme)
{
static char res[4][128];
static int curr = 0;
	char* n;

	switch (t)
	{
	case NOTHING:
		n = "NOTHING";
		break;

	case EOS:
		n = "EOS";
		break;

	case NUM:
		n = "NUM";
		break;

	case ID:
		n = "ID";
		break;

	case END:
		n = "<END OF INPUT>";
		break;

	case TYPE1_LITERAL:
		n = "TYPE1_LITERAL";
		break;

	case TYPE2_LITERAL:
		n = "TYPE2_LITERAL";
		break;

	case LE:
		n = "<=";
		break;

	case GE:
		n = ">=";
		break;

	default:
		sprintf(res[curr], "%c (%d)", t, t);
		return res[curr++ % 4];
	}

	if ( withLexeme
	&& ( t == TYPE1_LITERAL || t == TYPE2_LITERAL || t == ID || t == NUM )
	)
	{
		sprintf(res[curr], "%s (%s)", n, lexer->lexeme);
		return res[curr++ % 4];
	}

	return n;
}



