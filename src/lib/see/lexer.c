/*
	A lexer demo.
*/

#include "see.i"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


static char source_string[4 * 1024];
static int len;
static int cursor;

//////////////////////////////////////////////////////////////////////
static
int getNextChar(SEE_Source source)
{
	if ( cursor >= len )
		return -1;
	else
		return source_string[cursor++];
}



//////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{
	int t;
	RSEE_Source source = {getNextChar};
	SEE_LexicalAnalizer lexer;

	if ( argc < 2 )
	{
		printf("USAGE: lexer expression\n");
		return 1;
	}
	strncpy(source_string, argv[1], sizeof(source_string) -1);
	len = strlen(source_string);
	cursor = 0;
	lexer = SEE_CreateLexicalAnalizer(&source);

	printf("tokens\n");
	while ( ( t = SEE_getNextToken(lexer)) != END )
	{
		printf("%s\n", SEE_getTokenName(lexer, t, 1));
	}


	return 0;
}

