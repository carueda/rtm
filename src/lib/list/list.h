/* **************************************************************************
**
** A list manager. Each element has a given fixed size.
** list.h - Public declarations.
** Author: Carlos Rueda
** Date:   5/25/01
** Modified: 4/9/02 - Added list_setSize(), list_ensureCapacity(), list_capacity().
**
** **************************************************************************/
#ifndef __LIST_H
#define __LIST_H


//////////////////////////////////////////////////////////////////////
/**
 * The type for a list.
 */
typedef void* List;


//////////////////////////////////////////////////////////////////////
/**
 * Creates a new list.
 * Returns the handle, -1 if error.
 */
List list_create(
	int  element_size,	// element size
	long capacity,		// initial capacity, must be > 0 
	long increment		// increment if more memory is needed
);


//////////////////////////////////////////////////////////////////////
/**
 * Adds an element to a list.
 */
void list_addElement(List list_id, void* element);


//////////////////////////////////////////////////////////////////////
/**
 * Gets the size of a list.
 */
long list_size(List list_id);

//////////////////////////////////////////////////////////////////////
/**
 * Sets the size of a list.
 * If the new size is > current capacity, then list_ensureCapacity()
 * is first called.
 * If the new size is > current size, then the new elements are 
 * "zeroed". 
 *
 * Returns 0 iff OK.
 */
int list_setSize(List list_id, long new_size);

//////////////////////////////////////////////////////////////////////
/**
 * Gets the current capacity of a list.
 */
long list_capacity(List list_id);

//////////////////////////////////////////////////////////////////////
/**
 * Increases the capacity of a list, if necessary, to ensure that it can
 * hold at least the number of elements specified by the minimum capacity
 * argument.
 * Returns 0 iff OK
 */
int list_ensureCapacity(List list_id, long minCapacity);

//////////////////////////////////////////////////////////////////////
/**
 * Gets an element.
 * Returns the element, or NULL if index is invalid.
 */
void* list_elementAt(List list_id, long index);


//////////////////////////////////////////////////////////////////////
/**
 * Destroys a list.
 */
void list_destroy(List list_id);



#endif	/* LIST */
