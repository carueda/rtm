/*
	A list manager.
*/

#include "list.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//////////////////////////////////////////////////////////////////////
typedef struct
{
	char* elements;
	int  element_size;
	long list_size;
	long capacity;
	long increment;

} RListObj, *ListObj;


//////////////////////////////////////////////////////////////////////
List list_create(int element_size, long capacity, long increment)
{
	ListObj list;
	
	if ( element_size <= 0 || capacity <= 0 )
	{
		fprintf(stderr, "list_create: element_size = %d  capacity = %d\n", 
			(int) element_size, (int)capacity
		);
		return NULL;
	}
	
	list = (ListObj) malloc(sizeof(RListObj));
	if ( list == NULL )
	{
		return NULL;
	}
	list->element_size = element_size;
	list->capacity = capacity;
	list->increment = increment;
	list->elements = (char*) malloc(list->capacity * list->element_size);
	if ( list->elements == NULL )
	{
		free(list);
		return NULL;
	}
	list->list_size = 0;
	
	return list;
}


//////////////////////////////////////////////////////////////////////
void list_addElement(List list_id, void* element)
{
	ListObj list = (ListObj) list_id;
	
	if ( list->list_size == list->capacity )
	{
		long new_capacity;
		
		if ( list->increment == 0 )
		{
			fprintf(stderr, "list_addElement: list->increment == 0!\n");
			return;
		}
		new_capacity = list->capacity + list->increment;
		if ( 0 != list_ensureCapacity(list_id, new_capacity) )
		{
			return;
		}
	}
	
	// copy the element:
	memcpy(
		list->elements + list->list_size * list->element_size, 
		element, 
		list->element_size
	);
	
	list->list_size++;
}

//////////////////////////////////////////////////////////////////////
int list_ensureCapacity(List list_id, long minCapacity)
{
	ListObj list = (ListObj) list_id;
	if ( list->capacity < minCapacity )
	{
		long nbytes = minCapacity * list->element_size;
		char* tmp = (char*) realloc(list->elements, nbytes);
		if ( tmp == NULL )
		{
			fprintf(stderr, "No memory in reallocation of list, new capacity=%d  element size=%d\n", 
				(int)minCapacity, (int)list->element_size
			);
			return 1; // problems
		}
		list->elements = tmp;
		list->capacity = minCapacity;
	}
	return 0;  // OK
}

//////////////////////////////////////////////////////////////////////
long list_size(List list_id)
{
	ListObj list = (ListObj) list_id;
	return list->list_size;
}

//////////////////////////////////////////////////////////////////////
int list_setSize(List list_id, long new_size)
{
	ListObj list = (ListObj) list_id;
	
	if ( new_size > list->capacity )
	{
		// ensure capacity:
		int status = list_ensureCapacity(list_id, new_size);
		if ( 0 != status )
		{
			return status;
		}
	}
	
	if ( new_size > list->list_size )
	{
		// fill with zeroes the new elements:
		memset(
			list->elements + list->list_size * list->element_size, 
			0, 
			(new_size - list->list_size) * list->element_size
		);
	}

	// update size:
	list->list_size = new_size;
	
	return 0; // OK
}

//////////////////////////////////////////////////////////////////////
long list_capacity(List list_id)
{
	ListObj list = (ListObj) list_id;
	return list->capacity;
}


//////////////////////////////////////////////////////////////////////
void* list_elementAt(List list_id, long index)
{
	ListObj list = (ListObj) list_id;

	if ( index < 0L || index >= list->list_size )
	{
		return NULL;
	}
	else
	{
		char* ptr = list->elements + index * list->element_size;
		return ptr;
	}
}

//////////////////////////////////////////////////////////////////////
void list_destroy(List list_id)
{
	ListObj list = (ListObj) list_id;
	free(list->elements);
	free(list);
}


