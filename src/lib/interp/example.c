/*
/////////////////////////////////////////////////////////////////////////
//
// interpolation
//
// Carlos Rueda - CSTARS
// Sept 2001
//
/////////////////////////////////////////////////////////////////////////
*/

#include "interp.h"


#include <stdio.h>
#include <math.h>



/////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{
	float xa[] = {-2, -1, 0, 1, 2, 3};
	float ya[] = { 4,  1, 0, 1, 4, 9};
	int   n = 6;
	float x = 1.5;
	float y;
	float dy;
	int status;

	if ( argc != 2 )
	{
		fprintf(stderr, "USAGE:  example x\n");
		return 1;
	}
	sscanf(argv[1], "%f", &x);

	status = interp_polint(
		xa,
		ya,
		n,
		x,
		&y,
		&dy
	);
	if ( status != 0 )
	{
		fprintf(stdout, "oops, status = %d\n", status);
		return status;
	}

	fprintf(stdout, "f(%f) = %f   dy=%f\n", x, y, dy);

	return status;
}

