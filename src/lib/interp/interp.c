/*
/////////////////////////////////////////////////////////////////////////
//
// interpolation
//
// Carlos Rueda - CSTARS
// Sept 2001
//
/////////////////////////////////////////////////////////////////////////
*/

#include "interp.h"

#include <math.h>
#include <malloc.h>
#include <stdio.h>


/////////////////////////////////////////////////////////////////////////
/**
 * Returns an n-sized array to be indexed from 1.
 * Free this array using free_vector.
 */
static float* vector(int n)
{
	float* v = malloc(n * sizeof(float));
	if ( v == NULL )
	{
		fprintf(stderr, "Not enough memory for %d floats\n", n);
		return NULL;
	}
	else
	{
		return v - 1;
	}
}

/////////////////////////////////////////////////////////////////////////
/**
 * Frees an array allocated with vector().
 */
static void free_vector(float* v)
{
	free(v + 1);
}

/////////////////////////////////////////////////////////////////////////
int interp_polint(
	float xa[],
	float ya[],
	int   n,
	float x,
	float *y,
	float *dy
)
{
	int i, m, ns = 1;
	float den, dif, dift, ho, hp, w;
	float *c, *d;
	int ret = 0;

	// Input arrays are zero-based, so make them 1-based to keep
	// the original NR code:
	xa -= 1;
	ya -= 1;

	dif = fabs(x - xa[1]);
	if ( (c = vector(n)) == NULL
	||   (d = vector(n)) == NULL )
	{
		return 1;
	}
	for ( i = 1; i <= n ; i++ )
	{
		if ( (dift = fabs(x - xa[i])) < dif )
		{
			ns = i;
			dif = dift;
		}
		c[i] = ya[i];
		d[i] = ya[i];
	}
	*y = ya[ns--];
	for ( m = 1; m < n; m++ )
	{
		for ( i = 1; i <= n - m; i++ )
		{
			ho = xa[i] - x;
			hp = xa[i + m] - x;
			w = c[i + 1] - d[i];
			if ( (den = ho - hp) == 0.0 )
			{
				fprintf(stderr, "Error in interp_polint\n");
				/* This error can occur only if two input xa's are
				 * (to within roundoff) identical.
				 */
				ret = 2;
				goto exit;
			}
			den = w / den;
			d[i] = hp * den;
			c[i] = ho * den;
		}
		*y += (*dy = (2*ns < (n - m) ? c[ns + 1] : d[ns--]));
	}

exit:
	free_vector(d);
	free_vector(c);

	return ret;
}

