/*
/////////////////////////////////////////////////////////////////////////
//
// interpolation
//
// Carlos Rueda - CSTARS
// Sept 2001
//
/////////////////////////////////////////////////////////////////////////
*/

#ifndef __INTERP_H
#define __INTERP_H




/////////////////////////////////////////////////////////////////////////
/**
 * Adapted from Numerical Recipes.
 * Given arrays xa[0..n-1] and ya[0..n-1], and a given value x, this routine
 * returns a value y, and an error estimate dy.
 *
 * Note that array indexing is zero-based here.
 *
 * Returns 0 iff success.
 */
int interp_polint(
	float xa[],
	float ya[],
	int   n,
	float x,
	float *y,
	float *dy
);



////////////////////////////////////////////////////////////////
/**
 * Adapted from Numerical Recipes.
 * Given arrays x[0..n], y[0..n] containing a tabulated function
 * yi = f(xi), and an auxiliar s[0..n],  this routine returns an array
 * of coefficients cof[0..n] such that yi = SUMj cof[j]*x[i]^j
 */
void polcoe(float x[], float y[], int n, float cof[], float s[]);



///////////////////////////////////////////////////////////////////////
/**
 * a[0..n]:
 * p(x) = a[0] + a[1]*x^1 + a[2]*x^2 + ... + a[n]*x^n
 */
float evalpol(int n, float a[], float x);



#endif   /* ifndef __INTERP_H */
