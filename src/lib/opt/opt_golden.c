/* 
/////////////////////////////////////////////////////////////////////////
//
// optimization - Golden section method
//
// Carlos Rueda - CSTARS
// June 2001
//
//	NOTE
//	----
//	Define OMIT_COUNT_EVALUATION_ESTIMATION to omit a
//	conditioned fragment below.
//
///////////////////////////////////////////////////////////////////////// 
*/

#include "opt.i"

#include <math.h>


#define MAX_NUM_VARIABLES 16

#define R 0.61803399
#define C (1.0 - R)
#define tol (10e-3)



static OPT_Object opt = NULL;

static 	OPT_RValue current_input[MAX_NUM_VARIABLES];




/////////////////////////////////////////////////////////////////////////
static 
int initialize()
{
	int n = opt->n;
	
	if ( n > MAX_NUM_VARIABLES )
	{
		fprintf(stderr, "number of variables (%d) exceeds the maximum (%d)\n",
			n, MAX_NUM_VARIABLES
		);
		return 1;
	}
	
	return 0;
}


/////////////////////////////////////////////////////////////////////////
/**
 * Evaluates the function in the (last) point x.
 * Assumes that previous variables are already assigned.
 */
static 
double evaluate(double x)
{
	// only update last variable:
	current_input[opt->n - 1].d = x;
	
	return opt->fun(current_input);
}


/////////////////////////////////////////////////////////////////////////
static 
void copy_input(OPT_RValue dest_input[], OPT_RValue source_input[])
{
	memcpy(dest_input, source_input, opt->n * sizeof(OPT_RValue));
}

/////////////////////////////////////////////////////////////////////////
static 
double  golden_search(int k, double ax, double bx, double cx)
{
	OPT_RVariableDefinition* varDefs = opt->varDefs;
	double f1, f2, x0, x1, x2, x3;
	double k1_mid = 0;    // only assigned and used when k < opt->n - 1 

	if ( k < opt->n - 1 )
		k1_mid = varDefs[k + 1].lower.d + C*(varDefs[k + 1].upper.d - varDefs[k + 1].lower.d);
	
	x0 = ax;
	x3 = cx;
	if ( fabs(cx - bx) > fabs(bx - ax) )
	{
		x1 = bx;
		x2 = bx + C*(cx - bx);
	}
	else
	{
		x2 = bx;
		x1 = bx - C*(bx - ax);
	}
	
	if ( k == opt->n - 1 )
	{
		f1 = evaluate(x1);
		f2 = evaluate(x2);
	}
	else
	{
		current_input[k].d = x1;
		f1 = golden_search(k + 1, varDefs[k + 1].lower.d, k1_mid, varDefs[k + 1].upper.d);

		current_input[k].d = x2;
		f2 = golden_search(k + 1, varDefs[k + 1].lower.d, k1_mid, varDefs[k + 1].upper.d);
	}
	
	while ( fabs(x3 - x0) > tol*(fabs(x1) + fabs(x2)) )
	{
		if ( f2 < f1 )
		{
			x0 = x1;
			x1 = x2;
			x2 = R*x1 + C*x3;
			
			f1 = f2;
			if ( k == opt->n - 1 )
				f2 = evaluate(x2);
			else
			{
				current_input[k].d = x2;
				f2 = golden_search(k + 1, varDefs[k + 1].lower.d, k1_mid, varDefs[k + 1].upper.d);
			}
		}
		else
		{
			x3 = x2;
			x2 = x1;
			x1 = R*x2 + C*x0;
			
			f2 = f1;
			if ( k == opt->n - 1 )
				f1 = evaluate(x1);
			else
			{
				current_input[k].d = x1;
				f1 = golden_search(k + 1, varDefs[k + 1].lower.d, k1_mid, varDefs[k + 1].upper.d);
			}
		}

	}
	
	if ( f1 < f2 )
	{
		current_input[k].d = x1;
		return f1;
	}
	else
	{
		current_input[k].d = x2;
		return f2;
	}
	
	
	return 0;	
}



/////////////////////////////////////////////////////////////////////////
static
int search(OPT_Object opt_, OPT_RValue* best_input, double *best_output)
{
	OPT_RVariableDefinition* varDefs;
	double mid;
	int status;
	
	opt = opt_;

	varDefs = opt->varDefs;

	status = initialize();
	if ( status != 0 )
		return status;
	
	mid = varDefs[0].lower.d + C*(varDefs[0].upper.d - varDefs[0].lower.d);
	*best_output = golden_search(0, varDefs[0].lower.d, mid, varDefs[0].upper.d);
	
fprintf(stderr, "MIN = %f\n", *best_output);

	copy_input(best_input, current_input);

	return 0;
}



/////////////////////////////////////////////////////////////////////////
int OPT_golden(OPT_Object opt_, OPT_RValue* best_input_, double *best_output_)
{

	return search(opt_, best_input_, best_output_);
}


