/* 
/////////////////////////////////////////////////////////////////////////
//
// optimization - Binary descent method
//
// Carlos Rueda - CSTARS
// June 2001
//
//	NOTE
//	----
//	Define OMIT_COUNT_EVALUATION_ESTIMATION to omit a
//	conditioned fragment below.
//
///////////////////////////////////////////////////////////////////////// 
*/

#include "opt.i"


#define MAX_NUM_VARIABLES 16


static OPT_Object opt = NULL;

static 	OPT_RValue current_input[MAX_NUM_VARIABLES];




/////////////////////////////////////////////////////////////////////////
static 
int initialize()
{
	int n = opt->n;
	
	if ( n > MAX_NUM_VARIABLES )
	{
		fprintf(stderr, "number of variables (%d) exceeds the maximum (%d)\n",
			n, MAX_NUM_VARIABLES
		);
		return 1;
	}
	
	return 0;
}


/////////////////////////////////////////////////////////////////////////
/**
 * Evaluates the function in the (last) point x.
 * Assumes that previous variables are already assigned.
 */
static 
float evaluate(float x)
{
	// only update last variable:
	current_input[opt->n - 1].d = x;
	
	return opt->fun(current_input);
}


/////////////////////////////////////////////////////////////////////////
static 
void copy_input(OPT_RValue dest_input[], OPT_RValue source_input[])
{
	memcpy(dest_input, source_input, opt->n * sizeof(OPT_RValue));
}

/////////////////////////////////////////////////////////////////////////
static 
float  search_min_1d(int k, float lower, float upper)
{
	OPT_RVariableDefinition* varDefs = opt->varDefs;
	
	switch ( varDefs[k].type )
	{
	case OPT_DOUBLE:
	{
		float mid_x, mid_y;
		float left_x, left_y;
		float right_x, right_y;
		
		mid_x = (lower + upper) / 2.0;
		
		if ( k == opt->n - 1 )
		{
			mid_y = evaluate(mid_x);
		}
		else
		{
			current_input[k].d = mid_x;
			mid_y = search_min_1d(k + 1, varDefs[k + 1].lower.d, varDefs[k + 1].upper.d);
		}


		if ( upper == lower
		||   upper - lower <= 2 * varDefs[k].step.d )
		{
			// no more search possible in this variable.
			return mid_y;	// this variable ready.
		}

		
		// decide which direction to follow:
		
		// see if to the left:

		left_x = mid_x - varDefs[k].step.d;
		
		if ( k == opt->n - 1 )
		{
			left_y = evaluate(left_x);
		}
		else
		{
			current_input[k].d = left_x;
			left_y = search_min_1d(k + 1, varDefs[k + 1].lower.d, varDefs[k + 1].upper.d);
		}

		
		if ( left_y < mid_y )
		{
			// follow left
			return search_min_1d(k, lower, mid_x);
		}

		// see if to the right:

		right_x = mid_x + varDefs[k].step.d;
		
		if ( k == opt->n - 1 )
		{
			right_y = evaluate(right_x);
		}
		else
		{
			current_input[k].d = right_x;
			right_y = search_min_1d(k + 1, varDefs[k + 1].lower.d, varDefs[k + 1].upper.d);
		}

		if ( right_y < mid_y )
		{
			// follow right:
			return search_min_1d(k, mid_x, upper);
		}
		else
		{
			// mid point is the best: keep it
			current_input[k].d = mid_x;
			return mid_y;
		}
		break;
	}
	case OPT_LONG:
		assert(0 && "PENDING");
		break;
	}
	
	assert(0 && "this fragment cannot be reached");
	return 0;	
}



/////////////////////////////////////////////////////////////////////////
static
int search(OPT_Object opt_, OPT_RValue* best_input, double *best_output)
{
	OPT_RVariableDefinition* varDefs;
	int status;
	
	opt = opt_;

	varDefs = opt->varDefs;

	status = initialize();
	if ( status != 0 )
		return status;
	
	*best_output = search_min_1d(0, varDefs[0].lower.d, varDefs[0].upper.d);
	
fprintf(stderr, "MIN = %f\n", *best_output);

	copy_input(best_input, current_input);

	return 0;
}


//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//
//	BEGIN CONDITIONED FRAGMENT
//
//	THE FOLLOWING CONDITIONED FRAGMENT IS ONLY TO COUNT HOW MANY 
//	EVALUATIONS	WILL BE REQUIRED (MINIMUM AND MAXIMUM) TO FIND A 
//	SOLUTION.
//
//	Define OMIT_COUNT_EVALUATION_ESTIMATION to omit this fragment.
//
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
#if !defined OMIT_COUNT_EVALUATION_ESTIMATION

static int max_evaluations;

/////////////////////////////////////////////////////////////////////////
static 
void  maxEvals_min_1d(int k, float lower, float upper)
{
	OPT_RVariableDefinition* varDefs = opt->varDefs;
	
	switch ( varDefs[k].type )
	{
	case OPT_DOUBLE:
	{
		float mid_x;
		float left_x;
		float right_x;
		
		mid_x = (lower + upper) / 2.0;
		
		if ( k == opt->n - 1 )
		{
			max_evaluations++;
		}
		else
		{
			maxEvals_min_1d(k + 1, varDefs[k + 1].lower.d, varDefs[k + 1].upper.d);
		}


		if ( upper == lower
		||   upper - lower <= 2 * varDefs[k].step.d )
		{
			return;
		}

		
		// decide which direction to follow:
		
		// see if to the left:

		left_x = mid_x - varDefs[k].step.d;
		
		if ( k == opt->n - 1 )
		{
			max_evaluations++;
		}
		else
		{
			maxEvals_min_1d(k + 1, varDefs[k + 1].lower.d, varDefs[k + 1].upper.d);
		}

		
		right_x = mid_x + varDefs[k].step.d;
		
		if ( k == opt->n - 1 )
		{
			max_evaluations++;
		}
		else
		{
			maxEvals_min_1d(k + 1, varDefs[k + 1].lower.d, varDefs[k + 1].upper.d);
		}

		maxEvals_min_1d(k, mid_x, upper);
		return;
	}
	case OPT_LONG:
		assert(0 && "PENDING");
		break;
	}
	
	assert(0 && "this fragment cannot be reached");
}

/////////////////////////////////////////////////////////////////////////
static
void count_max_evaluations(OPT_Object opt_, OPT_RValue* best_input, double *best_output)
{
	OPT_RVariableDefinition* varDefs;
	
	opt = opt_;

	varDefs = opt->varDefs;

	max_evaluations = 0;
	maxEvals_min_1d(0, varDefs[0].lower.d, varDefs[0].upper.d);
}



static int min_evaluations;


/////////////////////////////////////////////////////////////////////////
static 
void  minEvals_min_1d(int k, float lower, float upper)
{
	OPT_RVariableDefinition* varDefs = opt->varDefs;
	
	switch ( varDefs[k].type )
	{
	case OPT_DOUBLE:
	{
		float mid_x;
		float left_x;
		
		mid_x = (lower + upper) / 2.0;
		
		if ( k == opt->n - 1 )
		{
			min_evaluations++;
		}
		else
		{
			minEvals_min_1d(k + 1, varDefs[k + 1].lower.d, varDefs[k + 1].upper.d);
		}


		if ( upper == lower
		||   upper - lower <= 2 * varDefs[k].step.d )
		{
			return;
		}

		
		left_x = mid_x - varDefs[k].step.d;
		
		if ( k == opt->n - 1 )
		{
			min_evaluations++;
		}
		else
		{
			minEvals_min_1d(k + 1, varDefs[k + 1].lower.d, varDefs[k + 1].upper.d);
		}

		
		minEvals_min_1d(k, lower, mid_x);
		return;
		break;
	}
	case OPT_LONG:
		assert(0 && "PENDING");
		break;
	}
	
	assert(0 && "this fragment cannot be reached");
}


/////////////////////////////////////////////////////////////////////////
static
void count_min_evaluations(OPT_Object opt_, OPT_RValue* best_input, double *best_output)
{
	OPT_RVariableDefinition* varDefs;
	
	opt = opt_;

	varDefs = opt->varDefs;

	min_evaluations = 0;
	minEvals_min_1d(0, varDefs[0].lower.d, varDefs[0].upper.d);
}


//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//
//	END CONDITIONED FRAGMENT
//
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
#endif // !defined OMIT_COUNT_EVALUATION_ESTIMATION


/////////////////////////////////////////////////////////////////////////
int OPT_binary(OPT_Object opt_, OPT_RValue* best_input_, double *best_output_)
{

//-----------------------------------------------------------------------
// conditioned fragment that uses the definition above:
#if !defined OMIT_COUNT_EVALUATION_ESTIMATION

	count_max_evaluations(opt_, best_input_, best_output_);
	count_min_evaluations(opt_, best_input_, best_output_);
	fprintf(stdout, "Evaluations: min=%d, max=%d, average=%d\n", 
		min_evaluations,
		max_evaluations,
		(min_evaluations + max_evaluations) / 2
	);

//-----------------------------------------------------------------------
#endif // !defined OMIT_COUNT_EVALUATION_ESTIMATION
	

	return search(opt_, best_input_, best_output_);
}


