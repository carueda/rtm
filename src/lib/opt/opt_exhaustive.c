/* 
/////////////////////////////////////////////////////////////////////////
//
// optimization - Exhaustive method
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

#include "opt.i"


static OPT_Object opt = NULL;
static OPT_RValue* current_input = NULL;

static OPT_RValue* best_input = NULL;
static double *best_output;


/////////////////////////////////////////////////////////////////////////
static 
int initialize()
{
	int i;
	int n = opt->n;
	OPT_RVariableDefinition* varDefs = opt->varDefs;
	
	current_input = (OPT_RValue*) malloc(n * sizeof(OPT_RValue));
	if ( current_input == NULL )
	{
		return 1;	// no memory
	}
	
	// assign initial values:
	for ( i = 0; i < n; i++ )
	{
		current_input[i] = varDefs[i].lower;
	}
	
	// get initial output:
	memcpy(best_input, current_input, n * sizeof(OPT_RValue));
	
	*best_output = opt->fun(current_input);
	
	return 0;
}


/////////////////////////////////////////////////////////////////////////
static 
void evaluate()
{
	double current_output = opt->fun(current_input);
	
	if ( *best_output > current_output )
	{
		*best_output = current_output;
		memcpy(best_input, current_input, opt->n * sizeof(OPT_RValue));
	}
}




/////////////////////////////////////////////////////////////////////////
static 
int  increment(int k)
{
	OPT_RVariableDefinition* varDefs = opt->varDefs;
	
	switch ( varDefs[k].type )
	{
	case OPT_DOUBLE:
		current_input[k].d += varDefs[k].step.d;
		if ( current_input[k].d <= varDefs[k].upper.d )
		{
			// increment OK
			return 1;
		}
		
		if ( k == 0 )
		{
			// increment not possible
			return 0;
		}
		
		// rewind k-th variable to its lower value and call increment(k-1):
		current_input[k].d = varDefs[k].lower.d;
		return increment(k - 1);
		break;

	case OPT_LONG:
		current_input[k].l += varDefs[k].step.l;
		if ( current_input[k].l <= varDefs[k].upper.l )
		{
			// increment OK
			return 1;
		}
		
		if ( k == 0 )
		{
			// increment not possible
			return 0;
		}
		
		// rewind k-th variable to its lower value and call increment(k-1):
		current_input[k].l = varDefs[k].lower.l;
		return increment(k - 1);
		break;
	}
	
	assert(0 && "this fragment cannot be reached");
	return 0;	
}



/////////////////////////////////////////////////////////////////////////
static 
void finalize()
{
	free(current_input);
	current_input = NULL;
}



/////////////////////////////////////////////////////////////////////////
static
int search()
{
	int status;
	
	status = initialize();
	if ( status != 0 )
		return status;
	
	while ( increment(opt->n - 1) )
	{
		evaluate();
	}
	
	finalize();

	return 0;
}


/////////////////////////////////////////////////////////////////////////
/**
 * Counts the number of evaluations required.
 */
static 
int  evaluations()
{
	OPT_RVariableDefinition* varDefs = opt->varDefs;
	double evals = 1.0;
	int k;

	for ( k = 0; k < opt->n; k++ )
	{
		switch ( varDefs[k].type )
		{
		case OPT_DOUBLE:
			if ( varDefs[k].lower.d < varDefs[k].upper.d )
			{
				double x = (varDefs[k].upper.d - varDefs[k].lower.d)
				         / varDefs[k].step.d
				;
				evals *= x;
			}
			break;
			
		case OPT_LONG:
			if ( varDefs[k].lower.l < varDefs[k].upper.l )
			{
				long x = (varDefs[k].upper.l - varDefs[k].lower.l)
				         / varDefs[k].step.l
				;
				evals *= (double) x;
			}
			break;
		}
	}
	
	return (int) evals;
}


/////////////////////////////////////////////////////////////////////////
int OPT_exhaustive(OPT_Object opt_, OPT_RValue* best_input_, double *best_output_)
{
	opt = opt_;
	best_input = best_input_;
	best_output = best_output_;

fprintf(stdout, "Evaluations=%d\n", evaluations());

	return search();
}



