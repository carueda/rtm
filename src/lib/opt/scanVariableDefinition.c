/* 
/////////////////////////////////////////////////////////////////////////
//
// prospect inversion
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

#include "opt.h"

#include <string.h>
#include <stdio.h>


/////////////////////////////////////////////////////////////////////////
void opt_scanVariableDefinition(char* str, OPT_VariableDefinition varDef)
{
	char* tok;
	
	varDef->type = OPT_DOUBLE;
	
	tok = strtok(str, ", ");
	sscanf(tok, "%lf", &varDef->lower.d);

	tok = strtok(NULL, ", ");
	if ( tok == NULL )
	{
		varDef->upper.d = varDef->lower.d;
		varDef->step.d = 999.9;
		return;
	}
	
	sscanf(tok, "%lf", &varDef->upper.d);

	tok = strtok(NULL, ", ");
	if ( tok == NULL )
	{
		varDef->step.d = varDef->upper.d - varDef->lower.d;
		return;
	}
	sscanf(tok, "%lf", &varDef->step.d);
}


