/* 
/////////////////////////////////////////////////////////////////////////
//
// optimization
// tracker module
//
// Carlos Rueda - CSTARS
// 04/14/03
//
///////////////////////////////////////////////////////////////////////// 
*/

#include "opt.i"

#include <stdlib.h>    // for qsort


#define MAX_NUM_VARIABLES 16


/////////////////////////////////////////////////////////////////////////
typedef struct
{
	OPT_RValue best_input[MAX_NUM_VARIABLES];
	double best_output;
	
} RElement;
	


static int n;
static int m;
static int no_best;   // number of best so far

static RElement* elements;




/////////////////////////////////////////////////////////////////////////
int opt_tracker_init(int n_, int m_)
{
	opt_tracker_end();
	
	assert( m_ > 0 ) ;
	
	n = n_;
	m = m_;

	elements = (RElement*) malloc(m * sizeof(RElement));
	if ( !elements )
		return 1;
	
	no_best = 0;
	return 0;
}

/////////////////////////////////////////////////////////////////////////
int opt_tracker_end()
{
	if ( elements )
		free(elements);
	
	elements = NULL;
	no_best = 0;
	
	return 0;
}


/////////////////////////////////////////////////////////////////////////
// for qsort
static
int compar(const void *a, const void *b)
{
	RElement* aa = (RElement*) a;
	RElement* bb = (RElement*) b;
	
	if ( aa->best_output < bb->best_output )
		return -1;
	else if ( aa->best_output > bb->best_output )
		return +1;
	else
		return 0;
}

/////////////////////////////////////////////////////////////////////////
int opt_track(OPT_RValue* input, double output)
{
	if ( no_best == m )
	{
		if ( output < elements[m - 1].best_output )
		{
			// replace last element and re-sort (see below):
			memcpy(elements[m - 1].best_input,  input, n * sizeof(OPT_RValue));
			elements[m - 1].best_output = output;
		}
		else
		{
			// nothing to change:
			return 0;
		}
	}
	else // no_best < m
	{
		// simply add to list and (re)sort (see below):
		memcpy(elements[no_best].best_input,  input, n * sizeof(OPT_RValue));
		elements[no_best].best_output = output;
		no_best++;
	}
	
	qsort(elements, no_best, sizeof(RElement), compar);
	
	return 0;
}

/////////////////////////////////////////////////////////////////////////
int opt_tracker_getNoBest()
{
	return no_best;
}

/////////////////////////////////////////////////////////////////////////
double opt_tracker_getBest(int pos, OPT_RValue* input)
{
	memcpy(input,  elements[pos].best_input, n * sizeof(OPT_RValue));
	return elements[pos].best_output;
}

