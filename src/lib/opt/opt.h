/* 
/////////////////////////////////////////////////////////////////////////
//
// optimization
//
// Carlos Rueda - CSTARS
// June 2001
//
// 04/14/03 - New tracker service.
//
///////////////////////////////////////////////////////////////////////// 
*/

#ifndef __OPT_H
#define __OPT_H


/////////////////////////////////////////////////////////////////////////
/**
 * Types.
 */
enum 
{
	OPT_DOUBLE,		// double type
	OPT_LONG		// long type
};

/////////////////////////////////////////////////////////////////////////
/**
 * Methods.
 */
enum 
{
	OPT_EXHAUSTIVE,		// exhaustive method
	OPT_BINARY,		// binary descent search method
	OPT_GOLDEN		// golden section search method
};


/////////////////////////////////////////////////////////////////////////
/**
 * A variable value.
 */
typedef union opt_value
{
	double d;
	
	long l;
	
} OPT_RValue, *OPT_Value;


/////////////////////////////////////////////////////////////////////////
/**
 * A variable definition.
 */
typedef struct opt_variable_definition
{
	/** This variable's type. One of OPT_DOUBLE, OPT_LONG. */
	int type;
	
	/** Lower limit. */
	OPT_RValue lower;
	
	/** Upper limit. */
	OPT_RValue upper;
	
	/** Step. */
	OPT_RValue step;
	
} OPT_RVariableDefinition, *OPT_VariableDefinition;




/////////////////////////////////////////////////////////////////////////
/**
 * Inversion object.
 */
typedef struct opt_object
{
	// Definition of function input.

	/** Number of variables. */
	int n;
	
	/** Variable definitions. */
	OPT_RVariableDefinition* varDefs;
	
	
	/** The function to be minimized. */
	double (*fun)(OPT_RValue* input);

} OPT_RObject, *OPT_Object;
 


/////////////////////////////////////////////////////////////////////////
/**
 * opt_minimize
 */
int opt_minimize(
	OPT_Object opt, 		// I- optimization object
	int method,			// I- method to be used
	OPT_RValue* best_input, 	// O- Input that produces the minimum value
	double *best_output		// O- Minimum value found
);



/////////////////////////////////////////////////////////////////////////
/**
 * Helper.
 * Scans a string for a variable definition. This string is assumed to have
 * the form ``lower,upper,step''.
 */
void opt_scanVariableDefinition(
	char* str, 
	OPT_VariableDefinition varDef
);




/////////////////////////////////////////////////////////////////////////
// tracker functions

/////////////////////////////////////////////////////////////////////////
/**
 * Call this before any other tracker function.
 *
 * n = number of variables
 * m = number of best solutions to keep track on
 */
int opt_tracker_init(int n, int m);

////////////////////////////////////////////////////////////////////////
/**
 * Ends tracking.
 */
int opt_tracker_end();

/////////////////////////////////////////////////////////////////////////
/**
 * Gives an input/output to keep track on.
 */
int opt_track(OPT_RValue* input, double output);

/////////////////////////////////////////////////////////////////////////
/**
 * Gets number of best solutions being tracked.
 */
int opt_tracker_getNoBest();

/////////////////////////////////////////////////////////////////////////
/**
 * Gets an specific solution.
 */
double opt_tracker_getBest(int pos, OPT_RValue *input);




#endif	/* ifndef __OPT_H */
