/* 
/////////////////////////////////////////////////////////////////////////
//
// optimization
//
// Carlos Rueda - CSTARS
// June 2001
//
///////////////////////////////////////////////////////////////////////// 
*/

#include "opt.i"


/////////////////////////////////////////////////////////////////////////
int opt_minimize(
	OPT_Object opt, 			// I- optimization object
	int method,					// I- method to be used
	OPT_RValue* best_input, 	// O- Input that produces the minimum value
	double *best_output			// O- Minimum value found
)
{
	if ( method == OPT_EXHAUSTIVE )
	{
		return OPT_exhaustive(opt, best_input, best_output);
	}
	else if ( method == OPT_BINARY )
	{
		return OPT_binary(opt, best_input, best_output);
	}
	else if ( method == OPT_GOLDEN )
	{
		return OPT_golden(opt, best_input, best_output);
	}
	else
	{
		fprintf(stderr, "ERROR: invalid method (%d) in opt_minimize\n", method);
		return -1;
	}
}




