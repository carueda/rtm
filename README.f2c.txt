F2C - Fortran to C translator Library

What follows are steps to download, compile, and install libf2c.a and
f2c on a Linux system. See the accompanying F2C documentation for
troubleshooting and instructions for other operating systems.

Download from ftp://netlib.bell-labs.com/netlib/f2c/ the following:

    libf2c.zip -- to create libf2c.a         (REQUIRED)
    src.tar    -- to create executable f2c   (OPTIONAL)
    
libf2c.a:
    This is s REQUIRED library to be able to compile some of the models
    included in CSTARS RTM. 
    The steps are:
    
        unzip libf2c.zip
        cd libf2c
        make -f makefile.u
        # libf2c.a created in current directory.
        
        Installation:
        Although makefile.u contains an `install' target, I would 
        suggest installing the library and its header as follows:
        
        su   # to become root because of the prefix /usr/local/ I'm using:
        install libf2c.a /usr/local/lib/
        install f2c.h    /usr/local/include/
    
f2c:
    This is an OPTIONAL executable (it's not required as the fortran
    sources included have been already converted to C).
    The steps are:
        tar xf src.tar
        cd src
        for i in *.gz; do gunzip $i; done
        make -f makefile.u
        # f2c created in current directory
    
        Installation:
        su   # to become root because of the prefix /usr/local/ I'm using:
        install f2c /usr/local/bin/

That's it!

--------------------------------------------------------
$Id$

